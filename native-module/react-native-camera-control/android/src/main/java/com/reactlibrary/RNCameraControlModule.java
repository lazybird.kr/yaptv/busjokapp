
package com.reactlibrarycameracontrol;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import android.util.Log;
import com.facebook.react.bridge.*;
import android.media.SoundPool;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.content.Context;

import android.media.MediaPlayer;
import android.media.AudioAttributes;
import android.os.Bundle;
import android.os.Build;
import android.hardware.Camera;
import java.util.List;
import java.lang.Exception;


public class RNCameraControlModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  private SoundPool soundPool;
  private int soundIds;

  private AudioManager mAudioManager;
  int userVolume;
  private MediaPlayer mp;



  Camera mCamera;
  private Camera.Parameters mCameraParameters;

  private static final String E_ERROR = "E_ERROR";

  public RNCameraControlModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    mAudioManager = (AudioManager) reactContext.getSystemService(Context.AUDIO_SERVICE);
    userVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
    mp = new MediaPlayer();

    mp.setAudioStreamType(AudioManager.STREAM_ALARM);
    mp.setLooping(false);
    mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
      @Override
      public void onPrepared(final MediaPlayer mediaPlayer){
        mediaPlayer.start();
      }
    });

    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(final MediaPlayer mediaPlayer){
        mediaPlayer.reset();
        mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, userVolume, 0);
      }
    });
  }
  /*
  public RNCameraControlModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;

    soundIds = 0;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      AudioAttributes audioAttributes = new AudioAttributes.Builder()
              .setUsage(AudioAttributes.USAGE_NOTIFICATION)
              .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
              .build();
      soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).setMaxStreams(8).build();
    }
    else {
      soundPool = new SoundPool(8, AudioManager.STREAM_NOTIFICATION, 0);
    }

    soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
      @Override
      public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        soundPool.play(sampleId, 1.0f, 1.0f, 1, 0, 1f);
      }
    });

  }
  */
  @Override
  public String getName() {
    return "RNCameraControl";
  }

  @ReactMethod
  public void show(String text, final Promise promise){
    Log.i("TT", text);
    try {
      WritableArray result = Arguments.createArray();

      result.pushString("1".toString());
      result.pushString("2".toString());
      promise.resolve(result);
    } catch (Exception e) {
      e.printStackTrace();
      promise.reject(e);
    }

  }

  @ReactMethod
  public void playCaptureSound(){
    /*
    //mediaplayer.prepare();
    if (soundIds == 0) {
      soundIds = soundPool.load(this.reactContext, R.raw.sound, 1);
    } else {
      soundPool.play(soundIds, 0.5f, 0.5f, 0, 0, 1f);
    }
    //MediaActionSound sound = new MediaActionSound();
    //sound.play(MediaActionSound.SHUTTER_CLICK);
  */
    try {
      String uri = "android.resource://" + this.reactContext.getPackageName() + "/" + R.raw.sound;
      Uri notification  = Uri.parse(uri);
      //Ringtone r = RingtoneManager.getRingtone(this.reactContext, notification);
      //r.play();
      mp.setDataSource(this.reactContext, notification);
      mp.prepareAsync();
      mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM,
              (int)(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM) * 0.3),
              0);

    } catch(Exception e){
      e.printStackTrace();
    }
  }

  @ReactMethod
  public void getCameraScale(final Promise promise) {
    try {
      WritableArray result = Arguments.createArray();
      mCamera = getCameraInstance();
      mCameraParameters = mCamera.getParameters();
      if (mCameraParameters.isZoomSupported()) {
        List<Integer> ratios = mCameraParameters.getZoomRatios();
        for(int i=0; i<ratios.size();i++){
          result.pushString(Integer.toString(ratios.get(i)));
        }
      } else {
        result.pushString(Integer.toString(100));
      }
      mCamera.release();
      promise.resolve(result);
    } catch(Exception e) {
      e.printStackTrace();
      promise.reject(e);
    }
  }

  private static Camera getCameraInstance(){
    Camera c = null;
    try {
      c = Camera.open(); // attempt to get a Camera instance
    }
    catch (Exception e){
      // Camera is not available (in use or does not exist)
    }
    return c; // returns null if camera is unavailable
  }

}