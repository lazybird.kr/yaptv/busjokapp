#import "RNCameraControl.h"
#import <React/RCTLog.h>

@import AVFoundation;
@implementation RNCameraControl

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
{
   RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
}

RCT_EXPORT_METHOD(playCaptureSound)
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];

    //AVAudioPlayer *player;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
    RCTLogInfo(@"Path%@", path);
    NSURL *url = [NSURL fileURLWithPath:path];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}

RCT_EXPORT_METHOD(getZoomFactor:(RCTResponseSenderBlock)callback)
{
   RCTLogInfo(@"Pretending %d", 1);
   //AVCaptureDevice *device = AVCaptureDevice.default(.builtInDualCamera,
   //                                     for: .video, position: .back);
   AVCaptureDevice *device = nil;
   RCTLogInfo(@"Pretending %d", 2);
   //NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
   ///AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera] 
   ///                                      mediaType:AVMediaTypeVideo 
   ///                                       position:AVCaptureDevicePositionBack];
   ///NSArray *captureDevices = [captureDeviceDiscoverySession devices];
   NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
   RCTLogInfo(@"Pretending %d", 3);
   for(AVCaptureDevice *camera in devices) {
      RCTLogInfo(@"Pretending %d", 4);
      if([camera position] == AVCaptureDevicePositionBack) { // is front camera  
      RCTLogInfo(@"Pretending %d", 5);
        device = camera;
        break;
      }  
   }
   RCTLogInfo(@"Pretending %d", 6);
    NSError *error = nil;

    if(device == nil){
       RCTLogInfo(@"Pretending %d", 7);
        return;
    }
    RCTLogInfo(@"Pretending %d", 8);

    if (![device lockForConfiguration:&error]) {
        if (error) {
            RCTLogError(@"%s: %@", __func__, error);
        }
        return;
    }
    RCTLogInfo(@"Pretending to create an event %f", device.activeFormat.videoMaxZoomFactor);


///    float maxZoom;
///    if(self.maxZoom > 1){
///        maxZoom = MIN(self.maxZoom, device.activeFormat.videoMaxZoomFactor);
///    }
///    else{
///        maxZoom = device.activeFormat.videoMaxZoomFactor;
///    }

///    device.videoZoomFactor = (maxZoom - 1) * self.zoom + 1;

    float mz = device.activeFormat.videoMaxZoomFactor;
    [device unlockForConfiguration];
    callback(@[[NSNull null], @[@(mz)]]);
   
}


@end
