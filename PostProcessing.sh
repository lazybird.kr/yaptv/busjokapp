#!/bin/bash
set -x o
sed -i 's/DEFAULT_COMPILE_SDK_VERSION = 27/DEFAULT_COMPILE_SDK_VERSION = 28/g' node_modules/@react-native-seoul/naver-login/android/build.gradle
sed -i 's/DEFAULT_BUILD_TOOLS_VERSION = "27.0.3"/DEFAULT_BUILD_TOOLS_VERSION = "28.0.3"/g' node_modules/@react-native-seoul/naver-login/android/build.gradle
