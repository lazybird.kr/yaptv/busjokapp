# BusjokApp

## ReactNative 실행 전 준비 사항

1. [yarn 설치](https://classic.yarnpkg.com/en/docs/install#windows-stable)

2. [Getting Started ReactNative](https://developers.facebook.com/docs/react-native/getting-started?locale=ko_KR)

## 실행 순서

1. `$> yarn install`

2. Android: `$> npx react-native run-android`

## 윈도우즈에서 뭔가 잘 안될 때

1. `$> cd android`

2. `$> gradlew.bat clean`

## iOS 빌드시

1. https://github.com/navermaps/ios-map-sdk
2. https://github.com/git-lfs/git-lfs/issues/911 


