import { Platform } from 'react-native';
import {Dimensions, PixelRatio} from 'react-native';

const getFontSize =  (originalSize) => {
    console.log("PixelRatio.get() : ", PixelRatio.get());
    /*
    if(Platform.OS === "android"){
        if(PixelRatio.get() < 1.5) {
            return (originalSize * 0.5 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 1.5 && PixelRatio.get() < 2.5) {
            return (originalSize * 1.5 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 2.5 && PixelRatio.get() < 3.5){
            return (originalSize * 2.5 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 3.5 && PixelRatio.get() < 4){
            return (originalSize * 3 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 4 && PixelRatio.get() < 4.5){
            return (originalSize * 3.5 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 5 && PixelRatio.get() < 5.5){
            return (originalSize * 4.5 ) / PixelRatio.get()
        } else if(PixelRatio.get() >= 5.5 && PixelRatio.get() < 6){
            return (originalSize * 5 ) / PixelRatio.get()
        } else {
            return originalSize
        }
    } else {
        return originalSize
    }
    */
   return originalSize
}


export default {
    colors: {
        buttonColor: '#d64409',
        startButtonColor: '#000000',
        lineColor: '#bdbdbd',
        baseColor: '#00a3ff',
        baseColor2: '#01a3ff',
        pinkColor: '#e90042',
        greyColor: '#9e9e9e',
        greyColor2: '#f5f5f5',
        greyColor3: '#757575',
    },
    subway: {
        "1" :{color : "#0052A4"},
        "2" :{color : "#009D3E"},
        "3" :{color : "#EF7C1C"},
        "4" :{color : "#00A5DE"},
        "5" :{color : "#996CAC"},
        "6" :{color : "#CD7C2F"},
        "7" :{color : "#747F00"},
        "8" :{color : "#EA545D"},
        "9" :{color : "#A17E46"},
        "수인분당": {color : "#F5A200"},
        "신분당": {color : "#D4003B"},
        "경인중앙": {color : "#77C4A3"},
        "경의중앙": {color : "#77C4A3"},
    },
    // 0: 공용버스, 1:공항버스, 2:마을버스, 3:간선버스, 4: 지선버스, 5:순환버스, 6:광역버스, 7:인천버스, 8:경기버스, 9:없음, 10:순환버스(?)
    bus: {
        busTypes: [
            '공용버스', '공항버스', '마을버스', '간선버스', '지선버스',
            '순환버스', '광역버스', '인천버스', '경기버스', '폐기',
            '순환버스',
        ],
        busColors: [
            'black', '#5f4d45', '#28b128', '#01a3ff', '#28b128',
            'yellow', '#ce0b39', '#ce0b39', '#ce0b39', 'black', 'yellow',
        ],
        busImages: [
            require('src/images/3_bus_3.png'),
            require('src/images/3_bus_1.png'),
            require('src/images/3_bus_4.png'),
            require('src/images/3_bus_3.png'),
            require('src/images/3_bus_4.png'),
            require('src/images/3_bus_1.png'),
            require('src/images/3_bus_6.png'),
            require('src/images/3_bus_6.png'),
            require('src/images/3_bus_6.png'),
            require('src/images/3_bus_3.png'),
            require('src/images/3_bus_3.png'),
        ],
    },
    styles: {
        text: {
            flex: 1,
            textAlign: 'left',
            textAlignVertical: 'center',
            fontSize: 14,
            borderWidth: 0,
            paddingVertical: 10,
            paddingHorizontal: 10,
            // fontFamily: "Courier",
            fontFamily: 'NotoSansKR-Regular',
        },
        text2: {
            flex: 1,
            textAlign: 'left',
            textAlignVertical: 'center',
            fontSize: 14,
            // fontFamily: "Courier",
            fontFamily: 'NotoSansKR-Regular',
        },
        button: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        },
        icon: {
            width: 24,
            height: 24,
        },
    },
    value: {
        maxTitleLength: 32,
        textInputHeight: 80,
    },
    deviceInfo: {
        oneThirdWidth: Dimensions.get('window').width / 3,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        device_id: '',
    },
    url: {
        qrcode: __DEV__ ? 'https://yaptvtest.lazybird.kr/' : 'https://yaptv.lazybird.kr/',
        qrcodeForLadderGame : "https://busjok.page.link/Go1D",
        //apiSvr: __DEV__ ? 'https://yaptvtest.lazybird.kr/api' : 'https://yaptv.lazybird.kr/api',
        apiSvr: 'http://sss-33.com:8100/api',
        host: 'http://www.yaptv.kr',
    },
    sessionInfo: {
        session_id: '',
    },
    images: {
        busStation: require('src/images/bus-station.png'),
        homeTitle: require('src/images/1_home_title.png'),
        gnbHomeOn: require('src/images/HOME_ON.png'),
        gnbHomeOff: require('src/images/HOME_OFF.png'),
        gnbBusOn: require('src/images/BUS_ON.png'),
        gnbBusOff: require('src/images/BUS_OFF.png'),
        gnbQROn: require('src/images/QR_ON.png'),
        gnbQROn2: require('src/images/QR_ON2.png'),
        gnbQROff: require('src/images/QR_OFF.png'),
        gnbBoardOn: require('src/images/COMM_ON.png'),
        gnbBoardOff: require('src/images/COMM_OFF.png'),
        gnbMyOn: require('src/images/MY_ON.png'),
        gnbMyOff: require('src/images/MY_OFF.png'),
        tabBar: require('src/images/bar.png'),
        detail: require('src/images/2_detail.png'),
        detailOff: require('src/images/3_detail_off.png'),
        detailOn: require('src/images/3_detail_on.png'),
        avatar: require('src/images/2_profile.png'),
        avatarOff: require('src/images/avatar_off.png'),
        mapBusStataion: require('src/images/2_station_off.png'),
        mapBusStataionSelected: require('src/images/2_station_select.png'),
        camera: require('src/images/3_camera.png'),
        commentProfile1: require('src/images/4_comment_profile_1.png'),
        commentProfile2: require('src/images/4_comment_profile_2.png'),
        questionOff: require('src/images/3_question_off.png'),
        questionOn: require('src/images/3_question_on.png'),
        likeOff: require('src/images/2_like_off.png'),
        likeOn: require('src/images/2_like_on.png'),
        loginLogo: require('src/images/login_b_logo.png'),
        loginLogoK: require('src/images/login_k_logo.png'),
        loginLogoN: require('src/images/login_n_logo.png'),
        loginLogoF: require('src/images/login_f_logo.png'),
        newPost: require('src/images/1_new_post.png'),
        searchPost: require('src/images/1_search_post.png'),
        goBack: require('src/images/1_go_back.png'),
        refreshButton : require('src/images/3_refresh.png'),
        nextPage : require('src/images/3_next_page.png'),
        prevPage : require('src/images/3_prev_page.png'),
        searchPostOff : require('src/images/2_search_post_off.png'),
        deleteInput : require('src/images/2_delete_input.png'),
        threeDot : require('src/images/1_three_dot.png'),
        toggleOn: require('src/images/toggle_on.png'),
        toggleOff: require('src/images/toggle_off.png'),
        sharePost : require('src/images/2_share_post.png'),
        success : require('src/images/2_success.png'),
        fail : require('src/images/2_fail.png'),
        busAlarmOn : require('src/images/2_bus_alarm_on.png'),
        busAlarmOff : require('src/images/2_bus_alarm_off.png'),
    },
    member: {
        member_id: '',
        nickname: '',
        setting: {
            bus_board_notification: true,
            my_content_notification: true,
            roll_dice_notification: true,
        },
    },
    access_info: null,
    cameraDeviceMaxZoomIOS: 8,
    cameraDeviceDefaultZoomIOS: 8,
    constant: {
        headerHeight: 52,
    },
    thumbnail: {
        width: 256,
        height: 256,
    },
    size: {
        _1440px: 360,
        _1400px: 350,
        _1200px: 300,
        _1080px: 270,
        _1000px: 250,
        _840px: 210,
        _800px: 200,
        _680px: 170,
        _648px: 162,
        _616px: 151.5,
        _600px: 150,
        _580px: 145,
        _560px: 140,
        _552px: 138,
        _520px: 130,
        _440px: 110,
        _328px: 82,
        _280px: 70,
        _240px: 60,
        _220px: 55,
        _212px: 53,
        _208px: 52,
        _200px: 50,
        _184px: 46,
        _160px: 40,
        _140px: 48,
        _136px: 34,
        _128px: 32,
        _120px: 30,
        _112px: 28,
        _100px: 25,
        _96px: 24,
        _84px: 21,
        _80px: 20,
        _70px: 17.5,
        _64px: 16,
        _60px: 15,
        _56px: 14,
        _50px: 12.5,
        _48px: 10,
        _44px: 11,
        _42px: 10,
        _40px: 9,
        _38px: 8,
        _36px: 9,
        _32px: 8,
        _28px: 6,
        _16px: 4,
        _8px: 2,
        _4px: 1,
        _2px: 0.5,
    },
    fontSize: {
        _172px: getFontSize(43),
        _104px: getFontSize(26),
        _84px: getFontSize(21),
        _76px: getFontSize(19),
        _68px: getFontSize(17),
        _64px: getFontSize(16),
        _60px: getFontSize(15),
        _56px: getFontSize(14),
        _52px: getFontSize(13),
        _48px: getFontSize(12),
        _44px: getFontSize(11),
        _40px: getFontSize(10),
        _36px: getFontSize(9),
        _28px: getFontSize(8),
    },
    defaultFontFamily: 'NotoSansKR-Regular',
    defaultMediumFontFamily: 'NotoSansKR-Medium',
    defaultLightFontFamily: 'NotoSansKR-Light',
    defaultBoldFontFamily: 'NotoSansKR-Bold',
    altFontFamily: Platform.OS === 'ios' ? 'NotoSansKR-Regular' : 'monospace',
    notification: {
        senderIDForIOS: '742281845086',
        serverKeyForIOS: 'AAAA8SnjlZE:APA91bFIcz-kWjN3jyWcSb7o4hug_2fPJM8H97fkJiz-jxuBKvpGT_eWwijxbAqdpdj1IxYK38OJrVSxZR6JfjgPyslpCHUL5OpnmPvz3DbyR4tlfXfc8nXK5HKZCagTyMtXDVSLT7zC',
        transTokenForIOS: 'https://iid.googleapis.com/iid/v1:batchImport',
        appIDForIOS: 'com.yaptv.busjok',
        senderID: "1035789899153",
        serverKey: "AAAA8SnjlZE:APA91bFIcz-kWjN3jyWcSb7o4hug_2fPJM8H97fkJiz-jxuBKvpGT_eWwijxbAqdpdj1IxYK38OJrVSxZR6JfjgPyslpCHUL5OpnmPvz3DbyR4tlfXfc8nXK5HKZCagTyMtXDVSLT7zC"
    },
    version: {
        ios: "",
        android: "",
    },
    appStoreID : "1546582845",
    packageName : "com.yaptv.busjok",
    googlePlayStoreUrl : "market://details?id=com.yaptv.busjok"
};



