import React, { Component }  from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Image, Platform, StatusBar, Linking,
} from 'react-native';
import config from 'src/config'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AsyncStorage from "@react-native-community/async-storage";
import Apple from "./Apple"
import Kakao from "./Kakao"
import Naver from "./Naver"
import Facebook from "./Facebook"
import axios from 'axios';
import Loading from "src/containers/Community/Loading"
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';
import {getStatusBarHeight} from "react-native-status-bar-height";

export default class LoginScreen extends Component {



  static navigationOptions =  ({ navigation }) => {
    // const onUserGoBack = null;
    const onUserGoBack = navigation.state.params? navigation.state.params.onUserGoBack: null;
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"white"} onUserGoBack={onUserGoBack}/>,
      headerTitle :() => <HeaderTitle style={[styles.headerTitle]} title={"로그인"} />,
      // headerTitle :() => <HeaderImage img={config.images.busStation}
      //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#01a3ff",
        borderBottomWidth: 0.5,
        borderBottomColor: "#01a3ff",
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }
  }

  state = {
    isLoading: false,
    isFocused: false,
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack()

  }
  componentDidMount() {
    if (this.props.navigation.state.params && this.props.navigation.state.params.onUserGoBack)
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    const {navigation} = this.props;
    this.subs = [
      navigation.addListener("didFocus", () => {
        this.setState({ isFocused: true });
      }),
      navigation.addListener("willBlur", () => {
        this.setState({ isFocused: false,});
      }),
    ];

  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.subs && this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  login = async (info) => {

    if(!info) {
      this.setLoading(false)
      return
    }

    console.log('LoginScreen login info', info)

    info.profile.email = info.profile.email || info.profile.id.toString();
    info.profile.nickname = info.profile.nickname || info.profile.id.toString();
    info.profile.name = info.profile.name || info.profile.id.toString();

    let send_data = {
      "device": {
        "device_id": config.deviceInfo.device_id,
        "session_id": config.sessionInfo.session_id
      },
      "access_token": info.access_token,
      "account": info.profile.email,
      "sns_type": info.type,
      "nickname": info.profile.nickname,
      "name": info.profile.name,
    }

    console.log('XXXXXXXXXXXXX send_Data', send_data, JSON.stringify(send_data))
    fetch(config.url.apiSvr + "/private/Login", {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
          'data': send_data,
      }),
    })
    .then(res => {
      if (res) {
        return res.json()
      }
    })
    .then(json => {
      if (json.err) {
        console.log('/private/login error', json);
        config.access_info = null;
        this.setLoading(false)

      }
      else {
        console.log("/private/Login success : ", json.data)
        config.member = json.data.member;

        info.profile.nickname = json.data.member.nickname || info.profile.nickname;

        AsyncStorage.setItem("access_info", JSON.stringify(info))
        console.log('****************login success', config.deviceInfo.device_id)
        console.log('****************login success', config.sessionInfo.session_id)
        console.log('****************login success', info.access_token)
        console.log('****************login success', info.type)
        console.log('****************login success : email', info.profile.email)
        console.log('****************login success : nickname', info.profile.nickname)
        console.log('****************login success : name', info.profile.name)
        console.log('****************login success : id', info.profile.id)

        config.access_info = info;

        this.props.navigation.popToTop();

      }
    }).catch(err => {
      console.log("/private/Login err : ", err)
    })

  }

  goHome = () => {
    this.props.navigation.popToTop();
    this.props.navigation.navigate('homeScreen')
  }

  setLoading = (flag) => {
    this.setState({
      isLoading: flag
    })
  }

  render() {
    // const {navigation} = this.props;

    // console.log('LOGINSCREEN - navigation.state.params', navigation.state.params)

    return (
      <View style={styles.container}>
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'#01a3ff'}/>}
        <View>

        </View>
        {this.state.isLoading?
          <View style={{position: 'absolute', height: '100%', width: '100%', backgroundColor: '#01a3ff'}}>
              <Loading color={'#eb6a0b'}/>
          </View>
        : <>
            <View style={styles.header}>
              <Image style={styles.headerLogo} source={config.images.loginLogo} />
            </View>
            <View style={styles.inform}>
              <StyledText style={[styles.text, styles.anonymous]}>이벤트 응모를 원하시면</StyledText>
              <StyledText style={[styles.text, styles.anonymous]}>
                <StyledText style={{fontFamily:config.defaultBoldFontFamily, color: 'black'}}>로그인</StyledText> 해주세요</StyledText>
                <StyledText style={[styles.text, styles.anonymous]}></StyledText>
                <StyledText style={[styles.text, styles.anonymous]}></StyledText>
                <StyledText style={[styles.text, styles.anonymous]}></StyledText>
                <StyledText style={[styles.text, styles.anonymous]}></StyledText>
            </View>
            
            <View style={styles.login}>
                {/* <StyledText style={[styles.text, styles.anonymous]}>간편 로그인</StyledText>
                <StyledText style={[styles.text, styles.anonymous]}></StyledText> */}
                {Platform.OS === 'ios' && <Apple do="login"
                  preOperation={()=>this.setLoading(true)}
                  callback={this.login} 
                />}
                <Kakao do="login"
                  style={[styles.button, styles.kakao]}
                  textStyle={[styles.buttonText, styles.kakao]}
                  iconStyle={[styles.iconStyle]}
                  iconSource={config.images.loginLogoK}
                  preOperation={()=>this.setLoading(true)}
                  callback={this.login}></Kakao>
              <Naver do="login"
                  style={[styles.button, styles.naver]}
                  textStyle={[styles.buttonText, styles.naver]}
                  iconStyle={[styles.iconStyle, {padding: 4}]}
                  iconSource={config.images.loginLogoN}
                  preOperation={()=>this.setLoading(true)}
                  callback={this.login}></Naver>
              <Facebook do="login"
                  style={[styles.button, styles.facebook]}
                  textStyle={[styles.buttonText, styles.facebook]}
                  iconStyle={[styles.iconStyle, {marginBottom: 3}]}
                  iconSource={config.images.loginLogoF}
                  preOperation={()=>this.setLoading(true)}
                  // before={()=> this.props.navigation.popToTop()}
                  callback={this.login}></Facebook>
              <View style={{flexDirection:"row"}}>
                <StyledText style={[styles.text, styles.anonymous,{fontSize:12}]}>{"*로그인 버튼을 누르면 "}</StyledText>
                <TouchableOpacity onPress={()=>{Linking.openURL(config.url.qrcode + "?command=servicePolicy")}}>
                  <StyledText style={[styles.text, styles.anonymous, {textDecorationLine: 'underline', color:"black", fontSize:12}]}>{" 이용약관"}</StyledText>
                </TouchableOpacity>
                
                <StyledText style={[styles.text, styles.anonymous,{fontSize:12}]}>{"에 동의한 것으로 간주합니다"}</StyledText>
              </View>
            </View>
            

          </>
        }
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container : {
    position: 'relative',
    flex: 1,
    backgroundColor: '#01a3ff',
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    position: 'absolute',
    top: config.size._328px,
    left: 0,
    width: '100%',
    height: config.size._440px,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  headerLogo: {
    width: config.size._1440px,
    height: config.size._440px,
  },
  inform: {
    // position: 'absolute',
    // top: config.size._1080px,
    // left: 0,
    width: '100%',
    height: config.size._400px,
    justifyContent: "center",
    alignItems: "center",
    // flexDirection: "row"
  },
  text : {
    alignItems : "center",
    textAlign : "center",
    fontSize : 17,
  },
  login: {
    position: 'absolute',
    bottom: config.size._136px,
    left: 0,
    width: '100%',
    justifyContent: "center",
    alignItems: "center"
  },
  button : {
    // flex:1,
    display: 'flex',
    flexDirection: 'row',
    width: config.size._1080px,
    height: config.size._200px,
    backgroundColor: config.colors.startButtonColor,
    // padding:10,
    margin : 3,
    marginBottom: config.size._40px,
    justifyContent : "center",
    alignItems: 'center',
    alignSelf : "center",
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
  },
  buttonText : {
    alignItems : "center",
    textAlign : "center",
    fontSize : 17,
    width: config.size._600px,
  },
  kakao: {
    backgroundColor: "#fee500",
    color: "#1a1a1c"
  },
  facebook: {
    backgroundColor: "#1877f2",
    color: "#fff"
  },
  naver: {
    backgroundColor: "#19ce60",
    color: "#fff"
  },
  iconStyle: {
    width: config.size._100px,
    height: config.size._100px,
  },
  anonymous: {
    backgroundColor: "transparent",
    color: "white",
    fontSize: 15
  },
  title : {
    alignItems : "center",
    color : "black",
    textAlign : "center",
    fontSize : 48,
  },
  headerTitle: {
    alignSelf: 'center',
    fontSize: 18,
    //fontWeight: 'bold',
    color: '#ffffff',
    fontFamily:config.defaultMediumFontFamily
  },

});

