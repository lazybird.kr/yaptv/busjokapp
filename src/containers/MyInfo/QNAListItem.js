import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    FlatList,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    Image,
    Text,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/AntDesign';
import config from '../../config';
import {StyledText} from 'src/components/StyledComponents';

const QNAListItem = ({info, isSelected, selectedPost, onDelete, onEdit, onView}) => {
    // console.log('\n\nXXXXXXXXXXX selectedPost: =>>>', selectedPost)
    // console.log('\n\nXXXXXXXXXXX selectedPost.comment_list: =>>>', selectedPost.comment_list)

    const renderContent = (post, type) => {
        // const post = selectedPost;
        if (post && post.content_list) {
            let renderElement = post.content_list.map((e, i) => {
                if (e.type === 'text') {
                    const textStyle = type === 'post'?
                        styles.questionText
                        :
                        styles.answerText;

                    return (
                        <View style={{
                            flex: 1,
                            // paddingVertical: 10,
                            // paddingTop: 10,
                            // paddingHorizontal: 10,
                        }}
                              key={e.type + i}
                        >
                            <StyledText selectable={true} style={textStyle}>
                                {e.text}
                            </StyledText>
                        </View>
                    );
                } else if (e.type === 'image') {
                    // console.log(e, (e.height * Dimensions.get('window').width) / e.width, Dimensions.get('window').width);
                    const paddingLeft = config.size._96px + config.size._28px;
                    const depth = 0;
                    const adjWidth = 20 * (2 * depth + 1) + paddingLeft;
                    
                    const imageStyle = type === 'post'? 
                        {
                            flex: 1,
                            paddingVertical: 10,
                            height: (e.height * Dimensions.get('window').width - adjWidth) / e.width,
                            width: Dimensions.get('window').width - adjWidth,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }
                        :
                        {
                            flex: 1,
                            height: (e.height * (Dimensions.get('window').width - adjWidth)) / e.width,
                            width: Dimensions.get('window').width - adjWidth,
                            justifyContent: 'center',
                            alignItems: 'center',
                            // marginLeft: 0,
                            // marginTop: 10,
                            // marginBottom: 10,
                            backgroundColor: 'rgba(0, 0, 0, .5)',
                        }
                        ;
                    return (
                        <View style={imageStyle}
                              key={e.type + i}
                        >
                            <Image source={{uri: e.uri}}
                                   style={{
                                    width: '100%',
                                    height: '100%',
                                    resizeMode: 'contain',
                                   }}
                            />
                        </View>
                    );
                }
            });
            return renderElement;
        } else {
            return <></>;
        }
    };

    const renderComment = (post) => {

        if (post && post.comment_list) {
            return post.comment_list.map((e, i) => {
                return renderContent(e, 'comment')
            })
        }

    }

    let titleAdjusted = info.title;
    let maxLen = 16;
    if (titleAdjusted.length > maxLen) {
        titleAdjusted = titleAdjusted.substr(0, maxLen) + '...';
    }

    const complete = info.comment_count? true: false;

    return (
    <View style={{flex:1}}>
        {/* <View > */}
        <TouchableOpacity style={[styles.item, styles.padding]} onPress={onView}>
            <View style={styles.icon}>
                <Image source={config.images.questionOff} style={{width: config.size._96px, height: config.size._96px}}/>
            </View>
            <View style={styles.contents}>
                <View style={styles.titleArea}>
                    <StyledText style={styles.title}>{titleAdjusted}</StyledText>
                    {!info.comment_count? 
                        <View style={styles.titleButtonArea}>
                            <TouchableOpacity style={styles.titleButton} onPress={onEdit}>
                                <StyledText style={styles.titleButtonText}>수정</StyledText>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.titleButton} onPress={onDelete}>
                                <StyledText style={styles.titleButtonText}>삭제</StyledText>
                            </TouchableOpacity>
                        </View>
                        :
                        <></>
                    }
                </View>
                <View style={styles.info}>
                    <StyledText style={[styles.infoText, styles.time]}>{info.published_at}</StyledText>
                    <StyledText style={[styles.status, styles.titleButton, styles.titleButtonText, complete? styles.complete: '']}>{complete? '답변완료': '답변대기'}</StyledText>
                </View>
            </View>     
            <View style={styles.icon}>
                <Image source={isSelected? config.images.detailOff: config.images.detailOn} style={{width: config.size._96px, height: config.size._96px}}/>
            </View>
        </TouchableOpacity>
               

        {/* </View> */}
        {isSelected? 
        <View style={styles.detail}>
            <View style={styles.question}>{renderContent(selectedPost, 'post')}</View>
            {info.comment_count?
                <View style={styles.answer}>
                    <View style={styles.icon}>
                        <Image source={config.images.questionOn} style={{width: config.size._96px, height: config.size._96px}}/>
                    </View>
                    <View style={styles.answerContent}>
                        {renderComment(selectedPost)}
                    </View>
                </View>
                :
                <></>
            }
            
        </View>
        :
        <></>}


    </View>
    ) 

}
    
export default QNAListItem;

const styles=StyleSheet.create({
    padding: {
        paddingHorizontal: config.size._64px,
    },
    item: {
        height: config.size._220px,
        width: '100%',
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        flexDirection: 'row',
        backgroundColor: '#fff',
        marginTop: 0.5,
        // paddingBottom: 0.5,
    },
   
    icon: {
        width: config.size._96ox,
        height: config.size._96ox,
        justifyContent: 'center',
        alignContent: 'center',
    },
    contents: {
        flex: 9,
        paddingLeft: config.size._28px,
        justifyContent: 'center',
        alignContent: 'center',
    },
    titleArea: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        // alignItems: 'flex-end'
    },
    title: {
        // height: '80%',
        fontSize: config.fontSize._60px,
    },
    titleButtonArea: {
        height: '80%',
        flexDirection: 'row',
        marginLeft: 10,
        alignItems: 'center',
    },
    titleButton: {
        marginRight: 6,


    },
    titleButtonText: {
        color: config.colors.greyColor,
        fontSize: config.fontSize._36px,

        borderWidth: 0.5,
        borderColor: config.colors.greyColor,
        borderRadius: 7,
        paddingHorizontal: 5,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    info: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start'

    },
    infoText: {
        fontSize: config.fontSize._40px,
        color: config.colors.greyColor
    },
    status: {
        marginLeft: 10,
        
    },
    complete: {
        color: config.colors.baseColor2,
        borderColor: config.colors.baseColor2,
    },
    detail: {
        flex: 1,
        paddingVertical: 10,
        paddingLeft: 42,
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        marginBottom: 0.5,

    },
    question: {
        flex: 1,
        // paddingVertical: 15,
    },
    questionText: {
        fontSize: config.fontSize._48px,
        color: '#424242'
    },
    answer: {
        flexDirection: 'row',
        paddingVertical: 15,
    },
    answerContent: {
        paddingLeft: config.size._28px,
    },
    answerText: {
        fontSize: config.fontSize._48px,
        color: 'black'
    }

});