import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
    Appearance, Keyboard,
} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import Profile from './Profile';
import config from 'src/config';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledText, StyledTextInput} from 'src/components/StyledComponents';
import Kakao from "../Login/Kakao"
import Naver from "../Login/Naver"
import Facebook from "../Login/Facebook"
import Apple from "../Login/Apple"
import {PopupDialog} from 'src/components/PopupDialog';
import {getStatusBarHeight} from "react-native-status-bar-height";


export default class AccountScreen extends Component {
    //닉네임변경시 저장버튼 활성화

    static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
      headerTitle :() => <HeaderTitle style={[styles.headerTitle]} title={"내정보"} />,
      headerRight: () => {
        let button = <></>;
        let buttonDisabled = navigation.getParam('disabled');
        // if (navigation.getParam('submitText'))
          button = (
            <TouchableOpacity
                disabled={buttonDisabled}
                style={{marginRight: 15}}
                onPress={navigation.getParam('onPressSubmit')}>
                <StyledText style={{fontSize: config.fontSize._60px, color: buttonDisabled? config.colors.greyColor:config.colors.baseColor2}}>
                    저장
                </StyledText>
            </TouchableOpacity>
          );

            return button;
        },
        headerStyle : {
            backgroundColor: "#ffffff",
            borderBottomWidth: 0.5,
            elevation: 0,       //remove shadow on Android
            shadowOpacity: 0,   //remove shadow on iOS
            height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
        }
        }
    }



    state = {
        nickname: '',
        orgNickname: '',
        openConfirm: false,
        isFocused: false,
        showErrorDialog: false,
        errMessage : ""
    }

    logoutRef =  React.createRef();

    onPressSubmit = () => {
        const {nickname} = this.state;
        if(!nickname){
            this.setState({showErrorDialog:true,
                errMessage : "닉네임이 너무 짧습니다." 
            });
            return;
        }
        if(nickname && (nickname.length<2 || nickname.length>10 )){
            this.setState({showErrorDialog:true,
                errMessage : "닉네임은 2자이상 10자이내입니다." 
            });
            return;
        }
        if(nickname && (nickname.includes(" ") )){
            this.setState({showErrorDialog:true,
                errMessage : "닉네임은 공백이 포함되면 안됩니다." 
            });
            return;
        }
        
        let send_data = {
            "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id
            },
            "access_token": config.access_info.access_token,
            "member": {
                "nickname": this.state.nickname,
            }
        }
        console.log('XXXXXXXXXXXXX send_Data', send_data, JSON.stringify(send_data))

        fetch(config.url.apiSvr + "/private/UpdateProfile", {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
        .then(res => {
            if (res) {
            return res.json()
            }
        })
        .then(json => {
            if (json.err) {
                console.log('/private/UpdateProfile error', json);
            }
            else {
                console.log("/private/UpdateProfile success : ", json.data);
                config.access_info.profile.nickname = this.state.nickname;
                config.member.nickname = this.state.nickname;
                this.props.navigation.setParams({disabled: true})
                this.textInput && this.textInput.getInnerRef().blur();
            }
        }).catch(err => {
            console.log("/private/UpdateProfile err : ", err)
        })

    }


    componentDidMount() {
        const {access_info} = this.props.navigation.state.params;
        const profile = access_info.profile;

        const {navigation} = this.props;
        this.subs = [
            navigation.addListener("didFocus", () => {
                this.setState({ isFocused: true });
            }),
            navigation.addListener("willBlur", () => {
                this.setState({ isFocused: false,});
            }),
        ];
        this.setState({
            nickname: config.member.nickname || profile.nickname,
            orgNickname: config.member.nickname || profile.nickname
        })

    }
    componentWillUnmount() {
        this.subs && this.subs.forEach((sub) => {
            sub.remove();
        });
    }

    changeRegisterButton = () => {
        let disabled = true;
        if (this.state.nickname !== this.state.orgNickname)
            disabled = false;
        this.props.navigation.setParams({disabled, onPressSubmit: this.onPressSubmit})
      };


    componentDidUpdate(prevProps, prevState) {
        if (this.state.nickname !== prevState.nickname) {
            this.changeRegisterButton()
        }
    }

    logout = async () => {
        const {access_info} = config;
        let send_data = {
            "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id
            },
            "access_token": access_info.access_token,
        }
        await AsyncStorage.removeItem("access_info")
        config.member = {
          member_id: '',
          nickname: '',
        };
        config.access_info = null;
        fetch(config.url.apiSvr + "/private/Logout", {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                if (res) {
                    return res.json()
                }
            })
            .then(json => {
                if (json.err) {
                    console.log('/private/login error', json)
                }
                else {
                    console.log("/private/Logout success : ", json.data)
                }
                this.props.navigation.popToTop();
            }).catch(err => {
            console.log("/private/Login err : ", err)
        })
    }

    openConfirm = () => {
        this.setState({
            openConfirm: true,
        })
    }

    onDialog = (result) => {
        if (result) { //로그아웃 - 예
            this.setState({
                openConfirm: false,
            });
            this.logoutRef.current.logout()
        }
        else { //로그아웃 - 아니요
            this.setState({
                openConfirm: false,
            });
        }

    }

    renderOpenConfirm = () => {

        return (
            <PopupDialog
                visible={this.state.openConfirm}
                message={'로그아웃 하시겠습니까?'}
                leftButtonLabel={'예'}
                leftButtonStyle={{borderWidth:1, borderColor: config.colors.greyColor}}
                leftButtonTextStyle={{color: config.colors.greyColor}}
                onLeftButtonPress={() => {
                    this.onDialog(true)
                }}
                rightButtonLabel={'아니요'}
                rightButtonStyle={{backgroundColor: config.colors.baseColor2}}
                rightButtonTextStyle={{color: '#ffffff'}}
                onRightButtonPress={() => {
                    this.onDialog(false)
                }}
                onBackdropPress={() =>
                    this.setState({
                        openConfirm: false,
                    })}
                onBackButtonPress={() =>
                    this.setState({
                        openConfirm: false,
                    })}
                secureTextEntry={true}
            />
        )

    }
    render() {
        const {access_info} = config;

        const loginComponent = {
          kakao: Kakao,
          naver: Naver,
          facebook: Facebook,
          apple: Apple,
        }

        const LogoutButton = loginComponent[access_info.type];
        return (
            <SafeAreaView style={styles.container}>
                {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
                <PopupDialog
                    visible={this.state.showErrorDialog}
                    message={this.state.errMessage}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                    this.setState({
                        showErrorDialog: false,
                    });
                    }}
                />
                <View style={styles.profile}>
                    <Image style={styles.profileImage} source={config.images.avatar}></Image>
                    <View style={styles.inputField}>
                        <StyledTextInput style={styles.profileText}
                                        ref={(input) => this.textInput = input}
                                        onChangeText={(e) => {
                                            this.setState({
                                                nickname: e
                                            })
                                        }}
                        >
                            {this.state.nickname}
                        </StyledTextInput>
                        <TouchableOpacity style={styles.deleteButtonWrapper}
                            onPress={()=>this.setState({nickname:''})}>
                            <Image style={styles.deleteButton} source={require('src/images/delete.png')}/>
                        </TouchableOpacity>
                    </View>
                    <StyledText style={styles.notice}>최소 2자 이상 최대 10자 이내로 작성해 주세요.</StyledText>
                </View>
                <View style={styles.bottom}>
                    <LogoutButton do='logout'
                        ref={this.logoutRef}
                        onPress={this.openConfirm}
                        textStyle={{fontSize: config.fontSize._52px, color: config.colors.greyColor3}}
                        token={access_info.access_token}
                        callback={this.logout}
                    />
                </View>
                {this.state.openConfirm? this.renderOpenConfirm(): null}
            </SafeAreaView>

        )
    }
}



const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: config.colors.greyColor2
    },
    profile: {
        height: config.size._800px,
        // flex: 1,
        paddingTop: 30,
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white',
        borderBottomWidth: config.size._2px,
        // borderTopWidth: 1,
        borderColor: config.colors.lineColor,
    },
    profileImage: {
        // flex: 1,
        width: config.size._240px,
        height: config.size._240px,
        borderRadius: config.size._240px
    },
    inputField: {
        flexDirection: 'row',
        height: config.size._220px,
        // width: '100%',
        alignItems: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: config.colors.lineColor,
        // borderWidth: 1,
        // borderColor: 'red'

    },
    profileText: {
        // flex: 1,
        // width: '100%',
        height: '70%',
        minWidth: config.size._840px,
        // height: config.size._64px,
        paddingBottom: 8,
        paddingLeft: config.size._84px,
        paddingRight: config.size._8px,
        color: 'black',
        fontSize: config.size._64px,

        textAlignVertical: 'center',
        textAlign: 'center',
        // borderWidth: 1,
        // borderColor: 'red'
    },
    deleteButtonWrapper: {
        height: '65%',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    deleteButton: {
        width: config.size._80px,
        height: config.size._80px,
    },
    notice: {
        paddingTop: config.size._28px,
        fontSize: config.fontSize._44px,
        color: '#757575'
    },
    bottom: {
        paddingTop: config.size._64px,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        alignSelf: 'center',
        fontSize: 18,
        //fontWeight: 'bold',
        fontFamily:config.defaultMediumFontFamily
    },

  });
