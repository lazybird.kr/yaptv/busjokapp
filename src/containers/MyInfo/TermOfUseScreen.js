import React,{useState, useEffect} from 'react';
import {Platform, StyleSheet, StatusBar, View, Image, TouchableOpacity, } from 'react-native';
import config from 'src/config';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import IconEmbededMenu from 'src/components/IconEmbededMenu';
import {getStatusBarHeight} from "react-native-status-bar-height";
import {StyledText} from 'src/components/StyledComponents';

export default function TermOfUse(props) {

    const openWebView = (menu, title) => {
        props.navigation.navigate("webViewComponent", {
          uri : config.url.qrcode + "?command=" + menu,
          title
        });
      }

    return (
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" backgroundColor={"#ffffff"}/>
            <IconEmbededMenu mainStyle={styles.list} onPress={()=>openWebView('privacyPolicy', '개인정보 처리 방침')}>
                <StyledText style={styles.listText}>개인정보 처리 방침
                </StyledText>
            </IconEmbededMenu>
            <IconEmbededMenu mainStyle={styles.list} onPress={()=>openWebView('licenseScreen', '오픈소스 라이선스')}>
                <StyledText style={styles.listText}>오픈소스 라이선스
                </StyledText>
            </IconEmbededMenu>
            <IconEmbededMenu mainStyle={styles.list} onPress={()=>openWebView('servicePolicy', '서비스 이용약관')}>
                <StyledText style={styles.listText}>서비스 이용약관
                </StyledText>
            </IconEmbededMenu>
            <IconEmbededMenu mainStyle={[styles.list, styles.listTail]} onPress={()=>openWebView('locationPolicy', '위치기반 서비스 이용약관')}>
                <StyledText style={styles.listText}>위치기반 서비스 이용약관
                </StyledText>
            </IconEmbededMenu>
        </View>

    )
}

TermOfUse.navigationOptions =  ({ navigation }) => {
  return {
    headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
    headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"이용약관 및 정책"} />,
    headerRight : () => <></>,
    headerStyle : {
      backgroundColor: "#ffffff",
      borderBottomWidth: 0.5,
      borderColor: config.colors.lineColor,
      elevation: 0,       //remove shadow on Android
      shadowOpacity: 0,   //remove shadow on iOS
      height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
    }
  }
}

const styles=StyleSheet.create({
    container : {
      flex: 1,
      backgroundColor: 'white',
    },
    list: {
      height: config.size._240px,
      width: '100%',
      borderBottomWidth: 0.5,
      borderColor: config.colors.lineColor,
      paddingLeft: config.size._100px,
      paddingRight: config.size._64px 
    },
    listTail: {
      borderBottomWidth: 0.5
    },
    listText: {
      flex: 1,
      fontSize: config.size._60px,
      textAlignVertical: 'center',
      textAlign: 'left',
    },    
  });
  