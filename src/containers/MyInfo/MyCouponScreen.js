import React, {Component} from 'react';
import {Platform, 
    StyleSheet, 
    Image, 
    View, 
    StatusBar, 
    TouchableOpacity, 
    SafeAreaView, 
    FlatList,
    Linking,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import config from 'src/config';
import {getStatusBarHeight} from "react-native-status-bar-height";
import {StyledText, StyledTextInput} from 'src/components/StyledComponents';
import Loading from 'src/components/Loading';
import axios from 'axios';
import PopupDialog from '../../components/PopupDialog/PopupDialog';
import showToast from 'src/components/Toast';

export default class MyCoupon extends Component {
    state = {
        prize_list : [],
        loading : false,
        showDeletePopup: false,
        deletePrizeId: "",
    }

    getFormattedDate = (date) => {
        let year = date.getFullYear();	//yyyy
        let month = (1 + date.getMonth());	//M
        month = month >= 10 ? month : '0' + month;	//month 두자리로 저장
        let day = date.getDate();	//d
        day = day >= 10 ? day : '0' + day;	//day 두자리로 저장
        return year + "." + month + "." + day + "까지"
      }

    static navigationOptions =  ({ navigation }) => {
        return {
          headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
          headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"내쿠폰함"} />,
          // headerTitle :() => <HeaderImage img={config.images.busStation}
          //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
          headerRight : () => <></>,
          headerStyle : {
            backgroundColor: "#ffffff",
            borderBottomWidth: 0.5,
            borderColor: config.colors.lineColor,
            elevation: 0,       //remove shadow on Android
            shadowOpacity: 0,   //remove shadow on iOS
            height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
          }
        }
    }
    onPressDelete = (prize_id) => {
        this.setState({
            showDeletePopup : true,
            deletePrizeId : prize_id
        })
    }

    deleteCoupon = async (prize_id) => {
        console.log("prize_id : ", prize_id);
        try {
            const resp =  await axios.post(config.url.apiSvr + "/private/RemoveCoupon", {
                "data": {
                  "device": {
                    "device_id": config.deviceInfo.device_id,
                    "session_id": config.sessionInfo.session_id,
                  },
                  "access_token": config.access_info.access_token,
                  "prize":{
                      "prize_id" : prize_id
                  }
                } 
            });
            if(resp.data.err){
                //error
            } else {
                console.log("resp : ", resp.data.data);
                this.setState({loading:true});
                this.getCoupons();
            }
            
          } catch(e){
            console.log("e : ", e); 
          }
    }

    componentDidMount = () =>{
        this.setState({loading:true});
        this.getCoupons();
    }

    getCoupons = async () => {
        try {
            const resp =  await axios.post(config.url.apiSvr + "/private/GetCoupons", {
                "data": {
                  "device": {
                    "device_id": config.deviceInfo.device_id,
                    "session_id": config.sessionInfo.session_id,
                  },
                  "access_token": config.access_info.access_token,
                } 
            });
            console.log("resp : ", resp.data.data);
            const {prize_list} = resp.data.data;
            this.setState({prize_list : prize_list || [], loading:false})
          } catch(e){
            console.log("e : ", e); 
            this.setState({loading:false})
          }
    }

    renderItem = ({item}) => {
        console.log("item : ", item)
        return (
            <TouchableOpacity style={styles.item} onPress={()=>{
                this.props.navigation.navigate("myCouponImage",{image:item.coupon.uri.image})
            }}>
                <View style={styles.itemLeft}>
                    <View style={{flex:2, justifyContent:"center", alignItems:"center"}}>
                        <Image source={{uri:item.coupon.uri.thumbnail}} style={{width:100, height:100,resizeMode:"contain"}}/>
                    </View>
                    <View style={{flex:3}}>
                        <View style={{flex:1}}>
                            <StyledText numberOfLines={1} style={styles.itemName}>{item.coupon.title}</StyledText>
                            <StyledText numberOfLines={1} style={styles.itemCode}>{item.coupon.content}</StyledText>
                            <StyledText numberOfLines={1} style={styles.itemDate}>{this.getFormattedDate(new Date(item.coupon.finished_at))}</StyledText>
                        </View>
                        <View style={{flex:1}}>
                            <View style={{justifyContent:"flex-end", alignItems:"flex-end",flex:1}}>
                                <TouchableOpacity style={{width:100, height:30, borderRadius:15, borderColor:"#9e9e9e", borderWidth:0, backgroundColor:"#02a3fe", justifyContent:"center",alignItems:"center"}}
                                onPress={()=>{
                                    Clipboard.setString(item.coupon.coupon_code);
                                    showToast('복사되었습니다.');
                                }}
                                >
                                    <StyledText style={{color:"#ffffff"}}
                                        adjustsFontSizeToFit={true}
                                        numberOfLines={1}
                                    >{"쿠폰 복사하기"}</StyledText>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                    </View>
                        
                    
                </View>
                <View style={styles.itemRight}>
                    <TouchableOpacity style={{alignItems:"center",justifyContent:"center",flex:1}}
                    onPress={()=>{this.onPressDelete(item.prize_id)}}
                    >
                        <StyledText style={{color:"white"}}>{"삭제"}</StyledText>
                    </TouchableOpacity>
                    
                </View>
                
            </TouchableOpacity>
        )
    } 


    render() {
        const {prize_list, loading, showDeletePopup, deletePrizeId} = this.state;
        
        return (
            <SafeAreaView style={styles.container}>
                {loading && <Loading color={'#01a3ff'}/>}
                {
                    <PopupDialog
                    visible={showDeletePopup}
                    message={'정말 삭제하시겠습니까?'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDeletePopup: false,
                            deletePrizeId: "",
                        });
                    }}
                    rightButtonLabel={'삭제'}
                    rightButtonStyle={{backgroundColor: config.colors.baseColor}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.deleteCoupon(deletePrizeId);
                        this.setState({
                            deletePrizeId:"",
                            showDeletePopup: false,
                        });
                    }}
                />
                }
                <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
                <View style={{paddingLeft:15, height:50, flexDirection:"row",alignItems:"center"}}>
                    <StyledText style={{fontSize:16}}>{"전체"}</StyledText>
                    <StyledText style={{color:"#707070"}}>{"(" + prize_list.length + "건)"}</StyledText>
                </View>
                <FlatList
                    data={prize_list}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => 'coupon'+index}
                />
            </SafeAreaView>
        )
    }

}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        
        backgroundColor:"white"
    },
    item: {
        margin: 10, marginTop:0,
        height:120,flexDirection:"row"
    },
    itemLeft:{
        flex:8,
        padding:10,borderColor:"#bfbfbf", flexDirection:"row",
        borderWidth:1,borderRightWidth:0,borderTopLeftRadius:20,borderBottomLeftRadius:20,
    },
    itemRight:{
        flex:2,backgroundColor:"#02a3fe",borderTopRightRadius:20,borderBottomRightRadius:20,
        borderColor:"#bfbfbf",
        borderWidth:1,borderLeftWidth:0
    },
    itemName: {
        fontSize: 16,
        marginLeft: 10,
        lineHeight:20 , fontFamily:config.defaultMediumFontFamliy
    },
    itemCode:{
        fontSize: 14,lineHeight:18,
        marginLeft: 12, color:"#757575"
    },
    itemDate:{
        fontSize: 12,
        marginLeft: 12, color:"#c2c2c2"
    },
    itemCommon: {
        marginLeft: 20
    },  
    itemUrl: {
        color: 'blue'
    },
    itemCopyright: {
        color: 'darkgray'
    }
});