import React, {useState, useEffect} from 'react';
import {Platform, StyleSheet, StatusBar, View, Image, TouchableOpacity} from 'react-native';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import config from 'src/config';
import IconEmbededMenu from 'src/components/IconEmbededMenu';
import {getStatusBarHeight} from "react-native-status-bar-height";
import {StyledText} from 'src/components/StyledComponents';

export default function MyNotification(props) {

    const settingService = () => {
        
        const host = config.url.apiSvr;
        const deviceId = config.deviceInfo.device_id;
        const sessionId = config.sessionInfo.session_id;
    
        let send_data = {
          category: 'private',
          service: 'UpdateSetting',
          device: {
            device_id: deviceId,
            session_id: sessionId,
          },
          access_token: config.access_info.access_token,
          member: {
            setting: {
                bus_board_notification: bus,
                my_content_notification: post,
                roll_dice_notification: gift,
            }
          }
        };
    
        return fetch(host, {
          method: 'post',
          headers: {'Content-type': 'application/json'},
          body: JSON.stringify({
            data: send_data,
          }),
        })
          .then((res) => {
            // console.log('service', res);
            if (res) {
              return res.json();
            }
          })
          .then((json) => {
    
            if (!json) {
              return;
            }
    
            if (json.err) {
              console.log('service 실패', send_data.service, json);
            } else {
              console.log('service 성공', send_data.service, json);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      };
    
    const {bus_board_notification, my_content_notification, roll_dice_notification} = config.member.setting;

    const [all, setAll] = useState(bus_board_notification &&
                                  my_content_notification &&
                                  roll_dice_notification );


    const [bus, setBus] = useState(bus_board_notification);
    const [post, setPost] = useState(my_content_notification);
    const [gift, setGift] = useState(roll_dice_notification);

    useEffect(() => {
      setAll(bus && post && gift)
      settingService().then(() => {
        config.member.setting = {
          bus_board_notification: bus,
          my_content_notification: post,
          roll_dice_notification: gift,
        }
      })
    }, [bus, post, gift])

    return (
        <View style={[styles.container]}>
        {/* <View style={styles.lists}> */}
          <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
          <IconEmbededMenu mainStyle={[styles.list, styles.padding]} 
            icon={all? config.images.toggleOn: config.images.toggleOff}
            iconStyle={styles.icon} 
            onPress={() => { 
              setAll(!all)
              setBus(!all)
              setPost(!all)
              setGift(!all)
            }}
            >
            <StyledText style={styles.listText}>모든 알림</StyledText>
          </IconEmbededMenu>
          <IconEmbededMenu mainStyle={[styles.list, styles.padding]} 
            icon={bus? config.images.toggleOn: config.images.toggleOff}
            iconStyle={styles.icon} 
            onPress={() => setBus(!bus)}
            >
            <StyledText style={styles.listText}>버스 승차 알림</StyledText>
          </IconEmbededMenu>
          <IconEmbededMenu mainStyle={[styles.list, styles.padding]} 
            icon={post? config.images.toggleOn: config.images.toggleOff}
            iconStyle={styles.icon} 
            onPress={() => setPost(!post)}
            // onPress={()=>this.onPress('termOfUseScreen')}
            >
            <StyledText style={styles.listText}>내 게시글 알림</StyledText>
          </IconEmbededMenu>
          <IconEmbededMenu mainStyle={[styles.list, styles.padding, styles.listTail]} 
            icon={gift? config.images.toggleOn: config.images.toggleOff}
            iconStyle={styles.icon} 
            onPress={() => setGift(!gift)}
            // onPress={()=>this.onPress('termOfUseScreen')}
            >
            <StyledText style={styles.listText}>경품 응모 시간 알림</StyledText>
          </IconEmbededMenu>
        </View>

    )
}

MyNotification.navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"알림설정"} />,
      // headerTitle :() => <HeaderImage img={config.images.busStation}
      //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }
  }

const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // flexDirection: 'row',
        // alignItems: 'center',
        // borderWidth: 2,
        // borderColor: 'red'
    },
    list: {
        display: 'flex',
        height: config.size._240px,
        width: '100%',
        borderTopWidth: 0.5,
        borderColor: config.colors.lineColor,
        // justifyContent:"center",
        // alignContent:"center",
        // alignItems: 'center',
    },
    listTail: {
        borderBottomWidth: 0.5
      },
    padding: {
        paddingLeft: config.size._100px,
        paddingRight: config.size._64px 
    },
    listText: {
        flex: 1,
        fontSize: config.size._60px,
        textAlignVertical: 'center',
        textAlign: 'left',
    },
    icon: {
        width: config.size._184px,
        height: config.size._96px,
    }
  });