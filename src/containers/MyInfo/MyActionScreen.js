import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  StatusBar,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Linking,
} from 'react-native';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {getStatusBarHeight} from "react-native-status-bar-height";
import config from 'src/config';
import Loading from 'src/containers/Community/Loading';
import Post from 'src/containers/Community/Post';
import Tabs from 'src/components/Tabs';
import List from 'src/containers/Community/List';
import {StyledText} from 'src/components/StyledComponents';

const MENU = ['내 게시글', '내 댓글', '좋아요'];
const NUMBER_OF_POST_PER_PAGE = 25;

export default class MyActionScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={'black'} />,
      headerTitle: () => (
        <HeaderTitle
          style={{
            alignSelf: 'center',
            fontSize: 24,
          }}
          title={'내 게시물'}
        />
      ),
      // headerTitle :() => <HeaderImage img={config.images.busStation}
      //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight: () => <></>,
      headerStyle: {
        backgroundColor: '#ffffff',
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        elevation: 0, //remove shadow on Android
        shadowOpacity: 0, //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      },
    };
  };
  touch = {
    isTouchedStack: false,
  };
  state = {
    offset: 0,
    currentPage: 1,
    isFocused: false,
    postPerPage: NUMBER_OF_POST_PER_PAGE,
    pages: ['1'],
    currentPostList: [],
    noticeList: [],
    currentGroupName: 'default',
    hideLeft: true,
    hideRight: true,
    onRequesting: false,
    isSearchModal: false,
    itemHeight: 48,
    itemFontSize: 14,
    tabIndex: 0,
  };

  componentDidMount() {
    const {navigation} = this.props;
    this.subs = [
      navigation.addListener('didFocus', () => {
        this.setState({isFocused: true});
      }),
      navigation.addListener('willBlur', () => {
        this.setState({isFocused: false});
      }),
    ];
    this.serviceGetPostList();
  }

  componentWillUnmount() {
    this.subs &&
      this.subs.forEach((sub) => {
        sub.remove();
      });
  }
  componentDidUpdate(prevProps, prevState, snapshot){
    // const {navigation} = this.props;
    // const {prevNavigation} = prevProps;
    // // if (navigation) {
    // //     console.log('DEBUG--1');
    // //     navigation.navigate('list')
    // // }
    // console.log('--- componentDidUpdate', navigation.state.params);
    // if (navigation) {
    //   if (navigation.state.params && navigation.state.params.fromSharedLink) {
    //     console.log('FROM SHARED LINK', navigation.state.params.postId);
    //     if (this.lastDeepLinkTime !== navigation.state.params.at) {
    //       this.lastDeepLinkTime = navigation.state.params.at;
    //       this.onPressPost(navigation.state.params.postId);
    //     }
    //   }
    // }
  }

  // serviceGetPostListForTest = () => {
  //     this.serviceGetPostList('http://lazybird.mynetgear.com:1100/api', 'TEST-DEVICE-ID-XXX-1111', 'd6caf268-04b8-11eb-b870-02420aff0005', 'default', 0);
  // };
  serviceGetPostList = ({
    host = config.url.apiSvr,
    deviceId = config.deviceInfo.device_id,
    sessionId = config.sessionInfo.session_id,
    memberId = config.member.member_id,
    groupName = 'default',
    offset = this.state.offset,
    currentPage = this.state.currentPage,
    serviceName = 'GetPostList',
    length = this.state.postPerPage * 5 + 1,
  } = {}) => {
    
    
    let send_data = {
      category: 'board',
      service: serviceName,
      device: {
        device_id: deviceId,
        session_id: sessionId,
      },
      post: {
        group_name: groupName,
        offset: offset,
        length: length,
        // written_by: {
        //   member_id: writtenBy.memberId,
        // },

      },
    };

    switch(this.state.tabIndex) {
        case 0:
            send_data.post.written_by = {
                member_id: memberId
            };
            break;
        case 1:
            send_data.post.commented_by = {
                member_id: memberId
            };
            break;
        case 2:
            send_data.post.recommended_by = {
                member_id: memberId
            };
            break;
    }

    this.setState({
      onRequesting: true,
    });

    console.log('serviceGetPostList', send_data);
    fetch(host, {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        data: send_data,
      }),
    })
      .then((res) => {
        // console.log('service', res);
        if (res) {
          return res.json();
        }
      })
      .then((json) => {
        this.setState({
          onRequesting: false,
        });
        if (!json) {
          return;
        }

        if (json.err) {
          console.log('service 실패', send_data.service, json);
        } else {
          console.log('service 성공', send_data.service, json);
          let postList = [];
          let noticeList = [];
          if (json.data.post_list) {
            postList = json.data.post_list;
          }
          if (json.data.notice_list) {
            for (let i = 0; i < json.data.notice_list.length; i++) {
              noticeList.push({
                ...json.data.notice_list[i],
                isNotice: true,
              });
            }
          }
          let pages = [];
          let currentStartPageNumber = ~~(offset / this.state.postPerPage);
          let pageNumber = ~~(offset / this.state.postPerPage) + 1;
          let limit = 0;
          pages.push(pageNumber.toString());
          for (let i = 0; i < postList.length; i++) {
            if (i >= (limit + 1) * this.state.postPerPage) {
              pageNumber += 1;
              pages.push(pageNumber.toString());
              limit += 1;
              if (limit >= this.pageLimit - 1) {
                break;
              }
            }
          }
          // 포스팅 시작/끝
          let s =
            (currentPage - currentStartPageNumber - 1) * this.state.postPerPage;
          let e = s + this.state.postPerPage;

          // 앞/뒤 이동 화살표
          let l = true;
          let r = true;
          if (offset > 0) {
            l = false;
          }
          if (postList.length > this.state.postPerPage * 5) {
            r = false;
          }
          this.setState({
            currentPostList: postList.slice(s, e),
            noticeList: noticeList,
            currentPage: currentPage,
            pages: pages,
            offset: offset,
            hideLeft: l,
            hideRight: r,
          });
          this._refList.scrollToOffset(0);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  onPressPageNumber = (page, force) => {
    let group = '';
    console.log('onPressPageNumber', page, force);
    if (this.state.currentGroupName !== 'default') {
      group = this.state.currentGroupName;
    }

    let currentStartPage = ~~(page / (this.pageLimit + 1));
    console.log('currentStartPage', currentStartPage);
    let offset = currentStartPage * this.state.postPerPage * this.pageLimit;

    if (this.state.currentPage !== page || force) {
      if (group === '좋아요') {
        this.serviceGetPostList({
          offset: offset,
          currentPage: page,
          groupName: 'default',
          serviceName: 'GetHot',
          length: 3,
        });
      } else {
        this.serviceGetPostList({
          offset: offset,
          currentPage: page,
          groupName: group,
        });
      }
    }
  };
  onPressGroup = (group, index) => {
    this.setState({
        tabIndex: index
    }, () => {
        this.serviceGetPostList({
            offset: 0,
            currentPage: 1,
          });
    })
  };

  onPressMovePage = (direction) => {
    let offset =
      this.state.offset + direction * this.state.postPerPage * this.pageLimit;
    let startPageNumber = ~~(offset / this.state.postPerPage) + 1;
    let currentPage = startPageNumber;
    if (direction < 0) {
      currentPage += 4;
    }
    // console.log('DEBUG-1', this.state.offset, offset, currentPage);
    this.serviceGetPostList({
      currentPage: currentPage,
      offset: offset,
    });
  };

  setItemSize = (height, fontSize) => {
    this.setState({
      itemHeight: height,
      itemFontSize: fontSize,
    });
  };

  refreshCurrentPage = () => {
      this.serviceGetPostList();
  };

  onPressPost = (postId) => {
    if (this.touch.isTouchedStack) {
      return;
    }
    this.touch.isTouchedStack = true;
    const {touch} = this;
    setTimeout(() => {
      touch.isTouchedStack = false;
    }, 500);
    // console.log('post_id', postId);
    this.props.navigation.push('readPostScreen', {
      currentGroupName: this.state.currentGroupName,
      read: true,
      postId: postId,
      onGoBack: (e) => {
        console.log('=DEBUG=', e);
        if (e) {
          this.onPressGroup(e.groupName);
          if (e.pageNumber) {
            this.onPressPageNumber(e.pageNumber);
          }
        } else {
          this.refreshCurrentPage();
        }
      },
      onGoBackToLogin: (e) => {
        this.props.navigation.navigate('loginScreen');
      },
    });
  };
  render() {
    const {
      toggleSearch,
      onPressPost,
      serviceGetPostList,
      onPressSearch,
      onPressGoBack,
    } = this;

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
        <View style={styles.tabs}>
          <Tabs
            tabList={MENU}
            onPressGroup={this.onPressGroup}
            currentGroup={MENU[this.state.tabIndex]}></Tabs>
        </View>
        
        <View style={styles.body, styles.contents}>
          <List
            searchMode={true}
            serviceGetPostList={this.serviceGetPostList}
            postList={this.state.currentPostList}
            noticeList={this.state.noticeList}
            pages={this.state.pages}
            currentPage={this.state.currentPage.toString()}
            currentGroup={'default'}
            onPressPageNumber={this.onPressPageNumber}
            onPressGroup={this.onPressGroup}
            hideLeft={this.state.hideLeft}
            hideRight={this.state.hideRight}
            onPressMovePage={this.onPressMovePage}
            onPressPost={this.onPressPost}
            postPerPage={NUMBER_OF_POST_PER_PAGE}
            refreshCurrentPage={this.refreshCurrentPage}
            setItemSize={this.setItemSize}
            setRef={this.setRefList}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  contents: {
    flex: 10,
  },
  header: {
    backgroundColor: 'rgba(255, 255, 255, 1.0)',
    height: config.size._184px + 19,
    flexDirection: 'row',
    borderBottomWidth: config.size._2px,
    borderBottomColor: config.colors.lineColor,
},
headerTitle: {
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 18,
    //fontWeight: 'bold',
    fontFamily:config.defaultBoldFontFamily
},
headerBack: {
    position: 'absolute',
    left: 0,
    // paddingLeft: config.size._32px,
    height: '100%',
},
headerTool: {
    position: 'absolute',
    right: 0,
    flexDirection: 'row',
    height: '100%',
},
headerSearch: {
    flex: 1,
    paddingHorizontal: config.size._48px,
    justifyContent: 'center',
},
headerNewPost: {
    flex: 1,
    paddingRight: config.size._64px,
    justifyContent: 'center',
},
padding: {
    width: 10,
},
body: {
    flex: 1.1,
},
modalStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0,
},
tabs: {
  height: config.size._200px,
  backgroundColor: 'rgba(255, 255, 255, 1.0)',
},

});
