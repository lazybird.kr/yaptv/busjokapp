import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    StatusBar,
    View,
    Image,
    TouchableOpacity,
    SafeAreaView,
    Dimensions,
    BackHandler,

} from 'react-native';
import config from 'src/config';
import Loading from "src/containers/Community/Loading"
import Post from 'src/containers/Community/Post';
import Tabs from 'src/components/Tabs';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {PopupDialog} from 'src/components/PopupDialog';
import {getStatusBarHeight} from "react-native-status-bar-height";
import QNAList from './QNAList';
import {StyledText} from 'src/components/StyledComponents';

const NUMBER_OF_POST_PER_PAGE = 5;

export default class MyQuestion extends Component {
  state = {
    writing: true,
    readOnly: false,
    editMode: false,
    currentGroupName: '1:1문의',
    doSubmitPost: false,
    doEditPost: false,
    post: {},
    onRequesting: false,
    offset: 0,
    length: NUMBER_OF_POST_PER_PAGE,
    postList: [],
    selectedPost: {},
    endOfList: true,
    openConfirm: false,
    showDialogCancelNewPost: false,

  };

  _refPost = null;

 
  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"} />,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"1:1문의"} />,
      headerRight: () => {
        let button = <></>;

        if (navigation.getParam('submitText'))
          button = (
            <TouchableOpacity
              style={{marginRight: 15}}
              onPress={navigation.getParam('onPressSubmit')}>
              <StyledText style={{fontSize: config.fontSize._60px, color: config.colors.baseColor}}>
                {navigation.getParam('submitText')}
              </StyledText>
            </TouchableOpacity>
          );

        return button;
      },
      headerStyle : {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    };
  };

  onPressSubmit = () => {
    if (this.state.writing && this.state.editMode) {
      this.setState({
        doEditPost: !this.state.doEditPost,
        offset: 0,
      });
    } else {
      this.setState({
        doSubmitPost: !this.state.doSubmitPost,
        offset: 0,
      });
    }
  };

  setPost = (post) => {
    this.setState({
      post: post,
    });
  };

  setReadOnly = () => { //post등록후 실행됨

    this.openConfirm()

  };

  gotoList = () => {
    this.setState({
      writing: false,
      post: {},
      selectedPost: {},
    });
  };

  setRequesting = (isRequest) => {
    this.setState({
      onRequesting: isRequest,
    });
  };

  isReadOnly = () => {
    return this.state.readOnly && !this.state.editMode;
  };


  onPressGoBack = (e) => {
    const {navigation} = this.props;
    // let isEdited = false;
    // console.log('XXXXXXXXXXXXXXXXXXX_____________',this._refPost.state.data);
    // if (this._refPost) {
    //     const {data, title} = this._refPost.state;
    //     for (let i = 0; i < data.length; i++) {
    //         let e = data[i];
    //         if (this.state.writing) {
    //             if (e.text && e.text.length > 0) {
    //                 isEdited = true;
    //             } else if (e.imageSrc && e.imageSrc.uri) {
    //                 isEdited = true;
    //             } else if (title && title.length > 0) {
    //                 isEdited = true;
    //             }

    //         }
    //         if (isEdited) {
    //             this.setState({
    //                 showDialogCancelNewPost: true,
    //             });
    //             return;
    //         }
    //     }
    // }
    // navigation.state.params.onGoBack(param);
    navigation.goBack();
  };

  onPressGroup = (name) => {
    console.log('onPressGroup: name', name)
    if (name === '1:1문의하기') {
      this.setState({
        writing: true,
        editMode: false,
        selectedPost: {},
        post: {},
      })
    }
    else {
      this.setState({
        writing: false,
        selectedPost: {},
        post: {},
    })
    }

  }

  openConfirm = () => {
    this.setState({
        openConfirm: true,
    })
  }

  goBack = () => {
    const {navigation} = this.props;
    // navigation.state.params.onGoBack();
    navigation.goBack();
};


  onDialog = (result) => {
    if (result) { //문의 리스트로 가기
        this.setState({
            openConfirm: false,
        });
        this.gotoList();
    }
    else { //어디로 가지?
        this.setState({
            openConfirm: false,
        });
        this.goBack()
    }

}



  serviceGetPostList = () => {
    let send_data = {
      category: 'board',
      service: 'GetPostList',
      device: {
        device_id: config.deviceInfo.device_id,
        session_id: config.sessionInfo.session_id,
      },
      post: {
        group_name: this.state.currentGroupName,
        offset: this.state.offset,
        length: NUMBER_OF_POST_PER_PAGE + 1,
        written_by: {
          // member_id: 0
          member_id: config.member.member_id,
        }
      },
    };

    if (!this.state.offset) {
      this.setState({
        onRequesting: true,
      });
    }


    fetch(config.url.apiSvr, {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        data: send_data,
      }),
    })
      .then((res) => {
        if (res) return res.json();
      })
      .then((json) => {
        if (!this.state.offset) {
          this.setState({
            onRequesting: false,
          });
        }

        if (!json) {
          return;
        }

        if (json.err) {
          console.log('service 실패', send_data.service, send_data, json);
        } else {
          console.log('service 성공', send_data.service, send_data, json);

          let result = json.data.post_list || [];

          let endOfList = true
          if (result.length > NUMBER_OF_POST_PER_PAGE) {
            endOfList = false;
            result = result.slice(0, 5);
          }

          let postList = [];

          if (this.state.offset === 0) {
              postList = result;
          }
          else {
            postList = this.state.postList.concat(result);
          }

          console.log('XXXXXXXXXXXXXXXX', postList.length, this.state.offset)

          this.setState({
            postList,
            offset: postList.length,
            endOfList
          });

        }
      });
  };

  serviceGetPost = (post_id, {
    host = config.url.apiSvr,
    deviceId = config.deviceInfo.device_id,
    sessionId = config.sessionInfo.session_id,
  } = {}) => {

    if (this.state.selectedPost.post_id === post_id) {
        this.setState({
            selectedPost: {}
        })
        return;
    }

    let send_data = {
      category: 'board',
      service: 'GetPost',
      device: {
        device_id: deviceId,
        session_id: sessionId,
      },
      post: {
        post_id,
      },
    };


    fetch(host, {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        data: send_data,
      }),
    })
      .then((res) => {
        // console.log('service', res);
        if (res) {
          return res.json();
        }
      })
      .then((json) => {

        if (!json) {
          return;
        }

        if (json.err) {
          console.log('service 실패', send_data.service, json);
        } else {
          console.log('service 성공', send_data.service, json);
          if (json.data) {
            this.setState({
                selectedPost: json.data.post,
            });
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  serviceEditPost = (post_id) => {

    console.log('serviceEditPost: post_id', this.state.post.post_id, post_id)
    // this.setState({
    //     post: {
    //         post_id: post_id
    //     }
    // }, ()=> {
    //     this.setState({
    //         writing: true,
    //         editMode: true,
    //         readOnly: false,
    //     })
    // })


    this.setState({
        writing: true,
        editMode: true,
        readOnly: false,
        },
        () => {
            this.setState({
                post: {
                    post_id
                }
            })
        })



    // this.serviceGetPost(post_id)
    // if (this.state.post.post_id !== post_id) {
    //     this.setState({
    //         post: {
    //             post_id: post_id
    //         }
    //     })
    // }

  }


  serviceDeletePost = (
    post_id,
    {
      host = config.url.apiSvr,
      deviceId = config.deviceInfo.device_id,
      sessionId = config.sessionInfo.session_id,
      writtenBy = config.access_info.profile.nickname,
      deletedBy = config.access_info.profile.nickname,
      memberId = config.member.member_id,
      password = '',
    } = {},
  ) => {
    const {navigation} = this.props;
    let send_data = {
      category: 'board',
      service: 'DeletePost',
      device: {
        device_id: deviceId,
        session_id: sessionId,
      },
      post: {
        post_id,
        deleted_by: {
          member_id: memberId,
          name: deletedBy,
        },
        written_by: {
          member_id: memberId,
          name: writtenBy,
        },
        password: password,
      },
    };
    this.setState({
      onRequesting: true,
    });
    fetch(host, {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        data: send_data,
      }),
    })
      .then((res) => {
        // console.log('service', res);
        if (res) {
          return res.json();
        }
      })
      .then((json) => {
        this.setState({
          onRequesting: false,
        });
        if (!json) {
          return;
        }

        if (json.err) {
          console.log('service 실패', send_data.service, json);
        } else {
          console.log('service 성공', send_data.service, json);
          //   navigation.state.params.onGoBack();
          //   navigation.goBack();
        
          this.setState({
            offset: 0
          }, () => {
            this.serviceGetPostList();
          })
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  changeRegisterButton = () => {
    this.props.navigation.setParams({
      onPressSubmit: this.onPressSubmit,
      submitText: this.state.writing ?  (this.state.editMode? '수정': '등록') : '',
    });
  };

  componentDidMount() {
    this.changeRegisterButton();

  }


  componentDidUpdate(prevProps, prevState) {
    if (prevState.writing !== this.state.writing) {
      if (!this.state.writing) {
        this.serviceGetPostList();
      }

      this.changeRegisterButton();
    }
  }


  setPostRef = (ref) => {
    this._refPost = ref;
  };

  renderOpenConfirm = () => {

    return (
        <PopupDialog
            title={'1:1 문의가 등록되었습니다.'}
            visible={this.state.openConfirm}
            message={'문의하신 사항은 빠른 시일내에\n답변드리겠습니다.'}
            leftButtonLabel={'닫기'}
            leftButtonStyle={{borderWidth:1, borderColor: config.colors.greyColor}}
            leftButtonTextStyle={{color: config.colors.greyColor}}
            onLeftButtonPress={() => {
                this.onDialog(false)
            }}
            rightButtonLabel={'문의내역 확인'}
            rightButtonStyle={{backgroundColor: config.colors.baseColor2}}
            rightButtonTextStyle={{color: '#ffffff'}}
            onRightButtonPress={() => {
                this.onDialog(true)
            }}
            onBackdropPress={() =>
                this.setState({
                    openConfirm: false,
                })}
            onBackButtonPress={() =>
                this.setState({
                    openConfirm: false,
                })}
            secureTextEntry={true}
        />
    )

}

  render() {
    const {writing} = this.state;

    const contents = writing ? (
      <View style={styles.writing}>
        <Post
          readOnly={this.state.readOnly}
          setReadOnly={this.setReadOnly}
          post={this.state.post}
          setPost={this.setPost}
          setRequesting={this.setRequesting}
          groupName={this.state.currentGroupName}
          editMode={this.state.editMode}
          onPressGoBack={this.onPressGoBack}
          doSubmitPost={this.state.doSubmitPost}
          doEditPost={this.state.doEditPost}
          disableInquiry={true}
          setRef={this.setPostRef}
          />
      </View>
    ) : (
      <View style={styles.list}>
        <QNAList
          selectedPost={this.state.selectedPost}
          postList={this.state.postList}
          serviceDeletePost={this.serviceDeletePost}
          serviceEditPost={this.serviceEditPost}
          serviceGetPost={this.serviceGetPost}
          serviceGetPostList={this.serviceGetPostList}
          endOfList={this.state.endOfList}
        />
      </View>
    );

    const currentGroup = writing? '1:1문의하기': '1:1문의내역';

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
        <View style={styles.tabs}>
          <Tabs tabList={['1:1문의하기', '1:1문의내역']} onPressGroup={this.onPressGroup} currentGroup={currentGroup}></Tabs>
        </View>
        
        <View style={styles.contents}>{contents}</View>
        {this.state.onRequesting ? (
          <View
            style={{
              position: 'absolute',
              height: '100%',
              width: '100%',
              backgroundColor: '#fff',
            }}>
            <Loading color={'#eb6a0b'} />
          </View>
        ) : (
          <></>
        )}
        {this.state.openConfirm? this.renderOpenConfirm(): null}
        {this.state.showDialogCancelNewPost?
          <PopupDialog
            visible={this.state.showDialogCancelNewPost}
            // title={'비밀번호를 입력하세요'}
            message={'"확인" 버튼을 누르면 작성 중인 글이 저장되지 않습니다.'}
            leftButtonLabel={'취소'}
            onLeftButtonPress={() => {
                this.setState({
                    showDialogCancelNewPost: false,
                });
            }}
            rightButtonLabel={'확인'}
            rightButtonStyle={{backgroundColor: '#ff2c45'}}
            rightButtonTextStyle={{color: '#ffffff'}}
            onRightButtonPress={() => {
                this.setState({
                    showDialogCancelNewPost: false,
                });
                goBack();
            }}/>
            : null
          }
      </SafeAreaView>
    );
  }
}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    select: {
        flex: 1,
        flexDirection: 'row'
    },
    selectItem: {
        flex:1,
        justifyContent: 'center',
        borderColor: 'lightgray',
    },
    selected: {
        color: config.colors.baseColor
    },
    unselected: {
        color: 'black'
    },
    selectItemText: {
        textAlign: 'center'
    },
    contents: {
        flex: 10
    },
    writing: {
        flex: 1,
        width: '100%',
    },
    list: {
        flex: 1,
        width: '100%',
    },
    tabs: {
      height: config.size._200px,
      backgroundColor: 'rgba(255, 255, 255, 1.0)',
    },

});
