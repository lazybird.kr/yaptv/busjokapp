import React from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import config from 'src/config';
import {StyledText} from 'src/components/StyledComponents';

export default function Profile(props) {
    const {profile}= props;
    return (
        <View style={styles.container}>
            <Image style={styles.profileImage} 
                source={profile.nickname? config.images.avatar: config.images.avatarOff}
                />
            <StyledText style={styles.profileText}>
                {config.member.nickname || profile.nickname || '로그인을 해주세요'}
            </StyledText>
        </View>

    )
}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        // borderWidth: 2,
        // borderColor: 'red'
    },
    profileImage: {
        // flex: 1,
        width: config.size._212px,
        height: config.size._212px,
        borderRadius: config.size._212px
    },
    profileText: {
        flex: 1,
        // height: 100,
        color: 'black',
        fontSize: config.size._64px,
        textAlignVertical: 'center',
        marginLeft: 20,
    }
  });