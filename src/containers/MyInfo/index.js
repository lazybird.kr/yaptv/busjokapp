import MyInfoScreen from './MyInfoScreen';
import AccountScreen from './AccountScreen';
import TermOfUseScreen from './TermOfUseScreen';
import MyCouponScreen from './MyCouponScreen';
import MyCouponImageScreen from './MyCouponImageScreen';
import MyActionScreen from './MyActionScreen';
import MyQuestionScreen from './MyQuestion';
import MyNotification from './MyNotification';
import PostScreen from '../Community/PostScreen';

import WebViewScreen from 'src/components/WebView';
import LoginScreen from 'src/containers/Login';

import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

const MyInfoStack=createStackNavigator({
    myInfoScreen : { 
      screen : MyInfoScreen,

    },
    loginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        tabBarVisible: false,
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    accountScreen: {
      screen: AccountScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    termOfUseScreen: {
      screen: TermOfUseScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    myCoupon: {
      screen: MyCouponScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    myCouponImage: {
      screen: MyCouponImageScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    myActions: {
      screen: MyActionScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    myQuestion: {
      screen: MyQuestionScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    webViewComponent: {
      screen : WebViewScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    myNotification: {
      screen: MyNotification,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    readPostScreen: {
      screen: PostScreen,
      navigationOptions: {
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
      },
  },
  },
);

MyInfoStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
          if (route.routeName === "loginScreen") {
              tabBarVisible = false;
          } else {
              tabBarVisible = true;
          }
      });
  }

  return {
      tabBarVisible,
  };
};

export {
  MyInfoStack,  
}