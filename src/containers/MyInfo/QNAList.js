import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    FlatList,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    Image,
    Text,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/AntDesign';
import config from '../../config';
import QNAListItem from './QNAListItem';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';


export default function QNAList(props) {

    const postRequest = (command, post_id) => {
        if (command === 'delete')
            props.serviceDeletePost(post_id)
        else if (command === 'edit') 
            props.serviceEditPost(post_id)
        else if (command === 'view')
            props.serviceGetPost(post_id)

    }

    const renderItem = ({item}) => {
        const isSelected = props.selectedPost.post_id === item.post_id? true: false;

        return <QNAListItem 
            info={item}
            isSelected={isSelected}
            selectedPost={props.selectedPost}
            onEdit={() => postRequest('edit', item.post_id)}
            onDelete={() => postRequest('delete', item.post_id)}
            onView={() => postRequest('view', item.post_id)}
            />
    }

    const footerComponent = () => {
        return <View style={[styles.listFooter, styles.padding]}>
            {!props.endOfList?
                <TouchableOpacity style={styles.more} onPress={props.serviceGetPostList}>
                    <StyledText style={styles.moreText}>더보기</StyledText>
                </TouchableOpacity>
                :
                <></>
            }    
            {!props.postList.length? 
                <StyledText style={styles.listFooterTextEmpty}>1:1문의내역이 없습니다.</StyledText>
                :
                <StyledText style={styles.listFooterText}>*답변이 완료된 1:1문의는 삭제 또는 수정을 할 수 없습니다.</StyledText>
            }        
            
        </View>
    }

    return (
        <SafeAreaView style={{flex:1}}>
            <StyledText style={[styles.countTitle,styles.padding, styles.item]}>전체<StyledText style={styles.count}>({props.postList.length}건)</StyledText></StyledText>
            <FlatList 
                data={props.postList}
                renderItem={renderItem}
                keyExtractor={(item, index) => 'QNA_'+index }
                ListFooterComponent={footerComponent}
            />
            
        </SafeAreaView>
    )

}


const styles=StyleSheet.create({
    countTitle: {
        fontSize: config.fontSize._56px,
        textAlignVertical: 'center',
        color: '#000',
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        marginBottom: 0.5,
    },
    count: {
        fontSize: config.fontSize._48px,
        textAlignVertical: 'center',
        color: config.colors.greyColor,
    },
    padding: {
        paddingHorizontal: config.size._64px,
    },
    item: {
        height: config.size._220px,
        width: '100%',
        backgroundColor: '#fff'
    },
    listFooter: {
        flex: 1,
        paddingVertical: config.size._36px,
        backgroundColor: '#fff'
    },
    more: {
        flex: 1,
        marginBottom: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    moreText: {
        width:'20%',
        backgroundColor: 'lightgray',
        height: config.size._120px,
        borderRadius: 8,

        textAlignVertical: 'center',
        textAlign: 'center'
    },
    listFooterText: {
        fontSize: config.fontSize._40px,
        color: config.colors.greyColor
    },
    listFooterTextEmpty: {
        padding: config.size._100px,
        fontSize: config.fontSize._60px,
        color: config.colors.greyColor,
        textAlignVertical: 'center',
        textAlign: 'center'
    }
   

});