import React, {Component} from 'react';
import {Platform, 
    StyleSheet, 
    Image, 
    SafeAreaView, 
} from 'react-native';
import {HeaderTitle, BackIcon, } from 'src/components/header';
import config from 'src/config';
import {getStatusBarHeight} from "react-native-status-bar-height";


export default class MyCouponImage extends Component {
    static navigationOptions =  ({ navigation }) => {
        return {
          headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
          headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"내쿠폰함"} />,
          // headerTitle :() => <HeaderImage img={config.images.busStation}
          //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
          headerRight : () => <></>,
          headerStyle : {
            backgroundColor: "#ffffff",
            borderBottomWidth: 0.5,
            borderColor: config.colors.lineColor,
            elevation: 0,       //remove shadow on Android
            shadowOpacity: 0,   //remove shadow on iOS
            height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
          }
        }
    }

    componentDidMount = () =>{
    }

    


    render() {
        const {image} = this.props.navigation.state.params;
        console.log("image : ", image)
        return (
            <SafeAreaView style={styles.container}>
                <Image source={{uri:image}} style={{height:"100%"}} resizeMode={"contain"}/>
                
            </SafeAreaView>
    
        )
    }

}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        
        backgroundColor:"white"
    },
    
});