import React, { Component }  from "react";
import {SafeAreaView, View, BackHandler, Image, StyleSheet, TouchableOpacity, Platform, StatusBar, Linking} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";

import config from 'src/config';
import Profile from "./Profile"
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import LoginScreen from 'src/containers/Login';
import IconEmbededMenu from 'src/components/IconEmbededMenu';
import {getStatusBarHeight} from "react-native-status-bar-height";
import {PopupDialog} from 'src/components/PopupDialog';
import VersionCheck from 'react-native-version-check';
import DeviceInfo from 'react-native-device-info';
import compareVersions from 'compare-versions';
import {StyledText} from 'src/components/StyledComponents';

export default class MyInfoScreen extends Component {

  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"} onUserGoBack={()=>{navigation.popToTop(); navigation.navigate('homeTab')}}/>,
      headerTitle :() => <HeaderTitle style={[styles.headerTitle]} title={"나는 버스족"} />,
      // headerTitle :() => <HeaderImage img={config.images.busStation}
      //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }
  }

  state = {
    isFocused : false,
    isLogin: false,
    currentVersion : "",
    latestVersion : "",
    updateUrl : ""
  }

  onUserGoBack = () => {
    const { navigation } = this.props;

    navigation.popToTop();
    navigation.navigate('homeTab')
  }

  componentDidMount() {

    const {navigation} = this.props;
    this.subs = [
      navigation.addListener(
        "didFocus",
        () => {
          console.log("about screen didFocus");
          this.setState({
            isFocused: true,
            // isLoading: true
          });
        }),
      navigation.addListener(
        "willBlur",
        () => {
          console.log("about screen willBlur");
          this.setState({
            isFocused: false,
            // isLoading: false
          });
        }),
      // navigation.addListener("willFocus", () => {console.log("about screen willFocus"); this.setState({ isFocused: true});}),
      // navigation.addListener("willFocus", () => console.log("about screen willFocus")),
      // navigation.addListener("didBlur", () => console.log("about screen didBlur")),
    ];

      this.checkLoginStatus();

  }

  componentWillUnmount() {
    this.subs && this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  componentDidUpdate(prevProps, prevState){
    const { isFocused } = this.state;
    if(prevState.isFocused != isFocused && isFocused){
      console.log('this.props.navigation', this.props.navigation)
      this.checkLoginStatus();
      this.checkUpdateNeeded();
    }
  }

  isNeededUpdate = (latestVersion, currentVersion) => {
    console.log("ver1 : ", latestVersion);
    console.log("ver2 : ", currentVersion);
    if(compareVersions(latestVersion, currentVersion, '>') === 1){
      console.log("isNeededUpdate : true")
      return true
    }
    console.log("isNeededUpdate : false")
    return false
  }

  checkUpdateNeeded = async () => {
    if(Platform.OS === "android"){
      
      const curVer = VersionCheck.getCurrentVersion();
      const lastVer = config.version.android || curVer;
      const url = config.googlePlayStoreUrl;
      this.setState({currentVersion:curVer, latestVersion : lastVer, updateUrl : url});
      this.setState({ isNeededUpdate: this.isNeededUpdate(lastVer, curVer)});
    } else {
      try{
        let lastVer = await VersionCheck.getLatestVersion();
        const curVer = VersionCheck.getCurrentVersion();
        const url = await VersionCheck.getStoreUrl({packageName:config.packageName, appID:config.appStoreID});
        console.log("lastVer : ", lastVer);
        console.log("curVer : ", curVer);
        console.log("url : ", url);
        lastVer = lastVer === "." || !lastVer? curVer:lastVer
        this.setState({currentVersion:curVer, latestVersion : lastVer, updateUrl : url});
        this.setState({ isNeededUpdate: this.isNeededUpdate(lastVer, curVer)});
      } catch(e){
        const curVer = DeviceInfo.getVersion();
        this.setState({currentVersion:curVer, latestVersion : curVer});
      }
    }
    
  }

  checkLoginStatus = () => {
    if (!config.access_info) {
      this.setState({
        isLogin: false
      })
      // this.props.navigation.navigate('loginScreen', {
      //   onUserGoBack: this.onUserGoBack
      // });


      // this.props.navigation.navigate('loginScreen');
    } else {
      this.setState({
        isLogin: true
      })
    }
  }


  reset = async () => {
    try {
      await AsyncStorage.removeItem("timeScanned")
    } catch(e){

    }

  }

  onPressAccount = () => {
    if (this.state.isLogin) {
      this.props.navigation.navigate('accountScreen', {
        access_info: config.access_info
      });
    } else {
      this.props.navigation.navigate('loginScreen');

    }

  };

  onPressFAQ = () => {
    this.props.navigation.navigate("webViewComponent", {
      uri : config.url.qrcode + "?command=faq",
      title: '자주 묻는 질문'
    });
  }

  onPress = (screen) => {
    this.props.navigation.navigate(screen)
  }

  openConfirm = () => {
    this.setState({
        openConfirm: true,
    })
  }

  onDialog = (result) => {
    const {updateUrl} = this.state;
      if (result) { //업데이트 - 예
        this.setState({
            openConfirm: false,
        });
        if(Platform.OS === "android"){
          BackHandler.exitApp();
        }
        Linking.openURL(updateUrl);
      }
      else { //업데이트 - 아니요
          this.setState({
              openConfirm: false,
          });
      }

  }

  renderPersonalArea = () => {

      return  <>
          <TouchableOpacity style={[styles.personalButton, styles.personalButtonBorder]} onPress={()=>this.onPress('myCoupon')}>
            <Image style={styles.buttonImage} source={require('src/images/myCoupons.png')}></Image>
            <StyledText style={styles.personalText}>내쿠폰함</StyledText>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.personalButton, styles.personalButtonBorder]} onPress={()=>this.onPress('myActions')}>
            <Image style={styles.buttonImage} source={require('src/images/myActions.png')}></Image>
            <StyledText style={styles.personalText}>내게시물</StyledText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.personalButton} onPress={()=>this.onPress('myQuestion')}>
            <Image style={styles.buttonImage} source={require('src/images/myQuestions.png')}></Image>
            <StyledText style={styles.personalText}>1:1문의</StyledText>
          </TouchableOpacity>
      </>

  }

  renderProfile = () => {
    /*const currentVersion = DeviceInfo.getVersion();
    const serverVersion = config.version[Platform.OS];*/
    const {currentVersion, latestVersion, isNeededUpdate} = this.state;
    
    return <View style={{flex:1}}>
          <View style={[styles.list, {flexDirection: 'row', alignItems:"center"}, styles.padding]} onPress={this.reset}>
            <StyledText style={[styles.listText, styles.listVersion]}>버전정보</StyledText>
            { isNeededUpdate &&
              <TouchableOpacity style={styles.updateButton} onPress={() => this.setState({openConfirm: true})}>
                <StyledText style={[styles.listText, styles.updateText]}>업데이트</StyledText>
              </TouchableOpacity>
            }
            { latestVersion && currentVersion ?
              <View style={{alignItems:"center"}}>
                <View style={{flex:0.5}}></View>
                <StyledText style={[styles.listText, styles.listVersion, styles.versionText]}>{"최신 : " + latestVersion}</StyledText>
                <StyledText style={[styles.listText, styles.listVersion, styles.versionText]}>{"현재 : " + currentVersion}</StyledText>
                <View style={{flex:0.5}}></View>
              </View> :
              <></>}
            
          </View>
          <IconEmbededMenu mainStyle={[styles.list, styles.padding]} onPress={()=>this.onPress('termOfUseScreen')}>
            <StyledText style={styles.listText}>이용약관 및 정책</StyledText>
          </IconEmbededMenu>
          { this.state.isLogin &&
            <IconEmbededMenu mainStyle={[styles.list, styles.padding]} onPress={()=>this.onPress('myNotification')}>
              <StyledText style={styles.listText}>알림설정</StyledText>
            </IconEmbededMenu>
          }
          <IconEmbededMenu mainStyle={[styles.list, styles.listTail, styles.padding]} onPress={this.onPressFAQ}>
            <StyledText style={styles.listText}>자주 묻는 질문</StyledText>
          </IconEmbededMenu>
    </View>

  }
  renderOpenConfirm = () => {

    return (
        <PopupDialog
            visible={this.state.openConfirm}
            message={'업데이트 하시겠습니까?'}
            leftButtonLabel={'취소'}
            leftButtonStyle={{borderWidth:1, borderColor: config.colors.greyColor}}
            leftButtonTextStyle={{color: config.colors.greyColor}}
            onLeftButtonPress={() => {
                this.onDialog(false)
            }}
            rightButtonLabel={'업데이트'}
            rightButtonStyle={{backgroundColor: config.colors.baseColor2}}
            rightButtonTextStyle={{color: '#ffffff'}}
            onRightButtonPress={() => {
                this.onDialog(true)
            }}
            onBackdropPress={() =>
                this.setState({
                    openConfirm: false,
                })}
            onBackButtonPress={() =>
                this.setState({
                    openConfirm: false,
                })}
            secureTextEntry={true}
        />
    )

}


  render() {

    const {access_info} = config;

    console.log('MYINFOSCREEN_________', access_info)
    console.log('MYINFOSCREEN_________DeviceInfo.getVersion()=>', DeviceInfo.getVersion())
    console.log('MYINFOSCREEN_________config.version=>', config.version[Platform.OS])

    const profile = access_info? access_info.profile: {};

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <IconEmbededMenu mainStyle={[styles.profile, styles.padding]} onPress={this.onPressAccount}>
            <Profile profile={profile} />
        </IconEmbededMenu>
        { this.state.isLogin &&
          <View style={styles.personalArea}>
            {this.renderPersonalArea()}
          </View>
        }
        <View style={styles.personalBottom}></View>
        <View style={styles.lists}>
          {this.renderProfile()}
        </View>
        <View style={styles.corporation}>
          <StyledText style={styles.corporationKr}>한국버스방송(주)</StyledText>
          <StyledText style={styles.corporationEn}>KOREA BUS BROADCASTING CORPORATION</StyledText>
        </View>
        {this.state.openConfirm? this.renderOpenConfirm(): null}

      </SafeAreaView>
    )

  }
}


const styles=StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: 'white',
  },
  profile: {
    height: config.size._440px,
    width: '100%',
    alignItems: 'center',
  },
  personalArea: {
    height: config.size._220px,
    width: '100%',
    flexDirection: 'row',
    justifyContent:"center",
    alignContent:"center",
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: config.colors.lineColor,
    paddingVertical: 10,
  },
  personalBottom: {
    height: config.size._60px,
    backgroundColor: '#eee',
    borderTopWidth: 0.5,
    borderColor: config.colors.lineColor,
  },
  lists: {
    flex: 1
  },
  corporation: {
    height: config.size._240px,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  padding: {
    paddingLeft: config.size._100px,
    paddingRight: config.size._64px,
  },

  personalButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  personalButtonBorder: {
    borderRightWidth: 0.5,
    borderColor: config.colors.lineColor,
  },
  personalText: {
    textAlign: 'center',
    fontSize: config.size._50px,
  },
  buttonImage: {
    width: config.size._96px,
    height: config.size._96px,
    marginRight: config.size._16px,
  },
  list: {
    height: config.size._240px,
    width: '100%',
    borderTopWidth: 0.5,
    borderColor: config.colors.lineColor,
    // justifyContent:"center",
    // alignContent:"center",
    // alignItems: 'center',
  },
  listTail: {
    borderBottomWidth: 0.5
  },
  listText: {
    flex: 1,
    fontSize: config.size._60px,
    textAlignVertical: 'center',
    textAlign: 'left',
  },

  updateButton: {
    flex:2,
    alignItems: 'flex-start',
    justifyContent:"center",
    alignContent: "flex-end",
  },
  updateText: {
    flex:0,
    textAlign: 'center',
    fontSize: config.size._44px,
    width: config.size._212px,
    textAlignVertical: 'center',
    color: '#01a3ff',
    borderWidth:1, borderRadius:10,borderColor:'#01a3ff'
  },
  versionText: {
    color: config.colors.lineColor,
    textAlign: 'right',
    fontSize: config.size._44px,
    paddingRight: config.size._60px,
  },
  listVersion: {
    flex:1,
  },
  kakao: {
    backgroundColor: "#fee500",
    color: "#1a1a1c"
  },
  button : {
    flex:1,
    width: "90%",
    backgroundColor: config.colors.startButtonColor,
    padding:10,
    margin : 3,
    borderRadius: 3,
    justifyContent : "center",
    alignSelf : "center",
  },
  buttonText : {
    alignItems : "center",
    textAlign : "center",
    fontSize : 24,
    color: 'white'
  },
  corporationKr: {
    fontSize: config.fontSize._40px,
    color: config.colors.greyColor,
  },
  corporationEn: {
    fontSize: config.fontSize._28px,
    color: config.colors.greyColor,
  },
  headerTitle: {
    alignSelf: 'center',
    fontSize: 18,
    fontFamily:config.defaultMediumFontFamily
  },
});
