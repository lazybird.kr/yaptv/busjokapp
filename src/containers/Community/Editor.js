import React, {Component} from 'react';
import {
    Appearance,
    Button,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    View,
    Dimensions,
    TouchableOpacity,
    PanResponder,
    Animated,
    TouchableWithoutFeedback,
    Image, KeyboardAvoidingViewComponent,
} from 'react-native';

import {actions, defaultActions, RichEditor, RichToolbar} from 'react-native-pell-rich-editor';
import { WebView } from 'react-native-webview';
import {InsertLinkModal} from './insertLink';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Loading from './Loading';
import Comment from './Comment';
import Anonymous from './Anonymous';
import Post from './Post';
import TestCollapsibleNavBarScrollView from './test/TestCollapsibleNavBarScrollView';
import RNDraftView from 'react-native-draftjs-editor';
import TestRNDraft from './test/TestRNDraft';

import {DragTextEditor} from 'react-native-drag-text-editor';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import TestDraggableFlatList from './test/TestDraggableFlatList';
import config from '../../config';


const WINDOW = Dimensions.get('window');
const phizIcon = require('./assets/phiz.png');
const htmlIcon = require('./assets/h5.png');
const videoIcon = require('./assets/video.png');
const strikethrough = require('./assets/strikethrough.png');
const labelSubmit = '작성';
const HEIGHT_NAME_PASSWORD_VIEW = 60;

export default class Editor extends Component {

    richText = React.createRef();
    linkModal = React.createRef();

    constructor(props) {
        super(props);
        const theme = props.theme || Appearance.getColorScheme();
        const contentStyle = this.createContentStyle(theme);
        this.currentGroupName = 'default';
        this.scrollView = null;
        this.lastPointerPosition = {
            x: 0,
            y: 0,
        };
        this.lastScrollPosition = {
            x: 0,
            y: 0,
        };
        this.contentOffset = {
            x: 0,
            y: 0,
        };
        this._draftRef = React.createRef();

        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => {
                // console.log('multi-touch', 'onStartShouldSetPanResponder', evt, gestureState);
                // return true

            },
            onStartShouldSetPanResponderCapture: (evt, gestureState) => {
                // console.log('multi-touch', 'onStartShouldSetPanResponderCapture', evt, gestureState);
                // return true

            },
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                // console.log('multi-touch', 'onMoveShouldSetPanResponder', evt, gestureState);
                // return true

            },
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
                // console.log('multi-touch', 'onMoveShouldSetPanResponderCapture', evt, gestureState);
                // return true
            },

            onPanResponderGrant: (evt, gestureState) => {
                // console.log('onPanResponderGrant', evt.nativeEvent.pageX, evt.nativeEvent.pageY, this.scrollView);
                this.lastPointerPosition = {
                    x: evt.nativeEvent.pageX,
                    y: evt.nativeEvent.pageY,
                };
                this.lastScrollPosition = {
                    x: 0,
                    y: 0,
                };
                // if (this.state.keyboardHeight === 0) {
                //    this.scrollView.scrollTo({x: evt.nativeEvent.pageX, y: evt.nativeEvent.pageY})
                // }
            },
            onPanResponderMove: (evt, gestureState) => {
                this.checkHidingHeader(evt, null);
            },
            onPanResponderTerminationRequest: (evt, gestureState) => {
                // console.log('multi-touch', 'onPanResponderTerminationRequest', evt, gestureState);

            },
            onPanResponderRelease: (evt, gestureState) => {
            },
            onPanResponderTerminate: (evt, gestureState) => {
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
            },
        });
        this.state = {
            theme: theme,
            disabled: false,
            title: '',
            nickname: '',
            password: '',
            contentStyle: contentStyle,
            toolbarStyle: {
                position: 'absolute',
                width: '100%',
                bottom: 0,
            },
            keyboardHeight: 0,
            editorHeight: 0,
            scrollViewInitFlag: false,
            onRequesting: false,
            toolbarLayout: {
                height: 0,
                width: 0,
            },
            containerLayout: {
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width,
            },
            scrollParentViewLayout: {
                height: 0,
                width: 0,
            },
            post: {},
            nameViewHeight: HEIGHT_NAME_PASSWORD_VIEW,
            commentHeight: '10%',
            commentImage: null,
            commentLayout: {
                height: 0,
                width: 0,
            },
        };
    }

    componentDidMount() {
        Appearance.addChangeListener(this.themeChange);
        Keyboard.addListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.addListener('keyboardDidHide', this.onKeyBoardHide);

        const {currentGroupName} = this.props.navigation.state.params;
        if (currentGroupName && currentGroupName.length > 0) {
            this.currentGroupName = currentGroupName;
        }
        const {read, postId} = this.props.navigation.state.params;
        if (read) {
            this.serviceGetPost({
                postId: postId,
            });
            this.setState({
                disabled: true,
            });
        }
    }

    componentWillUnmount() {
        Appearance.removeChangeListener(this.themeChange);
        Keyboard.removeListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.removeListener('keyboardDidHide', this.onKeyBoardHide);
    }

    onKeyBoard = (e) => {
        // console.log(Dimensions.get('window').height, e.endCoordinates.height);
        console.log('onKeyBoard', TextInput.State.currentlyFocusedInput());
        TextInput.State.currentlyFocusedInput() && this.setState({emojiVisible: false});

        this.setState({
            toolbarStyle: {
                ...this.state.toolbarStyle,
                bottom: e.endCoordinates.height,
            },
            keyboardHeight: e.endCoordinates.height,
        });
    };

    onKeyBoardHide = (e) => {
        // console.log('onKeyBoardHide', Dimensions.get('window').height, e.endCoordinates.height);
        this.setState({
            toolbarStyle: {
                ...this.state.toolbarStyle,
                bottom: 0,
            },
            keyboardHeight: 0,
        });
    };

    servicePublishPost = ({
                              host = config.url.apiSvr,
                              deviceId = config.deviceInfo.device_id,
                              sessionId = config.sessionInfo.session_id,
                              groupName = this.currentGroupName,
                              title = '',
                              writtenBy = '',
                              password = '',
                              contentList = [],
                          } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'PublishPost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                written_by: {
                    member_id: '',
                    name: writtenBy,
                },
                password: password,
                title: title,
                content_list: contentList,
                group_name: groupName,

            },
        };
        console.log(send_data);
        this.setState({
            onRequesting: true,
        });
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    serviceGetPost = ({
                          host = config.url.apiSvr,
                          deviceId = config.deviceInfo.device_id,
                          sessionId = config.sessionInfo.session_id,
                          postId = '',
                      } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'GetPost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
            },
        };
        this.setState({
            onRequesting: true,
        });
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    if (json.data) {
                        this.setState({
                            post: json.data.post,
                        });
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    async save() {
        // Get the data here and call the interface to save the data
        let html = await this.richText.current?.getContentHtml();
        // console.log(html);
        alert(html);
    }

    themeChange = ({colorScheme}) => {
        const theme = colorScheme;
        const contentStyle = this.createContentStyle(theme);
        this.setState({theme, contentStyle});
    };

    editorInitializedCallback = () => {
        const that = this;
        this.richText.current?.registerToolbar(function (items) {
            console.log('Toolbar click, selected items (insert end callback):', items);

        });
        this.richText.current?.setContentFocusHandler(function () {
            console.log('setContentFocusHandler', that.state.scrollViewInitFlag);
            if (!that.state.scrollViewInitFlag) {
                that.setState({
                    scrollViewInitFlag: true,
                });
            }
        });
    };

    createContentStyle = (theme) => {
        // Can be selected for more situations (cssText or contentCSSText).
        const contentStyle = {
            backgroundColor: '#000033',
            color: '#fff',
            placeholderColor: 'gray',
            // cssText: '#editor {background-color: #f3f3f3}', // initial valid
            contentCSSText: 'font-size: 16px; min-height: 200px; height: 100%;', // initial valid
        };
        if (theme === 'light') {
            contentStyle.backgroundColor = '#fff';
            contentStyle.color = '#000033';
            contentStyle.placeholderColor = '#a9a9a9';
        }
        return contentStyle;
    };

    onDisabled = () => {
        this.setState({disabled: !this.state.disabled});
    };

    onTheme = () => {
        let {theme} = this.state;
        theme = theme === 'light' ? 'dark' : 'light';
        let contentStyle = this.createContentStyle(theme);
        this.setState({theme, contentStyle});
    };

    /**
     * editor change data
     * @param {string} html
     */
    handleChange = (html) => {
        // UIManager.measure(this.richText.TextInputState.currentlyFocusedInput(), (originX, originY, width, height, pageX, pageY) => {
        //     console.log(originX, originY, width, height, pageX, pageY);
        // });
        console.log('editor data:', html);
    };

    /**
     * editor height change
     * @param {number} height
     */
    handleHeightChange = (height) => {
        console.log('editor height change:', height);
        // if (this.state.scrollViewInitFlag) {
        //     this.scrollView.scrollToEnd({animated: true});
        // }
    };
    onContentSizeChange = () => {
        console.log('onContentSizeChange');
    };
    onLinkDone = ({title, url}) => {
        this.richText.current?.insertLink(title, url);
    };
    onHome = () => {
        this.props.navigation.goBack();
    };
    insertVideo = () => {
        this.richText.current?.insertVideo(
            'https://mdn.github.io/learning-area/html/multimedia-and-embedding/video-and-audio-content/rabbit320.mp4',
        );
        // https://youtu.be/u2l2_zVtmmQ
    };
    insertHTML = () => {
        // this.richText.current?.insertHTML(`<span style="color: blue; padding:0 10px;">HTML</span>`);
        this.richText.current?.insertHTML(`<iframe src="https://www.youtube.com/embed/u2l2_zVtmmQ" frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen></iframe>`);
    };
    onPressAddImage = () => {
        // insert URL
        this.richText.current?.insertImage(
            'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/100px-React-icon.svg.png',
        );
        // insert base64
        // this.richText.current?.insertImage(`data:${image.mime};base64,${image.data}`);
        // this.richText.current?.blurContentEditor();
    };
    onInsertLink = () => {
        // this.richText.current?.insertLink('Google', 'http://google.com');
        this.linkModal.current?.setModalVisible(true);
    };
    onChangeTitle = (text) => {
        this.setState({
            title: text,
        });
    };
    onChangeNickname = (text) => {
        this.setState({
            nickname: text,
        });
    };
    onChangePassword = (text) => {
        this.setState({
            password: text,
        });
    };

    // **
    async onPressSubmit(that) {

        Keyboard.dismiss();
        let html = '';
        try {
            html = await that.richText.current?.getContentHtml();
        } catch (e) {
            console.log(e);
            return;
        }
        // react-native navigation props

        console.log(html);

        this.servicePublishPost({
            title: this.state.title,
            writtenBy: this.state.nickname,
            password: this.state.password,
            contentList: [html],
        });
        this.setState({
            disabled: true,
        });
    };

    onScrollContent = (e) => {
        console.log('onScrollContent', e.nativeEvent.contentOffset);
        this.contentOffset = e.nativeEvent.contentOffset;
        // this.checkHidingHeader(null, this.contentOffset)
    };
    checkHidingHeader = (pan, scroll) => {
        if (pan) {
            const diffY = pan.nativeEvent.pageY - this.lastPointerPosition.y;
            let factor = this.state.nameViewHeight + diffY;
            // console.log(factor);
            if (factor < 0) {
                factor = 0;
            } else if (factor > HEIGHT_NAME_PASSWORD_VIEW) {
                factor = HEIGHT_NAME_PASSWORD_VIEW;
            }
            this.lastPointerPosition = {
                x: pan.nativeEvent.pageX,
                y: pan.nativeEvent.pageY,
            };
            console.log(diffY, this.contentOffset.y, factor);
            if (diffY > 0 && this.contentOffset.y !== 0) {
                return;
            }
            this.setState({
                nameViewHeight: factor,
            });
            // console.log('multi-touch', 'onPanResponderMove', evt, gestureState);
            // console.log('pinch?', evt.nativeEvent, evt.nativeEvent.touches);
        } else {
            if (this.lastScrollPosition.y === 0) {
                this.lastScrollPosition = {
                    x: scroll.x,
                    y: scroll.y,
                };
            } else {
                const diffY = this.lastScrollPosition.y - scroll.y;
                let factor = this.state.nameViewHeight + diffY;
                if (scroll.y < HEIGHT_NAME_PASSWORD_VIEW) {
                    factor = HEIGHT_NAME_PASSWORD_VIEW - scroll.y;
                }
                // console.log(factor);
                if (factor < 0) {
                    factor = 0;
                } else if (factor > HEIGHT_NAME_PASSWORD_VIEW) {
                    factor = HEIGHT_NAME_PASSWORD_VIEW;
                }
                this.lastScrollPosition = {
                    x: scroll.x,
                    y: scroll.y,
                };
                this.setState({
                    nameViewHeight: factor,
                });
            }
        }
    };
    selectImage = (data) => {
        this.setState({
            commentImage: {
                uri: data.uri,
                width: data.width,
                height: data.height,
            },
        });
    };

    // TODO: Editor 를  TextInput 으로 할 지 RichEditor 로 할 지 결정해야 한다.
    render() {
        const that = this;
        const {contentStyle, disabled, theme} = this.state;
        const {backgroundColor, placeholderColor, color} = contentStyle;
        const themeBg = {backgroundColor};
        const {navigation} = this.props;
        const scrollStyle = {
            // flex: 1,
            height: this.state.keyboardHeight,
            // flex: 1,
            // flex: 0.8 - (this.state.keyboardHeight + this.state.toolbarLayout.height) / this.state.containerLayout.height,
            // flex: 0.5,
            // flex: 1,
        };
        let editorStyle = {
            flex: 1,
            minHeight: Dimensions.get('window').height,
            backgroundColor: '#ffff00',
        };
        // if (this.state.keyboardHeight > 0) {
        //     editorStyle = {
        //         flex: 1,
        //         minHeight: 0,
        //         maxHeight: this.state.scrollParentViewLayout.height,
        //     };
        // }
        const {read} = this.props.navigation.state.params;
        let isReadStatus = false;
        let initHTML =
            '<p>This editor is built with Draft.js. Hence should be suitable for most projects. However, Draft.js Isn’t fully compatible with mobile yet. So you might face some issues.</p><p><br></p><p>This is a simple implementation</p><ul>  <li>It contains <strong>Text formatting </strong>and <em>Some blocks formatting</em></li>  <li>Each for it’s own purpose</li></ul><p>You can also do</p><ol>  <li>Custom style map</li>  <li>Own css styles</li>  <li>Custom block styling</li></ol><p>You are welcome to try it!</p>';

        let placeHolder = '자유로운 이야기를 남겨주세요';
        let commentImageStyle = {
            position: 'absolute',
            width: '98%',
            margin: '1%',
            height: '50%',
            bottom: this.state.commentLayout.height,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
        };
        if (read && this.state.disabled) {
            placeHolder = '';
            // 글 읽기
            isReadStatus = true;
            editorStyle.minHeight = 0;
            if (this.state.post.content_list) {
                for (let i = 0; i < this.state.post.content_list.length; i++) {
                    initHTML += this.state.post.content_list[i];
                }
            }
        }
        const defaultValue =
            '<h1>A Full fledged Text Editor</h1><p>This editor is built with Draft.js. Hence should be suitable for most projects. However, Draft.js Isn’t fully compatible with mobile yet. So you might face some issues.</p><p><br></p><p>This is a simple implementation</p><ul>  <li>It contains <strong>Text formatting </strong>and <em>Some blocks formatting</em></li>  <li>Each for it’s own purpose</li></ul><p>You can also do</p><ol>  <li>Custom style map</li>  <li>Own css styles</li>  <li>Custom block styling</li></ol><p>You are welcome to try it!</p>';

        // console.log('editorStyle', scrollStyle, editorStyle);
        // console.log('post', this.state.post, isReadStatus);
        return (
            <SafeAreaView style={[styles.container, themeBg]}
                          onLayout={e => {
                              this.setState({
                                  containerLayout: e.nativeEvent.layout,
                              });
                          }}>
                {/*<StatusBar barStyle={theme !== 'dark' ? 'dark-content' : 'light-content'}/>*/}
                <InsertLinkModal
                    placeholderColor={placeholderColor}
                    color={color}
                    backgroundColor={backgroundColor}
                    onDone={that.onLinkDone}
                    ref={that.linkModal}
                />
                {/*<View style={styles.nav}>*/}
                {/*    <View style={styles.navBack}>*/}
                {/*        <TouchableOpacity*/}
                {/*            style={styles.navBackButton}*/}
                {/*            onPress={() => navigation.goBack()}>*/}
                {/*            <MaterialIcons name={'arrow-back-ios'} size={24} color={'#000000'}/>*/}
                {/*        </TouchableOpacity>*/}
                {/*    </View>*/}
                {/*    <View style={styles.navTitle}>*/}
                {/*        {isReadStatus ?*/}
                {/*            <Text*/}
                {/*                style={[styles.input, {color}]}*/}
                {/*                placeholderTextColor={placeholderColor}*/}
                {/*                maxLength={1024}*/}
                {/*            >*/}
                {/*                {this.state.post.title}*/}
                {/*            </Text>*/}
                {/*            :*/}
                {/*            <TextInput*/}
                {/*                style={[styles.input, {color}]}*/}
                {/*                placeholderTextColor={placeholderColor}*/}
                {/*                placeholder="제목을 입력하세요"*/}
                {/*                maxLength={1024}*/}
                {/*                onChangeText={this.onChangeTitle}*/}
                {/*            />*/}
                {/*        }*/}
                {/*    </View>*/}
                {/*    <View style={styles.navSubmit}>*/}
                {/*        {isReadStatus ? null :*/}
                {/*            <TouchableOpacity*/}
                {/*                onPress={() => that.onPressSubmit(that)}>*/}
                {/*                <Text style={{*/}
                {/*                    textAlign: 'center',*/}
                {/*                    textAlignVertical: 'center',*/}
                {/*                    fontSize: 18,*/}
                {/*                    fontFamily: 'Roboto',*/}
                {/*                    color: '#eb6a0b',*/}
                {/*                    fontWeight: 'bold',*/}
                {/*                }}>*/}
                {/*                    {labelSubmit}*/}
                {/*                </Text>*/}
                {/*            </TouchableOpacity>*/}
                {/*        }*/}
                {/*    </View>*/}
                {/*</View>*/}
                {/*<View*/}
                {/*    style={[scrollStyle, {backgroundColor: '#ff0000'}]}*/}
                {/*    onLayout={e => {*/}
                {/*        // console.log('scrollParentViewLayout', e.nativeEvent.layout);*/}
                {/*        this.setState({*/}
                {/*            scrollParentViewLayout: e.nativeEvent.layout,*/}
                {/*        });*/}
                {/*    }}*/}
                {/*>*/}
                <View style={{flex: 1}}>
                    <WebView
                        source={{uri: config.url.apiSvr}}
                    />
                </View>
                {/*<KeyboardAvoidingView*/}
                {/*    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}*/}
                {/*>*/}
                {/*    <RichEditor*/}
                {/*        ref={that.richText}*/}
                {/*        disabled={isReadStatus}*/}
                {/*        editorStyle={contentStyle} // default light style*/}
                {/*        containerStyle={themeBg}*/}
                {/*        style={[themeBg, editorStyle]}*/}
                {/*        initialContentHTML={initHTML}*/}
                {/*        placeholder={placeHolder}*/}
                {/*        editorInitializedCallback={() => this.editorInitializedCallback()}*/}
                {/*        onChange={that.handleChange}*/}
                {/*        onHeightChange={that.handleHeightChange}*/}
                {/*        useContainer={true}*/}
                {/*    />*/}
                {/*</KeyboardAvoidingView>*/}
                {/*<ScrollView*/}
                {/*    ref={ref => that.scrollView = ref}*/}
                {/*    onContentSizeChange={that.onContentSizeChange}*/}
                {/*    style={[scrollStyle, themeBg]}*/}
                {/*    keyboardDismissMode={'none'}*/}
                {/*    onScroll={this.onScrollContent}*/}
                {/*    showsVerticalScrollIndicator={false}*/}
                {/*    // keyboardDismissMode={'on-drag'}*/}
                {/*    // keyboardShouldPersistTaps={'handled'}*/}
                {/*>*/}
                {/*    {isReadStatus ? null :*/}
                {/*        <View style={{*/}
                {/*            height: this.state.nameViewHeight,*/}
                {/*        }}>*/}
                {/*            <Anonymous*/}
                {/*                onChangeNickname={this.onChangeNickname}*/}
                {/*                onChangePassword={this.onChangePassword}*/}
                {/*            />*/}
                {/*        </View>*/}
                {/*    }*/}
                {/*    <KeyboardAvoidingView*/}
                {/*        behavior={Platform.OS === 'ios' ? 'padding': 'height'}*/}
                {/*        >*/}
                {/*        <RichEditor*/}
                {/*            ref={that.richText}*/}
                {/*            disabled={isReadStatus}*/}
                {/*            editorStyle={contentStyle} // default light style*/}
                {/*            containerStyle={themeBg}*/}
                {/*            style={[themeBg, editorStyle]}*/}
                {/*            initialContentHTML={initHTML}*/}
                {/*            placeholder={placeHolder}*/}
                {/*            editorInitializedCallback={() => this.editorInitializedCallback()}*/}
                {/*            onChange={that.handleChange}*/}
                {/*            onHeightChange={that.handleHeightChange}*/}
                {/*            useContainer={true}*/}
                {/*        />*/}
                {/*    </KeyboardAvoidingView>*/}
                {/*</ScrollView>*/}

                {/*<KeyboardAvoidingView*/}
                {/*    style={{flex: 1}}*/}
                {/*    // keyboardVerticalOffset={this.state.keyboardHeight}*/}
                {/*    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}*/}
                {/*>*/}
                {/*<TestRNDraft/>*/}
                {/*</KeyboardAvoidingView>*/}

                {/*<ScrollView*/}
                {/*    ref={ref => that.scrollView = ref}*/}
                {/*    onContentSizeChange={that.onContentSizeChange}*/}
                {/*    style={[styles.scroll, themeBg]}*/}
                {/*    keyboardDismissMode={'none'}*/}
                {/*    onScroll={this.onScrollContent}*/}
                {/*    showsVerticalScrollIndicator={false}*/}
                {/*    // keyboardDismissMode={'on-drag'}*/}
                {/*    // keyboardShouldPersistTaps={'handled'}*/}
                {/*>*/}
                {/*</ScrollView>*/}
                {/*<DragTextEditor*/}
                {/*    FontColor={"#000000"}*/}
                {/*    LineHeight={15}*/}
                {/*    TextAlign={"left"}*/}
                {/*    LetterSpacing={0}*/}
                {/*    FontSize={15}*/}
                {/*    isDraggable={false}*/}
                {/*    isResizable={false}*/}
                {/*    TopRightAction={()=>console.log("-Top Right Pressed")}*/}
                {/*    centerPress={()=>console.log("-Center Pressed")}*/}
                {/*    onDragStart={()=>console.log("-Drag Started")}*/}
                {/*    onDragEnd={()=>console.log("- Drag ended")}*/}
                {/*    onDrag={()=>console.log("- Dragging...")}*/}
                {/*    onResizeStart={()=>console.log("- Resize Started")}*/}
                {/*    onResize={()=>console.log("- Resizing...")}*/}
                {/*    onResizeEnd={()=>console.log("- Resize Ended")}*/}
                {/*/>*/}

                {/*<TestCollapsibleNavBarScrollView/>*/}
                {/*<View style={{position: 'absolute', width: '100%', top: '10%'}}>*/}
                {/*    <TestDraggableFlatList/>*/}
                {/*</View>*/}
                {/*<ScrollView*/}
                {/*    ref={ref => that.scrollView = ref}*/}
                {/*    onContentSizeChange={that.onContentSizeChange}*/}
                {/*    style={[styles.scroll, themeBg]}*/}
                {/*    keyboardDismissMode={'none'}*/}
                {/*    onScroll={this.onScrollContent}*/}
                {/*    showsVerticalScrollIndicator={false}*/}
                {/*    // keyboardDismissMode={'on-drag'}*/}
                {/*    // keyboardShouldPersistTaps={'handled'}*/}
                {/*>*/}
                {/*    {isReadStatus ? null :*/}
                {/*        <View style={{*/}
                {/*            height: this.state.nameViewHeight,*/}
                {/*        }}>*/}
                {/*            <Anonymous*/}
                {/*                onChangeNickname={this.onChangeNickname}*/}
                {/*                onChangePassword={this.onChangePassword}*/}
                {/*            />*/}
                {/*        </View>*/}
                {/*    }*/}
                {/*    <RichEditor*/}
                {/*        ref={that.richText}*/}
                {/*        disabled={isReadStatus}*/}
                {/*        editorStyle={contentStyle} // default light style*/}
                {/*        containerStyle={themeBg}*/}
                {/*        style={[themeBg, editorStyle]}*/}
                {/*        initialContentHTML={initHTML}*/}
                {/*        placeholder={placeHolder}*/}
                {/*        editorInitializedCallback={() => this.editorInitializedCallback()}*/}
                {/*        onChange={that.handleChange}*/}
                {/*        onHeightChange={that.handleHeightChange}*/}
                {/*    />*/}
                {/*</ScrollView>*/}
                {/*</View>*/}
                {/*<View style={styles.empty}>*/}
                {/*</View>*/}
                {/*{this.state.commentImage !== null ?*/}
                {/*    <View style={commentImageStyle}>*/}
                {/*        <TouchableOpacity*/}
                {/*            style={styles.closeButton}*/}
                {/*            onPress={() => this.setState({commentImage: null})}>*/}
                {/*            <MaterialIcons name={'close'} size={24} color={'#000000'}/>*/}
                {/*        </TouchableOpacity>*/}
                {/*        <Image source={this.state.commentImage}*/}
                {/*               style={{width: this.state.commentImage.width, height: this.state.commentImage.height}}/>*/}
                {/*    </View> : null*/}
                {/*}*/}
                {/*{this.state.commentImage !== null ?*/}
                {/*    <View style={commentImageStyle}>*/}
                {/*        <Image source={this.state.commentImage}/>*/}
                {/*    </View>*/}
                {/*    : null}*/}
                {/*{isReadStatus ?*/}
                {/*    <View*/}
                {/*        style={[styles.comment, {height: this.state.commentHeight}]}*/}
                {/*        onLayout={e => {*/}
                {/*            this.setState({*/}
                {/*                commentLayout: e.nativeEvent.layout,*/}
                {/*            });*/}
                {/*        }}*/}
                {/*    >*/}
                {/*        <Comment*/}
                {/*            selectImage={this.selectImage}*/}
                {/*        />*/}
                {/*    </View> :*/}
                {/*    <View style={this.state.toolbarStyle}*/}
                {/*          onLayout={e => {*/}
                {/*              this.setState({*/}
                {/*                  toolbarLayout: e.nativeEvent.layout,*/}
                {/*              });*/}
                {/*          }}*/}
                {/*    >*/}
                {/*        <RichToolbar*/}
                {/*            style={[styles.richBar, themeBg]}*/}
                {/*            editor={that.richText}*/}
                {/*            disabled={disabled}*/}
                {/*            iconTint={color}*/}
                {/*            selectedIconTint={'#2095F2'}*/}
                {/*            disabledIconTint={'#8b8b8b'}*/}
                {/*            onPressAddImage={that.onPressAddImage}*/}
                {/*            onInsertLink={that.onInsertLink}*/}
                {/*            iconSize={40} // default 50*/}
                {/*            actions={[*/}
                {/*                'insertVideo',*/}
                {/*                ...defaultActions,*/}
                {/*                actions.setStrikethrough,*/}
                {/*                actions.heading1,*/}
                {/*                actions.heading4,*/}
                {/*                'insertHTML',*/}
                {/*            ]} // default defaultActions*/}
                {/*            iconMap={{*/}
                {/*                [actions.setStrikethrough]: strikethrough,*/}
                {/*                [actions.heading1]: ({tintColor}) => (*/}
                {/*                    <Text style={[styles.tib, {color: tintColor}]}>H1</Text>*/}
                {/*                ),*/}
                {/*                [actions.heading4]: ({tintColor}) => (*/}
                {/*                    <Text style={[styles.tib, {color: tintColor}]}>H3</Text>*/}
                {/*                ),*/}
                {/*                insertHTML: htmlIcon,*/}
                {/*                insertVideo: videoIcon,*/}
                {/*            }}*/}
                {/*            insertHTML={that.insertHTML}*/}
                {/*            insertVideo={that.insertVideo}*/}
                {/*        />*/}
                {/*    </View>*/}
                {/*}*/}
                {this.state.onRequesting ?
                    <View style={{position: 'absolute', height: '100%', width: '100%'}}>
                        <Loading color={'#eb6a0b'}/>
                    </View>
                    : null}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FF00FF',
    },
    nav: {
        position: 'absolute',
        top: 0,
        height: '10%',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#ffffff',
        // marginHorizontal: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#f5d9cc',
        zIndex: 99,
    },
    navBack: {
        flex: 0.1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#00ffff',
    },
    navBackButton: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#00ff00'
    },
    navTitle: {
        flex: 0.7,
        // backgroundColor: '#00ffff',
    },
    navSubmit: {
        flex: 0.2,
        justifyContent: 'center',
        alignContent: 'center',
    },
    rich: {
        //backgroundColor: '#00ff00',
        //minHeight: 300,
        //minHeight: 300,
        flex: 1,
    },
    empty: {
        // minHeight: 24,
        flex: 0.1,
    },
    richBar: {
        height: 50,
        backgroundColor: '#F5FCFF',
    },
    scroll: {
        // height: '80%',
        flex: 1,
        // backgroundColor: '#ffff00',
    },
    item: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: '#e8e8e8',
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    input: {
        flex: 1,
        textAlign: 'left',
        textAlignVertical: 'center',
        fontSize: 18,
        borderWidth: 0,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    nickname: {
        // textAlignVertical: 'top',
        backgroundColor: 'rgba(217, 217, 217, 1.0)',
        borderRadius: 10,
        margin: 10,
    },
    password: {
        backgroundColor: 'rgba(217, 217, 217, 1.0)',
        borderRadius: 10,
        margin: 10,
    },
    tib: {
        textAlign: 'center',
        color: '#515156',
    },
    comment: {
        position: 'absolute',
        height: 24,
        width: '100%',
        bottom: 0,
        // backgroundColor: '#00ff00',
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        padding: 5,
    },
});
