import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView,
    SafeAreaView,
    TextInput, Keyboard, Image,
} from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import config from '../../config';
import {StyledText} from '../../components/StyledComponents';

const labelSubmit = '등록';
const labelEdit = '수정';
const labelClose = '닫기';
const labelCancel = '취소';
const baseColor = '#00a3ff';
const labelPost = '글쓰기';
const labelEditPost = '글 수정하기';
const labelPostReadOnly = '게시판';

export default class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: undefined,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }


    // **
    onPressSubmit = () => {
        const {onPress, onPressEdit, editMode} = this.props;
        if (editMode) {
            onPressEdit();
        } else {
            onPress();
        }
    };
    onPressOption = () => {
        console.log('onPressOption');
        const {onPressOption} = this.props;
        onPressOption();
    };

    onChangeTitle = (text) => {
        const {onChangeTitle} = this.props;
        onChangeTitle(text);
    };

    onPressGoBack = () => {
        const {onPressGoBack, onPressCancel, editMode} = this.props;
        if (editMode) {
            onPressCancel();
        } else {
            onPressGoBack();
        }
    };

    render() {
        const {isReadStatus, navigation, post, editMode, postChanged} = this.props;
        const that = this;
        let title = labelPost;
        if (editMode) {
            title = labelEditPost;
        } else {
            if (isReadStatus) {
                title = labelPostReadOnly;
            }
        }

        return (
            <View style={styles.container}>
                <View style={styles.navBack}>
                    <TouchableOpacity
                        style={styles.navBackButton}
                        onPress={this.onPressGoBack}>
                        {isReadStatus ?
                            <View style={styles.icon}>
                                {/*<Image
                                    source={config.images.goBack}
                                    style={{height: config.size._96px, width: config.size._96px, alignSelf: 'center'}}
                                />*/}
                                <Icon
                                    style={{marginLeft : -3,}}
                                    name="chevron-left"
                                    color={"black"}
                                    size={36}
                                    underlayColor="#4BA6F8"/>
                            </View>
                            :
                            <StyledText style={[config.styles.text, {lineHeight: config.size._184px, color: '#000000'}]}>
                                {editMode ? labelCancel : labelClose}
                            </StyledText>
                        }
                    </TouchableOpacity>
                </View>
                <View style={styles.navTitle}>
                    <StyledText style={[styles.text, {color: '#000000', fontSize: 18, fontFamily:config.defaultBoldFontFamily}]}>
                        {title}
                    </StyledText>
                </View>
                {/*<View style={styles.navTitle}>*/}
                {/*    {isReadStatus ?*/}
                {/*        <Text*/}
                {/*            style={[styles.input]}*/}
                {/*            maxLength={1024}*/}
                {/*            numberOfLines={1}*/}
                {/*        >*/}
                {/*            {post.title}*/}
                {/*        </Text>*/}
                {/*        :*/}
                {/*        <TextInput*/}
                {/*            style={[styles.input]}*/}
                {/*            placeholder="제목을 입력하세요"*/}
                {/*            maxLength={1024}*/}
                {/*            onChangeText={this.onChangeTitle}*/}
                {/*        />*/}
                {/*    }*/}
                {/*</View>*/}
                <View style={styles.navSubmit}>
                    {isReadStatus ?
                        <TouchableOpacity
                            style={[styles.navBackButton, {width: '100%', height: '100%'}]}
                            onPress={that.onPressOption}>
                            <Image
                                source={config.images.threeDot}
                                style={{height: config.size._96px, width: config.size._96px, alignSelf: 'center'}}
                            />
                        </TouchableOpacity>
                        :
                        (postChanged ?
                            <TouchableOpacity
                                onPress={that.onPressSubmit}>
                                <StyledText style={styles.text}>
                                    {editMode ? labelEdit : labelSubmit}
                                </StyledText>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                disabled={true}
                                onPress={that.onPressSubmit}>
                                <StyledText style={[styles.text, {color: config.colors.lineColor}]}>
                                    {editMode ? labelEdit : labelSubmit}
                                </StyledText>
                            </TouchableOpacity>
                        )
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#ffffff',
        // marginHorizontal: 5,
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
        zIndex: 99,
    },
    navBack: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'flex-start',
        // backgroundColor: '#00ffff',
    },
    navBackButton: {
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#00ff00'
    },
    navTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#00ffff',
    },
    navSubmit: {
        flex: 0.2,
        justifyContent: 'center',
        alignContent: 'center',
    },
    input: {
        flex: 1,
        textAlign: 'left',
        textAlignVertical: 'center',
        fontSize: 18,
        borderWidth: 0,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    text: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 14,
        color: baseColor,
    },
    icon: {
        flex: 1,
        justifyContent: 'center',
        borderWidth: 0,
        paddingHorizontal: 10,
    },
});
