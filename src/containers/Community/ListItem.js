import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text, TouchableOpacity, TextInput, Image,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Author from './Author';
import config from 'src/config';
import {StyledText} from '../../components/StyledComponents';

const baseColor = '#00a3ff';

const getTextLength = (str) => {
    let len = 0;
    for (let i = 0; i < str.length; i++) {
        if (escape(str.charAt(i)).length == 6) {
            len++;
        }
        len++;
    }
    return len;
}

export default class ListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    renderWrittenBy = () => {
        return (
            <Author
                post={this.props.post}
                />
        );
        // return (
        //     <View style={{flex: 1, flexDirection: 'row'}}>
        //         <Text style={styles.textWrittenBy}>{post.written_by.name}</Text>
        //         <Text style={styles.textWrittenBy}>{post.published_at}</Text>
        //         <View style={styles.icon}>
        //             <SimpleLineIcons name={'eye'}/>
        //         </View>
        //         <Text style={styles.textWrittenBy}>{hitCount}</Text>
        //         <View style={styles.icon}>
        //             <Entypo name={'heart'} color={'#f2bbc5'}/>
        //         </View>
        //
        //         <Text style={styles.textWrittenBy}>{recommendationCount}</Text>
        //     </View>
        // );
    };
    render() {
        const {post} = this.props;
        const {height, fontSize} = this.props;
        // const itemHeight = height;
        const itemHeight = {
            height: config.size._220px,
        };
        const itemFontSize = {
            fontSize: 13,
        };
        console.log('post', post.report_count);
        let commentCount = '';
        if (post.comment_count) {
            commentCount = '(' + post.comment_count +')';
        }
        let imageCount = post.image_count;

        // console.log('Group', post);
        let titleAdjusted = post.title;
        // let maxLen = 16;
        // if (titleAdjusted.length > maxLen) {
        //     titleAdjusted = titleAdjusted.substr(0, maxLen) + '.....';
        // }
        let thumbnailUri = '';
        if (post.uri && post.uri.thumbnail && post.uri.thumbnail.length > 0) {
            thumbnailUri = post.uri.thumbnail
        }
        if (post.isNotice) {
            return (
                <View style={[styles.container, {backgroundColor: '#e9e9e9', }]}>
                    <View style={[styles.titleLayout, {flex: 1}]}>
                        <View style={styles.textWrapper}>
                            <View style={[styles.textLayout, {flexDirection: 'row', justifyContent: 'center'}]}>
                                <View style={[styles.group]}>
                                    <StyledText style={[config.styles.text2, {fontSize: 13, fontFamily: config.defaultMediumFontFamily, color: config.colors.baseColor, lineHeight: config.size._200px}]}>{post.group_name}</StyledText>
                                </View>
                                <View style={[styles.title]}>
                                    <View>
                                        <StyledText style={[config.styles.text2, itemFontSize, {lineHeight: config.size._200px}]} numberOfLines={1}>{titleAdjusted}</StyledText>
                                    </View>
                                    <View style={{marginLeft: config.size._48px}}>
                                        <StyledText style={[config.styles.text2, {fontSize: 10, color: config.colors.pinkColor, lineHeight: config.size._200px}]}>{commentCount}</StyledText>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={[styles.container]}>
                    <View style={[styles.titleLayout, {marginVertical: 10}]}>
                        {thumbnailUri.length > 0 ?
                            <View style={[styles.thumbnailLayout, {width: config.size._208px, height: config.size._160px}]}>
                                <Image source={{
                                    uri: thumbnailUri,
                                }}
                                       style={styles.thumbnailStyle}/>
                                {imageCount > 1 ?
                                    <View style={styles.thumbnailTextView}>
                                        <StyledText style={[config.styles.text2, {textAlign: 'center', textAlignVertical: 'center', fontFamily:config.defaultBoldFontFamily, fontSize: 8, color: '#ffffff'}]}>
                                            {imageCount}+
                                        </StyledText>
                                    </View>
                                    : null}
                            </View>
                            : null }
                        <View style={styles.textWrapper}>
                            <View style={styles.textLayout}>
                                {post.report_count && post.report_count > 9999? 
                                    <View style={[styles.title]}>
                                        <StyledText style={[config.styles.text2, {fontSize: 13}]} numberOfLines={1}>{"관리자에 의해 블라인드 처리된 글입니다"}</StyledText>
                                    </View>
                                    :
                                    <View style={[styles.title]}>
                                        <View >
                                            <StyledText style={[config.styles.text2, {fontSize: 13}]} numberOfLines={1}>{titleAdjusted}</StyledText>
                                        </View>
                                        <View style={{marginLeft: config.size._48px}}>
                                            <StyledText style={[config.styles.text2, {fontSize: 10, color: config.colors.pinkColor}]}>{commentCount}</StyledText>
                                        </View>
                                    </View>
                                }
                                <View style={styles.writtenBy}>
                                    {this.renderWrittenBy()}
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            );
        }
        // else {
        //     return (
        //         <View style={[styles.container]}>
        //             <View style={styles.titleLayout}>
        //                 {post.uri && post.uri.thumbnail && post.uri.thumbnail.length > 0 ?
        //                     <View style={[styles.thumbnailLayout, {height: itemHeight.height * 0.75, width: itemHeight.height * 0.75}]}>
        //                         <Image source={{
        //                             uri: post.uri.thumbnail,
        //                         }}
        //                                style={styles.thumbnailStyle}/>
        //                         {imageCount > 1 ?
        //                             <View style={styles.thumbnailTextView}>
        //                                 <Text style={[styles.text, {textAlign: 'center', textAlignVertical: 'center', fontWeight: 'bold', fontSize: 8, color: '#ffffff'}]}>
        //                                     {imageCount}+
        //                                 </Text>
        //                             </View>
        //                             : null}
        //                     </View>
        //                     : null }
        //                 <View style={styles.textLayout}>
        //                     <View style={styles.title}>
        //                         <Text style={[styles.text, itemFontSize]}>{titleAdjusted}</Text>
        //                     </View>
        //                     <View style={styles.writtenBy}>
        //                         {this.renderWrittenBy()}
        //                     </View>
        //                 </View>
        //             </View>
        //             <View style={styles.commentLayout}>
        //                 <Text style={[styles.text, {fontSize: 16, fontWeight: 'bold', textAlign: 'center'}]}>{commentCount}</Text>
        //             </View>
        //         </View>
        //     );
        // }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: config.size._220px,
        // borderWidth: 1,
    },
    titleLayout: {
        flex: 0.98,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginHorizontal: config.size._64px,
        paddingRight: 15,
    },
    thumbnailLayout: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        // backgroundColor: 'rgba(0, 0, 0, 0.3)'
    },
    thumbnailStyle: {
        width: '100%',
        height: '100%',
        // borderRadius: 10,
        // resizeMode: 'contain',
    },
    thumbnailTextView: {
        position: 'absolute',
        top: 5,
        right: 5,
        width: 14,
        height: 14,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    textWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    textLayout: {
        flex: 1,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        // backgroundColor: 'rgba(0, 0, 0, 0.1)'
    },
    group: {
        // flex: 0.2,
        marginRight: 15,
        // backgroundColor: 'rgba(255, 0, 0, 0.5)'

    },
    title: {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'rgba(255, 0, 0, 0.5)'

    },
    writtenBy: {
        flex: 0.8,
        // marginTop: 5,
        // justifyContent: 'center',
        // backgroundColor: 'rgba(255, 0, 0, 0.1)'
    },
    textWrittenBy: {
        textAlign: 'center',
        textAlignVertical: 'center',
        paddingRight: 5,
        fontSize: 8,
        // color: 'rgba(130, 124, 130, 1)',
        // backgroundColor: '#0000ff',
        // fontFamily: 'NanumBarunGothicOTF',
    },
    commentLayout: {
        flex: 0.1,
        justifyContent: 'flex-end',
        borderRadius: 10,
        margin: 5,
        backgroundColor: 'rgb(239,242,239)',
    },
    text: {
        flex: 1,
        textAlign: 'left',
        textAlignVertical: 'center',
        fontSize: 24,
    },
    padding: {
        width: 10,
    },
    icon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 2,
        // backgroundColor: '#00ff00',
    },
});
