import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    ScrollView,
    Text, Keyboard, TextInput, BackHandler,
} from 'react-native';
import ListItem from './ListItem';
import Tabs from './Tabs';
import PageNumbers from './PageNumbers';
import config from '../../config';

export default class List extends Component {

    constructor(props) {
        super(props);
        this._refFlatList = React.createRef();
        this.state = {
            refreshing: false,
            postListLayout: undefined,
            reachEnd: true,
            itemHeight: 48,
            itemFontSize: 14,
            hideKeyboard: true,
            keyboardHeight: 0,
        };
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.addListener('keyboardDidHide', this.onKeyBoardHide);

        const {setRef} = this.props;
        if (setRef) {
            setRef(this)
        }
    }

    componentWillUnmount() {
        Keyboard.removeListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.removeListener('keyboardDidHide', this.onKeyBoardHide);
    }

    onKeyBoard = (e) => {
        this.setState({
            hideKeyboard: false,
            keyboardHeight: e.endCoordinates.height,
        })
    };
    onKeyBoardHide = (e) => {
        this.setState({
            hideKeyboard: true,
            keyboardHeight: 0,
        })
    };

    scrollToOffset = (offset) => {
        if (this._refFlatList && this._refFlatList.scrollToOffset) {
            this._refFlatList.scrollToOffset({animated: false, offset: offset});
        }
    };
    handleRefresh = () => {
        const {refreshCurrentPage} = this.props;
        this.setState(
            {
                // refreshing: true,
            },
            () => {
                refreshCurrentPage();
            },
        );
    };
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: config.size._2px,
                    width: '100%',
                    backgroundColor: config.colors.lineColor,
                    marginTop: -config.size._2px,
                    // borderBottomWidth: config.size._2px,
                    // borderBottomColor: config.colors.lineColor,
                }}
            />
        );
    };

    renderHeader = () => {
        return <View></View>;
    };

    renderFooter = (list) => {
        const {postPerPage} = this.props;

        // if (list.length <= 0 || list.length >= postPerPage || this.state.reachEnd)  {
        if (list.length <= 0 || list.length >= postPerPage)  {

                return null
        }
        return (
            <View
                style={{
                    height: config.size._2px,
                    width: '100%',
                    backgroundColor: config.colors.lineColor,
                    marginTop: config.size._2px,
                    // backgroundColor: config.colors.lineColor,
                }}
            />
        );
    };
    handleLoadMore = () => {
        this.setState({
            reachEnd: true,
        })
    };
    onPressPageNumber = (page) => {
        const {onPressPageNumber} = this.props;
        onPressPageNumber(page);
    };
    onPressGroup = (group) => {
        const {onPressGroup} = this.props;
        this.setState({
            reachEnd: false,
        });
        onPressGroup(group);
    };
    onPressPost = (item) => {
        console.log('onPressItem', item);
        if (item.report_count && item.report_count > 9999) {
            return
        }
        
        const {onPressPost} = this.props;
        onPressPost(item.post_id);
    };

    render() {
        const {postList, noticeList, postPerPage, setItemSize} = this.props;
        let showPostList = [];
        const {
            pages,
            currentPage,
            currentGroup,
            hideLeft,
            hideRight,
            onPressMovePage,
            searchMode,
        } = this.props;
        const {
            hideKeyboard,
        } = this.state;

        if (searchMode) {

        } else {
            // console.log('currentGroup', currentGroup, 'currentPage', currentPage, noticeList.length, postList.length);
            if ((!currentGroup || currentGroup.length < 1) && currentPage < 2 && noticeList && noticeList.length > 0) {
                // 공지 사항이 있다면
                for (let i = 0; i < noticeList.length; i++) {
                    showPostList.push(noticeList[i]);
                }
            }
        }
        for (let i = 0; i < postPerPage; i++) {
            if (i < postList.length) {
                showPostList.push(postList[i]);
            }
            /* 항상 25개 보이게 하고 싶으면 주석 푸세요
            else {
                showPostList.push({
                    id: i.toString(),
                });
            }*/
        }
        // console.log(showPostList);
        return (
            <View style={styles.container}>
                {searchMode ? null :
                    <View style={styles.tabs}>
                        <Tabs
                            onPressGroup={this.onPressGroup}
                            currentGroup={currentGroup}
                        />
                    </View>
                }
                <View style={styles.postList}
                      onLayout={e => {
                          let itemHeight = ~~(e.nativeEvent.layout.height * 0.1 - 1);
                          let itemFontSize = e.nativeEvent.layout.height * 0.025;
                          this.setState({
                              postListLayout: e.nativeEvent.layout,
                              itemHeight: itemHeight,
                              itemFontSize: itemFontSize,
                          });
                          setItemSize(itemHeight, itemFontSize)
                      }}
                >
                    {this.state.postListLayout && showPostList ?
                        <FlatList
                            ref={(ref) => this._refFlatList = ref}
                            containerStyle={{borderTopWidth: 0, borderBottomWidth: 0}}
                            data={showPostList}
                            renderItem={({item, index}) => (
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                }}>
                                    <TouchableOpacity
                                        style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignContent: 'center',
                                        }}
                                        onPress={() => {
                                            this.onPressPost(item);
                                        }}
                                    >
                                        <ListItem
                                            key={item.post_id + index}
                                            post={item}
                                            height={{'height': this.state.itemHeight}}
                                            fontSize={{'fontSize': this.state.itemFontSize}}
                                        />
                                    </TouchableOpacity>

                                </View>
                            )}
                            keyExtractor={(item, index) => item.post_id + index }
                            ItemSeparatorComponent={this.renderSeparator}
                            // ListHeaderComponent={this.renderHeader}
                            ListFooterComponent={() => this.renderFooter(showPostList)}
                            onRefresh={this.handleRefresh}
                            refreshing={this.state.refreshing}
                            // stickyHeaderIndices={[0]}
                            initialNumToRender={postPerPage}
                            onEndReached={this.handleLoadMore}
                            // onEndReachedThreshold={50}
                            showsVerticalScrollIndicator={false}
                        />
                        : null}

                </View>
                {showPostList.length === 0 && searchMode ?
                    null
                    :
                    <View style={[styles.page, {bottom: -this.state.keyboardHeight}]}>
                        <PageNumbers
                            pages={pages}
                            currentPage={currentPage}
                            onPressPageNumber={this.onPressPageNumber}
                            hideLeft={hideLeft}
                            hideRight={hideRight}
                            onPressMovePage={onPressMovePage}
                        />
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'rgba(0, 0, 128, 1.0)',
    },
    tabs: {
        height: config.size._200px,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
    },
    postList: {
        flex: 0.9,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        flexDirection: 'column',
    },
    page: {
        flex: 0.1,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        marginTop: -config.size._2px,
        borderTopWidth: config.size._2px,
        borderTopColor: config.colors.lineColor,
    },
});
