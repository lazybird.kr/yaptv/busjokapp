import React, {Component, useRef} from 'react';
import {
    View,
    FlatList,
    Keyboard,
    TextInput,
    Text,
    TouchableOpacity,
    StyleSheet,
    BackHandler,
    SafeAreaView, Image, StatusBar,
} from 'react-native';
import config from 'src/config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';
import ListItem from '../../containers/Community/ListItem';
import Entypo from 'react-native-vector-icons/Entypo';
import List from '../../containers/Community/List';
import Loading from './Loading';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class SearchScreen extends Component {
    constructor() {
        super();
        this.pageLimit = 5;
        this.touch = {
            isTouchedStack: false,
        };
        this.textInput = null;
        this.state = {
            searchValue: '',
            isFocused: false,
            searchData: [],
            keyboardHeight: 0,
            offset: 0,
            currentPage: 1,
            postPerPage: 25,
            pages: ['1'],
            currentPostList: [],
            noticeList: [],
            currentGroupName: 'default',
            hideLeft: true,
            hideRight: true,
            onRequesting: false,
            itemHeight: 48,
            itemFontSize: 14,
            searchKeyword: '',
            called: false,
        };
        this._isMounted = false;
    }

    onChangeText = (text) => {
        if (!text) {
            this.setState({
                searchValue: '',
                called: false,
            });
            return;
        }
        this.setState({
            searchValue: text,
            called: false,
        });
        // if (!text) {
        //     this.setState({searchValue: '', searchData: []});
        //     return;
        // }
        //
        // /* fetch Data */
        // let tempData = ['A', 'B', 'C'];
        // this.setState({searchValue: text, searchData: tempData});
    };

    onClearText = () => {
        this.textInput && this.textInput.getInnerRef().focus();
        this.setState({
            searchValue: '',
            called: false,
        });
    };

    onTouchSearch = () => {
        console.log('onTouchSearch', this.state.searchValue);
        if (this.state.searchValue.length === 0) {
            this.setState({
                called: true,
            });
            return;
        }
        // TODO: 검색 필터링 설정 다 여기서 하자.
        Keyboard.dismiss();
        this.setState({
            onRequesting: true,
        });
        this.serviceGetPostList({
                offset: 0,
                currentPage: 1,
            },
        );
    };

    handleClearText = () => {
        this.setState({
            searchValue: '', searchData: [],
        });
    };

    componentDidMount() {
        const {navigation} = this.props;
        this._isMounted = true;
        this.subs = [
            Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
            Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
            // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick),
            navigation.addListener("didFocus", () => {
                this.setState({ isFocused: true });
            }),
        ];
        console.log('SearchScreen componentDidMount', this.textInput)
        const {textInput} = this;
        setTimeout(() => {
            textInput && textInput.getInnerRef().focus();
        }, 500);
    }

    componentWillUnmount() {
        this.subs.forEach((sub) => {
            sub.remove();
        });
        this._isMounted = false;
    }

    _keyboardDidShow(e) {
        if (this._isMounted) {
            this.setState({keyboardHeight: e.endCoordinates.height});
        }
    }

    _keyboardDidHide(e) {
        if (this._isMounted) {
            this.setState({keyboardHeight: 0});
        }
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    alignSelf: 'center',
                    width: '100%',
                    marginLeft: 10,
                    backgroundColor: '#CED0CE',
                }}
            />
        );
    };

    serviceGetPostList = ({
                              host = config.url.apiSvr,
                              deviceId = config.deviceInfo.device_id,
                              sessionId = config.sessionInfo.session_id,
                              groupName = '',
                              offset = this.state.offset,
                              currentPage = this.state.currentPage,
                              serviceName = 'Search',
                              length = this.state.postPerPage * 5 + 1,
                              searchKeyword = {
                                  title: this.state.searchValue,
                              },
                          } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: serviceName,
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                group_name: groupName,
                offset: offset,
                length: length,
            },
            search: {
                title: searchKeyword.title,
                content: searchKeyword.content,
            },
        };
        console.log('serviceGetPostList', send_data);
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                    called: true,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    let postList = [];
                    if (json.data.post_list) {
                        postList = json.data.post_list;
                    }
                    let pages = [];
                    let currentStartPageNumber = ~~(offset / this.state.postPerPage);
                    let pageNumber = ~~(offset / this.state.postPerPage) + 1;
                    let limit = 0;
                    pages.push(pageNumber.toString());
                    for (let i = 0; i < postList.length; i++) {
                        if (i >= (limit + 1) * this.state.postPerPage) {
                            pageNumber += 1;
                            pages.push(pageNumber.toString());
                            limit += 1;
                            if (limit >= this.pageLimit - 1) {
                                break;
                            }
                        }
                    }
                    // 포스팅 시작/끝
                    let s = (currentPage - currentStartPageNumber - 1) * this.state.postPerPage;
                    let e = s + this.state.postPerPage;

                    // 앞/뒤 이동 화살표
                    let l = true;
                    let r = true;
                    if (offset > 0) {
                        l = false;
                    }
                    if (postList.length > this.state.postPerPage * 5) {
                        r = false;
                    }
                    this.setState({
                        currentPostList: postList.slice(s, e),
                        currentPage: currentPage,
                        pages: pages,
                        offset: offset,
                        hideLeft: l,
                        hideRight: r,
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    onPressPageNumber = (page) => {
        console.log('onPressPageNumber', page);
        let currentStartPage = ~~(page / (this.pageLimit + 1));
        console.log('currentStartPage', currentStartPage);
        let offset = (currentStartPage * this.state.postPerPage * this.pageLimit);
        this.serviceGetPostList({
            offset: offset,
            currentPage: page,
        });
    };
    onPressMovePage = (direction) => {
        let offset = this.state.offset + (direction * this.state.postPerPage * this.pageLimit);
        let startPageNumber = ~~(offset / this.state.postPerPage) + 1;
        let currentPage = startPageNumber;
        if (direction < 0) {
            currentPage += 4;
        }
        // console.log('DEBUG-1', this.state.offset, offset, currentPage);
        this.serviceGetPostList({
            currentPage: currentPage,
            offset: offset,
        });
    };

    setItemSize = (height, fontSize) => {
        this.setState({
            itemHeight: height,
            itemFontSize: fontSize,
        });
    };
    onPressPost = (postId) => {
        if (this.touch.isTouchedStack) {
            return;
        }
        this.touch.isTouchedStack = true;
        const {touch} = this;
        setTimeout(() => {
            touch.isTouchedStack = false;
        }, 500);
        // console.log('post_id', postId);
        this.props.navigation.push('readPostScreen', {
                currentGroupName: this.state.currentGroupName,
                read: true,
                postId: postId,
                onGoBack: (e) => {

                },
            },
        );
    };
    // handleBackButtonClick = () => {
    //     this.goBack();
    // };
    goBack = () => {
        const {navigation} = this.props;
        navigation.state.params.onGoBack();
        navigation.goBack();
    };

    render() {
        const {searchValue} = this.state;
        const {
            numberOfPostPerPage,
        } = this.props.navigation.state.params;
        const {onChangeText, onClearText, onTouchSearch, goBack} = this;
        const searchTitle = '검색어를 입력하세요';

        return (
            <View style={{width: '100%', height: '100%', backgroundColor: '#ffffff'}}>
                <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0}}/>
                {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}

                <View style={{height: 65, backgroundColor: 'white', width: '100%', zIndex: 1}}>
                    <View style={{
                        height: 40, flexDirection: 'row',
                        margin: 10,
                    }}>
                        <View style={{height: '100%', justifyContent: 'center'}} onTouchStart={goBack}>
                            {/*<Image
                                source={config.images.goBack}
                                style={{height: config.size._96px, width: config.size._96px, alignSelf: 'center'}}
                            />*/}
                            <Icon
                                    style={{marginLeft : -3,}}
                                    name="chevron-left"
                                    color={"black"}
                                    size={36}
                                    underlayColor="#4BA6F8"/>
                        </View>
                        <View style={{
                            flex: 1,
                            marginLeft: 5,
                            paddingRight: 10,
                            borderRadius: 10,
                            borderWidth: config.size._4px,
                            borderColor: config.colors.lineColor,
                            flexDirection: 'row',
                        }}>
                            <StyledTextInput
                                style={[config.styles.text, {flex: 8,marginLeft: 10, color: 'grey', alignSelf: 'center', fontSize: 13}]}
                                ref={(input) => this.textInput = input}
                                onSubmitEditing={onTouchSearch}
                                onChangeText={onChangeText}
                                placeholder={searchTitle} placeholderTextColor="grey"
                            >
                                {searchValue}
                            </StyledTextInput>
                            {searchValue ?
                                <View style={{height: '100%', justifyContent: 'center'}} onTouchStart={onClearText}>
                                    <Image
                                        source={config.images.deleteInput}
                                        style={{
                                            height: config.size._96px,
                                            width: config.size._96px,
                                            alignSelf: 'center',
                                        }}
                                    />
                                </View>
                                : <View style={{flex: 1, width: 24}}/>}
                            <TouchableOpacity
                                activeOpacity={.2}
                                onPress={onTouchSearch}
                                style={[config.styles.button]}>
                                <Image
                                    source={config.images.searchPostOff}
                                    style={{height: config.size._96px, width: config.size._96px, alignSelf: 'center'}}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            borderBottomWidth: config.size._2px,
                            borderBottomColor: config.colors.lineColor,
                        }}
                    />
                </View>
                <View style={styles.searchResultList}>
                    {this.state.called && this.state.currentPostList.length === 0 ?
                        <View>
                            <View style={{height: config.size._200px}} />
                            <View style={styles.notFound}>
                                <StyledText style={[config.styles.text, {
                                    textAlign: 'center',
                                    textAlignVertical: 'top',
                                    // marginBottom: -10,
                                    // paddingVertical: 0,
                                    color: config.colors.greyColor,
                                    // backgroundColor: '#ff0000',
                                    fontSize: 16,
                                }]}>
                                    {'검색 결과가 없습니다.\n'}
                                    {'검색어를 다시 확인해주세요.'}
                                </StyledText>
                            </View>
                        </View> :
                        <List
                            serviceGetPostList={this.serviceGetPostList}
                            postList={this.state.currentPostList}
                            pages={this.state.pages}
                            currentPage={this.state.currentPage.toString()}
                            currentGroup={''}
                            onPressPageNumber={this.onPressPageNumber}
                            hideLeft={this.state.hideLeft}
                            hideRight={this.state.hideRight}
                            onPressMovePage={this.onPressMovePage}
                            onPressPost={this.onPressPost}
                            postPerPage={numberOfPostPerPage}
                            refreshCurrentPage={this.serviceGetPostList}
                            setItemSize={this.setItemSize}
                            searchMode={true}
                            searchKeyaord={this.state.searchKeyword}
                        />
                    }
                </View>
                {this.state.onRequesting ?
                    <View style={{position: 'absolute', height: '100%', width: '100%'}}>
                        <Loading color={'#eb6a0b'}/>
                    </View>
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    header: {
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        height: 48,
        flexDirection: 'row',
        borderBottomWidth: config.size._4px,
        borderBottomColor: '#f4f4f4',
    },
    headerTitle: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 16,
        //fontWeight: 'bold',
        fontFamily:config.defaultBoldFontFamily
    },
    headerTool: {
        position: 'absolute',
        right: 0,
        flexDirection: 'row',
        height: '100%',
    },
    headerSearch: {
        flex: 1,
        paddingHorizontal: 8,
        justifyContent: 'center',
    },
    headerNewPost: {
        flex: 1,
        paddingHorizontal: 8,
        justifyContent: 'center',
    },
    padding: {
        width: 10,
    },
    searchResultList: {
        flex: 1.1,
        // backgroundColor: '#00ff00'
    },
    notFound: {
        height: 152,
        justifyContent: 'center',
        flexDirection: 'column',
    },
});

