import React from 'react';
import { Text } from 'react-native';
import CommunityScreen from './CommunityScreen';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';
import PostScreen from './PostScreen';
import Editor from './Editor';
import EditPost from './EditPost';
import SearchScreen from './SearchScreen';
import Header from './Header';
import LoginScreen from '../Login';

const CommunityStack = createStackNavigator({
        communityScreen: {
            screen: CommunityScreen,
            navigationOptions: {
                headerShown: false,
            },
        },
        postScreen: {
            screen: PostScreen,
            navigationOptions: {
                headerShown: false,
                ...TransitionPresets.ModalSlideFromBottomIOS,
            },
        },
        readPostScreen: {
            screen: PostScreen,
            navigationOptions: {
                headerShown: false,
                ...TransitionPresets.SlideFromRightIOS,
            },
        },
        searchScreen: {
            screen: SearchScreen,
            navigationOptions: {
                headerShown: false,
                ...TransitionPresets.SlideFromRightIOS,
            },
        },
        editor: {
            screen: Editor,
            navigationOptions: {
                headerShown: false,
            },
        },
        editPost: {
            screen: EditPost,
            navigationOptions: {
                headerShown: false,
            },
        },
        loginScreen: {
            screen: LoginScreen,
            navigationOptions: {
                headerShown: true
            },
        }
    },
    {
        defaultNavigationOptions: {
             ...TransitionPresets.SlideFromRightIOS,
        },
    },
);
CommunityStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.routes.length > 0) {
        navigation.state.routes.map(route => {
            if (route.routeName === "postScreen") {
                tabBarVisible = false;
            } else if (route.routeName === "readPostScreen") {
                tabBarVisible = true;
            } else if (route.routeName === "editPost") {
                tabBarVisible = false;
            } else if (route.routeName === "searchScreen") {
                tabBarVisible = false;
            } else if (route.routeName === "loginScreen") {
                tabBarVisible = false; 
            } else {
                tabBarVisible = true;
            }
        });
    }

    return {
        tabBarVisible,
    };
};
export {
    CommunityStack,
};
