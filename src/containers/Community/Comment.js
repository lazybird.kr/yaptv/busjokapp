import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TextInput, TouchableOpacity,
    Text,
    Dimensions, Alert, Image, Keyboard,
} from 'react-native';

import Anonymous from './Anonymous';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import ImagePicker from 'react-native-image-picker';
import config from '../../config';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {StyledTextInput, StyledText} from '../../components/StyledComponents';
import {PopupDialog} from '../../components/PopupDialog';

const COMMENT_HEIGHT = config.value.textInputHeight;

export default class Comment extends Component {

    constructor(props) {
        super(props);
        this._refComment = React.createRef();
        this._refAnonymous = React.createRef();
        this.touch = {
            isTouched: false,
        };
        this.uploadCount = 0;
        this.state = {
            commentImage: null,
            commentLayout: {
                height: 0,
                width: 0,
            },
            commentTextInput: {},
            nickname: '',
            password: '',
            hideAnonymous: true,
            prepareMention: false,
            focusNickname: false,
            heightComment: {},
            showDialogNeedContent: false,
            showDialogNeeNickname: false,
            showDialogNeedPassword: false,
        };
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.addListener('keyboardDidHide', this.onKeyBoardHide);
        this.setState({
            commentTextInput: {
                type: 'TextInput',
                placeholder: '댓글을 남겨보세요.',
                text: '',
            },
        });
        const {setCommentRef} = this.props;
        setCommentRef(this);
    }

    componentWillUnmount() {
        Keyboard.removeListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.removeListener('keyboardDidHide', this.onKeyBoardHide);
    }

    onKeyBoard = (e) => {
        this.setState({
            hideAnonymous: false,
        });
    };

    onKeyBoardHide = (e) => {
        this.setState({
            hideAnonymous: true,
        });
        if (TextInput.State.currentlyFocusedInput()) {
            TextInput.State.currentlyFocusedInput().blur();
        }
    };

    async onPressSubmit(that) {
        // console.log('onPressSubmit', 'nickname', that.state.nickname, 'password', that.state.password, 'comment', that.state.commentTextInput)

        const {getMember} = this.props;
        const member = getMember();

        if (this.touch.isTouched) {
            return;
        }
        this.touch.isTouched = true;
        const {touch} = this;
        setTimeout(() => {
            touch.isTouched = false;
        }, 1000);
        Keyboard.dismiss();
        let memberId = '';
        let nickname = '';
        let password = '';
        const {_refAnonymous} = this;

        if ((!this.state.commentTextInput.text || (this.state.commentTextInput.text && this.state.commentTextInput.text.length === 0)) && !this.state.commentImage) {
            this.setState({
                showDialogNeedContent: true,
            });
            return;
        }
        if (member) {
            memberId = member.member_id;
            nickname = member.nickname;
        } else {
            if (!that.state.nickname || that.state.nickname.length === 0) {
                this.setState({
                    showDialogNeedNickname: true,
                })
                return;
            }
            if (!that.state.password || that.state.password.length === 0) {
                this.setState({
                    showDialogNeedPassword: true,
                })
                return;
            }
            nickname = this.state.nickname;
            password = this.state.password;
        }
        let imageList = [];

        const {uploadImageToServer, servicePrepareImage} = that.props;
        const {submitComment, onCompletedPublish} = this;
        if (this.state.commentImage) {

            this.uploadCount = 1;
            let commentImage = that.state.commentImage;
            // console.log('commentImage =========>', commentImage);
            imageList.push({
                file_name: commentImage.fileName,
            });
            servicePrepareImage({imageList: imageList}).then(images => {
                    // 업로드 이미지
                    images.map((e) => {
                        commentImage = {
                            ...commentImage,
                            serverUri: e.uri.image,
                            relativeUri: e.uri.relative,
                        }
                        // 동기
                        let file = {
                            uri: commentImage.uri,
                            name: commentImage.fileName,
                            type: commentImage.type,
                        };
                        // console.log('---->', e);
                        uploadImageToServer(e.uri.image, file, that).then(() => {
                            // console.log('Upload ok --> ', file);
                            that.uploadCount--;
                            if (that.uploadCount < 1) {
                                onCompletedPublish()
                            }
                        });
                    });
                    // console.log('#####', commentImage);
                    submitComment(commentImage, memberId, nickname, password);
                },
            );
        } else {
            submitComment(null, memberId, nickname, password);
        }
    };

    prepareMention = () => {
        this._refComment && this._refComment.getInnerRef().focus();
        this.setState({
            prepareMention: true,
        });
    };

    submitComment = (commentImage, memberId, nickname, password) => {
        const {post, mention} = this.props;
        let contentList = [];

        // console.log('submitComment', this.state.prepareMention, mention);

        let isMention = false;
        let parentCommentId = '';
        let commentMention = null;
        if (this.state.prepareMention && mention) {
            isMention = true;
            parentCommentId = mention.comment_id;
            commentMention = mention.written_by;
        }
        if (isMention) {
            let mentionTarget = this.getMentionName();
            contentList.push({
                type: 'text',
                text: this.state.commentTextInput.text.substring(mentionTarget.length + 1),
            });
        } else {
            contentList.push({
                type: 'text',
                text: this.state.commentTextInput.text,
            });
        }
        if (commentImage) {
            contentList.push({
                type: 'image',
                file_name: commentImage.fileName,
                uri: commentImage.relativeUri,
                width: commentImage.width,
                height: commentImage.height,
            });
        }

        this.servicePublishComment({
            memberId: memberId,
            writtenBy: nickname,
            password: password,
            postId: post.post_id,
            contentList: contentList,
            parentCommentId: parentCommentId,
            mention: commentMention,
        });
    };
    onChangeComment = (commentText) => {
        // console.log('onChangeComment', commentText);
        this.setState({
            commentTextInput: {
                ...this.state.commentTextInput,
                text: commentText,
            },
        });

        const mentionTarget = this.getMentionName();
        if (mentionTarget) {
            if (commentText.length < mentionTarget.length + 1) {
                this.setState({
                    prepareMention: false,
                });
            }
        }
    };
    onPressAttachImage = () => {
        const options = {
            quality: 1.0,
            maxWidth: 1920,
            maxHeight: 1080,
            // storageOptions: {
            //     skipBackup: true,
            // },
        };

        ImagePicker.launchImageLibrary(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {


                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // console.log(response);
                this.selectImage(response);
            }
        });
    };
    servicePublishComment = ({
                                 host = config.url.apiSvr,
                                 deviceId = config.deviceInfo.device_id,
                                 sessionId = config.sessionInfo.session_id,
                                 memberId = '',
                                 writtenBy = '',
                                 password = '',
                                 source = 'busjok-mobile-app',
                                 postId = '',
                                 parentCommentId = '',
                                 commentId = '',
                                 contentList = [],
                                 mention = null,
                             } = {},
    ) => {
        const {onCompletedPublish} = this;
        let send_data = {
            category: 'board',
            service: 'PublishComment',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            comment: {
                written_by: {
                    member_id: memberId,
                    name: writtenBy,
                },
                post_id: postId,
                parent_comment_id: parentCommentId,
                comment_id: commentId,
                password: password,
                source: source,
                content_list: contentList,
                mention: mention,
            },
        };
        // console.log('PublishComment', send_data);
        this.setState({
            onRequesting: true,
        });
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    if (this.uploadCount === 0) {
                        onCompletedPublish()
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    onCompletedPublish = () => {
        const {refreshPost, clearMention, post} = this.props;
        refreshPost({postId: post.post_id})
        this.setState({
            commentImage: null,
            commentTextInput: {
                ...this.state.commentTextInput,
                text: '',
            },
            heightComment: {
                height: config.constant.headerHeight,
            },
        });
        this._refComment && this._refComment.getInnerRef().clear();
        clearMention();
    }
    getFileName = (uri) => {
        console.log('getFileName', uri);
        let fileName = uri.split('/').pop();
        return fileName
    };
    selectImage = (data) => {
        let fileName = data.fileName;
        if (!fileName || fileName.length === 0) {
            fileName = this.getFileName(data.uri);
        }
        this.setState({
            commentImage: {
                uri: data.uri,
                width: data.width,
                height: data.height,
                fileName: fileName,
                type: data.type,
            },
        });
    };
    renderCommentImage = () => {
        // console.log('this.state.commentLayout.height', this.state.commentLayout.height);
        if (this.state.commentImage !== null) {
            let iWidth = 128;
            let iHeight = 128;
            if (this.state.commentImage.width > this.state.commentImage.height) {
                iHeight = (this.state.commentImage.height * iWidth) / this.state.commentImage.width;
            } else {
                iWidth = (this.state.commentImage.width * iHeight) / this.state.commentImage.height;
            }
            let layoutHeight = iHeight + 20;
            const commentImageStyle = {
                // flex: 0,
                // position: 'absolute',
                flex: 1,
                width: '100%',
                // margin: '1%',
                height: layoutHeight,
                // bottom: this.state.commentLayout.height,
                // borderRadius: 5,
                justifyContent: 'flex-start',
                // justifyContent: 'center',
                // alignItems: 'center',
                backgroundColor: '#ffffff',
                borderTopWidth: 1,
                borderTopColor: config.colors.lineColor,
            };

            return (
                <View style={commentImageStyle}>
                    <View style={[styles.imageContainer, {width: iWidth, height: iHeight}]}>
                        <Image
                            source={this.state.commentImage}
                            style={styles.commentImageStyle}
                        />
                        <TouchableOpacity
                            style={styles.closeButton}
                            onPress={() => {
                                this.setState({commentImage: null});
                                console.log('onPress');
                            }}>
                            <EvilIcons name={'close'} size={24} color={'#ffffff'}/>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else {
            return null;
        }
    };
    onChangeNickname = (nickname) => {
        this.setState({
            nickname,
        });
    };
    onChangePassword = (password) => {
        this.setState({
            password,
        });
    };
    onFocusCommentTextInput = () => {
        this.setState({
            hideAnonymous: false,
        });
    };
    onBlurCommentTextInput = () => {
    };
    getMentionName = () => {
        const {mention} = this.props;
        let mentionTarget = null;
        if (mention && mention.written_by && mention.written_by.name) {
            mentionTarget = '@' + mention.written_by.name;
        }
        return mentionTarget;
    };
    onSelectionChangeComment = (e) => {
        // const mentionTarget = this.getMentionName();
        // console.log('onSelectionChangeComment', e.nativeEvent);
        // if (mentionTarget) {
        //     if ( mentionTarget.length > e.nativeEvent.selection.start) {
        //         this.setState({
        //             selection: this.state.selection,
        //         });
        //         return
        //     }
        // }

        // this.selection = e.nativeEvent.selection;
    };

    render() {
        const {getMember} = this.props;
        const member = getMember();
        const that = this;
        const mentionTarget = this.getMentionName();
        let adjustHeight = this.state.heightComment;
        let iosStyle = {}
        if (Platform.OS === 'ios') {
            iosStyle = {lineHeight: this.state.heightComment.height}
        }
        return (
            <View style={styles.container}
                  onLayout={e => {
                      this.setState({
                          commentLayout: e.nativeEvent.layout,
                      });
                  }}
            >
                {this.renderCommentImage()}
                <View style={{flex: 1, justifyContent: 'flex-end', backgroundColor: '#ffffff'}}>
                    {member || this.state.hideAnonymous ? null :
                        <View style={styles.anonymous}>
                            <Anonymous
                                onChangeNickname={this.onChangeNickname}
                                onChangePassword={this.onChangePassword}
                                setRef={(ref) => this._refAnonymous = ref}
                                focusNickname={this.state.focusNickname}
                                focusPassword={this.state.focusPassword}
                                resetFocus={() => {
                                    this.setState({
                                        focusNickname: false,
                                        focusPassword: false,
                                    });
                                }}
                                // nickname={this.state.nickname}
                                // password={this.state.password}
                            />
                        </View>
                    }
                    <View style={styles.comment}>
                        <View style={styles.inputView}>
                            <StyledTextInput
                                ref={(ref) => {
                                    this._refComment = ref;
                                }}
                                style={[config.styles.text2, styles.input, adjustHeight]}
                                multiline={true}
                                placeholder={this.state.commentTextInput.placeholder}
                                numberOfLines={1}
                                onChangeText={this.onChangeComment}
                                onFocus={this.onFocusCommentTextInput}
                                onSelectionChange={this.onSelectionChangeComment}
                                onContentSizeChange={(e) => {
                                    console.log('onContentSizeChange', e.nativeEvent.contentSize.height);
                                    let h = e.nativeEvent.contentSize.height;
                                    if (h > COMMENT_HEIGHT) {
                                        h = COMMENT_HEIGHT;
                                    }
                                    this.setState({
                                        heightComment: {
                                            height: h,
                                        },
                                    });
                                }}
                                // value={this.state.commentTextInput.text}
                            >
                                {/* 커서를 막는 꼼수 */}
                                {/*{mentionTarget ? <>*/}
                                {/*    {*/}
                                {/*        this.state.prepareMention ? <>*/}
                                {/*            <Text style={{color: config.colors.baseColor}}>{mentionTarget}</Text>*/}
                                {/*            <Text style={{color: '#000000'}}>{' '}</Text>*/}
                                {/*        </> : <>*/}
                                {/*            <Text style={{color: config.colors.baseColor}}>{mentionTarget}</Text>*/}
                                {/*            <Text style={{color: '#000001'}}>{' '}</Text>*/}
                                {/*        </>*/}
                                {/*    }*/}
                                {/*</> : null}*/}
                                {mentionTarget ? <>
                                    {
                                        this.state.prepareMention ? <>
                                            <Text style={{color: config.colors.baseColor}}>{mentionTarget}</Text>
                                            <Text style={{color: '#000000'}}>{' '}</Text>
                                        </> : null
                                    }
                                </> : null}
                            </StyledTextInput>
                        </View>
                        <View
                            style={styles.attachImage}
                        >
                            <TouchableOpacity
                                onPress={this.onPressAttachImage}>
                                <Image
                                    source={config.images.camera} style={config.styles.icon}
                                />
                            </TouchableOpacity>
                        </View>
                        <View
                            style={styles.submit}
                        >
                            <TouchableOpacity
                                style={[styles.submitButton, {}]}
                                onPress={() => this.onPressSubmit(that)}>
                                <StyledText style={[config.styles.text2, iosStyle, styles.buttonText]}>등록</StyledText>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <PopupDialog
                    visible={this.state.showDialogNeedContent}
                    // title={'비밀번호를 입력하세요'}
                    message={'댓글 내용이 없습니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this._refComment && this._refComment.getInnerRef().focus();
                        this.setState({
                            showDialogNeedContent: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedPassword}
                    // title={'비밀번호를 입력하세요'}
                    message={'비밀 번호를 설정해야 합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        that._refComment && that._refComment.getInnerRef().focus();
                        that.setState({
                            focusPassword: true,
                            showDialogNeedPassword: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedNickname}
                    // title={'비밀번호를 입력하세요'}
                    message={'닉네임을 입력해야 합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this._refComment && this._refComment.getInnerRef().focus();
                        this.setState({
                            focusNickname: true,
                            showDialogNeedNickname: false,
                        });
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // maxHeight: 14 * 5 + 20,
        flexDirection: 'column',
        // backgroundColor: '#eeeeee',
    },
    comment: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        borderTopWidth: config.size._2px,
        borderTopColor: config.colors.lineColor,
    },
    inputView: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    input: {
        flex: 1,
        maxHeight: COMMENT_HEIGHT,
        // justifyContent: 'flex-end',
        textAlign: 'left',
        textAlignVertical: 'center',
        // paddingVertical: -20,
        // lineHeight: 17,
        // fontSize: 14,
        paddingHorizontal: 10,
    },
    buttonText: {
        // fontFamily: 'Roboto',
        // fontSize: 10,
        paddingHorizontal: 0,
        paddingVertical: 0,
    },
    anonymous: {
        flex: 1,
        backgroundColor: '#ffffff',
        borderTopWidth: 1,
        borderTopColor: config.colors.lineColor,
    },
    attachImage: {
        flex: 0,
        // backgroundColor: '#ff0000',
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 5,
        // position: 'absolute',
        // right: 54,
        // bottom: 10,
    },
    submit: {
        flex: 0,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 10,
        // position: 'absolute',
        // right: 5,
        // bottom: 10,
    },
    submitButton: {
        // backgroundColor: '#c0c0c0',
        padding: 6,
        borderRadius: 3,
    },
    commentImageStyle: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        borderRadius: 5,
    },
    commentImageStyle2: {
        width: '100%',
        height: '100%',
        borderRadius: 5,
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        padding: 5,
        borderTopRightRadius: 5,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
    },
    imageContainer: {
        width: '40%',
        height: '80%',
        marginHorizontal: 20,
        marginVertical: 10,
        // backgroundColor: '#00ff00',
    },
});

