import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TextInput, TouchableOpacity,
    Text, ScrollView, KeyboardAvoidingView, Platform, Dimensions, Alert,
    Image,
} from 'react-native';

import Anonymous from './Anonymous';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import ImagePicker from 'react-native-image-picker';
import config from '../../config';
import PopupDialog from '../../components/PopupDialog/PopupDialog';
import ImageResizer from 'react-native-image-resizer';

const ToolbarHeight = 36;

export default class Toolbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isAnonymous: false,
            showDialogNoMoreImage: false,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    loadPicture = () => {
        const options = {
            quality: 1.0,
            maxWidth: 1920,
            maxHeight: 1080,
        };
        const {selectImage} = this.props;

        console.log('SWS-DEBUG-002', config.deviceInfo);
        ImagePicker.launchImageLibrary(options, (response) => {

            console.log('SWS-DEBUG-003', config.deviceInfo);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {


                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // console.log(response);
                const org = response;
                ImageResizer.createResizedImage(org.uri, config.thumbnail.width, config.thumbnail.height, 'JPEG', 100)
                    .then(response => {
                        // response.uri is the URI of the new image that can now be displayed, uploaded...
                        // response.path is the path of the new image
                        // response.name is the name of the new image with the extension
                        // response.size is the size of the new image
                        selectImage(org, response);
                    })
                    .catch(err => {
                        // Oops, something went wrong. Check that the filename is correct and
                        // inspect err to get more details.
                    });
            }
        });
    };

    onPressIcon = (iconName) => {
        // console.log('onPressIcon', iconName);

        const {imageFiles} =this.props;
        if (imageFiles.length >= 10) {
            this.setState({
                showDialogNoMoreImage: true,
            });
            // setTimeout(() => {
            //     Alert.alert(
            //         '이미지를 더 이상 첨부할 수 없습니다.',
            //         '',
            //         [
            //             {
            //                 text: '확인', onPress: () => {
            //
            //                 },
            //             },
            //         ],
            //         {cancelable: false},
            //     );
            // }, 100);
            return
        }
        if (iconName === 'camera') {
            this.loadPicture()
        }
    };
    renderSimpleLineIcons = (iconName) => {
        return (
            <View
                style={styles.icons}
                onTouchEnd={() => this.onPressIcon(iconName)}
            >
                <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => {console.log('onPress')}}>
                    <Image
                        source={config.images.camera} style={config.styles.icon}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    render() {
        const {imageFiles} =this.props;
        let attachedCount =(
            <Text style={[config.styles.text, {lineHeight: config.size._120px, fontSize: 10, paddingHorizontal: 0}]}>
                {imageFiles.length}/10
            </Text>
        );
        return (
            <View style={styles.container}>
                <ScrollView style={styles.toolbar} horizontal={true} showsHorizontalScrollIndicator={false}>
                    {this.renderSimpleLineIcons('camera')}
                    {attachedCount}
                    {/*{this.renderSimpleLineIcons('social-youtube')}*/}
                </ScrollView>
                <PopupDialog
                    visible={this.state.showDialogNoMoreImage}
                    // title={'비밀번호를 입력하세요'}
                    message={'이미지를 더 이상 첨부할 수 없습니다.'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogNoMoreImage: false,
                        });
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: ToolbarHeight,
        // flexDirection: 'column',
        backgroundColor: '#ffffff',
        borderTopWidth: config.size._2px,
        borderTopColor: config.colors.lineColor,
    },
    toolbar: {
        flex: 1,
        flexDirection: 'row',
    },
    icons: {
        flex: 1,
        padding: 5,
        justifyContent: 'center',
    },
});
