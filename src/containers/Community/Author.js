import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
} from 'react-native';
import config from 'src/config';
import {StyledText} from '../../components/StyledComponents';

export default class Author extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render () {
        const {post, hideBar, customStyle} = this.props;
        let hitCount = 0;
        if (post.hit_count) {
            hitCount = post.hit_count;
        }
        let recommendationCount = 0;
        if (post.recommendation_count) {
            recommendationCount = post.recommendation_count;
        }
        return (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <StyledText style={[styles.textWrittenBy, customStyle]}>{post.written_by.name}</StyledText>
                <StyledText style={[styles.textWrittenBy, customStyle]}>{post.published_at}</StyledText>
                {hideBar ? null :
                    <StyledText style={[styles.textWrittenBy, customStyle]}>|</StyledText>
                }
                <StyledText style={[styles.textWrittenBy, customStyle]}>조회수 {hitCount}</StyledText>
                {hideBar ? null :
                    <StyledText style={[styles.textWrittenBy, customStyle]}>|</StyledText>
                }
                <StyledText style={[styles.textWrittenBy, customStyle]}>좋아요 {recommendationCount}</StyledText>
            </View>
        );
        // return (
        //     <View style={{flex: 1, flexDirection: 'row'}}>
        //         <Text style={styles.textWrittenBy}>{post.written_by.name}</Text>
        //         <Text style={styles.textWrittenBy}>{post.published_at}</Text>
        //         <View style={styles.icon}>
        //             <SimpleLineIcons name={'eye'}/>
        //         </View>
        //         <Text style={styles.textWrittenBy}>{hitCount}</Text>
        //         <View style={styles.icon}>
        //             <Entypo name={'heart'} color={'#f2bbc5'}/>
        //         </View>
        //
        //         <Text style={styles.textWrittenBy}>{recommendationCount}</Text>
        //     </View>
        // );
    };
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        // borderWidth: 1,
    },
    titleLayout: {
        flex: 0.9,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 15,
    },
    thumbnailLayout: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        // backgroundColor: 'rgba(0, 0, 0, 0.3)'
    },
    thumbnailStyle: {
        width: '100%',
        height: '100%',
        // borderRadius: 10,
        // resizeMode: 'contain',
    },
    thumbnailTextView: {
        position: 'absolute',
        top: 5,
        right: 5,
        width: 14,
        height: 14,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    textWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    textLayout: {
        flex: 0.1,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        // backgroundColor: 'rgba(0, 0, 0, 0.1)'
    },
    group: {
        // flex: 0.2,
        marginRight: 15,
        // backgroundColor: 'rgba(255, 0, 0, 0.5)'

    },
    title: {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'rgba(255, 0, 0, 0.5)'

    },
    writtenBy: {
        flex: 0.5,
        marginTop: 5,
        // justifyContent: 'center',
        // backgroundColor: 'rgba(255, 0, 0, 0.1)'
    },
    textWrittenBy: {
        textAlign: 'center',
        textAlignVertical: 'center',
        paddingRight: 5,
        fontSize: 9,
        color: 'rgba(130, 124, 130, 1)',
        // backgroundColor: '#0000ff',
        fontFamily: config.defaultFontFamily,
    },
    commentLayout: {
        flex: 0.1,
        justifyContent: 'flex-end',
        borderRadius: 10,
        margin: 5,
        backgroundColor: 'rgb(239,242,239)',
    },
    text: {
        flex: 1,
        textAlign: 'left',
        textAlignVertical: 'center',
        fontSize: 24,
    },
    padding: {
        width: 10,
    },
    icon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 2,
        // backgroundColor: '#00ff00',
    },
});
