import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Dimensions,
    SafeAreaView,
    Alert,
    BackHandler,
    KeyboardAvoidingView,
    Platform,
    Keyboard,
    Appearance,
    ScrollView,
    PanResponder,
    FlatList,
    RefreshControl,
    Image,
    Share,
} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import DraggableFlatList from 'react-native-draggable-flatlist/lib/index';
import Modal from 'react-native-modal';
// import Dialog from "react-native-dialog";
import DialogInput from 'react-native-dialog-input';
import Clipboard from '@react-native-community/clipboard';
//import Share2 from 'react-native-share';

// import Image from 'react-native-scalable-image';
import {actions, defaultActions, RichEditor, RichToolbar} from 'react-native-pell-rich-editor';
import Comment from './Comment';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Anonymous from './Anonymous';
import Octicons from 'react-native-vector-icons/Octicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Toolbar from './Toolbar';
import KeyboardAccessory from 'react-native-sticky-keyboard-accessory';
import config from '../../config';
import Author from './Author';
import ListItem from './ListItem';
import axios from 'axios';
import {PopupDialog} from '../../components/PopupDialog';
import {StyledTextInput, StyledText} from '../../components/StyledComponents';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import {isIphoneX} from 'src/utils/'

// const exampleData = [...Array(1)].map((d, index) => ({
//     key: `item-${index}`, // For example only -- don't use index as your key!
//     label: index,
//     backgroundColor: `rgb(${Math.floor(Math.random() * 255)}, ${index *
//     5}, ${132})`,
//     // backgroundColor: '#0000FF',
// }));

const HeaderHeight = config.constant.headerHeight;
const {call, onChange} = Animated;
const {timing} = Animated;
const warningForPolicy = `
    *업로드하는 사진/동영상의 저작권 문제 시 책임은 작성자에게 있으며, 부적절한 내용, 이미지는 삭제될 수 있습니다.
`;
const POST_TEXT_HEIGHT = config.value.textInputHeight;

class Post extends Component {

    constructor(props) {
        super(props);
        this.hideTail = true;
        this.lastTail = HeaderHeight;
        this.lastTail2 = 0;
        this.lastScrollOffset = 0;
        this.tailY = new Animated.Value(0);
        this.toolbarY = new Animated.Value(0);
        this._draggableRef = React.createRef();
        this.lastFocusedTextInput = {};
        this.dataKeyIndex = 0;
        this.state = {
            title: '',
            nickname: '',
            password: '',
            data: [],
            scrollY: new Animated.Value(0),
            tailValue: new Animated.Value(0),
            containerLayout: {
                height: 0,
                width: 0,
            },
            invalidated: 0,
            draggableEnabled: true,
            testFlex: 1,
            imagesAttachedInPost: [],
            refreshingPost: false,
            mention: null,
            visibleDialogDeleteComment: false,
            visibleDialogDeleteMemberComment: false,
            visibleDialogDeleteAttachedImage: false,
            showDialogPasswordIncorrect: false,
            showDialogCantMine: false,
            showDialogNeedLogin: false,
            showDialogNeedNickname: false,
            showDialogNeedPassword: false,
            showDialogAlreadyLike: false,
            adjustCommentStyle: {},
            passwordForDelete: '',
            keyboardHeight: 0,
            commentWillBeDeleted: null,
            itemWillBeRemoved: null,
            heightPostTextInput: 100,
            hideGrabber: false,
            multiLine: false,
            focusNickname: false,
            focusPassword: false,
        };
        this._refDialogCommentPassword = React.createRef();
        this.scrollContentOffset = null;
        this.scrollView = React.createRef();
        this._refDeletePassword = React.createRef();
        this.lastImageAttachedInPost = {};
        this.lastTouchEvent = null;
        this._refCommentComponent = null;
        this.uploadCount = 0;
        this._passFocusLastTextInput = false;
        this.touch = {
            isSubmit: false,
        }
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.addListener('keyboardDidHide', this.onKeyBoardHide);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

        this.initData();
        const {setRef} = this.props;
        if (setRef) {
            setRef(this);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot){
        const that = this;

        if (prevProps.post !== this.props.post) {
            this.initData();
        }
        const {touch} = this;
        console.log('isSubmit', touch.isSubmit)
        if (prevProps.doSubmitPost !== this.props.doSubmitPost && !touch.isSubmit) {
            touch.isSubmit = true;
            (async () => {
                await this.onPressSubmit(that);
            })();
            if (Platform.OS === 'ios') {
                setTimeout(() => {
                    touch.isSubmit = false
                }, 10000)
            } else {
                setTimeout(() => {
                    touch.isSubmit = false
                }, 3000)
            }
        }
        if (prevProps.doEditPost !== this.props.doEditPost) {
            (async () => {
                await this.onPressSubmit(that, true);
            })();
        }
        if (prevProps.post && this.props.post && prevProps.post.post_id !== this.props.post.post_id) {
            this.serviceGetPost({
                postId: this.props.post.post_id,
                memberId: config.member.member_id,
            });
        }
    }

    initData = () => {
        let initData = [
            {
                key: 'item-text-0',
                ref: React.createRef(),
                type: 'TextInput',
                placeholder: '내용을 작성하세요.',
            },
        ];
        const {post} = this.props;

        if (post && post.title && post.title.length > 0) {
            // console.log('----', post.content_list);
            this.onChangeTitle(post.title);
            initData = this.convContentToData(post.content_list);
        }
        if (initData.length > 0) {
            this.lastFocusedTextInput = {
                key: initData[0].key,
            };
        }
        console.log('SWS-DEBUG-999', initData);
        this.setState({
            data: initData,
        });
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        Keyboard.removeListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.removeListener('keyboardDidHide', this.onKeyBoardHide);
    }

    convContentToData = (contentList) => {
        let data = [];
        let imagesAttachedInPost = [];
        let adj = 0;
        if (!contentList || contentList.length === 0 || (contentList.length > 0 && contentList[0].type === 'image')) {
            data.push({
                key: 'item-text-0',
                ref: React.createRef(),
                type: 'TextInput',
                text: '',
            });
            adj = 1;
        }
        if (contentList) {
            contentList.map((e, i) => {
                this.dataKeyIndex += 1;
                if (e.type === 'text') {
                    data.push({
                        key: 'item-text-' + (i + adj),
                        ref: React.createRef(),
                        type: 'TextInput',
                        text: e.text,
                    });
                } else if (e.type === 'image') {
                    // console.log('------>', e);
                    data.push({
                        key: 'item-image-' + (i + adj),
                        type: 'Image',
                        status: 'pass',
                        imageSrc: e,
                    });

                    imagesAttachedInPost.push({
                        uri: e.uri,
                    });
                }
            });
        }
        if (imagesAttachedInPost.length > 0) {
            this.setState({
                imagesAttachedInPost: imagesAttachedInPost,
            });
        }
        return data;
    };
    onKeyBoard = (e) => {
        // this.setState({
        //     draggableEnabled: false,
        // })
        // console.log('DEBUG--XX');
        // console.log('onKeyBoard', TextInput.State.currentlyFocusedInput());

        this.setState({
            keyboardHeight: e.endCoordinates.height,
        });
    };
    onKeyBoardHide = (e) => {
        // console.log('DEBUG--YY');
        // if (this.lastFocusedTextInput) {
        //     console.log('onKeyBoardHide - 2');
        //     this.lastFocusedTextInput.blur();
        //     this.lastFocusedTextInput.focus();
        // }
        if (TextInput.State.currentlyFocusedInput()) {
            TextInput.State.currentlyFocusedInput().blur();
        }
        this.setState({
            keyboardHeight: 0,
        });
    };
    setCommentRef = (ref) => {
        this._refCommentComponent = ref;
    };
    handleBackButtonClick = (e) => {
        const {onPressGoBack} = this.props;
        onPressGoBack();
        return true;
    };
    serviceDeleteComment = ({
                                host = config.url.apiSvr,
                                deviceId = config.deviceInfo.device_id,
                                sessionId = config.sessionInfo.session_id,
                                postId = '',
                                commentId = '',
                                writtenBy = '',
                                memberId = '',
                                password = '',
                            } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'DeleteComment',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            comment: {
                post_id: postId,
                comment_id: commentId,
                written_by: {
                    member_id: memberId,
                    name: writtenBy,
                },
                password: password,
            },
        };
        const {setRequesting} = this.props;
        setRequesting(true);
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                setRequesting(false);
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    this.serviceGetPost({
                        postId: json.data.comment.post_id,
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceLikeComment = ({
                              host = config.url.apiSvr,
                              deviceId = config.deviceInfo.device_id,
                              sessionId = config.sessionInfo.session_id,
                              postId = '',
                              commentId = '',
                              memberId = '',
                          } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'LikeComment',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
            },
            comment: {
                comment_id: commentId,
            },
            member: {
                member_id: memberId,
            },
        };
        const {setRequesting} = this.props;
        setRequesting(true);
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                setRequesting(false);
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    // setTimeout(() => {
                    //     Alert.alert(
                    //         '',
                    //         '이미 좋아했어요',
                    //         [
                    //             {
                    //                 text: '확인', onPress: () => {
                    //
                    //                 },
                    //             },
                    //         ],
                    //         {cancelable: false},
                    //     );
                    // }, 100);
                } else {
                    console.log('service 성공', send_data.service, json);
                    this.serviceGetPost({
                        postId: json.data.post.post_id,
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceGetPost = ({
                          host = config.url.apiSvr,
                          deviceId = config.deviceInfo.device_id,
                          sessionId = config.sessionInfo.session_id,
                          postId = '',
                      } = {},
    ) => {
        const {setPost} = this.props;
        const member = this.getMember();
        let memberId = '';
        if (member) {
            memberId = member.member_id;
        }
        if (postId.length ===0) {
            const {post} = this.props;
            postId = post.post_id;
        }
        let send_data = {
            category: 'board',
            service: 'GetPost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            member: {
                member_id: memberId,
            },
            post: {
                post_id: postId,
            },
        };
        const {setRequesting} = this.props;
        setRequesting(true);
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                setRequesting(false);
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    if (json.data) {
                        this.setState({
                            post: json.data.post,
                        });
                        if (setPost) {
                            setPost(json.data.post);
                        }
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceLikePost = ({
                           host = config.url.apiSvr,
                           deviceId = config.deviceInfo.device_id,
                           sessionId = config.sessionInfo.session_id,
                           postId = '',
                           memberId = '',
                       } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'LikePost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
            },
            member: {
                member_id: memberId,
            },
        };
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                if (!json) {
                    return;
                }
                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    // setTimeout(() => {
                    //     Alert.alert(
                    //         '',
                    //         '이미 좋아했어요',
                    //         [
                    //             {
                    //                 text: '확인', onPress: () => {
                    //
                    //                 },
                    //             },
                    //         ],
                    //         {cancelable: false},
                    //     );
                    // }, 100);
                } else {
                    console.log('service 성공', send_data.service, json);
                    this.serviceGetPost({
                        postId: json.data.post.post_id,
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceSharePost = ({
                           host = config.url.apiSvr,
                           deviceId = config.deviceInfo.device_id,
                           sessionId = config.sessionInfo.session_id,
                           postId = '',
                           memberId = '',
                       } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'SharePost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
            },
            member: {
                member_id: memberId,
            },
        };
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                if (!json) {
                    return;
                }
                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                } else {
                    console.log('service 성공', send_data.service, json);
                    this.serviceGetPost({
                        postId: json.data.post.post_id,
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    onCompletedSubmitPost = () => {
        const {setReadOnly} = this.props;
        const {touch} = this;
        setTimeout(() => {
            touch.isSubmit = false
        }, 1000);
        setReadOnly()
    };
    servicePublishPost = ({
                              host = config.url.apiSvr,
                              deviceId = config.deviceInfo.device_id,
                              sessionId = config.sessionInfo.session_id,
                              groupName = this.props.groupName,
                              title = '',
                              writtenBy = '',
                              memberId = '',
                              password = '',
                              source = 'busjok-mobile-app',
                              contentList = [],
                              postId = '',
                              uploadCount = 0,
                          } = {},
    ) => {
        const {onCompletedSubmitPost} = this;
        if (groupName === '좋아요') {
            groupName = 'default'
        }
        let send_data = {
            category: 'board',
            service: 'PublishPost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
                written_by: {
                    member_id: memberId,
                    name: writtenBy,
                },
                password: password,
                title: title,
                source: source,
                content_list: contentList,
                group_name: groupName,
            },
        };
        console.log('******** ', config.deviceInfo, send_data)
        const {setRequesting, disableInquiry} = this.props;
        setRequesting(true);
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                setRequesting(false);
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    if (json.err.includes('비밀번호가 일치하지 않습니다')) {
                        this.setState({
                            showDialogPasswordIncorrect: true,
                        })
                    }
                    this.touch.isSubmit = false
                } else {
                    console.log('service 성공', send_data.service, json);

                    if (!disableInquiry) {
                        this.serviceGetPost({
                            postId: json.data.post.post_id,
                        });
                    }
                    if (uploadCount === 0) {
                        onCompletedSubmitPost();
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    async uploadImageToServer(fileServerPath, file, that) {
        let formData = new FormData();
        const {serviceGetPost} = that;
        formData.append('file', file);
        console.log('------------------ uploadImageToServer start', config.deviceInfo);
        return new Promise(async function (resolve, reject) {
            try {
                const response = await axios.post(
                    fileServerPath,
                    formData,
                    {timeout: 20000});
                console.log('status code=', response.status);
                if (response.status === 200) {
                    resolve(response.data);
                    serviceGetPost({
                        postId: that.state.post.post_id,
                    });
                } else {
                    console.log('uploadImageToServer error!!', response, fileServerPath, file);
                    throw new Error('uploadImageToServer error!!');
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    async servicePrepareImage({
                                  host = config.url.apiSvr,
                                  deviceId = config.deviceInfo.device_id,
                                  sessionId = config.sessionInfo.session_id,
                                  imageList = [],
                              } = {},
    ) {

        let rList = [];
        // console.log('imageList', imageList);
        let sendData = {
            'data': {
                'device': {
                    'device_id': deviceId,
                    'session_id': sessionId,
                },
                'image_list': imageList,
            },
        };
        console.log('PrepareImage', sendData);
        try {
            const resp = await axios.post(host + '/board/PrepareImage', sendData);
            const {image_list} = resp.data.data;
            // console.log('image_list:', image_list);

            rList = image_list;
        } catch (e) {
            console.log('e: ', e);
        }
        return rList;
    }

    validateContent = (contentList) => {
        let ok = false;
        contentList.map((e) => {
            // 글 내용에 이미지가 있다? 오케이
            if (e.type === 'Image') {
                ok = true;
            } else if (e.type === 'TextInput') {
                // 한 글자라도 썼다면?
                if (e.text && e.text.length > 0) {
                    ok = true;
                }
            }
        });
        if (!ok) {
            setTimeout(() => {
                Alert.alert(
                    '',
                    '글 내용을 입력해주세요',
                    [
                        {
                            text: '확인', onPress: () => {

                            },
                        },
                    ],
                    {cancelable: false},
                );
            }, 100);
        }
        return ok;
    };
    validateTitle = (title) => {
        if (!title || title.length === 0) {
            setTimeout(() => {
                Alert.alert(
                    '',
                    '글 제목을 입력해주세요',
                    [
                        {
                            text: '확인', onPress: () => {

                            },
                        },
                    ],
                    {cancelable: false},
                );
            }, 100);
            return false;
        }
        if (title.length > config.value.maxTitleLength) {
            setTimeout(() => {
                Alert.alert(
                    '',
                    '글 제목이 너무 길어요(최대 ' + config.value.maxTitleLength + '자)',
                    [
                        {
                            text: '확인', onPress: () => {

                            },
                        },
                    ],
                    {cancelable: false},
                );
            }, 100);
            return false;
        }
        return true;
    };
    onChangeTitle = (text) => {
        this.setState({
            title: text,
        });
        const {onChangePost} = this.props;
        if (onChangePost) {
            if (text && text.length > 0) {
                onChangePost(true)
            }
        }
    };
    onChangeNickname = (text) => {
        this.setState({
            nickname: text,
        });
    };
    onChangePassword = (text) => {
        console.log('onChangeNickname', text);
        this.setState({
            password: text,
        });
    };
    getThumbnailFileName = (fileName) => {
        console.log('getThumbnailFileName', fileName);
        let ext = fileName.split('.').pop();
        return fileName.slice(0, fileName.length - ext.length - 1) + '_thumbnail.jpg'
    };
    async onPressSubmit(that, isEdit) {
        Keyboard.dismiss();
        let memberId = '';
        let nickname = '';
        let password = '';
        const {title} = this.state;
        const {post} = this.props;
        const newData = this.state.data;
        let ok = this.validateTitle(title);
        if (!ok) {
            return;
        }
        ok = this.validateContent(newData);
        if (!ok) {
            return;
        }
        let postId = '';
        if (isEdit) {
            if (post.post_id) {
                postId = post.post_id;
            }
            if (post.written_by && (post.written_by.member_id && post.written_by.member_id.length > 0)) {
                memberId = post.written_by.member_id;
                nickname = post.written_by.name;
            } else {
                nickname = this.state.nickname;
                password = this.state.password;
                if (!nickname || nickname.length === 0) {
                    this.setState({
                        showDialogNeedNickname: true,
                    });
                    return
                }
                if (!password || password.length === 0) {
                    this.setState({
                        showDialogNeedPassword: true,
                    });
                    return
                }
            }
        } else {
            if (config.member && config.member.member_id && config.member.member_id.length > 0) {
                memberId = config.member.member_id;
                nickname = config.member.nickname;
            } else {
                if (!this.state.password || this.state.password.length === 0) {
                    setTimeout(() => {
                        Alert.alert(
                            '',
                            '비밀 번호를 설정해야 합니다.',
                            [
                                {
                                    text: '확인', onPress: () => {

                                    },
                                },
                            ],
                            {cancelable: false},
                        );
                    }, 100);
                    return;
                }
                if (!this.state.nickname || this.state.nickname.length === 0) {
                    setTimeout(() => {
                        Alert.alert(
                            '',
                            '닉네임을 입력해야 합니다.',
                            [
                                {
                                    text: '확인', onPress: () => {

                                    },
                                },
                            ],
                            {cancelable: false},
                        );
                    }, 100);
                    return;
                }
                nickname = this.state.nickname;
                password = this.state.password;
            }
        }

        // console.log('=========>', nickname, ', ', password);

        let imageList = [];
        let srcDic = {};

        const {getThumbnailFileName} = this;
        newData.map((e) => {
            // EDIT 이미지는 PASS
            if (e.type === 'Image' && e.status !== 'pass') {
                // console.log('------> DEBUG11]', e.imageSrc);
                imageList.push({
                    file_name: e.imageSrc.fileName,
                });
                srcDic[e.imageSrc.fileName] = e.imageSrc;
                srcDic[e.imageSrc.fileName + 'thumbnail'] = e.thumbnailSrc;
                // let thumbnail = getThumbnailFileName(e.imageSrc.fileName)
                // imageList.push({
                //     file_name: thumbnail,
                // });
                // srcDic[thumbnail] = e.thumbnailSrc;
            }
        });
        const {uploadImageToServer, servicePrepareImage, submitPost} = that;
        const {onCompletedSubmitPost} = this;
        that.uploadCount = imageList.length * 2;
        // console.log('uploadCount', that.uploadCount);
        if (imageList.length > 0) {
            servicePrepareImage({imageList: imageList}).then(images => {
                    // 업로드 이미지
                    images.map((e) => {
                        srcDic[e.file_name] = {
                            ...srcDic[e.file_name],
                            serverUri: e.uri.image,
                            relativeUri: e.uri.relative,
                        }
                        console.log('      ', srcDic[e.file_name]);
                        // 비동기
                        let file = {
                            uri: srcDic[e.file_name].uri,
                            name: e.file_name,
                            type: srcDic[e.file_name].type,
                        };
                        uploadImageToServer(e.uri.image, file, that).then(() => {
                            that.uploadCount--;
                            console.log('Upload ok --> ', that.uploadCount);
                            if (that.uploadCount < 1) {
                                onCompletedSubmitPost();
                            }
                        });
                        // 섬네일
                        file = {
                            uri: srcDic[e.file_name + 'thumbnail'].uri,
                            name: getThumbnailFileName(e.file_name),
                            type: srcDic[e.file_name + 'thumbnail'].type,
                        };
                        uploadImageToServer(getThumbnailFileName(e.uri.image), file, that).then(() => {
                            that.uploadCount--;
                            console.log('Upload ok --> ', that.uploadCount);
                            if (that.uploadCount < 1) {
                                onCompletedSubmitPost();
                            }
                        });
                        console.log('DEBUG1----- ', srcDic[e.file_name]);
                    });
                    console.log('async----- ', srcDic);
                    submitPost(newData, srcDic, postId, memberId, nickname, password, title, that.uploadCount);
                },
            );
        } else {
            submitPost(newData, {}, postId, memberId, nickname, password, title, imageList.length);
        }

        // let html = '';
        // try {
        //     html = await that.richText.current?.getContentHtml();
        // } catch (e) {
        //     console.log(e);
        //     return;
        // }
        // // react-native navigation props
        //
        // // console.log(html);
        //
        // this.servicePublishPost({
        //     title: this.state.title,
        //     writtenBy: this.state.nickname,
        //     password: this.state.password,
        //     contentList: [html],
        // });
        // this.setState({
        //     disabled: true,
        // });
    };

    submitPost = (post, srcDic, postId, memberId, nickname, password, title, uploadCount) => {

        let contentList = [];
        post.map((e) => {
            if (e.type === 'TextInput' && e.text && e.text.length > 0) {
                contentList.push({
                    type: 'text',
                    text: e.text,
                });
            } else if (e.type === 'Image') {
                // console.log('>', postId, e.imageSrc);
                if (e.status === 'pass') {
                    contentList.push({
                        type: 'image',
                        uri: e.imageSrc.relative_uri,
                        width: e.imageSrc.width,
                        height: e.imageSrc.height,
                    });
                } else {
                    contentList.push({
                        type: 'image',
                        file_name: e.imageSrc.fileName,
                        uri: srcDic[e.imageSrc.fileName].relativeUri,
                        width: srcDic[e.imageSrc.fileName].width,
                        height: srcDic[e.imageSrc.fileName].height,
                    });
                }
            }
        });
        this.servicePublishPost({
            title: title,
            memberId: memberId,
            writtenBy: nickname,
            password: password,
            contentList: contentList,
            postId: postId,
            uploadCount: uploadCount,
        });
    };
    //
    // renderPlaceholder = ({item, index}) => {
    //     return (<View style={{
    //         flex: 1,
    //         flexDirection: 'column'}}>
    //         <TextInput
    //             style={{
    //                 // fontWeight: 'bold',
    //                 color: 'black',
    //                 fontSize: 14,
    //                 // width: '100%',
    //                 textAlign: 'left',
    //                 textAlignVertical: 'top',
    //                 flex: 0.9,
    //                 // height: '90%',
    //                 // top: '10%'
    //             }}
    //             placeholder={'Hello'}
    //             multiline={true}
    //         >
    //         </TextInput>
    //         <Text style={{backgroundColor: '#ffff00', flex: 0.1}}>
    //             Drag
    //         </Text>
    //     </View>)
    // };
    onTouchStartView = (e) => {
        this.lastTouchEvent = e.nativeEvent;
    };
    onTouchMoveView = (e) => {
        if (this.scrollContentOffset === null) {
            this.scrollContentOffset = {
                y: 0,
            };
        }
        let diff = this.lastTouchEvent.pageY - e.nativeEvent.pageY;

        // this.scrollView.scrollTo({y: this.scrollContentOffset.y + diff, x: 0, animated: true});
        this.lastTouchEvent = e.nativeEvent;
        this.lastScrollOffset = this.scrollContentOffset.y + diff;
        // this.repeatScroll(this.lastScrollOffset, diff, Math.floor(Math.abs(diff)))
    };
    repeatScroll = (last, diff, repeat) => {
        if (repeat < 0) {
            return;
        }
        this.scrollView.scrollTo({y: last, x: 0, animated: true});
        this.lastScrollOffset = last + diff;
        this.repeatScroll(this.lastScrollOffset, diff, repeat - 1);
    };
    removeImageItem = (item) => {
        this.setState({
            visibleDialogDeleteAttachedImage: true,
            itemWillBeRemoved: item,
            adjustCommentStyle: {bottom: -HeaderHeight * 3},
        });
        // setTimeout(() => {
        //     Alert.alert(
        //         '이미지 첨부를 취소할까요?',
        //         '',
        //         [
        //             {
        //                 text: '아니오', onPress: () => {
        //
        //                 },
        //             },
        //             {
        //                 text: '네', onPress: () => {
        //                     let imagesAttachedInPost = [];
        //                     for (let i = 0; i < this.state.imagesAttachedInPost.length; i++) {
        //                         if (this.state.imagesAttachedInPost[i].uri !== item.imageSrc.uri) {
        //                             imagesAttachedInPost.push(this.state.imagesAttachedInPost[i]);
        //                         }
        //                     }
        //                     let newData = [];
        //                     for (let i = 0; i < this.state.data.length; i++) {
        //                         // console.log(this.state.data[i].key, item.key);
        //                         if (this.state.data[i].key !== item.key) {
        //                             newData.push(this.state.data[i]);
        //                         }
        //                     }
        //
        //                     this.setState({
        //                         data: newData,
        //                         imagesAttachedInPost: imagesAttachedInPost,
        //                     });
        //                 },
        //             },
        //         ],
        //         {cancelable: false},
        //     );
        // }, 100);
    };
    renderItem = ({item, index, drag, isActive}) => {
        console.log('heightPostTextInput', this.state.heightPostTextInput);
        const {onChangePost} = this.props;
        let heightTextInput = 0;
        if ( this.lastFocusedTextInput.key === item.key) {
            heightTextInput = this.state.heightPostTextInput
        }
        if (item.type === 'TextInput') {
            return (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        // height: Dimensions.get('window').height * 0.5,
                        backgroundColor: isActive ? 'rgba(0, 0, 0, 0.1)' : item.backgroundColor,
                        // alignItems: "center",
                        // justifyContent: "center"
                    }}
                >
                    <View style={{
                        // flex: 1,
                        // minHeight: 100,
                    }}
                    >
                        <StyledTextInput
                            style={[config.styles.text, {
                                // fontWeight: 'bold',
                                color: 'black',
                                fontSize: 15,
                                // lineHeight: 1,
                                width: '100%',
                                textAlign: 'left',
                                textAlignVertical: 'top',
                                minHeight: heightTextInput,
                                // flex: 0.9,
                                // minHeight: config.value.textInputHeight,
                            }]}
                            ref={(ref) => {
                                if (ref) {
                                    item.ref = ref;
                                }
                            }}
                            placeholder={isActive ? item.text : item.placeholder}
                            multiline={this.state.multiLine}
                            onChangeText={(e) => {
                                item.text = e;
                                if (onChangePost) {
                                    onChangePost(true)
                                }
                            }}
                            onFocus={() => {
                                this.lastFocusedTextInput = {
                                    textInput: TextInput.State.currentlyFocusedInput(),
                                    key: item.key,
                                };
                                console.log('onFocus', index);
                                this.setState({
                                    testFlex: 0.5,
                                    hideGrabber: true,
                                    heightPostTextInput: 65,
                                    multiLine: true,
                                }, () => {
                                    this.setState({
                                        // multiLine: true,
                                    })
                                });
                            }}
                            onBlur={() => {
                                this.setState({
                                    testFlex: 1,
                                    hideGrabber: false,
                                });
                            }}
                            defaultValue={item.text}
                            onContentSizeChange={(e) => {
                                console.log('onContentSizeChange', e.nativeEvent.contentSize.height);
                                let h = e.nativeEvent.contentSize.height;
                                if (h < 65) {
                                    h = 65;
                                }
                                this.setState({
                                    heightPostTextInput: h,
                                })
                            }}
                        >
                        </StyledTextInput>
                        {this.state.hideGrabber ? null :
                            <View style={{
                                position: 'absolute', right: '0%', height: '100%', width: 48,
                                // top: -10,
                                backgroundColor: 'rgba(0, 0, 255, 0)',
                            }}
                                // onTouchStart={() => {
                                //     if (TextInput.State.currentlyFocusedInput()) {
                                //         TextInput.State.currentlyFocusedInput().blur();
                                //     }
                                // }}
                            >
                                <TouchableOpacity
                                    style={[{
                                        flex: 1, width: '100%',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }]}
                                    onLongPress={drag}>
                                    <Octicons
                                        name={'grabber'} size={24} color={'rgba(0, 0, 0, 0.2)'}
                                    />
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                </TouchableOpacity>
            );
        } else if (item.type === 'Image') {
            return (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        // height: Dimensions.get('window').height * 0.5,
                        backgroundColor: isActive ? 'rgba(0, 0, 0, 0.0)' : item.backgroundColor,
                        // alignItems: "center",
                        // justifyContent: "center"
                        // paddingVertical: 10,
                    }}
                    // onPress={() => {
                    //     setTimeout(() => {
                    //         Alert.alert(
                    //             '이미지 첨부를 취소할까요?',
                    //             '',
                    //             [
                    //                 {
                    //                     text: '아니오', onPress: () => {
                    //
                    //                     },
                    //                 },
                    //                 {
                    //                     text: '네', onPress: () => {
                    //                         let newData = [];
                    //                         for (let i = 0; i < this.state.data.length; i++) {
                    //                             console.log(this.state.data[i].key, item.key);
                    //                             if (this.state.data[i].key !== item.key) {
                    //                                 newData.push(this.state.data[i]);
                    //                             }
                    //                         }
                    //
                    //                         const {onChangePost} = this.props;
                    //                         onChangePost(newData);
                    //                         this.setState({
                    //                             data: newData,
                    //                         });
                    //                     },
                    //                 },
                    //             ],
                    //             {cancelable: false},
                    //         );
                    //     }, 100);
                    // }}
                >
                    <View style={{
                        flex: 1,
                        // flexDirection: 'column',
                        height: (item.imageSrc.height * Dimensions.get('window').width) / item.imageSrc.width,
                        width: Dimensions.get('window').width,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.0)',
                    }}
                    >
                        {/* 중요함: TextInput 을 깔아줘야 드래깅이 됨 */}
                        <StyledTextInput
                            style={{position: 'absolute', width: '100%', height: '100%'}}
                            editable={false}
                        />
                        <Image
                            source={item.imageSrc}
                            style={[styles.postImageStyle]}
                        />
                        <TouchableOpacity
                            style={styles.closeButton}
                            onPress={() => this.removeImageItem(item)}>
                            <EvilIcons name={'close'} size={24} color={'#ffffff'}/>
                        </TouchableOpacity>
                        {/*<View style={{*/}
                        {/*    position: 'absolute', right: '0%', height: '100%', width: 48,*/}
                        {/*    backgroundColor: 'rgba(0, 0, 0, 0)',*/}
                        {/*}}*/}
                        {/*    // onTouchStart={() => {*/}
                        {/*    //     if (TextInput.State.currentlyFocusedInput()) {*/}
                        {/*    //         TextInput.State.currentlyFocusedInput().blur();*/}
                        {/*    //     }*/}
                        {/*    // }}*/}
                        {/*>*/}
                        {/*    <TouchableOpacity*/}
                        {/*        style={[{*/}
                        {/*            flex: 1, width: '100%',*/}
                        {/*            alignItems: 'center',*/}
                        {/*            justifyContent: 'center',*/}
                        {/*        }]}*/}
                        {/*        onLongPress={drag}*/}
                        {/*    >*/}
                        {/*        <Octicons*/}
                        {/*            name={'grabber'} size={24} color={'rgba(0, 0, 0, 0.2)'}*/}
                        {/*        />*/}
                        {/*    </TouchableOpacity>*/}
                        {/*</View>*/}
                        <View style={{
                            position: 'absolute', right: 0, height: 48, width: 48,
                            backgroundColor: 'rgba(0, 0, 0, 0)',
                        }}
                            // onTouchStart={() => {
                            //     if (TextInput.State.currentlyFocusedInput()) {
                            //         TextInput.State.currentlyFocusedInput().blur();
                            //     }
                            // }}
                        >
                            <TouchableOpacity
                                style={[{
                                    flex: 1, width: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }]}
                                onLongPress={drag}>
                                <Octicons
                                    name={'grabber'} size={24} color={'rgba(0, 0, 0, 0.2)'}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableOpacity>
            );
        } else {
            return (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        // height: Dimensions.get('window').height * 0.5,
                        backgroundColor: isActive ? 'rgba(0, 0, 0, 0.1)' : item.backgroundColor,
                        // alignItems: "center",
                        // justifyContent: "center"
                    }}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                    }}>
                        <StyledTextInput
                            style={{
                                // fontWeight: 'bold',
                                color: 'black',
                                fontSize: 14,
                                // width: '100%',
                                textAlign: 'left',
                                textAlignVertical: 'top',
                                flex: 0.9,
                                // height: '90%',
                                // top: '10%'
                            }}
                            placeholder={isActive ? '원하는 위치로 드래그해주세요' : placeholder}
                            multiline={true}
                        >
                        </StyledTextInput>
                        <StyledText style={{backgroundColor: '#ffff00', flex: 0.1}}
                              onLongPress={drag}
                        >
                            Drag
                        </StyledText>
                    </View>
                </TouchableOpacity>
            );
        }
    };
    getMember = () => {
        let member = null;
        if (config.member && config.member.member_id && config.member.member_id.length > 0) {
            member = config.member;
        }
        return member;
    };
    renderHeader = () => {
        const {editMode, post} = this.props;
        const member = this.getMember();

        let titleView = (
            <View style={styles.titleLayout}>
                <StyledTextInput
                    style={[config.styles.text, {fontSize: 16, fontFamily:config.defaultBoldFontFamily}]}
                    placeholder="제목"
                    placeholderTextColor={config.colors.greyColor}
                    maxLength={32}
                    onChangeText={this.onChangeTitle}
                >

                    {editMode ?
                        <StyledText
                            style={[config.styles.text, {fontSize: 16}]}
                            maxLength={32}
                            numberOfLines={1}
                        >
                            {post.title}
                        </StyledText>
                        : null
                    }
                </StyledTextInput>
            </View>
        );
        // const y = this.state.scrollY.interpolate({
        //     inputRange: [0, HeaderHeight],
        //     outputRange: [0, -HeaderHeight],
        //     extrapolateRight: 'clamp',
        // });
        // let anonymousView = (
        //     <Animated.View style={[styles.header,
        //         {transform: [{translateY: y}]}]}>
        //         <Anonymous
        //             onChangeNickname={this.onChangeNickname}
        //             onChangePassword={this.onChangePassword}
        //         />
        //     </Animated.View>
        // );
        let nickname = '';
        if (post && post.written_by) {
            nickname = post.written_by.name;
        }
        let anonymousView = (
            <View style={[styles.header]}>
                <Anonymous
                    onChangeNickname={this.onChangeNickname}
                    onChangePassword={this.onChangePassword}
                    editMode={editMode}
                    nickname={nickname}
                    focusNickname={this.state.focusNickname}
                    focusPassword={this.state.focusPassword}
                    resetFocus={() => {
                        this.setState({
                            focusNickname: false,
                            focusPassword: false,
                        });
                    }}
                />
            </View>
        );
        if (editMode) {
            if (post && post.written_by) {
                if (post.written_by.member_id && post.written_by.member_id.length > 0) {
                    anonymousView = null;
                }
            }
        } else {
            if (member) {
                anonymousView = null;
            }
        }

        return (
            <View>
                {anonymousView}
                {titleView}
            </View>
        );
    };
    onPressAttachImage = () => {

    };
    // renderComment = () => {
    //     const {readOnly, refreshPost} = this.props;
    //     if (!readOnly) {
    //         return null;
    //     }
    //     return (
    //         <View
    //             style={styles.comment}
    //             onLayout={e => {
    //                 this.setState({
    //                     commentLayout: e.nativeEvent.layout,
    //                 });
    //             }}
    //         >
    //             <Comment
    //                 selectImage={this.selectImage}
    //                 refreshPost={refreshPost}
    //             />
    //         </View>
    //     );
    // };
    onScrollOffsetChange = (offset) => {
        // console.log('onScrollOffsetChange', offset);
        // const {keyboardHeight} = this.props;
        // if (keyboardHeight === 0) {
        //     if (this.lastFocusedTextInput) {
        //         this.lastFocusedTextInput.focus();
        //     }
        // }
        // Animated.event(
        //     [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
        //     {useNativeDriver: false},
        // );
    };
    onScroll = ([scrollOffset]) => {
        // console.log('onScroll', this.lastScrollOffset, scrollOffset);
        // console.log('onScroll', scrollOffset);
        // Animated 메카니즘 이해 필요. scrollY 값이 갱신되서 자동으로 업데이트가 되어야 하는데
        // 그게 안됨. setState 한번 불러주삼

        if (this.lastScrollOffset >= scrollOffset && !this.hideTail) {

            // console.log('DEBUG 2');
            timing(this.tailY, {
                duration: 250,
                toValue: HeaderHeight,
                easing: Easing.out(Easing.ease),
            }).start();

            this.hideTail = true;
            // this.lastTail -= this.lastScrollOffset - scrollOffset;
            // this.tailY.setValue(this.lastTail);
            // this.lastTail2 = this.lastTail;
            // if (this.lastTail2 < 0) {
            //     this.lastTail2 = 0;
            // }
        } else if (this.lastScrollOffset < scrollOffset && this.hideTail) {
            // console.log('DEBUG 1');
            timing(this.tailY, {
                duration: 250,
                toValue: -HeaderHeight,
                easing: Easing.out(Easing.ease),
            }).start();
            this.hideTail = false;
            // this.lastTail2 += scrollOffset - this.lastScrollOffset;
            // this.tailY.setValue(this.lastTail2);
            // this.lastTail = this.lastTail2;
            // if (this.lastTail > HeaderHeight) {
            //     this.lastTail = HeaderHeight;
            // }
        }
        this.lastScrollOffset = scrollOffset;

        this.state.scrollY.setValue(scrollOffset);
        this.setState({
            // scrollY: new Animated.Value(scrollOffset),
        });
        // Animated.event(
        //     [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
        //     {useNativeDriver: true},
        // )
    };
    getFileName = (uri) => {
        console.log('getFileName', uri);
        let fileName = uri.split('/').pop();
        return fileName
    };
    selectPostImage = (data, thumbnail) => {
        let fileName = data.fileName;
        if (!fileName || fileName.length === 0) {
            fileName = this.getFileName(data.uri);
        }
        const postAttachedImage = {
            uri: data.uri,
            width: data.width,
            height: data.height,
            fileName: fileName,
            type: data.type,
        };
        // console.log(thumbnail);
        const postAttachedImageThumbnail = {
            uri: thumbnail.uri,
            width: thumbnail.width,
            height: thumbnail.height,
            fileName: thumbnail.name,
            type: 'image/jpeg',
        };
        let imagesAttachedInPost = this.state.imagesAttachedInPost;
        imagesAttachedInPost.push(postAttachedImage);

        let newData = [];
        let found = false;
        for (let i = 0; i < this.state.data.length; i++) {
            // console.log(this.state.data[i].key);
            newData.push(this.state.data[i]);
            if (this.state.data[i].key === this.lastFocusedTextInput.key && !found) {
                found = true;
                this.dataKeyIndex += 1;
                newData.push({
                    key: 'item-image-' + this.dataKeyIndex,
                    type: 'Image',
                    imageSrc: postAttachedImage,
                    thumbnailSrc: postAttachedImageThumbnail,
                });
                this.dataKeyIndex += 1;
                newData.push({
                    key: 'item-text-' + this.dataKeyIndex,
                    ref: React.createRef(),
                    type: 'TextInput',
                });
            }
        }
        this.setState({
            lastImageAttachedInPost: postAttachedImage,
            imagesAttachedInPost: imagesAttachedInPost,
            data: newData,
            invalidated: this.state.invalid + 1,
        });
        const {onChangePost} = this.props;
        if (onChangePost) {
            onChangePost(true)
        }
    };
    onDraggableFlatListDragEnd = (data) => {
        // console.log('onDraggableFlatListDragEnd', data, data.length);
        // let newData = [];
        // let newIndex = -1;
        // for (let i = 0; i < data.length; i++) {
        //     let needDelete = false;
        //     if ( newIndex > -1 ) {
        //         if ( newIndex > -1 ) {
        //             if (newData[newIndex].type == 'TextInput' && data[i].type == 'TextInput') {
        //                 if (data[i].text == null || data[i].text.length === 0) {
        //                     needDelete = true;
        //                 }
        //             }
        //         }
        //     }
        //     if ( !needDelete ) {
        //         newData.push(data[i]);
        //         newIndex = newData.length - 1;
        //     }
        //     // let needMerge = false;
        //     // if ( newIndex > -1 ) {
        //     //     if (newData[newIndex].type == 'TextInput' && data[i].type == 'TextInput') {
        //     //         needMerge = true;
        //     //     }
        //     // }
        //     // if (needMerge) {
        //     //     if (newData[newIndex].text) {
        //     //         if (data[i].text) {
        //     //             console.log('DEBUG1', newData[newIndex]);
        //     //             newData[newIndex].text = newData[newIndex].text.concat('\n', data[i].text);
        //     //             // newData[newIndex].ref.value = newData[newIndex].text;
        //     //         }
        //     //     } else {
        //     //         newData[newIndex].text = data[i].text;
        //     //     }
        //     // } else {
        //     //     newData.push(data[i]);
        //     //     newIndex = newData.length - 1;
        //     // }
        // }
        // console.log(newData);

        this.setState({
            data: data,
        });
    };
    onScroll2 = (e) => {
        this.scrollContentOffset = e.nativeEvent.contentOffset;
        this.onScroll([e.nativeEvent.contentOffset.y]);
    };
    renderTitle = () => {
        const {groupName, post} = this.props;
        // console.log(post);
        if (!post || !post.title) {
            return null;
        }
        let hitCount = 0;
        if (post.hit_count) {
            hitCount = post.hit_count;
        }
        let recommendationCount = 0;
        if (post.recommendation_count) {
            recommendationCount = post.recommendation_count;
        }

        let titleAdjusted = post.title;
        let maxLen = 48;
        if (titleAdjusted.length > maxLen) {
            titleAdjusted = titleAdjusted.substr(0, maxLen) + '.....';
        }
        let currentGroupName = groupName;
        if (currentGroupName === 'default') {
            currentGroupName = '전체';
        }
        const {onPressGoBack} = this.props;
        return (
            <View style={styles.titleContainer}>
                <View style={{height: 24, flexDirection: 'row'}}>
                    <TouchableOpacity
                        style={[config.styles.button, {flex: 0}]}
                        onPress={() => onPressGoBack({groupName: 'default', newPost: this.state.data})}>
                        <StyledText style={[config.styles.text, styles.prevText]}>
                            게시판
                        </StyledText>
                    </TouchableOpacity>
                    <StyledText
                        style={[config.styles.text, styles.prevText, {flex: 0, paddingHorizontal: 0, paddingRight: 0}]}>
                    </StyledText>
                    <TouchableOpacity
                        style={[config.styles.button, {flex: 0}]}
                        onPress={() => onPressGoBack({groupName: groupName, newPost: this.state.data})}>
                        <StyledText style={[config.styles.text, styles.prevText, {paddingLeft: 5}]}>
                            {currentGroupName}
                        </StyledText>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1}}>
                    <StyledText style={[config.styles.text, {fontSize: 16, fontFamily:config.defaultBoldFontFamily, paddingVertical: 5}]}>
                        {titleAdjusted}
                    </StyledText>
                </View>
                <View style={{height: 24, paddingHorizontal: 10, flexDirection: 'row'}}>
                    <Author
                        post={post}
                        hideBar={true}
                        customStyle={{fontSize: 9}}
                    />
                </View>

            </View>
        );

    };
    formatCommentAt = (at) => {
        return '';
    };
    renderDialogDeleteComment = () => {
        const comment = this.state.commentWillBeDeleted;
        const {post} = this.props;
        const {serviceDeleteComment} = this;
        const {_refDeletePassword} = this;
        console.log('renderDialogDeleteComment', comment);
        return (
            <PopupDialog
                visible={this.state.visibleDialogDeleteComment}
                // title={'비밀번호를 입력하세요'}
                message={'게시물을 삭제하시겠습니까?'}
                leftButtonLabel={'취소'}
                leftButtonStyle={{borderWidth: 1, borderColor: '#9e9e9e'}}
                leftButtonTextStyle={{color: '#9e9e9e'}}
                onLeftButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteComment: false,
                        adjustCommentStyle: {},
                    });
                }}
                rightButtonLabel={'삭제'}
                rightButtonStyle={{backgroundColor: config.colors.baseColor}}
                rightButtonTextStyle={{color: '#ffffff'}}
                onRightButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteComment: false,
                        adjustCommentStyle: {},
                    });
                    serviceDeleteComment({
                        postId: post.post_id,
                        commentId: comment.comment_id,
                        writtenBy: comment.written_by.name,
                        memberId: comment.written_by.member_id,
                        password: this.state.passwordForDelete,
                    });
                }}
                inputTitle={'비밀번호 입력'}
                onChangeText={(text) => {
                    this.setState({
                        passwordForDelete: text,
                    });
                }}
                onBackdropPress={() =>
                    this.setState({
                        visibleDialogDeleteComment: false,
                        adjustCommentStyle: {},
                    })}
                onBackButtonPress={() =>
                    this.setState({
                        visibleDialogDeleteComment: false,
                        adjustCommentStyle: {},
                    })}
                secureTextEntry={true}
            />
        );
        // return (
        //     <Modal
        //         isVisible={this.state.visibleDialogDeleteComment}
        //         animationType={'none'}
        //         backdropOpacity={0.2}
        //         onBackdropPress={() =>
        //             this.setState({
        //                 visibleDialogDeleteComment: false,
        //                 adjustCommentStyle: {},
        //             })}
        //         onBackButtonPress={() =>
        //             this.setState({
        //                 visibleDialogDeleteComment: false,
        //                 adjustCommentStyle: {},
        //             })}
        //         style={{
        //             flex: 1,
        //             justifyContent: 'center',
        //             alignItems: 'center',
        //             // marginHorizontal: 5,
        //             // marginBottom: 0,
        //         }}>
        //         <View style={
        //             {
        //                 // flex: 0.3,
        //                 height: 162,
        //                 width: '100%',
        //                 alignItems: 'center',
        //                 justifyContent: 'center',
        //                 backgroundColor: 'rgba(255, 255, 255, 1)',
        //                 flexDirection: 'column',
        //                 borderRadius: 10,
        //             }
        //         }
        //         >
        //             <Text style={config.styles.text}>
        //                 비밀번호를 입력하세요
        //             </Text>
        //             <TextInput
        //                 style={[config.styles.text, {textAlign: 'center'}]}
        //                 autoFocus={true}
        //                 secureTextEntry={true}
        //                 onChangeText={(text) => this.setState({
        //                     passwordForDelete: text,
        //                 })}
        //             />
        //             <TouchableOpacity
        //                 style={[config.styles.button, {}]}
        //                 onPress={() => {
        //                     this.setState({
        //                         visibleDialogDeleteComment: false,
        //                         adjustCommentStyle: {},
        //                     });
        //                     serviceDeleteComment({
        //                         postId: post.post_id,
        //                         commentId: comment.comment_id,
        //                         writtenBy: comment.written_by.name,
        //                         memberId: comment.written_by.member_id,
        //                         password: this.state.passwordForDelete,
        //                     });
        //                 }}
        //             >
        //                 <Text style={[config.styles.text, {}]}>삭제</Text>
        //             </TouchableOpacity>
        //         </View>
        //     </Modal>
        //
        // );

        {/*<DialogInput isDialogVisible={this.state.visibleDialogDeleteComment}*/
        }
        {/*             title={'DialogInput 1'}*/
        }
        {/*             message={'Message for DialogInput #1'}*/
        }
        {/*             hintInput={'HINT INPUT'}*/
        }
        {/*             submitInput={(inputText) => {*/
        }
        {/*                 serviceDeleteComment({*/
        }
        {/*                     postId: post.post_id,*/
        }
        {/*                     commentId: comment.comment_id,*/
        }
        {/*                     writtenBy: comment.written_by.name,*/
        }
        {/*                     memberId: comment.written_by.member_id,*/
        }
        {/*                     password: inputText,*/
        }
        {/*                 });*/
        }
        {/*             }}*/
        }
        {/*             closeDialog={() => {*/
        }
        {/*                 this.setState({*/
        }
        {/*                     visibleDialogDeleteComment: false,*/
        }
        {/*                 });*/
        }
        {/*             }}>*/
        }
        {/*</DialogInput>*/
        }
    };
    renderDialogDeleteMemberComment = () => {
        const comment = this.state.commentWillBeDeleted;
        const {post} = this.props;
        const {serviceDeleteComment} = this;
        return (
            <PopupDialog
                visible={this.state.visibleDialogDeleteMemberComment}
                // title={'비밀번호를 입력하세요'}
                message={'댓글을 삭제하시겠습니까?'}
                leftButtonLabel={'취소'}
                onLeftButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteMemberComment: false,
                        adjustCommentStyle: {},
                    });
                }}
                rightButtonLabel={'삭제'}
                rightButtonStyle={{backgroundColor: config.colors.baseColor}}
                rightButtonTextStyle={{color: '#ffffff'}}
                onRightButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteMemberComment: false,
                        adjustCommentStyle: {},
                    });
                    serviceDeleteComment({
                        postId: post.post_id,
                        commentId: comment.comment_id,
                        writtenBy: comment.written_by.name,
                        memberId: comment.written_by.member_id,
                    });
                }}
                onBackdropPress={() =>
                    this.setState({
                        visibleDialogDeleteMemberComment: false,
                        adjustCommentStyle: {},
                    })}
                onBackButtonPress={() =>
                    this.setState({
                        visibleDialogDeleteMemberComment: false,
                        adjustCommentStyle: {},
                    })}
            />
        );
    };
    renderDialogDeleteAttachedImage = () => {
        const item = this.state.itemWillBeRemoved;
        //
        // setTimeout(() => {
        //     Alert.alert(
        //         '이미지 첨부를 취소할까요?',
        //         '',
        //         [
        //             {
        //                 text: '아니오', onPress: () => {
        //
        //                 },
        //             },
        //             {
        //                 text: '네', onPress: () => {
        //                     let imagesAttachedInPost = [];
        //                     for (let i = 0; i < this.state.imagesAttachedInPost.length; i++) {
        //                         if (this.state.imagesAttachedInPost[i].uri !== item.imageSrc.uri) {
        //                             imagesAttachedInPost.push(this.state.imagesAttachedInPost[i]);
        //                         }
        //                     }
        //                     let newData = [];
        //                     for (let i = 0; i < this.state.data.length; i++) {
        //                         // console.log(this.state.data[i].key, item.key);
        //                         if (this.state.data[i].key !== item.key) {
        //                             newData.push(this.state.data[i]);
        //                         }
        //                     }
        //
        //                     this.setState({
        //                         data: newData,
        //                         imagesAttachedInPost: imagesAttachedInPost,
        //                     });
        //                 },
        //             },
        //         ],
        //         {cancelable: false},
        //     );
        // }, 100);

        return (
            <PopupDialog
                visible={this.state.visibleDialogDeleteAttachedImage}
                // title={'비밀번호를 입력하세요'}
                message={'이미지 첨부를 취소할까요?'}
                leftButtonLabel={'아니오'}
                leftButtonStyle={{borderWidth: 2, borderColor: '#9e9e9e'}}
                leftButtonTextStyle={{color: '#9e9e9e'}}
                onLeftButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteAttachedImage: false,
                        adjustCommentStyle: {},
                    });
                }}
                rightButtonLabel={'네'}
                rightButtonStyle={{backgroundColor: '#ff2c45'}}
                rightButtonTextStyle={{color: '#ffffff'}}
                onRightButtonPress={() => {
                    this.setState({
                        visibleDialogDeleteAttachedImage: false,
                        adjustCommentStyle: {},
                    });
                    let imagesAttachedInPost = [];
                    for (let i = 0; i < this.state.imagesAttachedInPost.length; i++) {
                        if (this.state.imagesAttachedInPost[i].uri !== item.imageSrc.uri) {
                            imagesAttachedInPost.push(this.state.imagesAttachedInPost[i]);
                        }
                    }
                    let newData = [];
                    let lastContent = '';
                    for (let i = 0; i < this.state.data.length; i++) {
                        if (this.state.data[i].key !== item.key) {
                            console.log('----->', this.state.data[i].type, this.state.data[i]);
                            if (this.state.data[i].type === 'TextInput') {
                                if (lastContent === 'TextInput') {
                                    if (!this.state.data[i].text || this.state.data[i].text.length === 0) {
                                        continue
                                    }
                                }
                                lastContent = 'TextInput'
                            }
                            newData.push(this.state.data[i]);
                        }
                    }

                    this.setState({
                        data: newData,
                        imagesAttachedInPost: imagesAttachedInPost,
                    });
                }}
                onBackdropPress={() =>
                    this.setState({
                        visibleDialogDeleteAttachedImage: false,
                        adjustCommentStyle: {},
                    })}
                onBackButtonPress={() =>
                    this.setState({
                        visibleDialogDeleteAttachedImage: false,
                        adjustCommentStyle: {},
                    })}
            />
        );
    };
    removeComment = (comment) => {
        const {post} = this.props;
        const {serviceDeleteComment} = this;
        const member = this.getMember();
        if (member) {
            this.setState({
                visibleDialogDeleteMemberComment: true,
                adjustCommentStyle: {bottom: -HeaderHeight * 3},
                commentWillBeDeleted: comment,
            });
            // setTimeout(() => {
            //     Alert.alert(
            //         '댓글을 삭제할까요?',
            //         '',
            //         [
            //             {
            //                 text: '아니오', onPress: () => {
            //
            //                 },
            //             },
            //             {
            //                 text: '네', onPress: () => {
            //                     serviceDeleteComment({
            //                         postId: post.post_id,
            //                         commentId: comment.comment_id,
            //                         writtenBy: comment.written_by.name,
            //                         memberId: comment.written_by.member_id,
            //                     });
            //                 },
            //             },
            //         ],
            //         {cancelable: false},
            //     );
            // }, 100);
        } else {
            this.setState({
                visibleDialogDeleteComment: true,
                adjustCommentStyle: {bottom: -HeaderHeight * 3},
                commentWillBeDeleted: comment,
            });
        }
    };
    clearMention = () => {
        this.setState({
            mention: null,
        });
    };
    mentionComment = (comment) => {
        // console.log('mentionComment', comment);
        if (this._refCommentComponent) {
            this._refCommentComponent.prepareMention();
            this.setState({
                mention: comment,
            });
        }
    };
    likeComment = (comment) => {
        const {post} = this.props;
        const {serviceLikeComment} = this;
        const member = this.getMember();
        // TODO: 일단은 로그인한 멤버만 좋아요를 누를 수 있다.
        // TODO: 좋아요 누른 사람 목록을 미리 읽어와서 에러를 낼 수 있다.
        if (member) {
            if (member.member_id === comment.written_by.member_id) {
                this.setState({
                    showDialogCantMine: true,
                })
            } else {
                serviceLikeComment({
                    postId: post.post_id,
                    commentId: comment.comment_id,
                    memberId: member.member_id,
                });
            }
        } else {
            this.setState({
                showDialogNeedLogin: true,
            })
        }
    };
    recursiveComment = (comment, depth) => {
        const {recursiveComment} = this;
        let padding = 0;
        let paddingLeft = 39;
        if (depth === 1) {
            padding = paddingLeft;
        }
        let commentIndent = {
            paddingLeft: padding,
            // paddingRight: padding
        };
        let isDeleted = false;
        if (comment.status && comment.status === 'delete') {
            isDeleted = true;
        }
        let isMention = false;
        let mentionTarget = '';
        if (comment.mention && comment.mention.name && comment.mention.name.length > 0) {
            isMention = true;
            mentionTarget = '@' + comment.mention.name + ' ';
        }
        let renderElement = comment.content_list.map(e => {
            if (e.type === 'text') {
                return (
                    <View style={{
                        flex: 1,
                        paddingLeft: paddingLeft,
                        marginTop: -5,
                    }}
                          key={'text' + comment.comment_id}
                    >
                        {isDeleted ?
                            <StyledText
                                style={[config.styles.text, styles.commentTextStyle, {color: config.colors.greyColor}]}
                                selectable={true}>
                                삭제된 댓글입니다
                            </StyledText>
                            :
                            <StyledText style={[config.styles.text, styles.commentTextStyle]}
                                  selectable={true}>
                                {isMention ?
                                    <StyledText style={{color: config.colors.baseColor}}>{mentionTarget}</StyledText> : null}
                                {e.text}
                            </StyledText>
                        }
                    </View>
                );
            } else if (e.type === 'image') {
                if (isDeleted) {
                    return null;
                } else {
                    let adjWidth = 20 * (2 * depth + 1) + paddingLeft;
                    return (
                        <View style={{
                            flex: 1,
                            // flexDirection: 'column',
                            height: (e.height * (Dimensions.get('window').width - adjWidth)) / e.width,
                            width: Dimensions.get('window').width - adjWidth,
                            justifyContent: 'center',
                            alignItems: 'center',
                            // paddingRight: 10,
                            marginLeft: paddingLeft,
                            marginTop: 10,
                            marginBottom: 10,
                            backgroundColor: 'rgba(0, 0, 0, .5)',
                        }}
                              key={'image' + comment.comment_id}
                        >
                            <Image source={{uri: e.uri}}
                                   style={[styles.postImageStyle]}
                            />
                        </View>
                    );
                }
            }
        });
        let likeText = '좋아요';
        let recommendStyle = {};
        console.log('=====>', comment)
        if (comment.recommendation_count && comment.recommendation_count > 0) {
            // console.log('recommendation_count', comment.recommendation_count);
            likeText += ' ' + comment.recommendation_count;
            if (comment.is_recommended_by_member) {
                recommendStyle = {
                    color: config.colors.baseColor,
                };
            }
        }
        let renderTail = (
            <View style={{
                flexDirection: 'row',
                // backgroundColor: '#00ff00',
                paddingLeft: paddingLeft - 3,
                paddingBottom: 20,
            }}>
                <TouchableOpacity
                    style={[config.styles.button, {flex: 0}]}
                    onPress={() => this.mentionComment(comment)}>
                    <StyledText style={[config.styles.text, styles.commentAtStyle, {}]}>댓글달기</StyledText>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[config.styles.button, {flex: 0}]}
                    onPress={() => this.likeComment(comment)}>
                    <StyledText style={[config.styles.text, styles.commentAtStyle, recommendStyle]}>{likeText}</StyledText>
                </TouchableOpacity>
            </View>
        );
        if (isDeleted) {
            renderTail = (
                <View style={{
                    paddingBottom: 20,
                }}>
                </View>
            );
        }
        let commentRenderer = (
            <View style={{
                // borderBottomWidth: 1,
                // borderBottomColor: config.colors.lineColor,
            }}>
                {renderElement}
                {renderTail}
            </View>
        );
        let childRenderElement = null;
        if (comment.comment_list && comment.comment_list.length > 0) {
            childRenderElement = comment.comment_list.map(e => {
                return recursiveComment(e, depth + 1);
            });
        }

        const member = this.getMember();
        let enableDelete = false;
        if (member) {
            if (member.member_id === comment.written_by.member_id && (!comment.status || comment.status !== 'delete')) {
                enableDelete = true;
            }
        } else {
            if (!comment.written_by.member_id || comment.written_by.member_id.length === 0) {
                if (config.deviceInfo.device_id === comment.written_by.device_id && (!comment.status || comment.status !== 'delete')) {
                    enableDelete = true;
                }
            }
        }

        let commentWrittenBy = (
            <View style={{flexDirection: 'row'}}>
                {depth === 0 ?
                    <View>
                        <Image
                            source={config.images.commentProfile1}
                            style={{height: config.size._136px, width: config.size._136px}}
                        />
                    </View> :
                    <View>
                        <Image
                            source={config.images.commentProfile2}
                            style={{height: config.size._120px, width: config.size._120px}}
                        />
                    </View>
                }
                <View style={{flexDirection: 'row', paddingLeft: config.size._28px}}>
                    {isDeleted ?
                        <StyledText style={[config.styles.text, styles.authorStyle]}>{''}</StyledText>
                        :
                        <StyledText style={[config.styles.text, styles.authorStyle]}>{comment.written_by.name}</StyledText>
                    }
                    <StyledText style={[config.styles.text, styles.commentAtStyle]}>{comment.commented_at}</StyledText>
                </View>
                {enableDelete ?
                    <View style={{position: 'absolute', height: '100%', right: 0}}>
                        <TouchableOpacity
                            style={[config.styles.button, {flex: 1}]}
                            onPress={() => this.removeComment(comment)}>
                            <StyledText style={[config.styles.text, styles.commentAtStyle, {}]}>삭제</StyledText>
                        </TouchableOpacity>
                    </View>
                    : null
                }
            </View>
        );

        return (
            <View style={commentIndent} key={'view' + comment.comment_id}>
                {commentWrittenBy}
                {commentRenderer}
                {childRenderElement}
            </View>
        );
    };
    // 반복해서 댓글을 붙이기.
    renderCommentList = () => {
        const {post} = this.props;
        const {recursiveComment} = this;
        // console.log('renderCommentList - 1');
        // console.log('renderCommentList', post);
        if (post && post.comment_list && post.comment_list.length > 0) {
            let renderElement = post.comment_list.map((e) => {
                return recursiveComment(e, 0);
            });

            return (
                <View style={{padding: 10}} key={'comment_list_recursive'}>
                    {renderElement}
                </View>);
        } else {
            return <View style={{flex: 1, backgroundColor: '#ffffff', paddingBottom: 48}} key={'comment_list'}></View>;
        }
    };
    renderContent = () => {
        const {post} = this.props;
        if (post && post.content_list) {
            let renderElement = post.content_list.map((e, i) => {
                if (e.type === 'text') {
                    return (
                        <View style={{
                            flex: 1,
                            padding: 10,
                        }}
                              key={e.type + i}
                        >
                            <StyledText style={{fontSize: 15}} selectable={true}>
                                {e.text}
                            </StyledText>
                        </View>
                    );
                } else if (e.type === 'image') {
                    // console.log(e, (e.height * Dimensions.get('window').width) / e.width, Dimensions.get('window').width);
                    return (
                        <View style={{
                            flex: 1,
                            // flexDirection: 'column',
                            // height: e.height,
                            paddingVertical: 10,
                            height: (e.height * Dimensions.get('window').width) / e.width,
                            width: Dimensions.get('window').width,
                            justifyContent: 'center',
                            alignItems: 'center',
                            // backgroundColor: 'rgba(0, 0, 0, 1.0)',
                        }}
                              key={e.type + i}
                        >
                            <Image source={{uri: e.uri}}
                                   style={[styles.postImageStyle]}
                            />
                        </View>
                    );
                }
            });
            return renderElement;
        } else {
            return <View style={{flex: 1, backgroundColor: '#00ff00'}}></View>;
        }
    };
    sharePost = () => {
        const {post} = this.props;
        const {serviceSharePost} = this;
        const member = this.getMember();
        async function onShare(link) {
            try {
                const result = await Share.share({
                    message: link,
                });
                if (result.action === Share.sharedAction) {
                    if (result.activityType) {
                        // shared with activity type of result.activityType
                    } else {
                        // shared
                    }
                } else if (result.action === Share.dismissedAction) {
                    // dismissed
                }
            } catch (error) {
                alert(error.message);
            }
        }
        async function buildLink() {
            let imageUrl = undefined;
            const content_length = (post.content_list && post.content_list.length) || 0;
            for(let i=0;i<content_length;i++) {
                let content = post.content_list[i];
                if(content.type === "image"){
                    imageUrl = content.uri;
                    break;
                }
            }
            /*
            const link2 = await dynamicLinks().buildShortLink({
                link: config.url.host + '/post/' + post.post_id,
                // domainUriPrefix is created in your Firebase console
                domainUriPrefix: 'https://busjok.page.link',
                // optional set up which updates Firebase analytics campaign
                // "banner". This also needs setting up before hand
                social:{
                    title:post.title || "",
                    descriptionText:"",
                    imageUrl: imageUrl
                },
                android: {
                    packageName:"com.yaptv.busjok",
                },
                ios: {
                    bundleId: "com.yaptv.busjok",
                  }
            }, dynamicLinks.ShortLinkType.UNGUESSABLE);
            */

            // console.log("buildShortLink : ", link2);
            
            let link2 = await dynamicLinks().buildLink({
                link: config.url.host + '/post/' + post.post_id ,
                // domainUriPrefix is created in your Firebase console
                domainUriPrefix: 'https://busjok.page.link',
                // optional set up which updates Firebase analytics campaign
                // "banner". This also needs setting up before hand
                social:{
                    title:post.title || "",
                    descriptionText:"",
                    imageUrl: imageUrl
                },

                android: {
                    packageName:"com.yaptv.busjok",
                    //fallbackUrl: 'https://bbc.com'
                },
                ios: {
                    bundleId: "com.yaptv.busjok",
                    //fallbackUrl: 'https://bbc.com'
                },

            });
            console.log("buildShortLink : ", link2 + "&ofl=https%3A%2F%2Fplay%2Egoogle%2Ecom%2Fstore%2Fapps%2Fdetails?id=com%2Eyaptv%2Ebusjok");
            link2 = link2 + "&ofl=https%3A%2F%2Fplay%2Egoogle%2Ecom%2Fstore%2Fapps%2Fdetails?id=com%2Eyaptv%2Ebusjok"
            Clipboard.setString(link2);
            onShare(link2);
            return link2;
        }
        buildLink();
        if (member) {
            serviceSharePost({
                postId: post.post_id,
                memberId: member.member_id,
            });
        }
    };
    likePost = () => {
        const {post} = this.props;
        const {serviceLikePost} = this;
        const member = this.getMember();
        const {is_recommended_by_member} = post;
        if (is_recommended_by_member) {
            return
        }
        // TODO: 일단은 로그인한 멤버만 좋아요를 누를 수 있다.
        // TODO: 좋아요 누른 사람 목록을 미리 읽어와서 에러를 낼 수 있다.
        if (member) {
            if (member.member_id === post.written_by.member_id) {
                this.setState({
                    showDialogCantMine: true,
                })
            } else {
                serviceLikePost({
                    postId: post.post_id,
                    memberId: member.member_id,
                });
            }
        } else {
            this.setState({
                showDialogNeedLogin: true,
            })
        }
    };
    renderShare = () => {
        const {post} = this.props;

        let likeCount = 0;
        if (post.recommendation_count) {
            likeCount = post.recommendation_count;
        }
        let shareCount = 0;
        if (post.share_count) {
            shareCount = post.share_count;
        }
        const {is_recommended_by_member} = post;
        const {is_reported_by_member} = post;
        return (
            <View
                style={{
                    flexDirection: 'row',
                    paddingTop: 0,
                    paddingBottom: config.size._48px,
                    borderBottomWidth: config.size._2px,
                    borderBottomColor: config.colors.lineColor,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <View style={styles.shareLayout}>
                    <TouchableOpacity
                        style={[config.styles.button, {flex: 0}]}
                        onPress={this.likePost}>
                        {is_recommended_by_member ?
                            <Image
                                source={config.images.likeOn}
                                style={{height: config.size._96px, width: config.size._96px}}
                            /> :
                            <Image
                                source={config.images.likeOff}
                                style={{height: config.size._96px, width: config.size._96px}}
                            />
                        }
                        {/*<Ionicons name={'ios-heart-sharp'} size={24} color={config.colors.baseColor}/>*/}
                    </TouchableOpacity>
                    <StyledText style={[config.styles.text, styles.shareText]}>
                        {likeCount}
                    </StyledText>
                </View>
                <View style={styles.shareLayout}>
                    <TouchableOpacity
                        style={[config.styles.button, {flex: 0}]}
                        onPress={this.sharePost}>
                        <Image
                            source={config.images.sharePost}
                            style={{height: config.size._96px, width: config.size._96px}}
                        />
                        {/*<Ionicons name={'ios-share-social-outline'} size={24} color={config.colors.greyColor}/>*/}
                    </TouchableOpacity>
                    <StyledText style={[config.styles.text, styles.shareText]}>
                        {shareCount}
                    </StyledText>
                </View>

            </View>
        );
    };
    renderCommentCount = () => {
        const {post} = this.props;
        let count = 0;
        if (post.comment_count) {
            count = post.comment_count;
        }
        // if (post.comment_list && post.comment_list.length > 0) {
        //     count = post.comment_list.length;
        // }
        let commentCountText = '댓글 ' + count;
        return (
            <View>
                <StyledText style={[config.styles.text, {
                    //fontWeight: 'bold',
                    fontFamily:config.defaultBoldFontFamily,
                    fontSize: 13,
                    paddingVertical: 0,
                    paddingTop: 10,
                }]}>
                    {commentCountText}
                </StyledText>
            </View>
        );
    };
    refreshPost = () => {
        const {post} = this.props;
        // console.log('refreshPost', post);
        this.setState({
            refreshingPost: true,
        });
        this.serviceGetPost({postId: post.post_id});
        this.setState({
            refreshingPost: false,
        });
    };
    onTouchStartPostScreen = () => {
        // console.log('onTouchStartPostScreen', this._passFocusLastTextInput);
        const {data} = this.state;
        if (this._passFocusLastTextInput === false) {
            let t = null;
            for (let i = 0; i < data.length; i++) {
                if (data[i].type === 'TextInput' && data[i].ref && data[i].ref.getInnerRef() && data[i].ref.getInnerRef().focus) {
                    t = data[i].ref;
                }
            }
            if (t) {
                t.getInnerRef().focus();
            }
        }
        this._passFocusLastTextInput = false;
    };
    renderBody = () => {
        const {readOnly} = this.props;
        const {keyboardHeight} = this.state;
        let addStyle = {};
        // let addStyle = {paddingTop: HeaderHeight};
        // const member = getMember();
        // if (member) {
        //     addStyle = {};
        // }

        // console.log('heightPostTextInput 2', this.state.heightPostTextInput);
        // console.log('renderElement', readOnly, this.props.post);
        if (readOnly) {
            return (
                <View
                    style={{flex: 1, marginBottom: HeaderHeight}}
                >
                    {/*<ScrollView*/}
                    {/*    style={{*/}
                    {/*        flex: 1,*/}
                    {/*    }}*/}
                    {/*    ref={(ref) => {*/}
                    {/*        this.scrollView = ref;*/}
                    {/*    }}*/}
                    {/*    // showsVerticalScrollIndicator={false}*/}
                    {/*>*/}
                    <FlatList
                        containerStyle={{borderTopWidth: 0, borderBottomWidth: 0}}
                        data={[this.renderTitle(), this.renderContent(), this.renderShare(), this.renderCommentCount(), this.renderCommentList()]}
                        renderItem={({item, index}) => (
                            item
                        )}
                        keyExtractor={(item, index) => '' + index}
                        // ItemSeparatorComponent={this.renderSeparator}
                        // ListHeaderComponent={this.renderHeader}
                        // ListFooterComponent={() => this.renderFooter(showPostList)}
                        onRefresh={this.serviceGetPost}
                        refreshing={this.state.refreshingPost}
                        // stickyHeaderIndices={[0]}
                        // initialNumToRender={postPerPage}
                        // onEndReached={this.handleLoadMore}
                        // onEndReachedThreshold={50}
                        showsVerticalScrollIndicator={false}
                    />

                    {/*{this.renderTitle()}*/}
                    {/*{this.renderContent()}*/}
                    {/*{this.renderShare()}*/}
                    {/*{this.renderCommentList()}*/}
                    {/*</ScrollView>*/}
                </View>
            );
        } else {
            return (
                <View
                    style={{flex: 1, marginBottom: HeaderHeight}}
                >
                    {this.renderHeader()}
                    {Platform.OS === 'ios' ?
                        <View
                            style={{flex: 1}}
                            onTouchStart={this.onTouchStartPostScreen}
                            >
                            <DraggableFlatList
                                ref={ref => {
                                    if (ref) {
                                        this._draggableRef = ref;
                                        this.state.scrollY = ref.scrollOffset;
                                    }
                                }}
                                contentContainerStyle={[addStyle]}
                                data={this.state.data}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => `draggable-item-${item.key}`}
                                onDragEnd={({data}) => this.onDraggableFlatListDragEnd(data)}
                                dragItemOverflow={true}
                                showsVerticalScrollIndicator={false}
                                layoutInvalidationKey={this.state.invalidated}
                                onScrollOffsetChange={this.onScrollOffsetChange}
                                extraData={this.state}
                                // removeClippedSubviews={false}
                                // renderPlaceholder={this.renderPlaceholder}
                            />
                        </View>
                        :
                    <ScrollView
                        style={{
                            flex: 1,
                            // backgroundColor: 'rgba(255, 0, 0, 0.5)',
                        }}
                        ref={(ref) => {
                            this.scrollView = ref;
                            // this.state.scrollY = ref.scrollOffset;
                        }}
                        showsVerticalScrollIndicator={false}
                        onScroll={this.onScroll2}
                        onTouchStart={this.onTouchStartPostScreen}
                    >
                        <View onTouchStart={(e) => this._passFocusLastTextInput = true}>
                            <DraggableFlatList
                                ref={ref => {
                                    if (ref) {
                                        this._draggableRef = ref;
                                        this.state.scrollY = ref.scrollOffset;
                                    }
                                }}
                                contentContainerStyle={[addStyle]}
                                data={this.state.data}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => `draggable-item-${item.key}`}
                                onDragEnd={({data}) => this.onDraggableFlatListDragEnd(data)}
                                dragItemOverflow={true}
                                showsVerticalScrollIndicator={false}
                                layoutInvalidationKey={this.state.invalidated}
                                onScrollOffsetChange={this.onScrollOffsetChange}
                                extraData={this.state}
                                // removeClippedSubviews={false}
                                // renderPlaceholder={this.renderPlaceholder}
                            />
                        </View>
                    </ScrollView>
        }
                    <View style={{
                        flex: 0.1,
                        // position: 'absolute',
                        height: '100%',
                        width: '100%',
                        // backgroundColor: '#0000ff',
                    }}
                          pointerEvents={'none'}>
                        <View style={{
                            position: 'absolute',
                            bottom: -keyboardHeight,
                            height: HeaderHeight,
                            width: '100%',
                            // backgroundColor: '#00ffff',
                        }}>
                            <StyledText style={[config.styles.text, {
                                fontSize: 10,
                                color: '#979797',
                                textAlignVertical: 'top',
                                paddingVertical: 0,
                                lineHeight: 14,
                            }]}>
                                {warningForPolicy}
                            </StyledText>
                        </View>
                    </View>
                    <Animated.Code>
                        {() => {
                            onChange(this.state.scrollY, call([this.state.scrollY], this.onScroll));
                        }}
                    </Animated.Code>
                </View>
            );
        }
    };
    renderTail = () => {
        // const diffClampY = diffClamp(this.state.scrollY, 0, HeaderHeight);
        // const translateY = interpolate(diffClampY, {
        //     inputRange: [0, HeaderHeight],
        //     outputRange: [0, -HeaderHeight],
        // });

        const {readOnly, post} = this.props;
        const {uploadImageToServer, servicePrepareImage, serviceGetPost} = this;
        const {keyboardHeight} = this.state;
        const member = this.getMember();

        // console.log('renderTail', parentLayout, Dimensions.get('window').height);

        let s = {
            position: 'absolute',
            width: '100%',
            bottom: 0,
            height: HeaderHeight,
        };
        if (Platform.OS === 'ios') {
            s.bottom = keyboardHeight
        }

        if (!readOnly) {
            timing(this.toolbarY, {
                duration: 100,
                toValue: -keyboardHeight,
                easing: Easing.inOut(Easing.ease),
            }).start();

            return (
                <View style={{flex: 0}}>
                    {/*<View style={{*/}
                    {/*    justifyContent: 'flex-end',*/}
                    {/*    height: 48,*/}
                    {/*    width: '100%',*/}
                    {/*    backgroundColor: '#00ffff',*/}
                    {/*}}>*/}
                    {/*</View>*/}
                    <View
                        // keyboardVerticalOffset={keyboardHeight}
                        // behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                        style={s}>
                        <Toolbar
                            selectImage={this.selectPostImage}
                            imageFiles={this.state.imagesAttachedInPost}
                        />
                    </View>
                </View>
            );
        } else {
            //console.log('--------DEBUG-COMMENT-001', keyboardHeight, this.state.adjustCommentStyle);
            let iosStyle = {}
            if (Platform.OS === 'ios') {
                iosStyle = {bottom: keyboardHeight}
                if (keyboardHeight > 0) {
                    iosStyle = {bottom: keyboardHeight - HeaderHeight}
                }
            }
            return (
                <View
                    style={[styles.commentContainer, this.state.adjustCommentStyle, iosStyle]}>
                    <Comment
                        refreshPost={serviceGetPost}
                        post={post}
                        uploadImageToServer={uploadImageToServer}
                        servicePrepareImage={servicePrepareImage}
                        getMember={this.getMember}
                        setCommentRef={this.setCommentRef}
                        mention={this.state.mention}
                        clearMention={this.clearMention}
                    />
                    {/*<Animated.View style={[styles.tail,*/}
                    {/*    {transform: [{translateY: this.tailY}]}]}>*/}
                    {/*</Animated.View>*/}
                </View>
            );
        }

        // if (keyboardHeight > 0 || true) {
        //     console.log('--->', keyboardHeight);
        //     return (
        //         <Animated.View style={{flex: 0}}>
        //             {this.renderComment()}
        //         </Animated.View>
        //     )
        // } else {
        //     return (
        //         <Animated.View style={[styles.tail,
        //             {transform: [{translateY: this.tailY}]}]}>
        //             {this.renderComment()}
        //         </Animated.View>
        //     );
        // }
        // return null;
    };

    render() {
        const {goBackToLoginPage} = this.props;
        return (
            <View style={{flex: 1, backgroundColor: '#ffffff', paddingBottom:isIphoneX()?34:0}}
                  onLayout={e => {
                      this.setState({
                          containerLayout: e.nativeEvent.layout,
                      });
                  }}
            >
                {this.renderBody()}
                {this.renderTail()}
                {this.state.visibleDialogDeleteComment ?
                    this.renderDialogDeleteComment()
                    : null}
                {this.state.visibleDialogDeleteMemberComment ?
                    this.renderDialogDeleteMemberComment()
                    : null}
                {this.state.visibleDialogDeleteAttachedImage ?
                    this.renderDialogDeleteAttachedImage()
                    : null}

                <PopupDialog
                    visible={this.state.showDialogPasswordIncorrect}
                    // title={'비밀번호를 입력하세요'}
                    message={'비밀번호가 일치하지 않습니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogPasswordIncorrect: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedLogin}
                    // title={'비밀번호를 입력하세요'}
                    message={'로그인이 필요합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogNeedLogin: false,
                        });
                        if (goBackToLoginPage) {
                            goBackToLoginPage();
                        }
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogCantMine}
                    // title={'비밀번호를 입력하세요'}
                    message={'내가 작성한 글은 "좋아요"를 누를 수 없습니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogCantMine: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedNickname}
                    // title={'비밀번호를 입력하세요'}
                    message={'닉네임을 입력해야 합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogNeedNickname: false,
                            focusNickname: true,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedPassword}
                    // title={'비밀번호를 입력하세요'}
                    message={'비밀번호를 입력해야 합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogNeedPassword: false,
                            focusPassword: true,
                        });
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        // top: 0,
        height: HeaderHeight + 20,
        width: '100%',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
        // position: 'absolute',
    },
    comment: {
        // position: 'absolute',
        // height: HeaderHeight,
        // flex: 1,
        flex: 0,
        width: '100%',
        // bottom: 0,
        backgroundColor: '#ffffff',
    },
    tail: {
        flex: 1,
        bottom: -HeaderHeight,
        height: HeaderHeight,
        width: '100%',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
    },
    postImageStyle: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    postImageStyle2: {
        width: '100%',
        height: '100%',
        // backgroundColor: '#00ff00',
    },
    postImageStyle3: {
        // width: '100%',
        // height: '100%',
        // backgroundColor: '#00ff00',
    },
    label: {fontSize: 16, color: '#222'},
    tab: {elevation: 0, shadowOpacity: 0, backgroundColor: '#FFCC80'},
    indicator: {backgroundColor: '#222'},
    titleLayout: {
        // flex: 1,
        // backgroundColor: 'rgba(0,0,0,0.5)',
        height: HeaderHeight,
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        padding: 5,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
    },
    commentContainer: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
    },
    titleContainer: {
        flex: 0,
        // height: 64,
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
        flexDirection: 'column',
    },
    prevText: {
        fontSize: 10,
        paddingVertical: 0,
        paddingRight: 5,
        color: config.colors.greyColor,
    },
    shareLayout: {
        flexDirection: 'row',
        // backgroundColor: '#00ff00',
    },
    shareText: {
        flex: 0,
        paddingLeft: 5,
        paddingRight: 15,
        color: config.colors.greyColor,
        fontSize: 10,
    },
    authorStyle: {
        flex: 0,
        fontSize: 13,
        //fontWeight: 'bold',
        fontFamily:config.defaultBoldFontFamily,
        paddingVertical: 0,
        paddingHorizontal: 0,
    },
    commentAtStyle: {
        flex: 0,
        paddingVertical: 0,
        paddingHorizontal: 5,
        fontSize: 10,
        // textAlignVertical: 'bottom',
        color: config.colors.greyColor,
    },
    commentTextStyle: {
        flex: 0,
        paddingVertical: 0,
        paddingHorizontal: 0,
        // fontSize: 12,
        // textAlignVertical: 'bottom',
        // color: config.colors.greyColor,
    },
});

export default Post;
;
