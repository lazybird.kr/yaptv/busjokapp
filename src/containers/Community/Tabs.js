import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text, ScrollView,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import config from '../../config';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {StyledText} from '../../components/StyledComponents';

const tabList = ['전체', '좋아요'];
const baseColor = '#00a3ff';

export default class Tabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: undefined,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }
    onPressTab = (item) => {
        const {onPressGroup} = this.props;
        let groupName = "";
        if (item !== tabList[0]) {
            groupName = item;
        }
        onPressGroup(groupName);
        // console.log(item);
        // const {serviceGetPostList} = this.props;
        // let groupName = "";
        // if (item !== '전체') {
        //     groupName = item;
        // }
        // serviceGetPostList({
        //     groupName: groupName,
        // });
        // this.setState({
        //     tabPressed: item,
        // })
    };

    render() {
        const {currentGroup, handleTabCondition, handleTabRefresh} = this.props;
        let tabs = null;
        let tabPressed = tabList[0];
        if (currentGroup !== "") {
            tabPressed = currentGroup
        }
        if (this.state.layout) {
            const w = this.state.layout.width / tabList.length;
            // const w = this.state.layout.width / 3;
            tabs = tabList.map((item, index) => {
                let tabStyle = {
                    width: w,
                    // backgroundColor: 'rgba(255, 255, 255, 1.0)',
                    borderBottomWidth: config.size._2px,
                    borderColor: config.colors.lineColor,
                    // marginBottom: 1,

                };
                let textStyle = {
                    // flex: 1,
                    justifyContent: 'center',
                    lineHeight: config.size._100px,
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    color: config.colors.greyColor,
                };
                let selectedDot = null;
                if (item === tabPressed) {
                    // tabStyle = {
                    //     ...tabStyle,
                    //     borderBottomWidth: 3,
                    //     marginBottom: 0,
                    // };
                    textStyle = {
                        ...textStyle,
                        color: baseColor,
                    };
                    selectedDot = (
                        <Entypo name={'dot-single'} size={18} color={baseColor}/>
                    )
                }

                let tabTitle = (
                    <StyledText style={[config.styles.text, textStyle]}>
                        {item}
                    </StyledText>
                );
                if (item === '좋아요') {
                    tabTitle = (
                        <StyledText style={[config.styles.text, textStyle]}>
                            {'베스트톡'}
                        </StyledText>
                    );
                }
                // TODO: 이랬다가 저랬다가
                // if (item === '좋아요' && tabPressed === '좋아요') {
                //     tabTitle = (
                //         <View>
                //             <EvilIcons name={'heart'} size={24} color={baseColor}/>
                //         </View>
                //     )
                // }

                return (
                    <View
                        style={tabStyle}
                        key={'tab' + index}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.tab}
                            onPress={() => this.onPressTab(item)}>
                            {tabTitle}
                            <View style={styles.tabDot}>
                                {selectedDot}
                            </View>
                        </TouchableOpacity>
                    </View>
                );
            });
        }

        return (
            <View style={styles.container}
                  onLayout={e => {
                      this.setState({
                          layout: e.nativeEvent.layout,
                      });
                  }}
            >
                <ScrollView style={styles.scrollTabs} horizontal={true} showsHorizontalScrollIndicator={false}>
                    {tabs}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollTabs: {
        flex: 1,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#00ffff'
    },
    tabDot: {
        position: 'absolute',
        width: '100%',
        height: '25%',
        bottom: '5%',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
