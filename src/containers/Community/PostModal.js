import React, {Component} from 'react';
import {
    View,
    Dimensions, Platform,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import Modal from 'react-native-modal';
import config from '../../config';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {StyledText} from '../../components/StyledComponents';
import {isIphoneX} from 'src/utils/'


export default class PostModal extends Component {

    constructor(props) {
        super(props);
    }

    state = {};

    toggleModal = () => {
        console.log('toggleModal');
        const {closeModal} = this.props;
        closeModal();
    };
    onPressDelete = (e) => {
        const {onPressDelete} = this.props;
        onPressDelete();
    };
    onPressEdit = (e) => {
        const {onPressEdit} = this.props;
        onPressEdit();
    };
    onPressReport = (e) => {
        const {onPressReport} = this.props;
        onPressReport();
    };

    render() {
        const {isVisible, enableDelete, enableReport} = this.props;
        console.log('enableDelete', enableDelete);
        return (
            <View>
                <Modal isVisible={isVisible}
                       animationType={'none'}
                       backdropOpacity={0.5}
                       onBackdropPress={() => this.toggleModal()}
                       onBackButtonPress={() => this.toggleModal()}
                       style={styles.modalStyle}>
                    <View style={styles.container}>
                        <TouchableOpacity
                            style={[styles.button, {flex: 0.5}]}
                            onPress={this.toggleModal}>
                            <Fontisto name={'minus-a'} size={24} color={config.colors.greyColor}/>
                        </TouchableOpacity>
                        {enableDelete ?
                            <TouchableOpacity
                                onPress={this.onPressDelete}
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text, {color: config.colors.baseColor}]}>
                                    {'삭제'}
                                </StyledText>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text, {color: config.colors.greyColor}]}>
                                    {'삭제'}
                                </StyledText>
                            </TouchableOpacity>
                        }
                        <View style={styles.delimiter}/>
                        {enableDelete ?
                            <TouchableOpacity
                                onPress={this.onPressEdit}
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text]}>
                                    {'수정'}
                                </StyledText>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text, {color: config.colors.greyColor}]}>
                                    {'수정'}
                                </StyledText>
                            </TouchableOpacity>
                        }
                        <View style={styles.delimiter}/>

                        {!enableReport ?
                            <TouchableOpacity
                                onPress={this.onPressReport}
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text]}>
                                    {'신고'}
                                </StyledText>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                disabled={true}
                                style={styles.button}>
                                <StyledText style={[config.styles.text, styles.text, {color: config.colors.greyColor}]}>
                                    {'신고'}
                                </StyledText>
                            </TouchableOpacity>
                        }
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalStyle: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginHorizontal: 5,
        marginBottom: 0,
    },
    container: {
        // flex: 0.3,
        // height: '30%',
        height: config.size._616px,
        // width: config.size._1400px,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 1)',
        flexDirection: 'column',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        // borderWidth: 2,
        // borderColor: 'rgba(204, 255, 204, 1.0)',
        // alignSelf:'flex-start'
        paddingBottom: isIphoneX()? 34 : 0
    },
    text: {
        fontSize: 14,
    },
    button: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    delimiter: {
        height: 1,
        width: '100%',
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
    },
});
