import React, {useState, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Platform, KeyboardAvoidingView,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import RNDraftView from 'react-native-draftjs-editor';

const ControlButton = ({text, action, isActive}) => {
    return (
        <TouchableOpacity
            style={[
                styles.controlButtonContainer,
                isActive ? {backgroundColor: 'gold'} : {},
            ]}
            onPress={action}
        >
            <Text>{text}</Text>
        </TouchableOpacity>
    );
};

const EditorToolBar = ({
                           activeStyles,
                           blockType,
                           toggleStyle,
                           toggleBlockType,
                       }) => {
    return (
        <View style={styles.toolbarContainer}>
            <ControlButton
                text={'B'}
                isActive={activeStyles.includes('BOLD')}
                action={() => toggleStyle('BOLD')}
            />
            <ControlButton
                text={'I'}
                isActive={activeStyles.includes('ITALIC')}
                action={() => toggleStyle('ITALIC')}
            />
            <ControlButton
                text={'H'}
                isActive={blockType === 'header-one'}
                action={() => toggleBlockType('header-one')}
            />
            <ControlButton
                text={'ul'}
                isActive={blockType === 'unordered-list-item'}
                action={() => toggleBlockType('unordered-list-item')}
            />
            <ControlButton
                text={'ol'}
                isActive={blockType === 'ordered-list-item'}
                action={() => toggleBlockType('ordered-list-item')}
            />
            <ControlButton
                text={'--'}
                isActive={activeStyles.includes('STRIKETHROUGH')}
                action={() => toggleStyle('STRIKETHROUGH')}
            />
        </View>
    );
};

const styleMap = {
    STRIKETHROUGH: {
        textDecoration: 'line-through',
    },
};

const TestRNDraft = () => {
    const _draftRef = React.createRef();
    const [activeStyles, setActiveStyles] = useState([]);
    const [blockType, setActiveBlockType] = useState('unstyled');
    const [editorState, setEditorState] = useState('');

    const defaultValue = "<h1>A Full fledged Text Editor</h1><p>This editor is built with Draft.js. Hence should be suitable for most projects. However, Draft.js Isn’t fully compatible with mobile yet. So you might face some issues.</p><p><br></p><p>This is a simple implementation</p><ul>  <li>It contains <strong>Text formatting </strong>and <em>Some blocks formatting</em></li>  <li>Each for it’s own purpose</li></ul><p>You can also do</p><ol>  <li>Custom style map</li>  <li>Own css styles</li>  <li>Custom block styling</li></ol><p>You are welcome to try it!</p>";
    // const defaultValue = "<div><div><video autoplay loop muted data-src=\'https://dcimg8.dcinside.co.kr/viewimage.php?no=24b0d769e1d32ca73dec87fa11d0283123a3619b5f9530e1a1316068e1d6ca0eacaa8d5948d35f217c36844dc155d50d584bfd9eda2e216ce391266d18a2f87813e066d213f0687061e00496d71c8aef43e796971b3b2290e990b60749de30f4c4ec5636a3\'><source src=\'https://dcimg8.dcinside.co.kr/viewimage.php?no=24b0d769e1d32ca73dec87fa11d0283123a3619b5f9530e1a1316068e1d6ca0eacaa8d5948d35f217c36844dc155d50d584bfd9eda2e216ce391266d18a2f87813e066d213f0687061e00496d71c8aef43e796971b3b2290e990b60749de30f4c4ec5636a372d3338b\'\'></video></div><br /><div>쿠팡<br />우체국<br />로젠<br />CJ<br />_________________<br /><br />통곡의 벽<br /><br />_________________<br /><br />맨날 로켓와우 배송하다보니 일반택배 5일 걸리는 중인데 폐암말기다</div><br /></div></div>";
    // const defaultValue = '<h1>Hello, world</h1>';
    // const defaultValue = '<video autoplay loop muted data-src=\\\'https://dcimg8.dcinside.co.kr/viewimage.php?no=24b0d769e1d32ca73dec87fa11d0283123a3619b5f9530e1a1316068e1d6ca0eacaa8d5948d35f217c36844dc155d50d584bfd9eda2e216ce391266d18a2f87813e066d213f0687061e00496d71c8aef43e796971b3b2290e990b60749de30f4c4ec5636a3\\\'><source src=\\\'https://dcimg8.dcinside.co.kr/viewimage.php?no=24b0d769e1d32ca73dec87fa11d0283123a3619b5f9530e1a1316068e1d6ca0eacaa8d5948d35f217c36844dc155d50d584bfd9eda2e216ce391266d18a2f87813e066d213f0687061e00496d71c8aef43e796971b3b2290e990b60749de30f4c4ec5636a372d3338b\\\'\\\'></video>';

    const editorLoaded = () => {
        _draftRef.current && _draftRef.current.focus();

    };

    const insertImage = ( url) => {
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity('IMAGE', 'IMMUTABLE', { src: url });
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
        const newEditorState = EditorState.push( editorState, { currentContent: contentStateWithEntity });
        return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey,' ');
    };

    const toggleStyle = style => {
        _draftRef.current && _draftRef.current.setStyle(style);
    };

    const toggleBlockType = blockType => {
        _draftRef.current && _draftRef.current.setBlockType(blockType);
    };

    useEffect(() => {
        /**
         * Get the current editor state in HTML.
         * Usually keep it in the submit or next action to get output after user has typed.
         */
        setEditorState(_draftRef.current ? _draftRef.current.getEditorState() : '');
    }, [_draftRef]);

    return (
        <SafeAreaView style={styles.containerStyle}>
                <RNDraftView
                    defaultValue={defaultValue}
                    onEditorReady={editorLoaded}
                    style={{flex: 1, backgroundColor: '#00f00f'}}
                    placeholder={'Add text here...'}
                    ref={_draftRef}
                    onStyleChanged={setActiveStyles}
                    onBlockTypeChanged={setActiveBlockType}
                    styleMap={styleMap}
                />
                {/*<EditorToolBar*/}
                {/*    activeStyles={activeStyles}*/}
                {/*    blockType={blockType}*/}
                {/*    toggleStyle={toggleStyle}*/}
                {/*    toggleBlockType={toggleBlockType}*/}
                {/*/>*/}
                {/*{Platform.OS === 'ios' ? <KeyboardSpacer/> : null}*/}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: '#00ffff',
    },
    toolbarContainer: {
        height: 56,
        flexDirection: 'row',
        backgroundColor: 'silver',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    controlButtonContainer: {
        padding: 8,
        borderRadius: 2,
    },
});

export default TestRNDraft;
