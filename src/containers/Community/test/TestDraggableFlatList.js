import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Animated, ScrollView} from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist/lib/index';

const exampleData = [...Array(20)].map((d, index) => ({
    key: `item-${index}`, // For example only -- don't use index as your key!
    label: index,
    backgroundColor: `rgb(${Math.floor(Math.random() * 255)}, ${index *
    5}, ${132})`,
}));

const HeaderHeight = 48;
const {call, onChange} = Animated;

class TestDraggableFlatList extends Component {


    state = {
        data: exampleData,
        scrollY: new Animated.Value(0),
    };

    renderItem = ({item, index, drag, isActive}) => {
        return (
            <TouchableOpacity
                style={{
                    height: 100,
                    backgroundColor: isActive ? 'blue' : item.backgroundColor,
                    // alignItems: "center",
                    // justifyContent: "center"
                }}
                onLongPress={drag}
            >
                <TextInput
                    style={{
                        fontWeight: 'bold',
                        color: 'white',
                        fontSize: 32,
                    }}
                >
                    {item.label}
                </TextInput>
            </TouchableOpacity>
        );
    };
    renderHeader = () => {
        const y = this.state.scrollY.interpolate({
            inputRange: [0, HeaderHeight],
            outputRange: [0, -HeaderHeight],
            extrapolateRight: 'clamp',
        });
        console.log('--------------->', this.state.scrollY);

        return (
            <Animated.View style={[styles.header,
                {transform: [{translateY: y}]}]}>
                {/*{() => onChange(this.state.scrollY, call([this.state.scrollY], this.onScroll))}*/}
                <Text>{'Header'}</Text>
            </Animated.View>
        );
    };
    onScrollOffsetChange = (offset) => {
        console.log(offset);
        Animated.event(
            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
            {useNativeDriver: false},
        );
    };
    onScroll = ([scrollOffset]) => {
        console.log('onScroll', scrollOffset);
        // Animated.event(
        //     [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
        //     {useNativeDriver: true},
        // )
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <TextInput multiline={true}/>
                </ScrollView>
                {/*<DraggableFlatList*/}
                {/*    contentContainerStyle={{paddingTop: HeaderHeight}}*/}
                {/*    data={this.state.data}*/}
                {/*    renderItem={this.renderItem}*/}
                {/*    keyExtractor={(item, index) => `draggable-item-${item.key}`}*/}
                {/*    onDragEnd={({data}) => this.setState({data})}*/}
                {/*    onScrollOffsetChange={(offset) => {*/}
                {/*        // this.state.scrollY = new Animated.Value(offset);*/}
                {/*        // console.log('onScrollOffsetChange', this.state.scrollY);*/}
                {/*        this.setState({*/}
                {/*            scrollY: new Animated.Value(offset),*/}
                {/*        })*/}
                {/*        // Animated.event(*/}
                {/*        //     [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],*/}
                {/*        //     {*/}
                {/*        //         useNativeDriver: false,*/}
                {/*        //     },*/}
                {/*        // );*/}
                {/*    }}*/}
                {/*/>*/}
                {/*{this.renderHeader()}*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        top: 0,
        height: 48,
        width: '100%',
        backgroundColor: '#40C4FF',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
    },
    label: {fontSize: 16, color: '#222'},
    tab: {elevation: 0, shadowOpacity: 0, backgroundColor: '#FFCC80'},
    indicator: {backgroundColor: '#222'},
});

export default TestDraggableFlatList;
