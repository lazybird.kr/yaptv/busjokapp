import React, {Component, memo, useMemo, useRef, useCallback} from 'react';
import {StyleSheet, View, SafeAreaView, Animated, Button} from 'react-native';
import {CollapsibleNavBarScrollView} from '@busfor/react-native-collapsible-navbar-scrollview';
import TestDraggableFlatList from './TestDraggableFlatList';

const HEADER_MAX_HEIGHT = 56;
const HEADER_MIN_HEIGHT = 0;

const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


class TestCollapsibleNavBarScrollView extends Component {

    animatedValue = new Animated.Value(0);
    scrollViewRef = React.createRef();
    viewRef = React.createRef();

    onPress = () => {
        if (this.scrollViewRef.current && this.viewRef.current) {
            this.scrollViewRef.current.scrollToView(this.viewRef.current);
        }
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <CollapsibleNavBarScrollView
                    ref={this.scrollViewRef}
                    headerMinHeight={HEADER_MIN_HEIGHT}
                    headerMaxHeight={HEADER_MAX_HEIGHT}
                    header={
                        <Animated.View
                            style={[
                                styles.header,
                                {
                                    backgroundColor: this.animatedValue.interpolate({
                                        inputRange: [0, HEADER_SCROLL_DISTANCE],
                                        outputRange: ['#16b1f7', '#16f7ac'],
                                    }),
                                },
                            ]}
                        />
                    }
                    // animatedValue={this.animatedValue}
                    // useNativeDriver={false}
                >
                    {/*<View style={styles.content} />*/}
                    {/*<View style={styles.content} />*/}
                    {/*<View ref={this.viewRef} style={[styles.content, styles.scrollToView]} />*/}
                    {/*<View style={styles.content} />*/}
                    {/*<View style={styles.content} />*/}
                    {/*<View style={styles.content} />*/}
                    {/*<Button title="Scroll to View" onPress={this.onPress} />*/}
                    <View>
                        <TestDraggableFlatList/>
                    </View>
                </CollapsibleNavBarScrollView>
            </SafeAreaView>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    header: {
        flex: 1,
    },
    content: {
        backgroundColor: '#b8e2f5',
        height: 200,
        marginBottom: 12,
    },
    scrollToView: {
        backgroundColor: '#b8bff5',
    },
});

export default TestCollapsibleNavBarScrollView;
