import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Text, ScrollView, KeyboardAvoidingView, Platform,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class EditPost extends Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: undefined,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    };

    render() {
        console.log(this.state.layout);
        return (
            <View style={styles.container}
                  onLayout={e => {
                      this.setState({
                          layout: e.nativeEvent.layout,
                      });
                  }}>
                <View style={{flex: 0.9, backgroundColor: '#aaafff'}}>

                </View>
                <View style={{flex: 0.1, backgroundColor: '#bbb000'}}>
                    <View style={{flex: 1}}>
                        <TextInput/>
                        <View style={{flex: 1, justifyContent: 'flex-end'}}>
                            <Text>
                                Bottom
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00cccc',
    },
});
