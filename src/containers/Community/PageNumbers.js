import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text, ScrollView, Image,
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {StyledText} from '../../components/StyledComponents';
import config from '../../config';

const baseColor = config.colors.baseColor;

export default class PageNumbers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: undefined,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    onPressPage = (item) => {
        const {onPressPageNumber} = this.props;
        onPressPageNumber(parseInt(item));
    };
    onPressArrowLeft = () => {
        const {onPressMovePage} = this.props;
        onPressMovePage(-1)
    };
    onPressArrowRight = () => {
        const {onPressMovePage} = this.props;
        onPressMovePage(1)
    };

    render() {
        const {
            pages,
            currentPage,
            handleTabCondition,
            handleTabRefresh,
            hideLeft,
            hideRight,
        } = this.props;

        let arrowLeft = null;
        let tabs = null;
        let arrowRight = null;

        if (this.state.layout) {
            const w = this.state.layout.width * 0.1;
            let tabStyle = {
                width: w,
                backgroundColor: 'rgba(255, 255, 255, 1.0)',
            };
            let arrowStyle = {
                width: w,
                backgroundColor: 'rgba(255, 255, 255, 1.0)',
            };
            tabs = pages.map((item, index) => {
                let textStyle = {
                    flex: 1,
                    justifyContent: 'center',
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    color: config.colors.greyColor,
                    lineHeight: config.size._136px,
                };
                if (item === currentPage) {
                    tabStyle = {
                        ...tabStyle,
                    };
                    textStyle = {
                        ...textStyle,
                        color: baseColor,
                    };
                }

                return (
                    <View
                        style={tabStyle}
                        key={'tab' + index}
                    >
                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={{flex: 1}}
                            onPress={() => this.onPressPage(item)}>
                            <StyledText style={[config.styles.text, textStyle]}>
                                {item}
                            </StyledText>
                        </TouchableOpacity>
                    </View>
                );
            });
            if (!hideLeft) {
                arrowLeft = (
                    <View style={arrowStyle}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={
                                {
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }
                            }
                            onPress={() => this.onPressArrowLeft()}>
                            <Image
                                source={config.images.prevPage}
                                style={{height: config.size._96px, width: config.size._96px}}
                            />
                            {/*<MaterialIcons name={'arrow-back-ios'}/>*/}
                        </TouchableOpacity>
                    </View>
                );
            }
            if (!hideRight) {
                arrowRight = (
                    <View style={arrowStyle}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={
                                {
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }
                            }
                            onPress={() => this.onPressArrowRight()}>
                            <Image
                                source={config.images.nextPage}
                                style={{height: config.size._96px, width: config.size._96px}}
                            />
                            {/*<MaterialIcons name={'arrow-forward-ios'}/>*/}
                        </TouchableOpacity>
                    </View>
                );
            }
        }

        return (
            <View style={styles.container}
                  onLayout={e => {
                      this.setState({
                          layout: e.nativeEvent.layout,
                      });
                  }}
            >
                <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}} horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                    {arrowLeft}
                    {tabs}
                    {arrowRight}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        textAlign: 'center',
        textAlignVertical: 'center',
    },
});
