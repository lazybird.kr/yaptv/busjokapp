import React, {Component} from 'react';
import {
    Appearance,
    Button,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    View,
    Dimensions,
    TouchableOpacity,
    PanResponder,
    Animated,
    TouchableWithoutFeedback,
    Image, KeyboardAvoidingViewComponent, Alert,
} from 'react-native';

import {actions, defaultActions, RichEditor, RichToolbar} from 'react-native-pell-rich-editor';
import {InsertLinkModal} from './insertLink';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Loading from './Loading';
import Comment from './Comment';
import Anonymous from './Anonymous';
import Post from './Post';
import Header from './Header';
import TestCollapsibleNavBarScrollView from './test/TestCollapsibleNavBarScrollView';
import RNDraftView from 'react-native-draftjs-editor';
import TestRNDraft from './test/TestRNDraft';

import {DragTextEditor} from 'react-native-drag-text-editor';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import config from 'src/config';
import axios from 'axios';
import PostModal from './PostModal';
import PopupDialog from '../../components/PopupDialog/PopupDialog';
import {getStatusBarHeight} from "react-native-status-bar-height";

const WINDOW = Dimensions.get('window');
const phizIcon = require('./assets/phiz.png');
const htmlIcon = require('./assets/h5.png');
const videoIcon = require('./assets/video.png');
const strikethrough = require('./assets/strikethrough.png');
const labelSubmit = '작성';
const HEIGHT_NAME_PASSWORD_VIEW = 60;
const NAVIGATOR_HEADER_HEIGHT = config.size._184px + 19;

export default class Editor extends Component {

    constructor(props) {
        super(props);
        this.scrollView = null;
        this.lastPointerPosition = {
            x: 0,
            y: 0,
        };
        this.lastScrollPosition = {
            x: 0,
            y: 0,
        };
        this.contentOffset = {
            x: 0,
            y: 0,
        };
        this.newPost = [];
        this._refPost = null;
        this.touch = {
            pressedSubmit: false,
        },
        this.state = {
            isFocused: false,
            toolbarStyle: {
                position: 'absolute',
                width: '100%',
                bottom: 0,
            },
            keyboardHeight: 0,
            editorHeight: 0,
            scrollViewInitFlag: false,
            onRequesting: false,
            toolbarLayout: {
                height: 0,
                width: 0,
            },
            containerLayout: {
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width,
            },
            scrollParentViewLayout: {
                height: 0,
                width: 0,
            },
            post: {},
            nameViewHeight: HEIGHT_NAME_PASSWORD_VIEW,
            commentHeight: '10%',
            commentImage: null,
            commentLayout: {
                height: 0,
                width: 0,
            },
            readOnly: false,
            currentGroupName: 'default',
            hideOption: true,
            editMode: false,
            doSubmitPost: false,
            doEditPost: false,
            showDialogDeletePost: false,
            showDialogDeleteMemberPost: false,
            showDialogEditPost: false,
            showDialogEditMemberPost: false,
            showDialogCancelNewPost: false,
            showDialogNeedLogin: false,
            showDialogCantMine: false,
            showDialogAlreadyReport: false,
            showDialogPasswordIncorrect: false,
            nameDeletePost: '',
            passwordDeletePost: '',
            postChanged: false,
        };
    }

    componentDidMount() {
        Appearance.addChangeListener(this.themeChange);
        Keyboard.addListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.addListener('keyboardDidHide', this.onKeyBoardHide);

        const {navigation} = this.props;
        this.subs = [
            navigation.addListener("didFocus", () => {
                this.setState({ isFocused: true });
            }),
            navigation.addListener("willBlur", () => {
                this.setState({ isFocused: false,});
            }),
        ];

        const {currentGroupName} = this.props.navigation.state.params;
        if (currentGroupName && currentGroupName.length > 0) {
            this.setState({
                currentGroupName: currentGroupName,
            });
        }
        const {read, postId} = this.props.navigation.state.params;
        if (read) {
            this.setState({
                post: {
                    post_id: postId,
                },
            });
        }
    }
    componentWillUnmount() {
        this.subs && this.subs.forEach((sub) => {
            sub.remove();
        });
        Appearance.removeChangeListener(this.themeChange);
        Keyboard.removeListener('keyboardDidShow', this.onKeyBoard);
        Keyboard.removeListener('keyboardDidHide', this.onKeyBoardHide);
    }
    onKeyBoard = (e) => {
        // console.log(Dimensions.get('window').height, e.endCoordinates.height);
        // console.log('onKeyBoard', TextInput.State.currentlyFocusedInput());
        TextInput.State.currentlyFocusedInput() && this.setState({emojiVisible: false});

        this.setState({
            toolbarStyle: {
                ...this.state.toolbarStyle,
                bottom: e.endCoordinates.height,
            },
            keyboardHeight: e.endCoordinates.height,
        });
    };
    onKeyBoardHide = (e) => {
        // console.log('onKeyBoardHide', Dimensions.get('window').height, e.endCoordinates.height);
        this.setState({
            toolbarStyle: {
                ...this.state.toolbarStyle,
                bottom: 0,
            },
            keyboardHeight: 0,
        });
    };
    setPostRef = (ref) => {
        this._refPost = ref;
    };
    onChangePost = (changed) => {
        this.setState({
            postChanged: changed,
        })
    };
    serviceReportPost = ({
                           host = config.url.apiSvr,
                           deviceId = config.deviceInfo.device_id,
                           sessionId = config.sessionInfo.session_id,
                           postId = '',
                           memberId = '',
                       } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'ReportPost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: postId,
            },
            member: {
                member_id: memberId,
            },
        };
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                if (!json) {
                    return;
                }
                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    if (json.err.includes('already')) {
                        this.setState({
                            showDialogAlreadyReport: true,
                        })
                    } else if (json.err.includes('my')) {
                        this.setState({
                            showDialogCantMine: true,
                        })
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceDeletePost = ({
                             host = config.url.apiSvr,
                             deviceId = config.deviceInfo.device_id,
                             sessionId = config.sessionInfo.session_id,
                             writtenBy = '',
                             memberId = this.state.post.written_by.member_id,
                             password = '',
                         } = {},
    ) => {
        const {navigation} = this.props;
        if (!memberId) {
            memberId = ''
        }
        let send_data = {
            category: 'board',
            service: 'DeletePost',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: this.state.post.post_id,
                written_by: {
                    member_id: memberId,
                    name: writtenBy,
                },
                password: password,
            },
        };
        console.log(send_data);
        this.setState({
            onRequesting: true,
        });
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    if (json.err.includes('password is not matched')) {
                        this.setState({
                            showDialogPasswordIncorrect: true,
                        })
                    }
                } else {
                    console.log('service 성공', send_data.service, json);
                    navigation.state.params.onGoBack();
                    navigation.goBack();
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    serviceVerifyPasswordEdit = ({
                             host = config.url.apiSvr,
                             deviceId = config.deviceInfo.device_id,
                             sessionId = config.sessionInfo.session_id,
                             writtenBy = '',
                             password = '',
                         } = {},
    ) => {
        let send_data = {
            category: 'board',
            service: 'VerifyPassword',
            device: {
                device_id: deviceId,
                session_id: sessionId,
            },
            post: {
                post_id: this.state.post.post_id,
                password: password,
            },
            member: {
                name: writtenBy,
            }
        };
        console.log(send_data);
        this.setState({
            onRequesting: true,
        });
        fetch(host, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                // console.log('service', res);
                if (res) {
                    return res.json();
                }
            })
            .then(json => {
                this.setState({
                    onRequesting: false,
                });
                if (!json) {
                    return;
                }

                if (json.err) {
                    console.log('service 실패', send_data.service, json);
                    this.setState({
                        showDialogPasswordIncorrect: true,
                    })
                } else {
                    console.log('service 성공', send_data.service, json);
                    if (json.data.ok) {
                        this.setState({
                            editMode: true,
                            hideOption: true,
                        });
                    } else {
                        this.setState({
                            showDialogPasswordIncorrect: true,
                        })
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
    };
    getMember = () => {
        let member = null;
        if (config.member && config.member.member_id && config.member.member_id.length > 0) {
            member = config.member;
        }
        return member;
    };
    onPressSubmit = (isEdit) => {
        if (this.touch.pressedSubmit) {
            return
        }
        this.touch.pressedSubmit = true;
        const {touch} = this;
        setTimeout(() => {
            touch.pressedSubmit = false
        }, 3000);
        if (isEdit) {
            this.setState({
                doEditPost: !this.state.doEditPost,
            });
        } else {
            this.setState({
                doSubmitPost: !this.state.doSubmitPost,
            });
        }
    };
    selectImage = (data) => {
        this.setState({
            commentImage: {
                uri: data.uri,
                width: data.width,
                height: data.height,
            },
        });
    };
    onPressDelete = () => {
        const member = this.getMember();
        if (this.state.post.written_by) {
            if (!this.state.post.written_by.member_id || this.state.post.written_by.member_id.length === 0) {

                if (Platform.OS === 'ios') {
                    this.setState({
                        hideOption: true,
                    })
                    setTimeout(() => this.setState({
                        showDialogDeletePost: true,
                        nameDeletePost: this.state.post.written_by.name,
                    }), 100);
                } else {
                    this.setState({
                        hideOption: true,
                        showDialogDeletePost: true,
                        nameDeletePost: this.state.post.written_by.name,
                    })
                }
            } else {
                if (member) {
                    if (member.member_id !== this.state.post.written_by.member_id) {
                        return;
                    }
                    if (Platform.OS === 'ios') {
                        this.setState({
                            hideOption: true,
                        })
                        setTimeout(() => this.setState({
                            showDialogDeleteMemberPost: true,
                            nameDeletePost: member.nickname,
                        }), 100);
                    } else {
                        this.setState({
                            hideOption: true,
                            showDialogDeleteMemberPost: true,
                            nameDeletePost: member.nickname,
                        })
                    }
                }
            }
        }
    };
    onPressEdit = () => {
        console.log('onPressEdit');
        const member = this.getMember();
        if (this.state.post.written_by) {
            if (!this.state.post.written_by.member_id || this.state.post.written_by.member_id.length === 0) {

                if (Platform.OS === 'ios') {
                    this.setState({
                        hideOption: true,
                    })
                    setTimeout(() => this.setState({
                        showDialogEditPost: true,
                        nameEditPost: this.state.post.written_by.name,
                    }), 100);
                } else {
                    this.setState({
                        hideOption: true,
                        showDialogEditPost: true,
                        nameEditPost: this.state.post.written_by.name,
                    })
                }
            } else {
                if (member) {
                    if (member.member_id !== this.state.post.written_by.member_id) {
                        return;
                    }
                    this.setState({
                        editMode: true,
                        hideOption: true,
                    });
                }
            }
        }
    };
    onPressCancelEdit = () => {
        this.setState({
            editMode: false,
        });
    };
    onPressReport = () => {
        const member = this.getMember();

        if (member) {
            if (member.member_id === this.state.post.written_by.member_id) {
                if (Platform.OS === 'ios') {
                    this.setState({
                        hideOption: true,
                    })
                    setTimeout(() => this.setState({
                        showDialogCantMine: true,
                    }), 100);
                } else {
                    this.setState({
                        hideOption: true,
                        showDialogCantMine: true,
                    })
                }
            } else {
                if (Platform.OS === 'ios') {
                    this.setState({
                        hideOption: true,
                    })
                    setTimeout(() => this.setState({
                        showDialogReport: true,
                    }), 100);
                } else {
                    this.setState({
                        hideOption: true,
                        showDialogReport: true,
                    })
                }
            }
        } else {
            // TODO: 익명

            if (Platform.OS === 'ios') {
                this.setState({
                    hideOption: true,
                })
                setTimeout(() => this.setState({
                    showDialogNeedLogin: true,
                }), 100);
            } else {
                this.setState({
                    hideOption: true,
                    showDialogNeedLogin: true,
                })
            }
        }
    };
    onPressOption = () => {
        this.setState({
            hideOption: !this.state.hideOption,
        });
    };
    onPressGoBack = (param) => {
        const {navigation} = this.props;
        let isEdited = false;
        console.log(this._refPost.state.data);
        if (this._refPost) {
            const {data, title} = this._refPost.state;
            for (let i = 0; i < data.length; i++) {
                let e = data[i];
                if (!this.isReadOnly()) {
                    if (e.text && e.text.length > 0) {
                        isEdited = true;
                    } else if (e.imageSrc && e.imageSrc.uri) {
                        isEdited = true;
                    } else if (title && title.length > 0) {
                        isEdited = true;
                    }

                }
                if (isEdited) {
                    this.setState({
                        showDialogCancelNewPost: true,
                    });
                    return;
                }
            }
        }
        if (param) {
            navigation.state.params.onGoBack(param);
        } else {
            navigation.state.params.onGoBack({groupName: this.state.currentGroupName});
        }
        navigation.goBack();
    };
    goBack = () => {
        const {navigation} = this.props;
        navigation.state.params.onGoBack({groupName: this.state.currentGroupName});
        navigation.goBack();
    };
    goBackToFirstPage = () => {
        const {navigation} = this.props;
        console.log('goBackToFirstPage');
        navigation.state.params.onGoBack({groupName: '', pageNumber: 1});
        navigation.goBack();
    };
    goBackToLoginPage = () => {
        const {navigation} = this.props;
        console.log('goBackToLoginPage');
        navigation.navigate('loginScreen');
        // navigation.state.params.onGoBackToLogin();
        // navigation.goBack();
    };
    isReadOnly = () => {
        const {read} = this.props.navigation.state.params;
        return (read || this.state.readOnly) && !this.state.editMode;
    };

    isEnabledDelete = () => {
        const member = this.getMember();
        const {post} = this.state;
        if (post.written_by) {
            if (!post.written_by.member_id || post.written_by.member_id.length === 0) {
                return true;
            } else {
                if (member) {
                    if (member.member_id === post.written_by.member_id) {
                        return true;
                    }
                } else {
                    if (!post.written_by.member_id || post.written_by.member_id.length === 0) {
                        if (config.deviceInfo.device_id === post.written_by.device_id) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    };
    isMyPost = () => {
        const member = this.getMember();
        const {post} = this.state;
        if (post.written_by) {
            if (member) {
                if (member.member_id === post.written_by.member_id) {
                    return true;
                }
            } else {
                if (!post.written_by.member_id || post.written_by.member_id.length === 0) {
                    if (config.deviceInfo.device_id === post.written_by.device_id) {
                        return true;
                    }
                }
            }
        }
        return false;
    };
    setPost = (post) => {
        this.setState({
            post: post,
        })
    };
    setReadOnly = () => {
        this.setState({
            readOnly: true,
            editMode: false,
        });
        this.touch.pressedSubmit = false
        this.goBackToFirstPage()
    };
    setRequesting = (isRequest) => {
        this.setState({
            onRequesting: isRequest,
        })
    };
    // TODO: Editor 를  TextInput 으로 할 지 RichEditor 로 할 지 결정해야 한다.
    render() {
        const that = this;
        const {navigation} = this.props;
        const {serviceDeletePost, serviceVerifyPasswordEdit, goBack, goBackToLoginPage, serviceReportPost} = this;
        const enableDelete = this.isEnabledDelete();
        const enableReport = this.isMyPost();
        let isReadStatus = this.isReadOnly();
        const member = this.getMember();

        return (
            <View style={[styles.container]}
                  onLayout={e => {
                      this.setState({
                          containerLayout: e.nativeEvent.layout,
                      });
                  }}>
                <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0}}/>
                {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
                {/*<StatusBar barStyle={theme !== 'dark' ? 'dark-content' : 'light-content'}/>*/}
                {/*<View style={styles.nav}>*/}
                {/*    <View style={styles.navBack}>*/}
                {/*        <TouchableOpacity*/}
                {/*            style={styles.navBackButton}*/}
                {/*            onPress={() => navigation.goBack()}>*/}
                {/*            <MaterialIcons name={'arrow-back-ios'} size={24} color={'#000000'}/>*/}
                {/*        </TouchableOpacity>*/}
                {/*    </View>*/}
                {/*    <View style={styles.navTitle}>*/}
                {/*        {isReadStatus ?*/}
                {/*            <Text*/}
                {/*                style={[styles.input]}*/}
                {/*                maxLength={1024}*/}
                {/*            >*/}
                {/*                {this.state.post.title}*/}
                {/*            </Text>*/}
                {/*            :*/}
                {/*            <TextInput*/}
                {/*                style={[styles.input]}*/}
                {/*                placeholder="제목을 입력하세요"*/}
                {/*                maxLength={1024}*/}
                {/*                onChangeText={this.onChangeTitle}*/}
                {/*            />*/}
                {/*        }*/}
                {/*    </View>*/}
                {/*    <View style={styles.navSubmit}>*/}
                {/*        {isReadStatus ? null :*/}
                {/*            <TouchableOpacity*/}
                {/*                onPress={() => that.onPressSubmit(that)}>*/}
                {/*                <Text style={{*/}
                {/*                    textAlign: 'center',*/}
                {/*                    textAlignVertical: 'center',*/}
                {/*                    fontSize: 18,*/}
                {/*                    fontFamily: 'Roboto',*/}
                {/*                    color: '#eb6a0b',*/}
                {/*                    fontWeight: 'bold',*/}
                {/*                }}>*/}
                {/*                    {labelSubmit}*/}
                {/*                </Text>*/}
                {/*            </TouchableOpacity>*/}
                {/*        }*/}
                {/*    </View>*/}
                {/*</View>*/}
                <View style={styles.nav}>
                    <Header
                        isReadStatus={isReadStatus}
                        editMode={this.state.editMode}
                        navigation={navigation}
                        post={this.state.post}
                        onPress={this.onPressSubmit}
                        onPressGoBack={this.onPressGoBack}
                        onPressOption={this.onPressOption}
                        onPressCancel={this.onPressCancelEdit}
                        onPressEdit={() => this.onPressSubmit('edit')}
                        postChanged={this.state.postChanged}
                    />
                </View>
                <View style={[styles.post]}>
                    <Post
                        readOnly={isReadStatus}
                        setReadOnly={this.setReadOnly}
                        post={this.state.post}
                        setPost={this.setPost}
                        setRequesting={this.setRequesting}
                        groupName={this.state.currentGroupName}
                        editMode={this.state.editMode}
                        onPressGoBack={this.onPressGoBack}
                        doSubmitPost={this.state.doSubmitPost}
                        doEditPost={this.state.doEditPost}
                        setRef={this.setPostRef}
                        disableInquiry={true}
                        onChangePost={this.onChangePost}
                        goBackToLoginPage={goBackToLoginPage}
                    />
                </View>
                {this.state.onRequesting ?
                    <View style={{position: 'absolute', height: '100%', width: '100%'}}>
                        <Loading color={'#eb6a0b'}/>
                    </View>
                    : null}

                {!this.state.hideOption ?
                    <PostModal
                        closeModal={this.onPressOption}
                        isVisible={!this.state.hideOption}
                        onPressDelete={this.onPressDelete}
                        onPressEdit={this.onPressEdit}
                        onPressReport={this.onPressReport}
                        enableDelete={enableDelete}
                        enableReport={enableReport}
                    />
                    : null}

                <PopupDialog
                    visible={this.state.showDialogDeletePost}
                    // title={'비밀번호를 입력하세요'}
                    message={'게시물을 삭제하시겠습니까?'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogDeletePost: false,
                        });
                    }}
                    rightButtonLabel={'삭제'}
                    rightButtonStyle={{backgroundColor: config.colors.baseColor}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogDeletePost: false,
                        });
                        serviceDeletePost({writtenBy: this.state.nameDeletePost, password: this.state.passwordDeletePost});
                    }}
                    inputTitle={'비밀번호 입력'}
                    onChangeText={(text) => {
                        this.setState({
                            passwordDeletePost: text,
                        })
                    }}
                    secureTextEntry={true}
                />
                <PopupDialog
                    visible={this.state.showDialogEditPost}
                    // title={'비밀번호를 입력하세요'}
                    message={'비밀번호를 입력하세요'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogEditPost: false,
                        });
                    }}
                    rightButtonLabel={'확인'}
                    rightButtonStyle={{backgroundColor: config.colors.baseColor}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogEditPost: false,
                        });
                        serviceVerifyPasswordEdit({writtenBy: this.state.nameEditPost, password: this.state.passwordEditPost});
                    }}
                    inputTitle={'비밀번호 입력'}
                    onChangeText={(text) => {
                        this.setState({
                            passwordEditPost: text,
                        })
                    }}
                    secureTextEntry={true}
                />
                <PopupDialog
                    visible={this.state.showDialogDeleteMemberPost}
                    // title={'비밀번호를 입력하세요'}
                    message={'게시물을 삭제하시겠습니까?'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogDeleteMemberPost: false,
                        });
                    }}
                    rightButtonLabel={'삭제'}
                    rightButtonStyle={{backgroundColor: '#ff2c45'}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogDeleteMemberPost: false,
                        });
                        serviceDeletePost({writtenBy: this.state.nameDeletePost, password: this.state.passwordDeletePost});
                    }}
                    />
                <PopupDialog
                    visible={this.state.showDialogCancelNewPost}
                    // title={'비밀번호를 입력하세요'}
                    message={'"확인" 버튼을 누르면 작성 중인 글이 저장되지 않습니다.'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogCancelNewPost: false,
                        });
                    }}
                    rightButtonLabel={'확인'}
                    rightButtonStyle={{backgroundColor: '#ff2c45'}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogCancelNewPost: false,
                        });
                        goBack();
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogNeedLogin}
                    // title={'비밀번호를 입력하세요'}
                    message={'로그인이 필요합니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogNeedLogin: false,
                        });
                        goBackToLoginPage();
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogAlreadyReport}
                    // title={'비밀번호를 입력하세요'}
                    message={'이미 신고했습니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogAlreadyReport: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogPasswordIncorrect}
                    // title={'비밀번호를 입력하세요'}
                    message={'비밀번호가 일치하지 않습니다'}
                    centerButtonLabel={'확인'}
                    onCenterButtonPress={() => {
                        this.setState({
                            showDialogPasswordIncorrect: false,
                        });
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogReport}
                    message={'해당 게시물을 검토 후 허위신고인 경우\n 대응하지 않을 수 있습니다'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogReport: false,
                        });
                    }}
                    rightButtonLabel={'신고'}
                    rightButtonStyle={{backgroundColor: '#ff2c45'}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogReport: false,
                        });
                        if (member) {
                            serviceReportPost({memberId: member.member_id, postId: this.state.post.post_id});
                        }
                    }}
                />
                <PopupDialog
                    visible={this.state.showDialogCancelNewPost}
                    // title={'비밀번호를 입력하세요'}
                    message={'"확인" 버튼을 누르면 작성 중인 글이 저장되지 않습니다.'}
                    leftButtonLabel={'취소'}
                    onLeftButtonPress={() => {
                        this.setState({
                            showDialogCancelNewPost: false,
                        });
                    }}
                    rightButtonLabel={'확인'}
                    rightButtonStyle={{backgroundColor: '#ff2c45'}}
                    rightButtonTextStyle={{color: '#ffffff'}}
                    onRightButtonPress={() => {
                        this.setState({
                            showDialogCancelNewPost: false,
                        });
                        goBack();
                    }}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"white",

        // flexDirection: 'column',
        // backgroundColor: '#FF00FF',
    },
    nav: {
        // position: 'absolute',
        height: NAVIGATOR_HEADER_HEIGHT,
        // top: 0,
        // width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#ffffff',
        // marginHorizontal: 5,
        // borderBottomWidth: 1,
        // borderBottomColor: '#f5d9cc',
        zIndex: 99,
    },
    post: {
        // position: 'absolute',
        // top: NAVIGATOR_HEADER_HEIGHT * 7,
        flex: 1,
        width: '100%',
        backgroundColor: '#ff0000',
    },
    rich: {
        //backgroundColor: '#00ff00',
        //minHeight: 300,
        //minHeight: 300,
        flex: 1,
    },
    empty: {
        // minHeight: 24,
        flex: 0.1,
    },
    richBar: {
        height: 50,
        backgroundColor: '#F5FCFF',
    },
    scroll: {
        // height: '80%',
        flex: 1,
        // backgroundColor: '#ffff00',
    },
    item: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: '#e8e8e8',
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    input: {
        flex: 1,
        textAlign: 'left',
        textAlignVertical: 'center',
        fontSize: 18,
        borderWidth: 0,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    nickname: {
        // textAlignVertical: 'top',
        backgroundColor: 'rgba(217, 217, 217, 1.0)',
        borderRadius: 10,
        margin: 10,
    },
    password: {
        backgroundColor: 'rgba(217, 217, 217, 1.0)',
        borderRadius: 10,
        margin: 10,
    },
    tib: {
        textAlign: 'center',
        color: '#515156',
    },
});
