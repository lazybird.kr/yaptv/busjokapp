import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TextInput,
    Text,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import config from '../../config';
import {StyledText, StyledTextInput} from '../../components/StyledComponents';

const message = '비회원의 경우, 닉네임과 비밀번호를 설정하면 글을 작성할 수 있습니다.';

export default class Anonymous extends Component {

    constructor(props) {
        super(props);
        this._refNickname = null;
        this._refPassword = null;
        this.state = {
            post_info: {
                nickname: '',
                password: '',
            },
        };
    }

    componentDidMount() {
        const that = this;
        const {onChangeNickname} = this.props;
        const {onChangePassword} = this.props;

        // 익명 정보 저장하기
        // AsyncStorage.getItem('post_info').then(post_info => {
        //     if (post_info) {
        //         console.log('Anonymous', post_info);
        //         let p = JSON.parse(post_info);
        //         that.setState({
        //             post_info: p,
        //         });
        //         onChangeNickname(p.nickname);
        //         onChangePassword(p.password);
        //     }
        // });
        const {setRef} = this.props;
        if (setRef) {
            setRef(this);
        }
        const {focusNickname, focusPassword, resetFocus} = this.props;
        if (focusNickname) {
            this.focusNickname();
        }
        if (focusPassword) {
            this.focusPassword();
        }
        if (resetFocus) {
            resetFocus();
        }
        const {nickname, password} = this.props;
        let n = '';
        let p = '';
        if (nickname) {
            n = nickname;
        }
        if (password) {
            p = password;
        }
        this.setState({
            post_info: {
                nickname: n,
                password: p,
            },
        });
        const {editMode} = this.props;
        if (editMode) {
            this.onChangeNickname(nickname)
        }
    }

    componentWillUnmount() {
    }
    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        const {focusNickname, focusPassword, resetFocus} = this.props;
        if (prevProps.focusNickname !== focusNickname && focusNickname) {
            this.focusNickname();
            if (resetFocus) {
                resetFocus();
            }
        }
        if (prevProps.focusPassword !== focusPassword && focusPassword) {
            this.focusPassword();
            if (resetFocus) {
                resetFocus();
            }
        }
    }

    onChangeNickname = (text) => {
        const {onChangeNickname} = this.props;
        onChangeNickname(text);
        let post_info = {
            ...this.state.post_info,
            nickname: text,
        };
        this.setState({
            post_info,
        });
        AsyncStorage.setItem('post_info', JSON.stringify(post_info));
    };
    onChangePassword = (text) => {
        const {onChangePassword} = this.props;
        onChangePassword(text);
        let post_info = {
            ...this.state.post_info,
            password: text,
        };
        this.setState({
            post_info,
        });
        AsyncStorage.setItem('post_info', JSON.stringify(post_info));
    };
    focusNickname = () => {
        this._refNickname && this._refNickname.getInnerRef().focus();
    };
    focusPassword = () => {
        this._refPassword && this._refPassword.getInnerRef().focus();
    };

    render() {
        console.log(this.state.post_info, this.state.post_info.nickname);
        const {editMode, nickname} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.messageContainer}>
                    <StyledText style={[config.styles.text2, styles.message]}>
                        {message}
                    </StyledText>
                </View>
                <View style={{flexDirection: 'row', height: config.size._140px}}>
                    {editMode ?
                        <StyledTextInput
                            ref={(ref) => this._refNickname = ref}
                            style={[config.styles.text, styles.textInput]}
                            defaultValue={nickname}
                            editable={false}
                        /> :
                        <StyledTextInput
                            ref={(ref) => this._refNickname = ref}
                            style={[config.styles.text, styles.textInput]}
                            placeholder="닉네임"
                            placeholderTextColor={config.colors.greyColor}
                            onChangeText={this.onChangeNickname}
                            value={this.state.post_info.nickname}
                        />
                    }
                    <StyledTextInput
                        ref={(ref) => this._refPassword = ref}
                        style={[config.styles.text, styles.textInput]}
                        secureTextEntry={true}
                        placeholder="비밀번호"
                        placeholderTextColor={config.colors.greyColor}
                        onChangeText={this.onChangePassword}
                        value={this.state.post_info.password}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
    },
    messageContainer: {
        height: config.size._80px,
        backgroundColor: '#e0e0e0',
        // borderTopWidth: 1,
        // borderTopColor: config.colors.lineColor,
        borderBottomWidth: config.size._2px,
        borderBottomColor: config.colors.lineColor,
    },
    message: {
        fontSize: config.fontSize._40px,
        color: config.colors.greyColor,
        textAlign: 'center',
        lineHeight: config.size._70px,
    },
    textInput: {
        fontSize: config.fontSize._48px,
    },
});
