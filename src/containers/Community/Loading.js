import React from 'react';
import {
    StyleSheet,
    View,
    ActivityIndicator,
} from 'react-native';

export default function Loading(props) {
    let indicatorSize = "large";
    let indicatorColor = "#0000ff";
    // console.log('props', props);
    if (props) {
        const {size, color} = props;
        if (size) {
            indicatorSize = size;
        }
        if (color) {
            indicatorColor = color;
        }
    }
    return (
        <View style={[styles.container, styles.horizontal]}>
            <ActivityIndicator
                size={indicatorSize}
                color={indicatorColor} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});
