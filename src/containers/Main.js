import React, {Component} from 'react';
import { createSwitchNavigator, createAppContainer,  } from 'react-navigation';
import axios from 'axios';
import { createBottomTabNavigator, /*BottomTabBar*/ } from 'react-navigation-tabs';
import { View, Image, StatusBar, Linking, BackHandler, Platform } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import config from 'src/config'
import { HomeStack } from 'src/containers/Home'
import { BusStack } from 'src/containers/Bus'
import { CommunityStack } from 'src/containers/Community'
import { QrStack } from 'src/containers/Qr'
import { MyInfoStack } from 'src/containers/MyInfo'
import NavigationService from 'src/components/Notification/NavigationService';
import {notification} from 'src/components/Notification'
import dynamicLinks from '@react-native-firebase/dynamic-links';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from "@react-native-community/async-storage";
import PushNotification from "react-native-push-notification"
import LottieView from 'lottie-react-native';
import BottomTabBar from 'src/components/bottomTabBar/BottomTabBar';
import showToast from 'src/components/Toast';
import {isNetworkAvailable} from 'src/components/NetworkUtil';



const tabBarOptions = {
  activeTintColor: '#d64409',
  inactiveTintColor: '#c1c1c1',
  showLabel: false,
  tabStyle:{backgroundColor:"red"},
  style:{borderTopWidth:0, backgroundColor:"blue"}
}

const barStyle = {
  //backgroundColor: '#FFC0CB',
  backgroundColor: 'blue',
}

const tabBarOnPress = ({ navigation, route }) => {
  navigation.popToTop();
  navigation.navigate(navigation.state.routeName);
};
const tabBarIcon = (icon, selectedIcon) => ({focused, tintColor}) => (
  <View>
    { !focused ?
    <Ionicons name={icon} size={25}  />
    :
    <Ionicons name={selectedIcon} size={25} />
    }
  </View>
)

const tabBarImage = (imgae, selectedImage, flag) => ({focused, tintColor}) => (
  <View>
    { !focused ?
      <Image source={imgae} style={{width: flag? 50 : 24, height: flag? 50 : 24, resizeMode:"contain"}}  />
      :
      <Image source={selectedImage} style={{opacity:1, width: flag? 50 : 24, height: flag? 50 : 24, resizeMode:"contain"}} />
    }
  </View>
)

const Tabs = createBottomTabNavigator({
  homeTab:{
    screen : HomeStack,
    navigationOptions: {
      //tabBarIcon: tabBarIcon("home-outline", "home"),
      tabBarIcon: tabBarImage(config.images.gnbHomeOff, config.images.gnbHomeOn),
      //tabBarOptions: tabBarOptions,
      tabBarOnPress: tabBarOnPress
    },
  },

  busTab: {
    screen : BusStack,
    navigationOptions: {
      //tabBarIcon: tabBarIcon("ios-bus-outline", "ios-bus"),
      tabBarIcon: tabBarImage(config.images.gnbBusOff, config.images.gnbBusOn),
      tabBarOnPress: tabBarOnPress
    },
  },
  qrTab:{
    screen : QrStack,
    navigationOptions: {
      //tabBarIcon: tabBarIcon("ios-qr-code-outline", "ios-qr-code"),
      tabBarIcon: tabBarImage(config.images.gnbQROff, config.images.gnbQROn2, true),
      tabBarOnPress: tabBarOnPress,
    },
  },
  communityTab: {
    screen : CommunityStack,
    navigationOptions: {
      //tabBarIcon: tabBarIcon("ios-people-outline", "ios-people"),
      tabBarIcon: tabBarImage(config.images.gnbBoardOff, config.images.gnbBoardOn),
      tabBarOnPress: tabBarOnPress,
    },
  },
  aboutTab:{
    screen : MyInfoStack,
    navigationOptions: {
      //tabBarIcon: tabBarIcon("ios-person-outline", "ios-person"),
      tabBarIcon: tabBarImage(config.images.gnbMyOff, config.images.gnbMyOn),
      tabBarOnPress: tabBarOnPress
    },
  },
  },{

    tabBarOptions: {
      activeTintColor: '#d64409',
      inactiveTintColor: '#c1c1c1',
      showLabel: false,
      style: {
        //height:100,
        borderTopColor: "#e2e2e2",
        borderTopWidth:1,
        shadowOpacity: 0.1,
        elevation: 3,
        //zIndex:1,
        backgroundColor: 'transparent' // TabBar background 아래 주석 풀면 transparent 로 변경, 아니면 #ffffff

      }
    },


    tabBarComponent: props => {
      console.log("tabBar : ", props)
      return (<BottomTabBar {...props} />)
    }

    /*
    tabBarComponent: props =>{
      console.log("tabBar : ", props)
      return(
          <React.Fragment>
              <BottomTabBar {...props} />
              <Image source={config.images.tabBar} style={{position:"absolute",bottom:Platform.OS === "ios"? 35 : 0, zIndex:-1,
                backgroundColor: "transparent", width:"100%", height:50, resizeMode:"stretch"}}/>

          </React.Fragment>
      )
    }
    */
 })

const MainStack = createSwitchNavigator({
  main: Tabs,
  // login: LoginStack,

});

const AppContainer = createAppContainer(Tabs);

export default class Main extends Component {

  constructor(props){
    super(props);
    DeviceInfo.isEmulator().then(isEmulator => {
      if(!isEmulator){
        notification.mount(this.getTokenCallback)
      }
    })
  }

  state={
    loading : false,
    didGetToken : false,
  }

  handleDynamicLink = link => {
    // 다이나믹 링크 별로 분기 탐
    if (link && link.url) {
      console.log("link --> :", link);
      const s = link.url.split(config.url.host)
      if (s && s.length === 2) {
        const params = s[1].split("/");
        console.log('params', params);
        if (params && params.length > 1) {
          if (params[1] === 'post') {
            if (params.length === 3) {
              console.log('post_id', params[2]);
              NavigationService.navigate("communityScreen", {fromSharedLink: true, postId: params[2], at: Date.now()})
            }
          } else {
            // 나머지는 사다리게임화면으로 이동
            console.log("link.url : ", link.url)
           // NavigationService.navigate("ladderGameScreen")
           NavigationService.navigate("homeScreen", {ladderPopup:true})
          }
        }
      } else {
        NavigationService.navigate("homeScreen")
      }
    }
  };

  setInitData = async () => {
    const uniqueId = DeviceInfo.getUniqueId();
    const access_info = await AsyncStorage.getItem("access_info");
    let access_token = "";
    if(access_info){
      const d = JSON.parse(access_info);
      access_token = d.access_token;
      config.access_info = d;
      console.log("access_info.access_token : ", access_token);
    }
    console.log("config.notificationToken : ", config.notificationToken)
    const brandName = DeviceInfo.getBrand();
    const systemVersion = DeviceInfo.getSystemVersion();
    const deviceModel = DeviceInfo.getModel();
    /*
    const manufacturer = await DeviceInfo.getManufacturer();
    const product = await DeviceInfo.getProduct();
    const hardware = await DeviceInfo.getHardware();
    const device = await DeviceInfo.getDevice();
    */

    try {
      console.log("AAAA1")
      const isConnected = await isNetworkAvailable();
      if(!isConnected) {
        throw new Error("네트워크 상태를 확인해 주세요.")
      }
      console.log("AAAA2", uniqueId)
      const resp = await axios.post(config.url.apiSvr + "/public/GetSession", {
        "data": {
          "access_token" : access_token || "",
          "device": {
            "device_id": uniqueId,
            "notification_token": config.notificationToken,
            "platform": Platform.OS === "android" ? "android" : "ios",
            "info": brandName + ", " + deviceModel + ", " + systemVersion
          },
        }
      }, {timeout:4000})
      console.log("resp : ", resp)
      const {device} = resp.data.data;
      const {member} = resp.data.data;
      console.log("session id:", device.session_id)
      config.sessionInfo.session_id = device.session_id;
      config.deviceInfo.device_id = uniqueId;
      if (member) {
        config.member = member;
      } else {
        await AsyncStorage.removeItem("access_info")
        config.member = {
          member_id: '',
          nickname: '',
        };
        config.access_info = null;
        // TODO: 주석 풀어야함. 테스트용도로 주석해놨음. 익명 기능 추가되면 주석 풀것
        // config.member = {
        //   member_id: '',
        //   nickname: ''
        // }
      }
      setTimeout(()=>this.setState({loading:true}),2000);

    } catch(e){
      console.log("MainComponent  getSession error : ", e)
      showToast(e.message, "LONG");
    }
  }
  getTokenCallback = () => {
    this.setState({didGetToken : true})
  }

  componentDidUpdate(prevProps, prevState ){
    if(prevState.didGetToken !== this.state.didGetToken && this.state.didGetToken){
      this.setInitData();
    }
    if(prevState.loading !== this.state.loading && this.state.loading){
      this.recoveringNotification();

      /* backgound or terminated dynamic link */
      dynamicLinks()
      .getInitialLink()
      .then(link => {
        this.handleDynamicLink(link);
      });
    }
  }

  recoveringNotification = ()=>{
    //IOS에서 JS debugger 키면 메시지 안들어 오는 경우 있음
    //여기에서 받은 데이터별로 화면 분기하면 됨, 일단은 버스탭으로 이동
    PushNotification.popInitialNotification((remoteMessage) => {
      if (remoteMessage) {
        console.log("recoveringNotification :", remoteMessage.data);
        const {data} = remoteMessage;
        if (data && data.title && data.body) {
          const body = JSON.parse(data.body);
          if (data.title === "post") {
            NavigationService.navigate("communityScreen", {fromSharedLink: true, postId: body.post_id, at: Date.now()})
          } else  if (data.title === "bus") {
            NavigationService.navigate("busScreen");
            NavigationService.navigate("StationDetail", {arsId: body.ars_id,});
          }
        } else {
          NavigationService.navigate("homeTab")
        }
      }
    });
  }

  componentDidMount() {
    /* foreground dynamic link */
    dynamicLinks().onLink(this.handleDynamicLink);


    DeviceInfo.isEmulator().then(isEmulator => {
      console.log("isEmulator : ", isEmulator)
      isEmulator && this.getTokenCallback();
    });
    /*
    this.unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      this.netInterval && clearInterval(this.netInterval);
      if(!state.isConnected || !state.isInternetReachable){
        this.netInterval = setInterval(()=>showToast('현재 네트워크에 연결되어 있지 않습니다.'), 2000);
      }  else if(state.type === "cellular"){
        showToast('현재 모바일 데이터에 연결되어 있습니다.');
      } else if (state.type === "wifi"){
        showToast('현재 WiFi에 연결되어 있습니다.');
      }
    });
    */
  }
  componentWillUnmount() {
    console.log("MainComponent WillUnMount");
    //this.unsubscribe();
  }

  render() {
    const {loading, didGetToken }= this.state;
    console.log("load", loading, didGetToken)
    if(!loading || !didGetToken){
      return (
        <View style={{flex:1, backgroundColor: '#ffffff'}}>
          <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
          <LottieView source={require("src/../assets/busjok_main3.json")} autoPlay loop={false} />
          
        </View>)
    }

    return (
        <AppContainer ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}>
        </AppContainer>


    );
  }
}





/* 테스트용 다이나믹 링크 만들기
    async function buildLink() {

      const link2 = await dynamicLinks().buildShortLink({
        link: 'http://www.yaptv.kr/post/12345678',
        // domainUriPrefix is created in your Firebase console
        domainUriPrefix: 'https://busjok.page.link',
        // optional set up which updates Firebase analytics campaign
        // "banner". This also needs setting up before hand
        social:{
          title:"테스트",
          descriptionText:"설명입니다.",
          imageUrl:"https://images.all-free-download.com/images/graphiclarge/modern_science_and_technology_picture_03_hd_picture_168739.jpg"
        },
        android: {
          packageName:"com.yaptv.busjok",
        },
      }, dynamicLinks.ShortLinkType.UNGUESSABLE);


      console.log("buildShortLink : ", link2)
      return link2;
    }
    buildLink();
    */

