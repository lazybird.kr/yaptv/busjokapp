import AboutScreen from './AboutScreen';
import { createStackNavigator } from 'react-navigation-stack';
import { LoginStack } from '../Login'
import { MyInfoStack } from '../MyInfo'

const AboutStack=createStackNavigator({
  aboutScreen: {
    screen: AboutScreen,
    navigationOptions:{
      headerShown: false
    }
  },
  loginScreen: {
    screen: LoginStack,
    navigationOptions:{
      headerShown: false
    }
  },
  myInfoScreen : { 
    screen : MyInfoStack,
    navigationOptions:{
      headerShown: false
    }
  },
},
  {
    headerMode: 'none',
    initialRouteName: 'aboutScreen'
  }

);

export {
  AboutStack,  
}