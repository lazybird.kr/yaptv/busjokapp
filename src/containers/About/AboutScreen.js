import React, { Component }  from "react";
import config from 'src/config';

export default class AboutScreen extends Component {
  state = {
    isFocused : false,
    // isLogin: 'initial',
    // isLoading: false
  }

  componentDidMount() {
    // console.log("=======================>ABOUT SCREEN mount")
    const {navigation} = this.props;
    this.subs = [
      navigation.addListener(
        "didFocus", 
        () => {
          console.log("about screen didFocus"); 
          this.setState({ 
            isFocused: true, 
            // isLoading: true 
          });
        }),
      navigation.addListener(
        "willBlur", 
        () => {
          console.log("about screen willBlur"); 
          this.setState({ 
            isFocused: false, 
            // isLoading: false
          });
        }),
      // navigation.addListener("willFocus", () => {console.log("about screen willFocus"); this.setState({ isFocused: true});}),
      // navigation.addListener("willFocus", () => console.log("about screen willFocus")),
      // navigation.addListener("didBlur", () => console.log("about screen didBlur")),
    ];

    this.checkLoginStatus();

  }

  componentWillUnmount() {
    this.subs && this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  componentDidUpdate(prevProps, prevState){

    const { isFocused } = this.state;
    if(prevState.isFocused != isFocused && isFocused){
      this.checkLoginStatus();
    }
  }

  checkLoginStatus = () => {

    if (config.access_info) {
      this.props.navigation.navigate("myInfoScreen", {
        access_info: config.access_info
      })

    }
    else {
      this.props.navigation.navigate("loginScreen");
    }

  }

  navigateScreen = (screen) => {
    this.props.navigation.navigate(screen)
  }


  render() {
    return <></>
  }
}
