import { Platform} from "react-native";
import config from 'src/config';

export const calcZoom = (zoomArray, zoomString) => {
    let smallDiff, tmpDiff;
    let closestIndex = 0;

    let index = zoomArray.indexOf(zoomString);
    if(index >= 0){
      return index / zoomArray.length;
    } else {
      for(let i=0; i<zoomArray.length; i++){
        if(i===0){
          smallDiff = Math.abs(parseInt(zoomString) - parseInt(zoomArray[i]));
          closestIndex = 0
        }
        tmpDiff = Math.abs(parseInt(zoomString) - parseInt(zoomArray[i]));
        if(smallDiff > tmpDiff){
          smallDiff = tmpDiff;
          closestIndex = i;
        }    
      }
      return closestIndex / zoomArray.length;
    }
  }

  export const getZoomScale = (zoom, zoomArray) => {
    if(Platform.OS === 'ios'){
      console.log("zoom : ", zoom)
      let maxZoom = config.cameraDeviceMaxZoomIOS;
      let scale = (maxZoom - 1) * zoom + 1;
      return scale.toFixed(1);
    } else {
      if(zoom === 0.0){
        return (1.0).toFixed(1);
      }
      if(zoom === 1.0){
        let tmpScale = zoomArray[zoomArray.length-1] / 100;
        return tmpScale.toFixed(1);
      }
      
      let index = zoomArray.length * (zoom.toFixed(2));
      let scale = zoomArray[index.toFixed(0)] / 100;
      
      return scale.toFixed(1);
    }
  }