import React, { Component }  from "react";
import { View, StyleSheet, StatusBar, Platform } from "react-native";
import { WebView } from 'react-native-webview';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import config from 'src/config';
import axios from 'axios';
import PopupDialog from 'src/components/PopupDialog/PopupDialog';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default class LadderGameScreen extends Component {
  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <></>,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"사다리 게임"} />,
      headerRight : () => <></>,
      headerStyle : {
        //backgroundColor: "#01a1ff",
        //elevation: 0,       //remove shadow on Android
        //shadowOpacity: 0,   //remove shadow on iOS
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }    
  }

  constructor() {
    super();
    this.state = {
      uri : null,
      isFocused: false,
      prize : null,
      showDialog : false,
      webViewLoaded : false,
      showErrorDialog : false,
      showGameDialog : false,
      errorMessage : "",
    }
  }

  getUrlOfLadderGame = async () => {
    console.log("prize_name : ", this.props.navigation.state.params.prize_name)
    try {
        console.log("config.access_info : ", config.access_info.access_token)
        const resp = await axios.post(config.url.apiSvr + "/private/RollDice", {
          "data": {
            "access_token" : config.access_info.access_token,
            "device": {
              "device_id": config.deviceInfo.device_id,
              "session_id": config.sessionInfo.session_id,
            },
            "prize_name" : this.props.navigation.state.params.prize_name,
          }
        })
        console.log("resp : ", resp.data.data)
        const data = resp.data.data;
        this.setState({
          uri : data.uri.ladder,
          prize: data.prize,
        });
    }catch(err){
      console.log(err);
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.subs = [
      navigation.addListener(
        "didFocus", () => {
        this.setState({
          isFocused: true,
        });
        
      }),
      navigation.addListener("willBlur", () => {
        this.setState({ isFocused: false});}),
      navigation.addListener("willFocus", () => console.log("LadderGameScreen willFocus")),
      navigation.addListener("didBlur", () => console.log("LadderGameScreen didBlur")),
    ];
  }

  onUserGoBack = () => {
    const { navigation } = this.props;
    navigation.popToTop(); 
    navigation.navigate('homeTab')
  }

  canPlayGame = async () => {
    return new Promise(async function (resolve, reject) {
      try {
        const resp = await axios.post(config.url.apiSvr + "/private/GetCoolTime", {
          "data": {
            "access_token" : config.access_info.access_token,
              "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id,
              },
            }
        })
        resolve(resp)
             
      } catch (e) {
          reject(e);
      }
  });
  }

  componentDidUpdate(prevProps, prevState){
    const { isFocused } = this.state;
    if(prevState.isFocused != isFocused && isFocused){
      if(!config.access_info){
        this.setState({showErrorDialog:true, 
          errorMessage: "로그인 후에 이용할 수 있습니다."});
        /*
        setTimeout(() => {
          Alert.alert(
              '',
              '로그인 후에 이용할 수 있습니다.',
              [
                  {
                      text: '확인', onPress: () => {
                        this.props.navigation.navigate("homeScreen");
                      },
                  },
              ],
              {cancelable: false},
          );
        }, 100);
        */
      } else {
        this.canPlayGame().then(resp => {
          const {remain_seconds} = resp.data.data;
          if(remain_seconds){
            this.setState({showErrorDialog:true, 
              errorMessage: "지금은 QR 코드를 찍을 수 없습니다. 기다려 주세요."});
            /*
            setTimeout(() => {
              Alert.alert(
                  '',
                  '지금은 QR 코드를 찍을 수 없습니다. 기다려 주세요.',
                  [
                      {
                          text: '확인', onPress: () => {
                            this.props.navigation.navigate("homeScreen");
                          },
                      },
                  ],
                  {cancelable: false},
              );
            }, 100);
            */
          } else {
            this.getUrlOfLadderGame();
          }
        })
      }
      
    }
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  handleFinish = async(e) => {
    console.log("AAA : ", e)
    const {prize} = this.state;
    try {
        const resp = await axios.post(config.url.apiSvr + "/private/CommitRollDice", {
          "data": {
            "access_token" : config.access_info.access_token,
            "device": {
              "device_id": config.deviceInfo.device_id,
              "session_id": config.sessionInfo.session_id,
            },
            "prize" :{
              "prize_id" : prize.prize_id
            }
          }
        })
        console.log("CommitRollDice resp : ", resp.data.data);
        this.setState({showDialog:true});
    } catch(e){
      console.log("err : ", e)
    }
    
  }

  loadEnd = () =>{
    this.setState({webViewLoaded:true, showGameDialog:true})
  }

  render() {
    const {uri, isFocused, showGameDialog, prize, showDialog, webViewLoaded, showErrorDialog, errorMessage} = this.state;

    const popup = () => {
      
      if(prize && prize.prize_id){
        const message = `'${prize.coupon.name}'에 당첨되었습니다.`;
        return (<PopupDialog
          visible={showDialog}
          message={message}
          //imageSrc={config.images.success}
          title={"당첨! 축하합니다."}
          leftButtonLabel={'확인'}
          onLeftButtonPress={() => {
              this.setState({
                showDialog: false,
              });
              this.props.navigation.popToTop();
              this.props.navigation.navigate("homeTab")
          }}
          rightButtonLabel={'쿠폰함 바로가기'}
          rightButtonStyle={{backgroundColor: '#ff2c45'}}
          rightButtonTextStyle={{color: '#ffffff',}}
          onRightButtonPress={() => {
            this.setState({
              showDialog: false,
            });
            this.props.navigation.popToTop();
            this.props.navigation.navigate("myInfoScreen");
            this.props.navigation.navigate("myCoupon");
          }}
        />)
      } else {
        const message = "버스족이라면 24시간후 재도전 하세요.";
      
      return (<PopupDialog
        visible={showDialog}
        message={message}
        title={"다음 기회에"}
        //imageSrc={config.images.fail}
        centerButtonLabel={'확인'}
        //rightButtonStyle={{backgroundColor: '#ffffff', borderColor:'#e2e2e2', borderWidth:2}}
        //rightButtonTextStyle={{color: '#e2e2e2',  fontSize:16, fontFamily:config.defaultBoldFontFamily}}
        onCenterButtonPress={() => {
          this.setState({
            showDialog: false,
          });
          this.props.navigation.popToTop();
          this.props.navigation.navigate("homeTab")
        }}
        />)
      }
    }
    const popup2 = () => {
      if(showErrorDialog && errorMessage){
        return (<PopupDialog
          visible={showErrorDialog}
          message={errorMessage}
         
          centerButtonLabel={'확인'}
          onCenterButtonPress={() => {
            this.setState({
              showErrorDialog: false,errorMessage:""
            });
            this.props.navigation.popToTop();
            this.props.navigation.navigate("homeTab")
          }}
        />)
      }
    }
    const popup3 = () => {
      if(showGameDialog){
        return (<PopupDialog
          visible={showGameDialog}
          message={"'버,스,족,이,다' 중\n 한 글자를 선택해 주세요."}
          centerButtonLabel={'게임시작'}
          centerButtonTextStyle={{color:"#ffffff"}}
          centerButtonStyle={{backgroundColor: '#ff2c45', borderWidth:0}}
          onCenterButtonPress={() => {
            this.setState({showGameDialog:false})
          }}
        />)
      }
    }

    console.log("uri : ", uri, typeof(uri));
    const INJECTED_JAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'initial-scale=1.0, maximum-scale=0.99, user-scalable=no'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `;
    return (
      <View style={styles.container}>
        {popup()}
        {popup2()}
        {popup3()}
        {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        {uri &&
          <WebView onMessage={(e)=> {this.handleFinish(e)}}
          onLoadEnd={() => this.loadEnd()}
          source={{ uri}}
          injectedJavaScript={INJECTED_JAVASCRIPT}
          style={{opacity: webViewLoaded? 1: 0}}
          
          />
        }

      </View>
    );
  }
}

const styles=StyleSheet.create({
  container: {
    flex:1, backgroundColor:"#01a3ff"
  },
  loading:{
    width:0,
    height:0
  },
  webView:{
    flex:1
  }
  
});