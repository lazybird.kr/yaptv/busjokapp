import React, { Component }  from "react";
import { View, StatusBar,TouchableOpacity, StyleSheet, Platform, SafeAreaView, PermissionsAndroid, Animated, Easing, Alert, Linking } from "react-native";
import { RNCamera } from 'react-native-camera';
import config from 'src/config';
import {ZoomView} from 'src/components/camera';
import RNCameraControl from 'react-native-camera-control';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {calcZoom, getZoomScale} from './utils';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledText} from 'src/components/StyledComponents';
import axios from 'axios';
import PopupDialog from '../../components/PopupDialog/PopupDialog';

const QR_CODE_WIDTH  = 300;
const QR_CODE_WIDTH_MAX = 300;
const QR_CODE_WIDTH_MIN = 280;

const ZOOM_F = Platform.OS === 'ios' ? 0.01 : 0.05;
const ZOOM_F2 = Platform.OS === 'ios' ? 0.0002 : 0.05;

export default class QrScreen extends Component {

  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"} onUserGoBack={()=>{navigation.popToTop(); navigation.navigate('homeTab')}}/>,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:18}} title={"버스족 QR 카메라"} />,
      headerRight : () => <></>,
      headerStyle : {
        //backgroundColor: "#01a1ff",
        //elevation: 0,       //remove shadow on Android
        //shadowOpacity: 0,   //remove shadow on iOS
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }    
  }

  constructor() {
    super();
    this.state = {
      isFocused : false,
      scanable : true,
      qrValueReaded : '',
      scanDone : false,
      qrCodePosition : null,
      qrCodeWidth : new Animated.Value(QR_CODE_WIDTH), 
      zoom : 0,
      isPinchedToZoom : false,
      cameraPermission:undefined,
      showDialog:false,
      showDialog2:false,
    }
    this.cameraZoomArray = [];
  }

  onPinchProgress = (p) => {
    let p2 = p - this._prevPinch;
    if(p2 > 0 && p2 > ZOOM_F){
      this._prevPinch = p;
      this.setState({zoom: Math.min(this.state.zoom + ZOOM_F, Platform.OS === 'android'? this.zoomFactor3XForAndroid : this.zoomFactor3XForIOS)})
    } else if (p2 < 0 && p2 < -ZOOM_F){
      this._prevPinch = p;
      this.setState({zoom: Math.max(this.state.zoom - ZOOM_F, 0)})
    }
  }

  onPinchProgress2 = (p) => {
    let p2 = p - this._prevPinch;
    const zoom = config.cameraDeviceMaxZoomIOS > 100 ? ZOOM_F2 : ZOOM_F;
    if(p2 > 0 && p2 > zoom){
      this._prevPinch = p;
      this.setState({zoom: Math.min(this.state.zoom + zoom  , Platform.OS === 'android'? this.zoomFactor3XForAndroid : this.zoomFactor3XForIOS)})
    } else if (p2 < 0 && p2 < -zoom){
      this._prevPinch = p;
      this.setState({zoom: Math.max(this.state.zoom - zoom, 0)})
    }
  }

  onPinchStart = () => {
    if(this.focusTimeout){
      clearTimeout(this.focusTimeout);
      this.focusTimeout = null;
    }
    this._prevPinch = 1;
    this.setState({isPinchedToZoom:true});
  }

  onPinchEnd = () => {
    this._prevPinch = 1;
    this.setState({isPinchedToZoom:false})
  }

  resetZoom = () => {
    this._prevPinch = 1;
    let nextZoom = this.getNextZoom(this.state.zoom);
    this.setState({zoom: nextZoom});
  }
  
   componentDidMount() {
    const { navigation } = this.props;
    
    this.subs = [
      navigation.addListener("didFocus",  async () => {
        console.log("QR CODE SCAN didFocus")
        if(Platform.OS === 'android'){
           await this.getZoomFactorInAndroid();
        } else {
          //this.getZoomFactorIOS();
        }
        
        this.reset();
        this.setState({ isFocused: true});}),
      navigation.addListener("willBlur", () => {
        console.log("QR CODE SCAN willBlur");
        
        this.reset();
        this.setState({ isFocused: false, scanable : false, cameraPermission:undefined });}),
      navigation.addListener("willFocus", () => console.log("QR CODE SCAN willFocus")),
      navigation.addListener("didBlur", () => console.log("QR CODE SCAN didBlur")),
    ];
  }

  getZoomFactorIOS = () => {
    RNCameraControl.getZoomFactor((err, data) => {
      if(err){
        console.log("getZoomFactor error : ", err);
      } else if(data) {
        config.cameraDeviceMaxZoomIOS = data[0];
        this.zoomFactor3XForIOS = (3-1) / (config.cameraDeviceMaxZoomIOS - 1);
        this.zoomFactor2XForIOS = (2-1) / (config.cameraDeviceMaxZoomIOS - 1); 
        console.log("data : ", data[0])
        console.log("zoom x2, x3 : ", this.zoomFactor2XForIOS, this.zoomFactor3XForIOS);
      } else {
        console.log(err, data)
      }
    });
  }

  getZoomFactorInAndroid = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA)
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const resp = await RNCameraControl.getCameraScale();
        console.log("resp : ", resp)
        this.cameraZoomArray = [...resp];
        this.zoomFactor2XForAndroid = calcZoom(this.cameraZoomArray, "200");
        this.zoomFactor3XForAndroid = calcZoom(this.cameraZoomArray, "300");
        console.log("zoom x2, x3 : ", this.zoomFactor2XForAndroid, this.zoomFactor3XForAndroid); 
        this.setState({cameraPermission:true})
      } else {
        //alert("카메라 설정을 켜 주시기 바랍니다.");
        this.props.navigation.navigate("homeTab");
        return;
      }
    } catch (err) {
      //alert("Camera permission err",err);
      console.log(err);
    }
  }

  reset = () => {
    this.setState({
      qrValueReaded: '',
      qrCodeWidth: new Animated.Value(QR_CODE_WIDTH),
      scanDone: false, zoom:0,
    });
    
    this.animation && this.animation.stop();
    this.animation = null;
    this.state.intervalId && clearInterval(this.state.intervalId);
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  animate = () => {
    const {qrCodeWidth} = this.state;
    this.animation = Animated.loop(
      Animated.sequence([
        Animated.timing(qrCodeWidth, {
          toValue: QR_CODE_WIDTH_MIN,
          duration: 500,
          useNativeDriver: false,
          easing : Easing.linear
        }),
        Animated.timing(qrCodeWidth, {
          toValue: QR_CODE_WIDTH_MAX,
          duration: 500,
          useNativeDriver: false,
          easing : Easing.linear
        })
      ])
    );
    this.animation.start();
  }

  goToLadderGame = async () => {
    try {
      const {qrValueReaded} = this.state;
      console.log("qrValueReaded : ", qrValueReaded)
      if(qrValueReaded.includes(config.url.qrcodeForLadderGame)){  // 수정 필요: 포함여부 판단하고 상품 정보 넘겨야함
        console.log("url : ", qrValueReaded)
        const prize_name = qrValueReaded.split("?prize_name=")[1];
        this.props.navigation.navigate("ladderGameScreen", { prize_name : prize_name
        });
      } else {
        throw new Error("올바른 QR 코드가 아닙니다.");
      }
    } catch(err){
      this.setState({showDialog:true});
      /*
      let msg = err.message;
      Alert.alert(
        'Alert',
        msg,
        [{text: 'OK', onPress: () => {
            this.reset(); this.setState({scanable:true});
            this.camera && this.camera.resumePreview()
        }}],
        {cancelable: false},
      );
      */
    }
  }

  renderEdge = (edgePosition) => {
    const defaultStyle = {
      width: 20, height: 20,
      borderColor: "#01a1ff",
    }
    const edgeBorderStyle = {
      topRight: {
        borderRightWidth: 3, borderTopWidth: 3,
      },
      topLeft: {
        borderLeftWidth: 3, borderTopWidth: 3,
      },
      bottomRight: {
        borderRightWidth: 3, borderBottomWidth: 3,
      },
      bottomLeft: {
        borderLeftWidth: 3, borderBottomWidth: 3,
      },
    };
    return <View style={[defaultStyle, styles[edgePosition + 'Edge'], 
              edgeBorderStyle[edgePosition]]} />;
  }

  startLineAnimation = () => {
    const intervalId = setInterval(() => {
      const {qrValueReaded, qrCodePosition, intervalId} = this.state;
      if (qrValueReaded === '' && !this.animation) {
        this.animate();
      } 
      else if (qrValueReaded !== '' && qrCodePosition) {
        this.animation && this.animation.stop();
        this.setState({scanDone: true});
        intervalId && clearInterval(intervalId);
      }
    }, 500);
    this.setState({
      intervalId,
    });
  }

  canQrRead = async () => {
    return new Promise(async function (resolve, reject) {
      try {
        const resp = await axios.post(config.url.apiSvr + "/private/GetCoolTime", {
          "data": {
            "access_token" : config.access_info.access_token,
              "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id,
              },
            }
        })
        resolve(resp)
      } catch (e) {
          reject(e);
      }
  });
  }

  onUserGoBack = () => {
    const { navigation } = this.props;

    navigation.popToTop(); 
    navigation.navigate('homeTab')
  }

  componentDidUpdate(prevProps, prevStates){
    const { scanDone, scanable, isFocused, cameraPermission } = this.state;
    if(scanDone !== prevStates.scanDone && scanDone){
      this.goToLadderGame();
    }
    if(scanable !== prevStates.scanable && scanable){
      this.startLineAnimation();
    }
    if((isFocused !== prevStates.isFocused && isFocused) ||
        (cameraPermission !== prevStates.cameraPermission && cameraPermission)
      ){
      if(!config.access_info && cameraPermission){
        this.props.navigation.navigate("loginScreen", {
          onUserGoBack: this.onUserGoBack
        })
      } else if(cameraPermission){      
        this.startLineAnimation();
        this.canQrRead().then(resp => {
          const {remain_seconds} = resp.data.data;
          if(remain_seconds){
            console.log("remain : ", remain_seconds)
            this.setState({showDialog2:true})
            /*
            setTimeout(() => {
              Alert.alert(
                  '',
                  '지금은 QR 코드를 찍을 수 없습니다. 기다려 주세요.',
                  [
                      {
                          text: '확인', onPress: () => {
                            this.props.navigation.navigate("homeTab");
                          },
                      },
                  ],
                  {cancelable: false},
              );
          }, 100);
          */
          } else {
            this.setState({scanable : true });
          }
        })
      }
    }
  }

  prepareRatio = async () => {
    if (Platform.OS === 'android' && this.camera) {
        const ratios = await this.camera.getSupportedRatiosAsync();
        console.log("Image Camara ratios : ", ratios);
    }
    if(Platform.OS === "ios"){
      
      this.getZoomFactorIOS();
    }
  }

  onBarcodeScan = (event) => {
    const {scanable} = this.state;
    if(!scanable){
      return;
    }
    
    let qrvalue;
    if (Platform.OS === "android" && event.data) {
      qrvalue = event;
    } else if (Platform.OS === 'android'){
      qrvalue = event[0]; 
    } else {
      qrvalue = event; 
    }

    this.camera && this.camera.pausePreview()
    this.setState({
      scanable : false,
      qrValueReaded: qrvalue.data
    });
  }

  getNextZoom = (zoom) => {
    if(Platform.OS === 'ios'){
      if(zoom === 0.0){
        return this.zoomFactor2XForIOS;
      } else if (zoom ===this.zoomFactor2XForIOS) {
        return this.zoomFactor3XForIOS
      } else {
        return 0.0
      }
    }
    if(zoom === 0.0){
      return this.zoomFactor2XForAndroid;
    } else if(zoom ===this.zoomFactor2XForAndroid){
      return this.zoomFactor3XForAndroid;
    } else {
      return 0.0;
    }
  }

  renderNotAuthView = () => {
    return (
      <View style={{alignItems: 'center',justifyContent: 'center',flex:1, backgroundColor:"#000000"}}>
        <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0}}/>
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        
        <View style={{flex:1,justifyContent:"center", alignItems:"center", }}>
          <StyledText style={{fontSize:20, color:"#9e9e9e",fontFamily:config.defaultBoldFontFamily}}>
            {"버스족 앱으로 QR 코드 찍기"}
          </StyledText>
          <StyledText style={{marginTop:10,fontSize:14, color:"#9e9e9e",fontFamily:config.defaultLightFamily}}>
            {"버스족 앱에서 QR 코드를 찍으시려면 카메라에 대한"}
          </StyledText>
          <StyledText style={{fontSize:14, color:"#9e9e9e",fontFamily:config.defaultLightFamily}}>
            {"액세스를 허용하세요."}
          </StyledText>
          <TouchableOpacity onPress={()=>{
            Linking.openSettings()
            .then((resp) => {
            })
            .catch((err) => {
              console.log(err);
            });
          }}>
            <StyledText style={{marginTop:30,fontSize:14, color:"#01a3ff",fontFamily:config.defaultFontFamily}}>
              {"카메라 액세스 허용"}
           </StyledText>
          </TouchableOpacity>
        </View>
      </View>
      )
  }
  

  render() {
    const {isFocused,showDialog, showDialog2,scanDone, qrCodeWidth, qrCodePosition, zoom, cameraPermission} = this.state;
    if(!cameraPermission  && Platform.OS === "android" ){
      return <></>
    }
    return (
        <View style={{flex:1, backgroundColor:"black",}}>
        {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        {isFocused ? 
          <RNCamera
            captureAudio={false}
            ref={ref => { this.camera = ref; }}
            onCameraReady = {this.prepareRatio}
            style={{flex:1, margin:0}} // tabBar 바뀌면 좀 수정 필요
            zoom={zoom}
            onGoogleVisionBarcodesDetected={({ barcodes }) => {
              if(Platform.OS === "ios"){
                return
              }
              if((barcodes[0].type && barcodes[0].type === "UNKNOWN_FORMAT")){
                return
              }
              this.onBarcodeScan(barcodes);
            }}
            onBarCodeRead={barcode => {
              console.log("onBarCodeRead : ", barcode)
              if(barcode.type && (barcode.type === "UNKNOWN_FORMAT" || !barcode.type.includes("QR"))){
                return;
              }
              setTimeout(()=>this.onBarcodeScan(barcode), 1000)
            }}
          >
            {({status})=>{
              
              if(Platform.OS === "ios"){
                if(status === "NOT_AUTHORIZED"){
                  return this.renderNotAuthView();
                } else if(status === "READY"){
                  if(cameraPermission !== true){
                    this.setState({cameraPermission:true})
                  }
                }
              }
            }}
          </RNCamera> : 
          <></>}
          <PopupDialog
            visible={showDialog}
            message={'버스족 QR 코드가 아닙니다.'}
            centerButtonLabel={'확인'}
            onCenterButtonPress={() => {
              this.reset();
              this.camera && this.camera.resumePreview()
                this.setState({
                  showDialog: false,
                  scanable:true
                });
            }}
            />
            <PopupDialog
            visible={showDialog2}
            message={'타이머가 끝난 후 사용가능합니다. 기다려 주세요.'}
            centerButtonLabel={'확인'}
            onCenterButtonPress={() => {
              this.setState({
                showDialog2: false,
              });
              this.props.navigation.navigate("homeTab");
            }}
            />
          {cameraPermission &&  <ZoomView style={[
              styles.container, 
              { backgroundColor : scanDone ? 'rgba(0,0,0,0.8)' : "transparent"},]}
                  onPinchProgress={Platform.OS === "ios" ? this.onPinchProgress2 :  this.onPinchProgress}
                  onPinchStart={this.onPinchStart}
                  onPinchEnd={this.onPinchEnd}
              >
              <View style={{position : "absolute", top : 0, alignSelf : "center", paddingTop: 25}}>
                <StyledText style={{alignSelf:"center", fontSize: config.fontSize._56px, color:"white"}}>
                  {`버스 TV속 QR코드를 스캔해주세요.`}
                </StyledText>
                
              </View>
              <Animated.View 
                onLayout={({nativeEvent:{layout:{x, y}}}) => {
                  if(!qrCodePosition) {
                    this.setState({
                      qrCodePosition: new Animated.ValueXY({x:x, y:y}),
                    });
                  }
                }}
                style={{width: qrCodeWidth, height: qrCodeWidth}}>
                {this.renderEdge('topLeft')} 
                {this.renderEdge('topRight')}
                {this.renderEdge('bottomLeft')} 
                {this.renderEdge('bottomRight')}
              </Animated.View>
              <SafeAreaView style={{...styles.actionStyles,...{bottom : 25, right:25}}}>
                <TouchableOpacity style={styles.buttonView}
                  onPress={this.resetZoom}>
                  <StyledText style={styles.buttonText}>{`${getZoomScale(this.state.zoom, this.cameraZoomArray)}x`}</StyledText>  
                </TouchableOpacity>  
              </SafeAreaView>
          </ZoomView>}

      </View>
      
    );
  }
}

const styles=StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    ...StyleSheet.absoluteFillObject,
    //backgroundColor:"black"
  },
  topLeftEdge: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  topRightEdge: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  bottomLeftEdge: {
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  bottomRightEdge: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  actionStyles: {
    position: 'absolute',
    width: 40,
 
  },
  buttonView: {
    borderColor : "#ffffff",
    borderWidth : 1,
    borderRadius : 20,
    alignItems: 'center',
    justifyContent : "center",
    width : 40,
    height : 40,
    ...Platform.select({ ios: { paddingTop: 0 }, android: {} }),
  },
  buttonText : {
    color : "#ffffff",
    textAlign:'center',
    fontSize:14,
  }
});