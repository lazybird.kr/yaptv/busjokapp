import QrScreen from './QrScreen';
import LadderGameScreen from './LadderGameScreen';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from 'src/containers/Login'

const QrStack=createStackNavigator({
  qrScreen : { 
    screen : QrScreen,
    navigationOptions:{
      headerShown: true,
    }
  },
  ladderGameScreen: {
    screen : LadderGameScreen,
    navigationOptions:{
      headerShown: true,
    }
  },
  loginScreen: {
    screen: LoginScreen,
    navigationOptions:{
      headerShown: true,
    }
  }
});

QrStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
          if (route.routeName === "loginScreen") {
              tabBarVisible = false; 
          } else {
              tabBarVisible = true;
          }
      });
  }

  return {
      tabBarVisible,
  };
};

export {
  QrStack,  
}