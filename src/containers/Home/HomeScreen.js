import React, { Component }  from "react";
import { View, BackHandler, StatusBar, ScrollView,StyleSheet, TouchableOpacity, Image, Linking, Platform, PermissionsAndroid, Alert } from "react-native";
import {BusSearchModal} from 'src/components/SearchModal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';
import config from 'src/config';
import AsyncStorage from "@react-native-community/async-storage";
import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import {StyledText} from 'src/components/StyledComponents'
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import PopupDialog from 'src/components/PopupDialog/PopupDialog';
import {ImageSlider} from 'src/components/ImageSlider';
import VersionCheck from 'react-native-version-check';
import compareVersions from 'compare-versions';

const {fontSize} = config;
export default class HomeScreen extends Component {

  /*
  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:24}} title={"버스족"} />,
      headerTitle :() => <HeaderImage img={config.images.busStation}
          style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#ffffff",
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
      }
    }
  }
  */
  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <></>,
      headerTitle :() => <HeaderImage img={config.images.homeTitle}
        style={{alignSelf:"center",width:306*60/184, height:60, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#ffffff",
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
      }
    }
  }

  state = {
    isFocused : false, showExitDialog : false,
    isSearchModal : false,
    notice : "",
    post_list: [],
    advertisement_list: [],
    status: '', // needToLogin, logged, waitForQR
    timeScanned : null,
    formattedTime : "",
    remain_seconds : 0,
    showQrDialog : false,
    showLadderPopup:false,
    version : {},
    showUpdatePopup:false,
    showUpdatePopup2:false,
    updateUrl:"",
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.subs = [
      navigation.addListener("didFocus", () => {
        this.setState({ isFocused: true });
      }),
      navigation.addListener("willBlur", () => {
        console.log("HomeScreen willBlur")
        this.setState({ isFocused: false, formattedTime:"" });
        this.coolTimerId && clearInterval(this.coolTimerId);
        this.timerId && clearInterval(this.timerId)
      }),
      navigation.addListener("willFocus", () => console.log("HomeScreen willFocus")),
      navigation.addListener("didBlur", () => console.log("HomeScreen didBlur")),
      BackHandler.addEventListener("hardwareBackPress", ()=>{
        if(this.state.isFocused && Platform.OS === "android"){
          this.setState({showExitDialog:true})
          return true;
        }
      })
    ];
    
  }

  isNeededUpdate = (latestVersion, currentVersion) => {
    if(compareVersions(latestVersion, currentVersion, '>') === 1){
      console.log("isNeededUpdate : true")
      return true
    }
    console.log("isNeededUpdate : false")
    return false
  }

  checkVersion = async ()=> {
    const currentVersion = VersionCheck.getCurrentVersion();
    
    if(Platform.OS === "android"){
      const latestVersion = config.version.android;
      const updateUrl = config.googlePlayStoreUrl;
      if(this.isNeededUpdate(latestVersion, currentVersion)){
        const currentVersion1depth = currentVersion ? currentVersion.split(".")[0] :"";
        const latestVersion1depth = latestVersion ? latestVersion.split(".")[0] : "";
        const currentVersion2depth = currentVersion ? currentVersion.split(".")[1]: "";
        const latestVersion2depth = latestVersion ? latestVersion.split(".")[1] : "";
        if(currentVersion1depth !== latestVersion1depth || currentVersion2depth !== latestVersion2depth){
          //this.setState({showUpdatePopup2 : true, updateUrl})
        } else {
          this.setState({showUpdatePopup : true, updateUrl})
        }
      }
    } else {
      try {
        let updateNeeded = await VersionCheck.needUpdate();
        console.log("updateNeeded : ", updateNeeded)
        if(updateNeeded && updateNeeded.isNeeded){
          const currentVersion1depth = updateNeeded.currentVersion ? updateNeeded.currentVersion.split(".")[0] :"";
          const latestVersion1depth = updateNeeded.latestVersion ? updateNeeded.latestVersion.split(".")[0] : "";
          const currentVersion2depth = updateNeeded.currentVersion ? updateNeeded.currentVersion.split(".")[1]: "";
          const latestVersion2depth = updateNeeded.latestVersion ? updateNeeded.latestVersion.split(".")[1] : "";
          if(currentVersion1depth !== latestVersion1depth || currentVersion2depth !== latestVersion2depth){
            this.setState({showUpdatePopup2 : true, updateUrl:updateNeeded.storeUrl})
          } else {
            this.setState({showUpdatePopup : true, updateUrl:updateNeeded.storeUrl})
          }
          return;
        }
      } catch(e){
        console.log("VersionCheck : ", e)
      }
    }
    

    this.setState({didCheckUpdate:true});
  }

  componentWillUnmount() {
    this.subs && this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  setInitData = async () => {
    const access_token = config.access_info ? config.access_info.access_token : "";
    axios.post(config.url.apiSvr + "/private/Home", {
      "data": {
        "device": {
          "device_id": config.deviceInfo.device_id,
          "session_id": config.sessionInfo.session_id,
        },
        "access_token" :access_token || ""
      }
    }).then(resp => {
      console.log("Home : ", resp.data.data);
      const {advertisement_list, post_list, remain_seconds, roll_dice_at, version} = resp.data.data;
      let status = "";
      let timeScanned = "";
      if(access_token && remain_seconds){
        status = "waitForQR";
        timeScanned = roll_dice_at;
        this.getTime(remain_seconds);
        this.timerId = setInterval(this.getTime, 1000);
        this.coolTimerId = setInterval(this.getCollTime, 30000);
      } else if(access_token) {
        status = "logged"
      } else {
        status = "needToLogin"
      }
      if(version) {
        config.version = version
      }
      
      this.setState({
        advertisement_list : advertisement_list || [], post_list : post_list || [], status, timeScanned, remain_seconds,
        version
      })
    }).catch(err => {
      console.log("err1 : ", err)
    })
  }

  componentDidUpdate(prevProps, prevState){
    const { isFocused } = this.state;
    const {navigation } = this.props;
    if(prevState.isFocused != isFocused && isFocused){
      this.setInitData();
      this.scrollView && this.scrollView.scrollTo({x: 0, y: 0, animated: true})
      
      console.log("AAA: ", navigation.state.params)
      if(!this.state.showLadderPopup && navigation.state && navigation.state.params && navigation.state.params.ladderPopup){
        this.setState({showLadderPopup:true})
        navigation.state.params.ladderPopup = false;
      }
    }
    if(JSON.stringify(prevState.version) !== JSON.stringify(this.state.version)){
      this.checkVersion();
    }
  }

  getCollTime = async () => {
    console.log("coolTime start");
    try {
      const resp = await axios.post(config.url.apiSvr + "/private/GetCoolTime", {
        "data": {
          "access_token" : config.access_info.access_token,
            "device": {
              "device_id": config.deviceInfo.device_id,
              "session_id": config.sessionInfo.session_id,
            },
          }
      })
      console.log("cooltime : ", resp.data.data);
      const {remain_seconds} = resp.data.data;
      this.setState({remain_seconds})
    } catch(e){
      console.log("GetCoolTime error : ", e)
    }
  }
  getTime = (seconds) => {
    let tmpMillisecs;
    if(seconds) {
      tmpMillisecs = seconds * 1000;
    } else {
      const {remain_seconds} = this.state;
      if(!remain_seconds){
        return;
      }
      tmpMillisecs = remain_seconds * 1000 - 1000;
    }


    if(tmpMillisecs <= 0){
      this.setState({
        formattedTime : "",
        status:"logged",
        timeScanned: null,
        remain_seconds : 0,
      })
      this.timerId && clearInterval(this.timerId);
      return;
    }

    let formattedTime = this.getFormattedTime(tmpMillisecs);
    this.setState({formattedTime, remain_seconds : tmpMillisecs / 1000})
  }

  onGoBack = () => {
    this.props.navigation.popToTop();
    this.props.navigation.navigate("homeTab")
  }

  goToSearch = () => {
    this.props.navigation.push("busScreen");
    this.props.navigation.push("busSearch",{
      onGoBack : this.onGoBack
    });
  }

  renderSearch = () => {
    const {isSearchModal} = this.state;
    const searchTitle = "버스, 정류장 검색"
    const {goToSearch} = this;

    return (
      <View style={{flex:1}}>
        <TouchableOpacity style={{height:40, borderRadius:10, borderWidth:1, borderColor:"#9e9e9e", flexDirection:"row",
          justifyContent:"center", alignItems:"center"}} onPress={goToSearch}>
          <StyledText style={{ flex:9, fontSize: fontSize._52px, marginLeft:10, color:"#9e9e9e", alignSelf:"center"}}>
            {searchTitle}
          </StyledText>
          <Ionicons style={{alignSelf:"center", width:24, marginRight:10}} name="search-outline" color="#9e9e9e" size={25}/>
        </TouchableOpacity>
      </View>
    )
  }

  renderNotice = () => {
    const {notice} = this.state;
    return (
      <View style={{flex:1}}>
        {notice ?
          <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
            <Entypo name="bell" size={24} color={"red"} />
            <StyledText style={{color:"red", marginLeft:10}}>
              {notice}
            </StyledText>
          </View>
          :
          <></>
        }
      </View>
    )
  }

  onPressPost = (post) => {
    // console.log('post_id', postId);
    this.props.navigation.push('readPostScreen', {
            currentGroupName: post.group_name,
            read: true,
            postId: post.post_id,
            onGoBack: () => {
                //this.props.navigation.goBack();
                this.props.navigation.popToTop();
                this.props.navigation.navigate("homeTab")
            },
        },
    );
  }
  getFormattedTime(tmpMillisecs) {
    let hours = Math.floor((tmpMillisecs % (1000 * 60 * 60 * 24)) / (1000*60*60));
    let miniutes = Math.floor((tmpMillisecs % (1000 * 60 * 60)) / (1000*60));
    let seconds = Math.floor((tmpMillisecs % (1000 * 60)) / 1000);

    hours = hours >= 10 ? hours : '0' + hours;
    miniutes = miniutes >= 10 ? miniutes : '0' + miniutes;
    seconds = seconds >= 10 ? seconds : '0' + seconds;
    return hours + ":" +  miniutes + ":" + seconds;
  }


 getFormattedDate = () => {
    const date = new Date();
    let year = date.getFullYear();	//yyyy
    let month = (1 + date.getMonth());	//M
    month = month >= 10 ? month : '0' + month;	//month 두자리로 저장
    let day = date.getDate();	//d
    day = day >= 10 ? day : '0' + day;	//day 두자리로 저장
    let hours = date.getHours(); // 시
    hours = hours >= 10 ? hours : '0' + hours;
    let minutes = date.getMinutes();  // 분
    minutes = minutes >= 10 ? minutes : '0' + minutes;
    let seconds = date.getSeconds();  // 초
    seconds = seconds >= 10 ? seconds : '0' + seconds;
    return year + "." + month + "." + day + " " + hours + ":" + minutes + ":" + seconds;
  }

  renderWrites = () => {
    const {post_list} = this.state;
    const date = this.getFormattedDate();

    return (
      <View style={{flex:1, padding:10}} >
        <View style={{width:"100%", height:30, flexDirection:"row", alignItems:"center"}}>
          <View style={{flex:8, flexDirection:"row", alignItems:"center"}}>
            <StyledText style={{fontFamily:config.defaultMediumFontFamily, fontSize:fontSize._56px}}>{"오늘의 베스트 톡    "}</StyledText>
            <StyledText style={{color:"lightgrey", fontFamily:config.defaultMediumFontFamily, fontSize:fontSize._40px}}>{date + " 기준"}</StyledText>
          </View>
          
          <View style={{flex:1.5}}></View>
          <TouchableOpacity style={{flex:1}}  onPress={()=>this.props.navigation.navigate("communityScreen", {group:"좋아요"})}>
            <StyledText style={{color:"grey",fontFamily:config.defaultMediumFontFamily, fontSize:fontSize._48px}}>더보기</StyledText>
          </TouchableOpacity>
        </View>

        <View style={{width:"100%", height:"100%", alignItems:"center", flexDirection:"row", justifyContent:"center", flexWrap:"wrap"}}>
          {post_list && post_list.map((post, i) => {
            return (
            <TouchableOpacity key={i.toString()}
              style={{height:config.deviceInfo.oneThirdWidth-10, backgroundColor:post.background_color,
                  width:config.deviceInfo.oneThirdWidth-10, justifyContent:"center", marginBottom:5,marginRight:i%3 !== 2 ? 5: 0}}
              onPress={()=>this.onPressPost(post)}>
                <View style={{flex:2}}>
                  {post.uri && post.uri.thumbnail && <Image source={{uri : post.uri.thumbnail}} style={{width:config.deviceInfo.oneThirdWidth-10, height:config.deviceInfo.oneThirdWidth-10, resizeMode:"cover"}}/> }
                </View>
                <View style={{flex:1.5, paddingLeft:5, justifyContent:"flex-end"}}>
                  <StyledText numberOfLines={2} style={{color:"#ffffff", fontSize:fontSize._64px, fontFamily:config.defaultBoldFontFamily}}>
                    {post.title}
                  </StyledText>
                </View>

            </TouchableOpacity>
            )
          })}
        </View>
      </View>
    )
  }

  pressBanner = (index) =>{
    const {advertisement_list} = this.state;
    advertisement_list[index].link_uri && Linking.openURL(advertisement_list[index].link_uri);
  }

  renderBanner = () => {
    const {advertisement_list} = this.state;
    let images = [];
    if(advertisement_list.length === 0){
      return <></>
    }
    advertisement_list.forEach(item => {
      images.push({
        image:item.uri,
      })
    })
    
    console.log("device width : ", config.deviceInfo.width)

    const bannerHeight = config.deviceInfo.width > 600 ? config.deviceInfo.width/8 : config.deviceInfo.width/4;
    const bannerWidth = config.deviceInfo.width > 600 ? config.deviceInfo.width/2 : config.deviceInfo.width;
    //const bannerHeight = 80;

    return (
      <ImageSlider
        data={images}
        height={bannerHeight}
        width={bannerWidth}
        customSlide={({ index, item, style, width, height }) => {
          console.log("height : ",  height)
          // It's important to put style here because it's got offset inside
          return (<View key={index} style={[{...style}]}>
            <Image source={item} style={{width:width, height:height, resizeMode:"contain"}} />
          </View>)
        }}
        indicator={true}
        animation={true}
        onPress={item=>this.pressBanner(item)}
        indicatorContainerStyle={{position:"absolute", bottom:10}}
        indicatorInActiveColor={"#FFFFFF"}
        indicatorActiveColor={"#9e9e9e"}
      />
    )
  }

  renderWinner = () => {
    return (
      <View style={{flex:1, alignItems:"center"}} >
      </View>
    )
  }

  processQr = (status) => {
    if(status === "needToLogin"){
      this.props.navigation.navigate("loginScreen")
    } else if(status === "waitForQR"){
      this.setState({showQrDialog : true})
      return;
    } else if(status === "logged"){
      this.props.navigation.navigate("qrScreen")
    }
  }

  renderQrMessage = () => {
    const {status, formattedTime,} = this.state;

    const setText = () => {
      let textArr = [...formattedTime];
      
      return textArr.map((text,i) => {
        return (
          <View key={i+"qrMessage"} style={{width:text !== ":" ? 30 : 15, backgroundColor:text !== ":" ? "white" : "transparent", marginRight:5, key:i+text.toString()}}>
            <StyledText style={{zIndex:2, fontSize:fontSize._172px , fontFamily:text !== ":" ? config.defaultBoldFontFamily : config.defaultLightFontFamily, color:text !== ":" ? "#cb67cd" : "white", alignSelf:"center"}}>
              {text}
            </StyledText>
            {text !== ":" &&
              <View style={{zIndex:1, position:"absolute", borderColor:"#01a3ff",height:0.5,width:30, borderWidth:0.5,top:"50%"}}>

              </View>
            }
          </View>
          )
      })
    }


    return (
      <TouchableOpacity style={{flex:1, paddingLeft: 0, justifyContent:"center",
        backgroundColor: "#01a1ff"}}
        onPress={()=>this.processQr(status)}>

          <View>
            { status === "waitForQR" && formattedTime !== "" &&
              <View style={{height:"100%", width:"100%", padding:5,justifyContent:"space-around", alignItems:"center"}}>
                <View style={{height:10}}/>
                <View>
                  <StyledText style={{fontSize:fontSize._76px, fontFamily:config.defaultBoldFontFamily, color:"white",alignSelf:"center"}}>
                    {`'버스족' 여러분`}
                  </StyledText>
                  <StyledText style={{fontSize:fontSize._76px, fontFamily:config.defaultBoldFontFamily,color:"white",alignSelf:"center"}}>
                    {`기다리면, 선물이?!?`}
                  </StyledText>
                </View>
                <View style={{flexDirection:"row"}}>
                  {setText()}
                </View>
                <View>
                  <StyledText style={{fontSize:15, color:"white",alignSelf:"center"}}>
                    {`24시간 기준으로 QR촬영이 가능합니다.`}
                  </StyledText>
                  <StyledText style={{fontSize:15, color:"white",alignSelf:"center"}}>
                    {`기다려주세요!`}
                  </StyledText>
                </View>
                <View style={{height:10}}/>
              </View>
            }
            { status === "needToLogin" &&
              <View style={{height:"100%", width:"100%", padding:5,justifyContent:"space-around", alignItems:"center"}}>
                <View style={{height:45}}/>
                <View>
                  <StyledText style={{fontSize:fontSize._172px, fontFamily:config.defaultBoldFontFamily,color:"white", alignSelf:"center"}}>
                    {`'너도 버스족?'`}
                  </StyledText>
                </View>
                <View style={{height:18, width:"100%"}} />
                <View>
                  <StyledText style={{fontSize:fontSize._76px, color:"white", alignSelf:"center"}}>
                    {`지금 버스족 가입하고`}
                  </StyledText>
                  <StyledText style={{fontSize:fontSize._76px, color:"white", alignSelf:"center"}}>
                    {`QR 찍고, 매일 선물 받자!`}
                  </StyledText>
                </View>
                <View style={{height:29, width:"100%"}} />
                <View style={{borderWidth:1, width:200, height:36, borderRadius:5, borderColor:"white", justifyContent:"center", alignItems:"center"}}>
                  <StyledText style={{fontSize:fontSize._60px, color:"white", alignSelf:"center"}}>
                    회원가입 바로가기
                  </StyledText>
                </View>
                <View style={{height:45, width:"100%"}} />
              </View>
            }
            { status === "logged" &&
              <View style={{height:"100%", width:"100%", padding:5, justifyContent:"center", alignItems:"center"}}>

                <StyledText style={{fontSize:fontSize._172px,lineHeight:50, fontFamily:config.defaultBoldFontFamily,color:"white"}}>
                  {`'버스 TV 속`}
                </StyledText>
                <StyledText style={{ fontSize:fontSize._172px,lineHeight:50,fontFamily:config.defaultBoldFontFamily,color:"white"}}>
                  {`QR 찍고,`}
                </StyledText>
                <StyledText style={{fontSize:fontSize._172px, lineHeight:50,fontFamily:config.defaultBoldFontFamily,color:"white"}}>
                  {`선물 받을래?`}
                </StyledText>
                <View style={{height:10}}/>
                <StyledText style={{fontSize:fontSize._60px, color:"white"}}>
                  {`지금 QR 찍으러 가기`}
                </StyledText>
                <StyledText style={{fontSize:fontSize._60px, color:"white"}}>
                  {`Click!`}
                </StyledText>
              </View>
            }
          </View>


      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <PopupDialog
            visible={this.state.showUpdatePopup}
            message={'새로운 버젼이 있습니다.\n업데이트를 진행하시겠습니까?'}
            leftButtonLabel={'나중에'}
            onLeftButtonPress={() => {
                this.setState({
                  showUpdatePopup: false,
                });
            }}
            rightButtonLabel={'업데이트'}
            rightButtonStyle={{backgroundColor: '#ff2c45'}}
            rightButtonTextStyle={{color: '#ffffff'}}
            onRightButtonPress={() => {
              if(Platform.OS === "android"){
                BackHandler.exitApp();
              }
              Linking.openURL(this.state.updateUrl);
            }}
          />
          <PopupDialog
            visible={this.state.showUpdatePopup2}
            message={'필수 업데이트가 있습니다.'}
            centerButtonLabel={'업데이트'}
            onCenterButtonPress={() => {
              if(Platform.OS === "android"){
                BackHandler.exitApp();
              }
              Linking.openURL(this.state.updateUrl);
            }}
          />
        <PopupDialog
            visible={this.state.showExitDialog}
            message={'앱을 종료하시겠습니까?'}
            leftButtonLabel={'취소'}
            onLeftButtonPress={() => {
                this.setState({
                  showExitDialog: false,
                });
            }}
            rightButtonLabel={'예'}
            rightButtonStyle={{backgroundColor: '#ff2c45'}}
            rightButtonTextStyle={{color: '#ffffff'}}
            onRightButtonPress={() => {
                BackHandler.exitApp();
            }}
        />
        <PopupDialog
            visible={this.state.showQrDialog}
            message={'타이머가 끝난 후 사용가능합니다. 기다려 주세요.'}
            centerButtonLabel={'확인'}
            onCenterButtonPress={() => {
              this.setState({
                showQrDialog: false,
              });
            }}
        />
        <PopupDialog
            visible={this.state.showLadderPopup}
            message={"이벤트는 버스족 앱을 통해 참여해야 합니다.\n'버스족 QR 카메라'에서 다시 찍어주세요."}
            centerButtonLabel={'확인'}
            onCenterButtonPress={() => {
              this.setState({
                showLadderPopup: false,
              });
            }}
        />
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <View style={styles.search}>
          {this.renderSearch()}
        </View>
        <ScrollView style={{flex: 1}} ref={ref => this.scrollView = ref}>
          <View style={styles.banner}>
            {this.renderBanner()}
          </View>
          <View style={styles.winner}>
            {this.renderWinner()}
          </View>
          <View style={styles.qrcode}>
            {this.renderQrMessage()}
          </View>
          <View style={styles.writes}>
            {this.renderWrites()}
          </View>
          <View style={{height:20}}></View>
        </ScrollView>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor:"white"
  },
  search: {
    height:45, width:"100%", marginBottom : 5,paddingLeft:15, paddingRight:15,
  },
  banner: {
    width:"100%", 
    height:config.deviceInfo.width > 600 ? config.deviceInfo.width/8 : config.deviceInfo.width/4, alignItems:"center"
    //height: 80 , width:"100%"
  },
  winner: {
    height:30, width:"100%"
  },
  qrcode: {
    height:250, width:"100%"
  },
  writes: {
    flex:1,//height: 20+20+(config.deviceInfo.oneThirdWidth),
  },
  modalStyle: {
    flex: 1,
    justifyContent: "flex-end",
    margin: 0,
  },
});
