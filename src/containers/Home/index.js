import HomeScreen from './HomeScreen';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import LoginScreen from '../Login'

const HomeStack=createStackNavigator(
  {
    homeScreen : { 
      screen : HomeScreen,
    },
    loginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: true
      },
    }
  },{
    /*defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },*/
  }
  
  // {
  //   // headerMode: 'none'
  //   // navigationOptions:{
  //   //   headerShown: false
  //   // }
  // }
);

HomeStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
          if (route.routeName === "loginScreen") {
              tabBarVisible = false; 
          } else {
              tabBarVisible = true;
          }
      });
  }

  return {
      tabBarVisible,
  };
};


export {
  HomeStack
} 