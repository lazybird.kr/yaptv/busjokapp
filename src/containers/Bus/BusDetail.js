import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  Text, Image,Alert,
  StyleSheet, TouchableOpacity, StatusBar,ToastAndroid,
  ScrollView, Animated, TouchableWithoutFeedback
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {BackIcon} from 'src/components/header';
import axios from 'axios';
import config from 'src/config';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {StyledText} from 'src/components/StyledComponents'
import showToast from 'src/components/Toast';
import Loading from 'src/components/Loading';


const headerTitleHeight = 126;

const {fontSize} = config;

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const getBusRouteDetail = (busRouteId, callback, errCallback) => {
  console.log("busRouteId : ", busRouteId);
  axios.post(config.url.apiSvr + "/bus/GetBusRouteInfo", {
    "data": {
      "device": {
        "device_id": config.deviceInfo.device_id,
        "session_id": config.sessionInfo.session_id,
      },
      "route": {
        "route_id": busRouteId
      }
    }
  }).then(resp => {
    const { station_list, route } = resp.data.data;
    console.log("station_list : ", station_list);
    console.log("route : ", route);
    if(callback) {
      callback({station_list, route})
    }
  }).catch(err => {
    if(errCallback){
      errCallback();
      showToast("서비스지역이 아닌 버스입니다.")
    }
    console.log("err : ", err)
  })
}

const ArriveBusIcon = ({bus}) => {
  
  return(
      <View style={{width:30, justifyContent:"center", alignItems:"center", backgroundColor:"white", height:30, borderRadius:15, borderColor:config.bus.busColors[parseInt(bus.busType || "0")], borderWidth:1}}>
        <Image source={config.bus.busImages[parseInt(bus.busType || "0")]} style={{width:30, height:30}} />
      </View>
  )
}

const StationComp = ({station, busType}) => {
  let stationName = station.name;
  if(station.is_trans == 'Y') {
    stationName += ' (회차)'
  }
  
  const getSubwayList = () => {
    
    return station.subway_line && station.subway_line.sort().map((line,i) => {
      const subwayColor = config.subway[line] ? config.subway[line].color : "#000000";
      const subwayTextWidth = 18 * line.length;
      return <View key={i + line} style={{marginRight:5, width:subwayTextWidth , height:18, borderRadius:9, justifyContent:"center", alignItems:"center", backgroundColor:subwayColor,borderWidth:0}}>
              <StyledText style={{textAlign:"center", color:"white", fontSize:fontSize._48px, lineHeight:16, fontFamily:config.defaultBoldFontFamily}}>{line}</StyledText>
            </View>
    })
  }

  const textColor = station.ars_id === "0" || station.ars_id ==="미정차" ?  "#9e9e9e" : "#000000";
  
  return (
    <View style={styles.stConainer}>
      
        <StyledText style={{...styles.stationText1, color:textColor}} numberOfLines={1} >{stationName}</StyledText>
        <View style={{height:30,flexDirection:"row", alignItems:"center",marginTop:-5}}>
          <StyledText style={{...styles.stationText2, color:"#9e9e9e"}}>{station.ars_id + (station.subway_line? " | 지하철 " : "")}</StyledText>
          {getSubwayList()}
        </View>
        
      
    </View>
  )
}

const RoadLine = ({location}) => {
  return(
      <View style={{...styles.roadLine, top: location === "first" ? 36: 0, height : location ==="last" || location === "first" ? 36 : 72}}></View>
  )
}

const Buses = ({station, busType}) => {
  let busIcons = [];
  let bus_arrive = station.bus_arrive;
  if(bus_arrive) {
    for(let i = 0; i < bus_arrive.length; i++ ) {
     // for(let i = 0; i < 1; i++ ) {
        bus_arrive[i].busType = busType;
        const busOrgNum = bus_arrive[i].arrive_bus.plain_no;
        const busNum = busOrgNum.substring(busOrgNum.length-4, busOrgNum.length );
        const isArrived = bus_arrive[i].arrive_bus.is_arrive;
        const bus_type = bus_arrive[i].arrive_bus.bus_type;
        /*
          console.log("----------------------------------------")
          console.log("busNum: ", busNum)
          console.log("isArrived: ", isArrived)
          console.log("full_section_dist: ",bus_arrive[i].arrive_bus.full_section_dist);
          console.log("sect_dist: ",bus_arrive[i].arrive_bus.sect_dist);
          console.log("----------------------------------------")
        */
        
          let locationTop = isArrived === "1" ? 20 : 30;
          const fullDistance = parseFloat(bus_arrive[i].arrive_bus.full_section_dist);
          const sectDistance = parseFloat(bus_arrive[i].arrive_bus.sect_dist);
          //console.log("fullDistance:", fullDistance)
          //console.log("sectDistance:", sectDistance)
          const relativeDistance = sectDistance === 0 ? 0 : (sectDistance * 72) / fullDistance;
          //console.log("sectDistance:", relativeDistance)
          locationTop = locationTop + Math.ceil(relativeDistance);
          //console.log("locationTop:", locationTop)
        


        if(true){
          busIcons.push(
            <View key={i + "busIcon"} style={{position:"absolute", flex:1,left : 75-50-18, top: locationTop,flexDirection:"row", zIndex:locationTop}}>
              <View style={{ backgroundColor:"white",alignItems:"center", justifyContent:"center", height: bus_type === "1" ? 40 : 30, borderColor:"#9e9e9e", borderWidth:1, borderRadius:5, width:50,  marginRight:5, }}>
                  <StyledText style={{fontSize:fontSize._56px, textAlign:"center", color:"#9e9e9e",}}>{busNum}</StyledText>
                  {bus_type === "1" && <StyledText style={{fontSize:fontSize._44px, marginTop:-5 ,textAlign:"center", color:"#9e9e9e"}}>{"저상"}</StyledText>}
                </View>
              <View style={{height:40, alignItems:"center"}}>
                <ArriveBusIcon  bus={bus_arrive[i]} />
              </View>
              
            </View>
          )
        } else {
          /*
          busIcons.push(
            <View key={i + "busIcon"} style={{position:"absolute",flex:1, backgroundColor:"blue",left : 75-50-18, top: bus_type === "1" ? 30 : 35,flexDirection:"row",}}>
              <View style={{backgroundColor:"white",height: bus_type === "1" ? 40 : 30, borderColor:"#9e9e9e", borderWidth:1, borderRadius:5, width:50,  marginRight:5, alignItems:"center", justifyContent:"center"}}>
                <StyledText style={{fontSize:15, textAlign:"center", color:"#9e9e9e"}}>{busNum}</StyledText>
                {bus_type === "1" && <StyledText style={{fontSize:12, textAlign:"center", color:"#9e9e9e"}}>{"저상"}</StyledText>}
              </View>
              <View style={{height:40,alignItems:"center"}}>
                <ArriveBusIcon  bus={bus_arrive[i]} />
              </View>
              
            </View>
          )
          */
        }
    }
  }

  return(
    <View style={{zIndex:3,height:72, position:"absolute"}}>
      {busIcons}
    </View>
  )
}

const BusDetail = ({navigation}) => {

  const busRouteId = navigation.getParam('busRouteId');
  const routeType = navigation.getParam('routeType');
  const arsId = navigation.getParam('arsId');
  const busName = navigation.getParam('busName')
  const myScroll = useRef();
  
  const [route, setRoute] = useState(0);
  const [busInfo, setBusInfo] = useState({});
  const [index, setIndex] = useState(-1);
  const [rotationIndex, setRotationIndex] = useState(-1);
  
  const [isFocused, setFocus] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const prevFocused = usePrevious(isFocused);
  const [animation, setAnimation] = useState(new Animated.Value(0));
  const [direction, setDirection] = useState(true);
  const headerHeight = config.constant.headerHeight;
  const {busColors} = config.bus;
  let userScroll = false;
  console.log("headerheight",  headerHeight);
  useEffect(() => {
    const subs = [
      navigation.addListener("didFocus", () => {setFocus(true)}),
      navigation.addListener("willBlur", () => {setFocus(false)}),
      navigation.addListener("willFocus", () => console.log("StationDetail willFocus")),
      navigation.addListener("didBlur", () => console.log("StationDetail didBlur")),
    ];

    return () => {
      subs && subs.forEach((sub) => {
        sub.remove();
      });
    }
  }, []);

  useEffect(()=>{
    if(isFocused){
      setLoading(true);
      getBusRouteDetail(busRouteId, function({station_list, route}) {
        setRoute(station_list);
        setBusInfo(route);
        let stationIdx = -1;
        let rotationIdx = 0;
        station_list.forEach((el, idx) => {
          if(el.ars_id === arsId){
            setIndex(idx);
            stationIdx = idx;
          }
          if(el.is_trans === "Y"){
            setRotationIndex(idx);
            rotationIdx = idx;
          }
        })
        stationIdx >= rotationIdx && setDirection(false);
        setLoading(false);
      }, function(){
        setLoading(false);
      });
    }
  }, [isFocused]);
  
  useEffect(() => {
    scrollToCenterOfScrollView(index)
  }, [index])

  const onScroll = (e) => {
    let currentOffset = e.nativeEvent.contentOffset.y;
    if(userScroll){
      if(currentOffset >= rotationIndex * 72 && direction === true){
        setDirection(false);
      } else if(currentOffset < rotationIndex * 72 && direction === false){
        setDirection(true);
      }
    }
  }

  
  const onPressStation = (e, station) => {
    if(station.ars_id === "0" || station.ars_id === "미정차"){
      showToast('해당 정류장은 서비스 지역이 아닙니다.');
      return;
    }
    console.log('onPressStation:', station)
    navigation.push('StationDetail', {
      arsId: station.ars_id,
      name: station.name,
    })
  }

  const scrollToElement =(indexOf)=>{
    setTimeout(()=>myScroll && myScroll.current && myScroll.current.scrollTo({ x: 0, y: indexOf * (72), animated: true }),1);
  }

  const scrollToCenterOfScrollView = (indexOf) =>{
    setTimeout(()=>myScroll && myScroll.current &&myScroll.current.scrollTo({ x: 0, y: Math.max(indexOf * (72) - (config.deviceInfo.height/2) + headerHeight + headerTitleHeight+ getStatusBarHeight() , 0), animated: true }),1);
  }

  const createRoute = (route) => {
    let station_list = <Text></Text>;
    if (route) {
      station_list = route.map((station, idx) => {
        return(
          <View key={idx + 'view'} style={{flex:1, position:"relative", backgroundColor:"transparent", zIndex:-idx}}>
            <TouchableOpacity key={idx} 
              style={{...styles.stContents, backgroundColor:idx=== index ? "#f5f5f5" : "#ffffff"}} 
              onPress={e => onPressStation(e, station)}>
              <StationComp station={station} busType={routeType}></StationComp>
            </TouchableOpacity>
            <View style={{zIndex:2,position:"absolute", left:68,top:24,width:20, height:20, borderRadius:10,justifyContent:"center", backgroundColor:"white"}}>
                <Entypo name={"chevron-with-circle-down"} size={20} color={"#9e9e9e"}/>
            </View>
            <View style={{zIndex:1,position:"absolute", left:75}}>
              <RoadLine location={idx === 0 ? "first" : idx+1 === route.length ? "last" : ""}/>  
            </View>
            <Buses station={station} busType={routeType} />
          </View>
        )
      })
    }
    return station_list;
  }

  const touchRefresh = () => {
    getBusRouteDetail(busRouteId, function({station_list, route}) {
      setRoute(station_list);
    })
    Animated.timing(
      animation,
      {
        toValue: 1,
        duration: 1500,
        useNativeDriver: true,
      }
    ).start((event)=>{
      if(event.finished){
        setAnimation(new Animated.Value(0))
      }
    });
  }

  const animationStyles = {
    transform: [
      {
        rotate: animation.interpolate({
          inputRange: [0, 1],
          outputRange: ['0deg', '360deg']
        })
      }
    ]
  };

  const setContainerHeader = () => {
    return (
      <View style={{height:headerHeight, justifyContent:"center", backgroundColor:busColors[parseInt(routeType)]}}>
        <BackIcon navigation={navigation} color={"white"}/>
      </View>
    )
  }

  const setBusHeader = () => {
    if(!busInfo.route_id){
      return <View style={{...styles.headerTitle, backgroundColor:busColors[parseInt(routeType)]}}/>
    }
    const firstBusTime = busInfo.first_bus_time.substr(8,4);
    const lastBusTime = busInfo.last_bus_time.substr(8,4);

    return (
      <View style={{...styles.headerTitle, backgroundColor:busColors[parseInt(routeType)]}}>
        <StyledText style={styles.headerText1}>{busName}</StyledText>
        <StyledText style={styles.headerText2}>{"서울" + "   |   " + busInfo.start_station_name + " ↔ " + busInfo.end_station_name}</StyledText>
        <StyledText style={styles.headerText3}>
          {"운행시간   " + firstBusTime.substr(0,2) + ":" +  firstBusTime.substr(2,2) + " - " +  lastBusTime.substr(0,2) + ":" +  lastBusTime.substr(2,2)} 
        </StyledText>
        <StyledText style={styles.headerText3}>{"배차간격   " + busInfo.term + " 분"} </StyledText>
      </View>
    )
  }

  const selectDirection = (val) => {
    if(!val){
      scrollToElement(rotationIndex)
    } else {
      scrollToElement(0)
    }
    setDirection(val);
  }

  const setBusDirection = () => {
    const textColor1 = direction ? "#01a3ff" : "#9e9e9e";
    const textColor2 = direction ? "#9e9e9e" : "#01a3ff";
    
    return (
      <View style={{height:50, backgroundColor:"#ffffff", flexDirection:"row", borderBottomColor:"#bdbdbd", borderBottomWidth:0.5,}}>
        <TouchableOpacity onPress={()=>selectDirection(true)} style={{flex:1,justifyContent:"center", alignItems:"center"}}>
          {busInfo.route_id && <StyledText style={{color:textColor1, fontSize:fontSize._64px}}>{busInfo.end_station_name + "  방면"}</StyledText>}
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>selectDirection(false)} style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          {busInfo.route_id && <StyledText style={{color:textColor2, fontSize:fontSize._64px}}>{busInfo.start_station_name + "  방면"}</StyledText>}
        </TouchableOpacity>
      </View>
    )
  }


  return(
    <View style={styles.container}>
      {isLoading && <Loading color={'#01a3ff'} />}
      <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0, backgroundColor:busColors[parseInt(routeType)]}}/>
      {isFocused && <StatusBar barStyle="dark-content" backgroundColor={busColors[parseInt(routeType)]}/>}
      {setContainerHeader()}
      {setBusHeader()}
      {setBusDirection()}
      <ScrollView
        onScroll={onScroll}
        scrollEventThrottle={16}
        contentContainerStyle={{backgroundColor:"transparent"}}
        onScrollBeginDrag={()=>{userScroll = true}}
        onContentSizeChange={() => {
          scrollToCenterOfScrollView(index); //ios에서 안되서 여기 코드 넣어 놓음;; 확인 필요
        }}
        ref={myScroll}>
        {createRoute(route)}
        
      </ScrollView>
      <TouchableWithoutFeedback onPress={touchRefresh}>
        <Animated.View style={[styles.refresh, animationStyles]}  >
          {/*<FontAwesome name={"refresh"} size={20} color={"white"}/>*/}
          <Image source={config.images.refreshButton} style={{width:36, height:36}}/>
        </Animated.View>
      </TouchableWithoutFeedback>
      
    </View>
  )
}



export default BusDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"white",
  },
  header: {
    height:100
  },
  headerTitle: {
    height:headerTitleHeight,
  },
  headerText1: {
    color:"#FFFFFF", fontSize:fontSize._104px, paddingLeft:19,
    fontFamily:config.defaultMediumFontFamily
  },
  headerText2:{
    color:"#FFFFFF",  paddingLeft:19,
    fontSize:fontSize._48px,
  },
  headerText3:{
    color:"#FFFFFF",  paddingLeft:19,
    fontSize:fontSize._44px, fontFamily: config.defaultLightFontFamily
  },
  stContents: {
    height: 72,
    justifyContent:"center",
  },
  stConainer: {
    flex:1,
    justifyContent:"center",
    paddingRight: 20,
    marginLeft:75,
    paddingLeft:20,
    borderBottomWidth: 0.5,
    borderBottomColor:"#bdbdbd",
  },
  stationText1: {
    color:"#000000", fontSize:fontSize._60px,
  },
  stationText2: {
    color:"#9e9e9e", fontSize:fontSize._48px,
  },
  roadContents:{
    height: 72,
    width:156,
    alignItems:"center",
    paddingLeft: 20,
    paddingRight: 20,
    position:"absolute",
  },
  roadContainer: {
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "center",
    
  },
  roadLine: {
    width: 5,
    height: 72,
    backgroundColor: '#9e9e9e',
  },
  refresh: {
    position:"absolute",
    width:40, height:40,
    bottom : 30,
    right : 15,
    //borderRadius:40/2,
    //backgroundColor:"grey",
    justifyContent:"center",
    alignItems:"center"
    
  }
})