import React, { useState, useEffect, useRef } from 'react';
import { 
  View, 
  Text, 
  StyleSheet, Image, StatusBar,
  ScrollView, BackHandler,
  TouchableOpacity, Animated, TouchableWithoutFeedback, Platform
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {BackIcon} from 'src/components/header';
import axios from 'axios';
import config from 'src/config';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {StyledText} from 'src/components/StyledComponents';
import TextTicker from 'src/components/TextTicker';
import showToast from 'src/components/Toast';
import Loading from 'src/components/Loading';
/*
import PushNotification from "react-native-push-notification"

const localPushNotification = () => {
  PushNotification.channelExists("fcm_default_channel", function (exists) {
    console.log("channel exists : ",exists); // true/false
  });
  PushNotification.localNotification({
    // Android Only Properties
    channelId: "my_channel", // (required) channelId, if the channel doesn't exist, it will be created with options passed above (importance, vibration, sound). Once the channel is created, the channel will not be update. Make sure your channelId is different if you change these options. If you have created a custom channel, it will apply options of the channel.
    showWhen: true, // (optional) default: true
    vibrate: true, // (optional) default: true
    vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
    tag: "bus", // (optional) add tag to message
    ongoing: false, // (optional) set whether this is an "ongoing" notification
    priority: "high", // (optional) set notification priority, default: high
    visibility: "public", // (optional) set notification visibility, default: private
    ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear). should be used in combine with `com.dieam.reactnativepushnotification.notification_foreground` setting
    invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true
    title: "My Notification Title", // (optional)
    message: "My Notification Message", // (required)
  });
}
*/

const {fontSize} = config;

const getStationDetail = (arsId, callback) => {
  axios.post(config.url.apiSvr + "/bus/GetStationInfo", {
    "data": {
      "device": {
        "device_id": config.deviceInfo.device_id,
        "session_id": config.sessionInfo.session_id,
      },
      "station": {
        "ars_id": arsId
      }
    }
  }).then(resp => {
    const { station } = resp.data.data;
    console.log("station : ", station);
    if(callback) {
      callback(station)
    }
  })
}



const getArriveSoon = ({ars_id, direction, name, bus_arrive}, myOpacity) => {
  if(!bus_arrive){
    return <></>
  }
  
  let getArriveSoonBuses = bus_arrive.map((item, i) => {
    if(item.first_arrive.arrive_msg == '곧 도착') {
      return (
        <StyledText key={i+"bus"} 
          style={{...styles.arriveSoonText3, color:config.bus.busColors[parseInt(item.route.route_type)]}}>
          {item.route.name + "  "}
        </StyledText>
      )
    }
  })
  console.log("getArrive : ", getArriveSoonBuses)
  if(getArriveSoonBuses.every(function(item){ return item === undefined})){
    getArriveSoonBuses = "없음";
  }

  return (
    <View style={{opacity:1-myOpacity, justifyContent:"center"}}>
      <TextTicker 
        style={{...styles.arriveSoonText1, includeFontPadding: false, textAlignVertical : "center",}}>
        {name}</TextTicker>
      <StyledText style={styles.arriveSoonText2}>{ars_id + " | "+ direction +" 방면"}</StyledText>
      <View style={{flexDirection:"row", alignItems:"center"}}>
        <StyledText style={styles.arriveSoonText3}>{"지금 도착 예정 "}</StyledText>
        <TextTicker
          shouldAnimateTreshold={10}
          style={{includeFontPadding: false, textAlignVertical : "center",}}
        >
          {getArriveSoonBuses}
        </TextTicker>
      </View>
    </View>
    
  );
}

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const StationDetail = ({navigation}) => {

  const arsId = navigation.getParam('arsId');
  const [station, setStation] = useState(0);
  const [isFocused, setFocus] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [currentOffset, setOffset] = useState(0);
  const [animation, setAnimation] = useState(new Animated.Value(0));
  const headerHeight = config.constant.headerHeight;
  const [myOpacity, setMyOpacity] = useState(0);
  const [busAlarm, setBusAlarm] = useState({
    arsId:"",busRouteId:"",vehId:"", stationId: ""
  });

  useEffect(() => {
    const subs = [
      navigation.addListener("didFocus", () => {console.log("StationDetail didFocus"); setFocus(true)}),
      navigation.addListener("willBlur", () => {console.log("StationDetail willBlur"); setFocus(false)}),
      navigation.addListener("willFocus", () => console.log("StationDetail willFocus")),
      navigation.addListener("didBlur", () => {
        console.log("Station didBlur");
      })
        ,
      BackHandler.addEventListener("hardwareBackPress", ()=>{
        if(isFocused || (navigation.dangerouslyGetParent().state && navigation.dangerouslyGetParent().state.index === 1)){
          const onGoBack = navigation.getParam('onGoBack', ()=>{});
          onGoBack();
          navigation.goBack();
          return true;
        }
        
      })
    ];

    return () => {
      console.log("Station Detail unmount: ", station)
      subs && subs.forEach((sub) => {
        sub.remove();
      });
    }
  }, [])

  useEffect(() => {
    /*
    if(prevOffset < 120 && currentOffset >= 120){
      console.log("여기여기여기 올라갑니다.")
    } else if(prevOffset >= 120 && currentOffset < 120){
      console.log("여기여기여기 내려갑니다.");
    }
    */
   /*
    console.log("currentOffset : ", currentOffset)
    if(currentOffset/120 <=1){
      setMyOpacity(currentOffset/120);
    } else if(currentOffset/120 >1 && myOpacity <1){
      setMyOpacity(currentOffset/120);
    }
    */
   
  }, [currentOffset])

  useEffect(() => {
    if(isFocused){
      setLoading(true);
      getStationDetail(arsId, function(station) {
        console.log("station : ", station);
        station.bus_arrive && station.bus_arrive.forEach(_station => {
          if(_station.first_arrive.check_alarm){
            setBusAlarm({
              arsId:station.ars_id,busRouteId:_station.route.route_id,vehId:_station.first_arrive.veh_id, stationId: station.id
            })
          } else if(_station.second_arrive.check_alarm){
            setBusAlarm({
              arsId:station.ars_id,busRouteId:_station.route.route_id,vehId:_station.second_arrive.veh_id, stationId: station.id
            })
          }
        })
        setStation(station);
        setLoading(false);
      });
    }
  }, [isFocused])

  const onPressBus = (e, bus) => {
    console.log("bus : ", bus)
    navigation.push('BusDetail', {
      busRouteId: bus.route.route_id,
      busName: bus.route.name,
      routeType: bus.route.route_type,
      arsId : arsId
    });
  }

  const requestBusAlarm = async ({arsId, vehId, stationId, busRouteId}) => {
    console.log("requestBusAlarm : ", arsId, vehId, stationId, busRouteId);
    
    try{
      const cancelResp = await axios.post(config.url.apiSvr+ "/bus/CancelAlarm", {
        "data": {
          "device": {
            "device_id": config.deviceInfo.device_id,
            "session_id": config.sessionInfo.session_id,
          }
        }
      });
      console.log("알람 취소 : cancelResp : ", cancelResp)

      const requestResp = await axios.post(config.url.apiSvr + "/bus/RequestAlarm", {
        "data": {
          "device": {
            "device_id": config.deviceInfo.device_id,
            "session_id": config.sessionInfo.session_id,
          },
          "station": {
            "id": stationId,
            "ars_id": arsId
          },
          "route": {
            "route_id": busRouteId
          },
          "bus": {
            "veh_id": vehId
          }
        }
      });
      console.log("알람설정 : requestResp : ", requestResp);
      setBusAlarm({
        arsId, vehId, stationId, busRouteId
      })      
      showToast('승차 알람이 설정되었습니다.\n3번째 전 정류장부터 알람이 시작됩니다');
    } catch(e){
      console.log("승차알람 요청이 실패하였습니다. : " ,e)
    }
  }

  const cancelBusAlarm = async ({arsId, vehId, stationId, busRouteId}) => {
    try{
      const resp = await axios.post(config.url.apiSvr+ "/bus/CancelAlarm", {
        "data": {
          "device": {
            "device_id": config.deviceInfo.device_id,
            "session_id": config.sessionInfo.session_id,
          },
          "station": {
            "id": stationId,
          },
          "route": {
            "route_id": busRouteId
          },
          "bus": {
            "veh_id": vehId
          }
        }
      });
      setBusAlarm({
        arsId:"", vehId: "",busRouteId:"", stationId:"",
      })
      showToast('승차 알람이 해제되었습니다.');
    } catch(e){
      console.log("승차알람 해제요청이 실패하였습니다. : " ,e)
    }
  }

  useEffect(() => {
  }, [station])

  const onPressBusAlarm = (bus, vehId) => {
    getStationDetail(arsId, function(station) {
      console.log("bus : ", bus)
      setStation(station);
      const busRouteId = bus.route.route_id;

      const alarmData = {
        arsId, vehId, stationId : station.id, busRouteId
      }

      // 버스 알람 설정
      if(busAlarm.busRouteId === busRouteId && busAlarm.vehId === vehId){
        cancelBusAlarm(alarmData)
        
        return;
      }
      if(vehId === bus.first_arrive.veh_id && (
        bus.first_arrive.arrive_msg === "곧 도착" ||    
        bus.first_arrive.arrive_msg.split("[")[1] === "0번째 전]"
        //bus.first_arrive.arrive_msg.split("[")[1] === "1번째 전]"
        )) {
        showToast('버스가 곧 도착합니다.\n알람 설정을 할 수 없습니다.');
        
        return;
      } else if(vehId === bus.second_arrive && (
        bus.second_arrive.arrive_msg === "곧 도착" ||    
        bus.second_arrive.arrive_msg.split("[")[1] === "0번째 전]"
        //bus.second_arrive.arrive_msg.split("[")[1] === "1번째 전]"
        )) {
        showToast('버스가 곧 도착합니다.\n알람 설정을 할 수 없습니다.');
        
        return;
      }
      requestBusAlarm(alarmData)
      
    });
  }

  const makeBusStop = (busList) => {
    let arriveBus = [];
    let arriveBus2 = [];
    let finishedBus = [];
    let prevRouteType = "0";

    const makeBusStopView = (key, bus, index) => {
      let busText = bus.route.name;
      let arriveInfo1 = bus.first_arrive && bus.first_arrive.arrive_msg.replace(/(\[|\]|출발대기|[0-9]{1,2})/g, function(vl){      
        switch(vl){
         case "[" : return "(";
         case "]" : return ")";
         case "출발대기" : return "도착정보 없음";
         default: return parseInt(vl)+1;
        }
      })
      let arriveInfo2 = bus.second_arrive && bus.second_arrive.arrive_msg.replace(/(\[|\]|출발대기[0-9]{1,2})/g, function(vl){      
        switch(vl){
         case "[" : return "(";
         case "]" : return ")";
         case "출발대기" : return "도착정보 없음";
         default: return parseInt(vl)+1;
        }
      });
      let busType = config.bus.busTypes[parseInt(bus.route.route_type)];
      let busImage = config.bus.busImages[parseInt(bus.route.route_type)];
      let direction = bus.first_arrive.next_station.trim().length !== 0? bus.first_arrive.next_station + " 방면" : "종점"
      let isSetAlarm0 =  bus.first_arrive && bus.first_arrive.veh_id === busAlarm.vehId ? true : false;
      let isSetAlarm1 = bus.second_arrive && bus.second_arrive.veh_id === busAlarm.vehId? true : false;
      const patt = new RegExp(/도착정보 없음|운행종료/);
      const busMsgColor1 = patt.test(arriveInfo1) ? "#9e9e9e":"#01a3ff";
      const busMsgColor2 = patt.test(arriveInfo2) ? "#9e9e9e":"#01a3ff";
      if(prevRouteType !== bus.route.route_type){
        prevRouteType = bus.route.route_type;
        return (
          <View key={key}>
            <View style={{paddingLeft: 19, height:36, backgroundColor:"#eeeeee", borderBottomColor:"#bdbdbd", borderBottomWidth: 1, justifyContent:"center"}}>
              <StyledText style={{color:"#757575", fontFamily:config.defaultMediumFontFamily, fontSize:fontSize._48px}}>{busType}</StyledText>
            </View>
            <View style={{paddingLeft: 19, height:90, borderBottomColor:"#bdbdbd", borderBottomWidth: 1}}>
              <TouchableOpacity key={key} style={styles.arriveBus} onPress={e => onPressBus(e, bus)}>
                <View style={{flexDirection:"row", alignItems:"center"}}>
                  <Image source={busImage} style={{width:30, height:30}} />
                  <StyledText style={styles.arriveBusText1}>{busText}</StyledText>
                </View>
                <View style={{paddingLeft:5}}>
                  <StyledText numberOfLines={1} style={styles.arriveBusText2}>{direction}</StyledText>
                </View>
                <View style={{paddingLeft:5, flexDirection:"row", alignItems:"center"}}>
                  <StyledText adjustsFontSizeToFit numberOfLines={1} style={{...styles.arriveBusText3, color:busMsgColor1 }}>{arriveInfo1}</StyledText>
                    {index !== null && !arriveInfo1.includes("도착정보 없음") &&
                      <TouchableOpacity style={{width:25, height:25, justifyContent:"center"}} onPress={()=>onPressBusAlarm(bus,bus.first_arrive.veh_id)}>
                        {isSetAlarm0 ? 
                        <Image source={config.images.busAlarmOn} style={{width:25,height:25}}/>
                        :
                        <Image source={config.images.busAlarmOff} style={{width:25,height:25}}/>}
                      </TouchableOpacity>
                    }
                    <StyledText style={{...styles.arriveBusText3, color:"#9e9e9e"}}>{" | "}</StyledText>
                    <StyledText adjustsFontSizeToFit numberOfLines={1} style={{...styles.arriveBusText3, color:busMsgColor2 }}>{arriveInfo2}</StyledText>
                    {index !== null && !arriveInfo2.includes("도착정보 없음") &&
                      <TouchableOpacity style={{width:25, height:25, justifyContent:"center"}} onPress={()=>onPressBusAlarm(bus,bus.second_arrive.veh_id)}>
                        {isSetAlarm1 ? 
                        <Image source={config.images.busAlarmOn} style={{width:25,height:25}}/>
                        :
                        <Image source={config.images.busAlarmOff} style={{width:25,height:25}}/>}
                      </TouchableOpacity>
                    }
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )
      } else {
        return (
          <View key={key} style={{paddingLeft: 19, height:90,  borderBottomColor:"#bdbdbd", borderBottomWidth: 1}}>
            <TouchableOpacity  style={styles.arriveBus} onPress={e => onPressBus(e, bus)}>
              <View style={{flexDirection:"row", alignItems:"center"}}>
                  <Image source={busImage} style={{width:30, height:30}} />
                  <StyledText style={styles.arriveBusText1}>{busText}</StyledText>
                </View>
                <View style={{paddingLeft:5}}>
                  <StyledText numberOfLines={1} style={styles.arriveBusText2}>{direction}</StyledText>
                </View>
                <View style={{paddingLeft:5, flexDirection:"row", alignItems:"center"}}>
                  <StyledText adjustsFontSizeToFit numberOfLines={1} style={{...styles.arriveBusText3, color:busMsgColor1 }}>{arriveInfo1}</StyledText>
                  {index !== null && !arriveInfo1.includes("도착정보 없음") &&
                    <TouchableOpacity style={{width:25, height:25, justifyContent:"center"}} onPress={()=>onPressBusAlarm(bus,bus.first_arrive.veh_id)}>
                      {isSetAlarm0 ? 
                      <Image source={config.images.busAlarmOn} style={{width:25,height:25}}/>
                      :
                      <Image source={config.images.busAlarmOff} style={{width:25,height:25}}/>}
                    </TouchableOpacity>
                  }
                  <StyledText style={{...styles.arriveBusText3, color:"#9e9e9e"}}>{" | "}</StyledText>
                  <StyledText adjustsFontSizeToFit numberOfLines={1} style={{...styles.arriveBusText3, color:busMsgColor2 }}>{arriveInfo2}</StyledText>
                  {index !== null && !arriveInfo2.includes("도착정보 없음") &&
                    <TouchableOpacity style={{width:25, height:25, justifyContent:"center"}} onPress={()=>onPressBusAlarm(bus,bus.second_arrive.veh_id)}>
                      {isSetAlarm1 ? 
                      <Image source={config.images.busAlarmOn} style={{width:25,height:25}}/>
                      :
                      <Image source={config.images.busAlarmOff} style={{width:25,height:25}}/>}
                    </TouchableOpacity>
                  }
                </View>
            </TouchableOpacity>
          </View>
        )
      }
    } 

    if(!busList) {
      arriveBus.push(<Text key={"noBusList"}></Text>)
    } else {
      arriveBus = busList.map((bus, index) => {
        if(bus.first_arrive.arrive_msg === "운행종료" && bus.second_arrive.arrive_msg === "운행종료"){
          finishedBus.push(bus);
        } else {
          return makeBusStopView(index +"arriveBus", bus, index )
        }
      })

      prevRouteType = "0"
      arriveBus2 = finishedBus.map((bus, index) => {
        return makeBusStopView(index +"finishedBus", bus, null )
      })
    }

    return [...arriveBus, ...arriveBus2];
    
  }
  
  const touchRefresh = () => {
    
    getStationDetail(arsId, function(station) {
      setStation(station)
    });
    
    Animated.timing(
      animation,
      {
        toValue: 1,
        duration: 1500,
        useNativeDriver: true,
      }
    ).start((event)=>{
      if(event.finished){
        setAnimation(new Animated.Value(0))
      }
    });
  }

  const animationStyles = {
    transform: [
      {
        rotate: animation.interpolate({
          inputRange: [0, 1],
          outputRange: ['0deg', '360deg']
        })
      }
    ]
  };

  const setContainerHeader = ({name}) => {
    const onAction = navigation.getParam('onGoBack', ()=>{});
    return (
      <View style={{height:headerHeight, alignItems:"center", backgroundColor:"#ffffff", flexDirection:"row"}}>
        <BackIcon navigation={navigation} color={"black"} onAction={onAction}/>
          <TextTicker 
            shouldAnimateTreshold={20}
            animationType="scroll"
            style={{
              includeFontPadding: false, textAlignVertical : "center",
              opacity:myOpacity,marginLeft:10, fontFamily:config.defaultMediumFontFamily, fontSize:fontSize._84px}}>
            {name}
          </TextTicker>
      </View>
    )
  }

  const onScroll = (e) => {
    let currentOffset = e.nativeEvent.contentOffset.y;
        //var direction = currentOffset > this.offset ? 'down' : 'up';
    //setOffset(currentOffset)
  }

  return (
    <View style={styles.container}>
      {isLoading && <Loading color={'#01a3ff'} />}
      
      <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0, backgroundColor:"#ffffff"}}/>
      {isFocused && <StatusBar barStyle="dark-content" backgroundColor={"#ffffff"}/>}
      {setContainerHeader(station)}
      <View style={styles.busList}>
        <View style={styles.arriveSoon}>
          {getArriveSoon(station, myOpacity)}
        </View>
        <ScrollView 
          onScroll={onScroll}
          scrollEventThrottle={16}
        >
          
          {makeBusStop(station.bus_arrive)}
        </ScrollView>
      </View>
      <TouchableWithoutFeedback onPress={touchRefresh}>
        <Animated.View style={[styles.refresh, animationStyles]}  >
          {/*<FontAwesome name={"refresh"} size={20} color={"white"}/>*/}
          <Image source={config.images.refreshButton} style={{width:36, height:36}}/>
        </Animated.View>
      </TouchableWithoutFeedback>
    </View>
  )
}
/*
StationDetail.navigationOptions = ({navigation}) => {
  
  const arsId = navigation.getParam('arsId', '00000');
  const name = navigation.getParam('name')
  const onAction = navigation.getParam('onGoBack', ()=>{});
  
  return {
    headerLeft: () => <BackIcon navigation={navigation} color={"black"} onAction={onAction}/>,
    headerTitle: () => {},
    headerStyle: {
      backgroundColor: "#ffffff",
      elevation: 0,       //remove shadow on Android
      shadowOpacity: 0,   //remove shadow on iOS
    }
  }
}
*/
export default StationDetail;

const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor:"white",
  },
  arriveSoon: {
    height:126,  // 420px
    justifyContent: 'center',
    paddingLeft: 19,  // 64px
    paddingRight: 19,  // 64px
    borderBottomColor:"#bdbdbd", borderBottomWidth: 1
  },
  arriveSoonText1 : {
      fontSize: fontSize._84px,
      fontFamily: config.defaultMediumFontFamily,
      color: "black",
  },
  arriveSoonText2 : {
    fontSize: fontSize._48px,  // 48px
    color: "#9e9e9e",
  },
  arriveSoonText3 : {
    fontSize: fontSize._56px,   // 56px
    fontFamily: config.defaultFontFamily,
    color: "#616161",
  },
  busList: {
    flex: 7,
    //paddingLeft: 20,
    //paddingRight: 20,
  },
  arriveBus: {
    height: 90,
    justifyContent:"center"
    //paddingLeft: 10,
    //marginTop: 10,
    
  },
  arriveBusText1: {
    fontSize: fontSize._68px, 
    fontFamily: config.defaultMediumFontFamily,
  },
  arriveBusText2: {
    fontSize: fontSize._48px,
    color: "#9e9e9e"
  },
  arriveBusText3: {
    fontSize: fontSize._52px,
    color: '#01a3ff',
    
  },
  refresh: {
    position:"absolute",
    width:40, height:40,
    bottom : 30,
    right : 15,
    //borderRadius:40/2,
    //backgroundColor:"grey",
    justifyContent:"center",
    alignItems:"center"
    
  }
})