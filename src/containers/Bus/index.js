import BusScreen from './BusScreen';
import StationDetail from './StationDetail';
import BusDetail from './BusDetail';
import {BusSearch} from 'src/components/SearchModal';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

const BusStack=createStackNavigator({
  busScreen : { 
    screen : BusScreen,
    navigationOptions:{
      headerShown: false,
      ...TransitionPresets.SlideFromRightIOS,
    }
  },
  busSearch: {
    screen : BusSearch,
    navigationOptions: {
      headerShown: false,
      ...TransitionPresets.SlideFromRightIOS,
    },
  },
  StationDetail: {
    screen: StationDetail,
    navigationOptions: {
      headerShown: false,
      ...TransitionPresets.SlideFromRightIOS,
    },
  },
  BusDetail: {
    screen : BusDetail,
    navigationOptions: {
      headerShown: false,
      ...TransitionPresets.SlideFromRightIOS,
    },
  },
}, {
  
});

export {
  BusStack,
}