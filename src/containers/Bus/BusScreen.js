import React, { Component }  from "react";
import { View, Image, StyleSheet,AppState, StatusBar, ToastAndroid, TouchableOpacity, PermissionsAndroid, Platform, Linking, Alert } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {BusSearchModal} from 'src/components/SearchModal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import NaverMapView, {Circle, Marker, Path, Polyline, Polygon} from "react-native-nmap";
import Modal from 'react-native-modal';
//import Geolocation from '@react-native-community/geolocation';
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios';
import config from 'src/config';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledText} from 'src/components/StyledComponents'
import TextTicker from 'src/components/TextTicker';
import showToast from 'src/components/Toast';
const ZOOM = 16;


const {fontSize} = config;

export default class BusScreen extends Component {

  /*
  static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <></>,
      headerTitle :() => <HeaderImage img={config.images.homeTitle}
        style={{alignSelf:"center",width:306*60/184, height:60, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      
      headerStyle : {
        backgroundColor: "#ffffff",
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
      }
    }    
  }
  */
  
  state = {
    isFocused : false,
    isSearchModal : false,
    currentPosition : {},
    zoom : 16,
    cameraPosition : {},
    station_list : [],
    selectedStation: {},
    isGoBack : false,
    locationPermission : undefined,
    appState : AppState.currentState,
    isMapInitialized: false,
  }

  componentDidMount() {
    const {navigation} = this.props;
    console.log("BusScreen Did Mount")
    //this.requestLocationPermission();
    this.subs = [
      navigation.addListener("didFocus", () => {
        this.setState({ isFocused: true});
        AppState.addEventListener('change', this.handleAppStateChange);
        }),
      navigation.addListener("willBlur", () => {
        console.log("BusScreen willBlur");
        this.timer && clearTimeout(this.timer);
        this.setState({ 
          isFocused: false,  isGoBack : false, isMapInitialized: false});
        AppState.removeEventListener('change', this.handleAppStateChange);
        
      }),
      navigation.addListener("willFocus", () => console.log("BusScreen willFocus")),
      navigation.addListener("didBlur", () => console.log("BusScreen didBlur")),
      
    ];
    
  }

  handleAppStateChange = (nextAppState) =>{
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log("AAA")
      this.setState({isFocused:true})
    }
    this.setState({ appState: nextAppState });
  }

  onGoBack = () => {
    console.log("onGoBack")
    this.setState({isGoBack : true, isMapInitialized:true});
  }

  componentWillUnmount() {
    console.log("BusScreen WillUnMount")
    this.subs && this.subs.forEach((sub) => {
      sub.remove();
    });
    
  }

  hasLocationPermissionIOS = async () => {
    
    const status = await Geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
      return true;
    }
    else if (status === 'denied' || status === 'disabled') {
      return false;  
    }
    return false;
  };

  hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      const hasPermission = await this.hasLocationPermissionIOS();
      return hasPermission;
    }
    if (Platform.OS === 'android' && Platform.Version < 23) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
      return true;
    }
    
    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } 
    
    return false;
  };

  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) {
      if(Platform.OS === "android"){
        this.props.navigation.navigate("homeTab")
      } else{
        this.setState({locationPermission:false})
      }
      
      return;
    }
    this.setState({locationPermission:true})
    Geolocation.getCurrentPosition(async (info) => {
      console.log(info)
     this.setState({
       currentPosition: {
        latitude : info.coords.latitude,
        longitude : info.coords.longitude,
      }})
    },
    (error) => {
      // See error code charts below.
      console.log(error.code, error.message);
    }, 
    {
      distanceFilter: 5,
      //enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 1 * 1000,
      //accuracy: Platform.OS === 'ios' ? 'best' : 'high'
    });   

  };
  componentDidUpdate(prevProps, prevState){
    const { isFocused, isGoBack, cameraPosition } = this.state;

    if(prevState.isFocused != isFocused && isFocused){
      if(isGoBack){

      } else {
        this.setState({
          currentPosition : {},cameraPosition:{},
          station_list : [], selectedStation: {},
          isMapInitialized : false,
        }, () =>this.getLocation())
      }
      
    }

    if(JSON.stringify(prevState.cameraPosition) != JSON.stringify(cameraPosition) && cameraPosition.longitude){
      axios.post(config.url.apiSvr + "/bus/GetNearbyStation", {
        "data": {
          "device": {
            "device_id": config.deviceInfo.device_id,
            "session_id": config.sessionInfo.session_id,
          },
          "gps_info": {
            "longitude": cameraPosition.longitude.toString(),
            "latitude" : cameraPosition.latitude.toString(),
            "radius" : "400",
          }
        }
      }).then(resp => {
        const {station_list} = resp.data.data;
        this.setState({
          station_list : station_list || []
        })
      }).catch(err => {
        console.log("GetNearbyStation Error : ", err)
      })
    }
  }

  onPressDetail = () => {
    const { selectedStation } = this.state;
    this.props.navigation.navigate('StationDetail', {
      arsId: selectedStation.ars_id,
      name: selectedStation.name,
      onGoBack : this.onGoBack
    });
  }

  goToSearch = () => {
    this.props.navigation.navigate("busSearch",{
      onGoBack : this.onGoBack
    });
  }

  renderSearch = () => {
    const searchTitle = "버스, 정류장 검색"
    const {goToSearch, } = this;

    return (
      <View style={{height:40, width:"100%", flexDirection:"row"}}> 
        <MaterialCommunityIcons
              name="chevron-left" color="black" size={36}
              onPress={()=>this.props.navigation.navigate('homeScreen')} style={{alignSelf:"center"}}
        />
        <TouchableOpacity style={{marginLeft:5, flex:1,borderRadius:10, borderWidth:1, borderColor:"#9e9e9e", flexDirection:"row",
          justifyContent:"center", alignItems:"center"}} onPress={goToSearch}>
          <StyledText style={{ flex:9, fontSize: fontSize._52px, marginLeft:10, color:"#9e9e9e", alignSelf:"center"}}>
            {searchTitle}
          </StyledText>
          <Ionicons style={{alignSelf:"center", width:24, marginRight:10}} name="search-outline" color="#9e9e9e" size={25}/>
        </TouchableOpacity>
      </View>
    ) 
  }

selectStation = async (selectedStation) => {
  console.log("selectedStation :", selectedStation)
  if(selectedStation.ars_id === "0"){
    showToast('해당 정류장은 서비스 지역이 아닙니다.');
    return;
  }
   const resp = await axios.post(config.url.apiSvr + "/bus/GetStationInfo", {
      "data": {
        "device": {
          "device_id": config.deviceInfo.device_id,
          "session_id": config.sessionInfo.session_id,
        },
        "station": {
          "ars_id": selectedStation.ars_id
        }
      }
    });
    console.log("resp :", resp)
    const { station } = resp.data.data;
    this.setState({
      selectedStation : {...station, 
        longitude : selectedStation.longitude,
        latitude : selectedStation.latitude }
    })
  }

  renderMap = () => {
    const {currentPosition, station_list, selectedStation, zoom, isMapInitialized} = this.state;
    console.log("AAAAA1");
    if(!currentPosition.latitude){
      console.log("AAAAA2");
      return <></>
    }
    console.log("AAAAA3");
    let markerFlag = false;
    if(Platform.OS === "android"){
      if(isMapInitialized){
        markerFlag = true;
      }
    } else {
      markerFlag = true;
    }
    console.log("isMapInitialzed : ", isMapInitialized, station_list)
    return (
      <NaverMapView style={{width: '100%', height: '100%'}}
                        ref={(map) => { 
                          this.map = map; 
                          if(Platform.OS === "ios"){
                            this.map && this.map.setLocationTrackingMode(1);  
                          }
                        }}
                        showsMyLocationButton={true}
                        onInitialized={(e)=>{
                          console.log("init : ", e);
                          this.map && this.map.setLocationTrackingMode(1);
                          this.timer = setTimeout(()=>{
                            this.setState({isMapInitialized : true});
                            //clearTimeout(this.timer);
                          },1000);
                        }}
                        center={{...currentPosition, zoom: ZOOM}}
                        onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                        onCameraChange={e => {
                          console.log('onCameraChange', JSON.stringify(e));
                          this.setState({cameraPosition : {
                            latitude : e.latitude,
                            longitude : e.longitude
                          }, zoom: e.zoom})
                        }}
                        onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>
                        
        {markerFlag && station_list && station_list.map((station, i) => {
          /*
          if(Platform.OS === "android" && !isMapInitialized){
            console.log("Not Yet")
            return null
          }
          */
          //let image = config.images.markers[i];
          if(selectedStation && station.ars_id === selectedStation.ars_id){
            return null
          }
          
          
          return (zoom > 15 &&
            <Marker key={i.toString()}
              coordinate={{
                longitude : parseFloat(station.longitude),
                latitude : parseFloat(station.latitude),
              }} 
              image={config.images.mapBusStataion}
              width={25} height={25}
              onClick={() => {
                console.log("station : ", station)
                this.selectStation(station);
              }}
              />
          )
        })}
        
        {selectedStation && selectedStation.name && 
            <Marker
              coordinate={{
                longitude : parseFloat(selectedStation.longitude),
                latitude : parseFloat(selectedStation.latitude),
              }}
              style={{zIndex:10}}
              image={config.images.mapBusStataionSelected}
              width={35} height={35}
              onClick={() => {
                console.log("AAAAA")
              }}/>
          
        }
      </NaverMapView>
    )
  }

 getArriveSoon = (busList) => {
   let flag = false;
   const {busColors} = config.bus;
   if(!busList){
    return <StyledText></StyledText>
   }
   const returnValue =  busList.map((bus, i) => {
     
     if(bus.first_arrive.arrive_msg == '곧 도착') {
       let busColor = busColors[parseInt(bus.route.route_type)];
       flag = true;
      return <StyledText key={"bus" + i} style={{color:busColor, fontSize:fontSize._52px}}>{bus.route.name + "  "}</StyledText>
     }
   });
   if(!flag){
    return <StyledText style={{color: "#000000", fontSize:fontSize._52px}}>없음</StyledText>
   } else {
     return returnValue
   }
 }

  renderBottom = () => {
    const {selectedStation, } = this.state;
    if(!selectedStation || !selectedStation.name){
      return (<View style={{flex:1, alignItems:"center", flexDirection:"row"}}> 
          <Image source={config.images.mapBusStataion} style={{width:30, height:30}}/>
          <StyledText style={{color:config.colors.greyColor, fontSize:fontSize._60px}}>{"정류장을 선택해 주세요."}</StyledText>
      </View>)
    }
    return (
      <TouchableOpacity style={{flex:1, marginTop:0  }} onPress={this.onPressDetail} >
        <View style={{flex:2,alignItems:"center", flexDirection:"row"}}>
            <TextTicker 
            shouldAnimateTreshold={20}
            style={{
              includeFontPadding: false, textAlignVertical : "center",
              fontSize:fontSize._84px, fontFamily:config.defaultBoldFontFamily}}>
          {selectedStation.name}
          </TextTicker>
        </View>
        <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
          <StyledText style={{color:config.colors.greyColor, fontSize:fontSize._48px}} >{selectedStation.ars_id + " | "} </StyledText>
          <StyledText style={{color:config.colors.greyColor, fontSize:fontSize._48px}} >{selectedStation.direction + " 방면"} </StyledText>
        </View>
        <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
          <StyledText style={{color:config.colors.darkGreayColor, fontSize:fontSize._56px}}>{"지금 도착 예정 : "}</StyledText>
          {selectedStation.bus_arrive ? 
          <TextTicker
          shouldAnimateTreshold={20}
          style={{
            includeFontPadding: false, textAlignVertical : "center",
          }}
          >
            {this.getArriveSoon(selectedStation.bus_arrive)}
          </TextTicker> :
          <StyledText style={{color:config.colors.darkGreayColor, fontSize:fontSize._56px}}>{"없음  "}</StyledText>
        }
         
        </View>
        
      </TouchableOpacity>
    )
  }

  renderNotAuthView = () => {
    return (
      <View style={styles.container}>
        <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0}}/>
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <View style={styles.search}>
          {this.renderSearch()}
        </View>
        <View style={{flex:1,justifyContent:"center", alignItems:"center", }}>
          <StyledText style={{fontSize:20, color:"#9e9e9e",fontFamily:config.defaultBoldFontFamily}}>
            {"버스족 앱에서 정류장 지도 보기"}
          </StyledText>
          <StyledText style={{marginTop:10,fontSize:14, color:"#9e9e9e",fontFamily:config.defaultLightFamily}}>
            {"버스족 앱에서 정류장 지도를 보시려면 위치에 대한"}
          </StyledText>
          <StyledText style={{fontSize:14, color:"#9e9e9e",fontFamily:config.defaultLightFamily}}>
            {"액세스를 허용하세요."}
          </StyledText>
          <TouchableOpacity onPress={()=>{
            Linking.openSettings()
            .then((resp) => {
              this.setState({isFocused:false})
              console.log(resp);
            })
            .catch((err) => {
              console.log(err);
            });
          }}>
          <StyledText style={{marginTop:30,fontSize:14, color:"#01a3ff",fontFamily:config.defaultFontFamily}}>
            {"위치 액세스 허용"}
          </StyledText>
          </TouchableOpacity>
        </View>
      </View>)
  }

  render() {
    const {locationPermission} = this.state;
    if(locationPermission === undefined){
      return <></>
    } else if(locationPermission === false){
      return this.renderNotAuthView()
    }
    
    return (
      <View style={styles.container}>
        <View style={{width: '100%', height: Platform.OS === 'ios' ? getStatusBarHeight() : 0}}/>
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <View style={styles.search}>
          {this.renderSearch()}
        </View>
        <View style={styles.map}>
          {this.renderMap()}
        </View>
        <View style={styles.bottom}>
          {this.renderBottom()}
        </View>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor:"white",
    paddingTop: 10,
  },
  search: {
    height:45, width:"100%", marginBottom : 5,paddingLeft:5, paddingRight:20,
  },
  map: {
    flex: 1,
  },
  bottom: {
    height:100, width:"100%", borderWidth:0.5, borderBottomWidth:0, borderColor: "#E2E2E2", borderTopLeftRadius:20, borderTopRightRadius:20,
    padding : 10, paddingLeft:16, marginTop : -10, backgroundColor:"#ffffff"
    //flex:2
  },
  modalStyle: { 
    flex: 1,
    justifyContent: "flex-end",
    margin: 0
  },
});