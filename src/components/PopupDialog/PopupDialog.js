import React, {Component, useRef} from 'react';
import {View, Image, Platform, TextInput, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';

import config from '../../config';

export default class PopupDialog extends Component {
    constructor() {
        super();
        this._refTextInput = null;
        this.state = {};
    }

    componentDidMount() {
        this._refTextInput && this._refTextInput.getInnerRef().focus();
    }

    componentWillUnmount() {
    }

    render() {
        const {
            visible,
            onBackdropPress,
            onBackButtonPress,
            onLeftButtonPress,
            onCenterButtonPress,
            onRightButtonPress,
            onChangeText,
            textInputStyle,
            title,
            message,
            inputTitle,
            imageSrc,
            leftButtonLabel,
            centerButtonLabel,
            rightButtonLabel,
            leftButtonStyle,
            leftButtonTextStyle,
            centerButtonStyle,
            centerButtonTextStyle,
            rightButtonStyle,
            rightButtonTextStyle,
            secureTextEntry,
        }
            = this.props;
        let buttonWidth = {
            width: '45%',
        };
        if (leftButtonLabel && centerButtonLabel && rightButtonLabel) {
            buttonWidth = {
                width: '30%',
            };
        }
        let containerStyle = {};
        if (onChangeText) {
            containerStyle = {
                height: styles.container.height + 32,
            };
        }
        return (
            <View>
                <Modal
                    isVisible={visible}
                    animationType={'none'}
                    animationInTiming={1}
                    animationOutTiming={1}
                    backdropOpacity={0.2}
                    onBackdropPress={onBackdropPress}
                    onBackButtonPress={onBackButtonPress}
                    style={styles.modal}>
                    <View style={[styles.container, containerStyle]}>
                        <View style={{...styles.bodyContainer, flex:title?2:1, ...styles.bodyContainerWithImage}}>
                            {imageSrc ?
                                <View style={styles.imageContainer}>
                                    <Image source={imageSrc} style={styles.image}/>
                                </View>
                                : null
                            }
                            <View style={[styles.titleContainer, imageSrc && styles.titleContainerWithImage]}>
                                {title &&
                                    <View style={{flex:0.5}} />
                                }
                                {title ?
                                        <StyledText style={[config.styles.text2, styles.title, imageSrc && styles.titleWithImage]}>
                                            {title}
                                        </StyledText>
                                    
                                    : null}
                                {message ?
                                    
                                        <StyledText style={[config.styles.text, styles.message, (imageSrc || title) && styles.messageWithImage]}>
                                            {message}
                                        </StyledText>
                                    
                                     : null}
                            </View>
                        </View>
                        {onChangeText ?
                            <View style={{flex: 1,}}>
                                {inputTitle ?
                                    <StyledText style={[config.styles.text2, styles.inputTitle]}>
                                        {inputTitle}
                                    </StyledText> : null}
                                <View style={styles.inputContainer}>
                                    <StyledTextInput
                                        ref={(ref) => this._refTextInput = ref}
                                        style={[config.styles.text, styles.textInput, textInputStyle]}
                                        onChangeText={onChangeText}
                                        secureTextEntry={secureTextEntry}
                                        autoFocus={true}
                                    />
                                </View>
                            </View> : null}
                        <View style={styles.buttonContainer}>
                            {leftButtonLabel ?
                                <TouchableOpacity
                                    style={[config.styles.button, styles.button, styles.lButton, buttonWidth, leftButtonStyle]}
                                    onPress={onLeftButtonPress}
                                >
                                    <StyledText
                                        style={[config.styles.text, styles.buttonText, leftButtonTextStyle]}>{leftButtonLabel}</StyledText>
                                </TouchableOpacity>
                                : null}
                            {centerButtonLabel ?
                                <TouchableOpacity
                                    style={[config.styles.button, styles.button, styles.cButton, buttonWidth, centerButtonStyle]}
                                    onPress={onCenterButtonPress}
                                >
                                    <StyledText
                                        style={[config.styles.text, styles.buttonText, centerButtonTextStyle]}>{centerButtonLabel}</StyledText>
                                </TouchableOpacity>
                                : null}
                            {rightButtonLabel ?
                                <TouchableOpacity
                                    style={[config.styles.button, styles.button, styles.rButton, buttonWidth, rightButtonStyle]}
                                    onPress={onRightButtonPress}
                                >
                                    <StyledText
                                        style={[config.styles.text, styles.buttonText, rightButtonTextStyle]}>{rightButtonLabel}</StyledText>
                                </TouchableOpacity>
                                : null}
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginHorizontal: 5,
        // marginBottom: 0,
    },
    container: {
        // flex: 0.3,
        height: 170,
        width: config.size._1200px,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 1)',
        flexDirection: 'column',
        borderRadius: config.size._70px,
    },
    bodyContainer: {
        flex: 1,
        width: '100%',
        flexDirection:"row"
        // backgroundColor: 'rgba(255, 0, 255, 0.5)',
    },
    bodyContainerWithImage:{
        marginLeft:10, marginRight:10
    },
    titleContainerWithImage:{
        flex:6,
        alignItems:"flex-start",
        justifyContent:"center"
    },
    imageContainer: {
        flex:4, 
        justifyContent:"flex-end", 
        alignItems:"flex-end", 
        marginRight:20
    },
    image : {
        width:80, height:80
    },
    titleContainer: {
        flex: 1,
        width: '100%',
        justifyContent: Platform.OS === "android"? "center" : "flex-end",
        alignItems: 'center',
        // backgroundColor: 'rgba(255, 255, 0, 0.5)',
    },
    title: {
        flex:0.5,
        color: config.colors.baseColor,
        fontFamily: config.defaultBoldFontFamily,
        fontSize: 18,
        //fontWeight: 'bold',
    },
    titleWithImage:{
        textAlign:"left",
    },
    message: {
        fontSize: config.fontSize._52px,
        textAlign: 'center',
        textAlignVertical: 'bottom',
        //fontWeight: 'bold',
        flex: Platform.OS === "ios" ? 0 : 1    ,  // ios는 textAlignVertical가 안먹기 때문에 flex:1을 적용하면 align이 안된다;;
        fontFamily: config.defaultBoldFontFamily,
    },
    messageWithImage:{
        textAlign:"left",
        textAlignVertical:"center",
        paddingHorizontal:0,
        paddingVertical:0,
    },
    inputTitle: {
        fontSize: config.fontSize._44px,
        color: config.colors.greyColor,
        // height: config.size._112px,
        textAlign: 'center',
        // fontFamily: config.defaultBoldFontFamily,
    },
    inputContainer: {
        // flex: 0.5,
        height: config.size._112px,
        width: config.size._520px,
        // backgroundColor: 'rgba(0, 255, 0, 0.5)',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: config.size._8px,
        marginBottom: 10,
    },
    textInput: {
        // width: '100%',
        textAlign: 'center',
        // padding: 0,
        paddingVertical: 0,
    },
    buttonText: {
        color: '#9e9e9e',
        fontSize: config.fontSize._64px,
    },
    buttonContainer: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        // padding: 24,
        paddingHorizontal: 10,
        marginBottom: config.size._64px,
        // backgroundColor: 'rgba(255, 0, 0, 0.5)',
        justifyContent: 'center',
    },
    button: {
        position: 'absolute',
        bottom: 0,
        height: config.size._140px,
        marginHorizontal: 5,
        borderRadius: config.size._120px,
    },
    lButton: {
        // flex: 0.5,
        position: 'absolute',
        left: '5%',
        // justifyContent: 'flex-start',
        borderWidth: 1,
        borderColor: '#9e9e9e',
    },
    cButton: {
        flex: 0,
        justifyContent: 'flex-end',
        borderWidth: 1,
        borderColor: '#9e9e9e',
    },
    rButton: {
        // flex: 0.5,
        position: 'absolute',
        right: '5%',
        // justifyContent: 'flex-end',
    },
});
