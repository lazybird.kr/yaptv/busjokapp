import PushNotification from "react-native-push-notification"
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import {Platform} from 'react-native'

class NotificationManager {
    configure = (onRegister, callback, onNotification, onOpenNotification, senderID) => {
        
        PushNotification.configure({
            onRegister: function(token) {
                onRegister(token.token, callback)
                console.log("[NotificationManager] onRegister token:", token.token);
                PushNotification.createChannel(
                  {
                  channelId: 'busjok-notifications', // (required)
                  channelName: 'Busjok Notifications', // (required)
                  channelDescription: 'A main channel to show the notifications on android', // (optional) default: undefined.
                  soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
                  importance: 4, // (optional) default: 4. Int value of the Android notification importance
                  vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
                  vibration: 2000
                  },
                  (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
                );

                PushNotification.getChannels(function(channels) {
                  console.log("channel  : ",channels);
                });
            },

            onNotification: function(notification) {
                console.log("[NotificationManager] onNotification:", notification);
                // 메시지에서 "content_available" : true 이면 사용자가 푸쉬 탭 안해도 여기로 들오온다.
                // 만약 "content_available" : true 이면 백그라운드에서 넘어올 경우 두 번 실행되는 경우 있다.

                // 안드로이드에서 foreground 에서 알림 온 경우 userInteraction이 false인데 실행되는 경우 있다.
                // 그래서 막아 둔다.
                if(Platform.OS === "android" && !notification.userInteraction){
                    notification.finish('backgroundFetchResultNoData')
                    return;
                }
                
                
                if (Platform.OS === 'ios') {
                    notification.userInteraction = true
                }
                
                if (Platform.OS === 'android') {
                    notification.userInteraction = true
                }
                
                if (notification.userInteraction) {
                    onOpenNotification(notification)
                } else {
                    onNotification(notification)
                }

                // Only call callback if not from foreground
                if (Platform.OS === 'ios') {
                    if (!notification.foreground) {
                        //notification.finish('backgroundFetchResultNoData')
                        notification.finish('UIBackgroundFetchResultNoData')
                    } 
                } else {
                        notification.finish('backgroundFetchResultNoData')
                    
                }
              },
              // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
            senderID: senderID,  
            popInitialNotification: false,  
            permissions: {
                alert: true,
                badge: true,
                sound: true,
              },
            
              // Should the initial notification be popped automatically
              // default: true
              
            
              /**
               * (optional) default: true
               * - Specified if permissions (ios) and token (android and ios) will requested or not,
               * - if not, you must call PushNotificationsHandler.requestPermissions() later
               */
              requestPermissions: true,
        });
    }

    recoveringNotification = ()=>{ 
        if (Platform.OS === 'ios'){ 
          PushNotificationIOS.getInitialNotification().then((remoteMessage)=>{ 
            if (remoteMessage) {
              let newNoti = {};
              if(typeof remoteMessage.foreground == 'boolean' && !remoteMessage.foreground){
                newNoti.data = remoteMessage;
                newNoti.notification = remoteMessage;
              }
              else {
                newNoti = remoteMessage;
              }
              NotificationHub.syncNotification(newNoti, 3);
              writeDebug('Noti abierta Background iOS',newNoti);  
            }
          });
        }
        else {
          PushNotification.popInitialNotification((remoteMessage) => {
            if (remoteMessage) { 
              let newNoti = {};
              if(typeof remoteMessage.foreground == 'boolean' && !remoteMessage.foreground){
                newNoti.data = remoteMessage;
                newNoti.notification = remoteMessage;
              }
              else {
                newNoti = remoteMessage;
              }
              NotificationHub.syncNotification(newNoti, 3);
              writeDebug('Noti abierta Background Android',newNoti); 
            } 
          });
        }
      }

    _buildAndroidNotification = (id, title, message, data = {}, options = {}) => {
        return {
            id: id,
            autoCancel: true,
            largeIcon: options.largeIcon || "ic_launcher",
            smallIcon: options.smallIcon || "ic_launcher",
            bigText: message || '',
            subText: title || '',
            vibrate: options.vibrate || false,
            vibration: options.vibration || 300,
            priority: options.priority || "high",
            importance: options.importance || "high",
            data: data
        }
    }

    _buildIOSNotification = (id, title, message, data ={}, options = {}) => {
        return {
            alertAction: options.alertAction || "view",
            category: options.category || "",
            userInfo: {
                id: id,
                item: data
            }
        }
    }

    showNotification = (id, title, message, data = {}, options = {}) => {
        PushNotification.localNotification({
            /* Android Only Properties */
            ...this._buildAndroidNotification(id, title, message, data, options),
            /* IOS Only Properties */
            ...this._buildIOSNotification(id, title, message, data, options),
             /* IOS and Android Properties */
             title: title || "",
             message: message || "",
             playSound: options.playSound || false,
             soundName: options.soundName || 'default',
             userInteraction: false // If the notification was opened by the user from the notification area or not
        })
    }

    cancelAllLocalNotification = () => {
        if (Platform.OS === 'ios') {
            PushNotificationIOS.removeAllDeliveredNotifications()
        } else {
            PushNotification.cancelAllLocalNotifications()
        }
    }

    unregister = () => {
        PushNotification.unregister()
    }
}

export const notificationManager = new NotificationManager()