import {notificationManager} from './NotificationManager'
import config from 'src/config';
import { Platform } from 'react-native';
import NavigationService from './NavigationService';
//import PushNotification from "react-native-push-notification"
import showToast from 'src/components/Toast';
class Notification {

  mount(callback){
    const {notification} = config;
    //this.navigationDeferred = navigationDeferred;
    this.localNotify = notificationManager
    this.localNotify.configure(this.onRegister, callback, this.onNotification,
      this.onOpenNotification, notification.senderID)
  }

  onRegister(token, callback) {
    const {notification} = config;
    console.log("[Notification] Registered: ", token);
    if(Platform.OS === 'ios') {
      fetch(notification.transTokenForIOS,{
        method: 'post',
        headers: {
          'Content-type': 'application/json',
          'Authorization' : `key=${notification.serverKeyForIOS}`
        },
        body: JSON.stringify({
          "application": notification.appIDForIOS,
          "sandbox":false,
          "apns_tokens":[
              token
           ]
         })
      })
      .then(resp => resp.json())
      .then(json => {
        if(json.results[0].status === "OK"){
          //console.log("json : ", json.results[0].status)
          console.log("[Notification] Registered IOS:", json.results[0].registration_token);
          config.notificationToken = json.results[0].registration_token;
          callback();
        }

      })
      .catch(err => {
        console.log("err : ", err)
        showToast(err.message, "LONG");
      })
    } else {
      config.notificationToken = token;
      callback();
    }

  }

  onNotification(notify) {
    console.log("[Notification] onNotification: ", notify);
    
    /*
    const {notification} = notify;
    if(!notification){
      
      PushNotification.localNotification({
        // Android Only Properties
        channelId: "rn-push-notification-channel-id", // (required) channelId, if the channel doesn't exist, it will be created with options passed above (importance, vibration, sound). Once the channel is created, the channel will not be update. Make sure your channelId is different if you change these options. If you have created a custom channel, it will apply options of the channel.
        showWhen: true, // (optional) default: true
        vibrate: true, // (optional) default: true
        vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
        tag: "bus", // (optional) add tag to message
        ongoing: false, // (optional) set whether this is an "ongoing" notification
        priority: "high", // (optional) set notification priority, default: high
        visibility: "public", // (optional) set notification visibility, default: private
        ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear). should be used in combine with `com.dieam.reactnativepushnotification.notification_foreground` setting
        invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true
        title: "My Notification Title", // (optional)
        message: "My Notification Message", // (required)
      });

    }
    */
  }

  onOpenNotification (notify) {
    console.log("[Notification] onOpenNotification: ", notify)
    // const message = JSON.parse(notify);
    const {data} = notify;
    if (data && data.title && data.body) {
      const body = JSON.parse(data.body);
      if (data.title === "post") {
        NavigationService.navigate("communityScreen", {fromSharedLink: true, postId: body.post_id, at: Date.now()})
      } else  if (data.title === "bus") {
        console.log("NavigationService : ", NavigationService);
        NavigationService.popToTop();
        NavigationService.navigate("busScreen");
        NavigationService.navigate("StationDetail", {arsId: body.ars_id,});
      } else {
        NavigationService.navigate("homeTab")
      }
    } else {

    }


    //alert("Open Notification : " + notify.data.title);
    // NavigationService.navigate("ladderGameScreen")
  }

  onPressCancelNotification = () => {
    this.localNotify.cancelAllLocalNotification()
  }

  onPressSendNotification = () => {
    const options = {
        soundName: 'default',//'notification1.mp3',
        playSound: true,
        vibrate: true
    }

    this.localNotify.showNotification(
      1,
      "App Notification",
      "Local Notification",
      {}, // data
      options // options
    )
  }
}

export const notification = new Notification()
