import React from 'react';
import {
    View, 
    StyleSheet,
    TouchableOpacity,
    Image, 
} from 'react-native';
import config from 'src/config';



export default function IconEmbededMenu(props) {


    return (
        <TouchableOpacity style={props.mainStyle} onPress={props.onPress}>
            <View style={styles.container}>
                {props.children}
                {/* <View style={[styles.imageWrapper, props.padding]}> */}
                    <Image source={props.icon? props.icon:config.images.detail} style={[styles.image, props.iconStyle]}></Image>
                {/* </View> */}
            </View>
        </TouchableOpacity>
        

    )
}

const styles=StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    // imageWrapper: {
    //     position: 'absolute',
    //     right: 0,
    // },
    image: {
        // alignItems: 'flex-end',
        width: config.size._96px,
        height: config.size._96px,
    }    
});