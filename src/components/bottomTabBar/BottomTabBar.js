import React from 'react';
import { Component } from 'react';
import {
    TouchableOpacity,
    View, Keyboard,
    Dimensions, Platform,
} from 'react-native';
import config from 'src/config';
import { StackActions } from 'react-navigation';
import {isIphoneX} from 'src/utils/'

export default class BottomTabBar extends Component {

    navigationStateIndex = null;
    
    // call when each time user click different tab
   

    constructor(props) {
        super(props);
        this.state={
            visible : true
        }
    }

    componentDidMount(){
        
        if (Platform.OS === 'android') {
            this.keyboardEventListeners = [
                Keyboard.addListener('keyboardDidShow', () => this.setState({visible : false})),
                Keyboard.addListener('keyboardDidHide', () => this.setState({visible : true})),
            ];
        }
    }
    componentWillUnmount(){
        if (Platform.OS === 'android') {
            this.keyboardEventListeners &&
            this.keyboardEventListeners.forEach(eventListener => eventListener.remove());
          }
    }

    renderTabBarButton(route, idx) {
        const {
            activeTintColor,
            inactiveTintColor,
            navigation,
            getLabelText,
            renderIcon,
        } = this.props;
        const currentIndex = navigation.state.index;
        const color = currentIndex === idx ? activeTintColor : inactiveTintColor;
        const label = getLabelText({ route, focused: currentIndex === idx, index: idx });
        return (
            <TouchableOpacity
                activeOpacity={0.8} 
                onPress={() => {
                    if (currentIndex != idx) {
                        navigation.dispatch(StackActions.popToTop());
                        navigation.navigate(route.routeName);
                    }
                }}
                
                key={route.routeName} 
                style={{
                    alignItems:"center", 
                    justifyContent:"center", height:50, 
                    width:config.deviceInfo.width/5,
                    backgroundColor:"transparent",
                    zIndex:0
                }}
            >
                {renderIcon({ route, tintColor: color, focused: currentIndex === idx, index: idx })}
                    
                
                
                
                {/* <Text style={[ StdStyles.tabBarButtonText, { color }]}>
                    {label}
                </Text>  */}
            </TouchableOpacity>
        );
    }

    render() {
        if(!this.state.visible){
            return null;
        }
        const { navigation, style } = this.props;
        console.log("navigation : ", navigation.state.routes[navigation.state.index])
        let centerBackgroundColor   = "transparent";
        let centerMarginTop = 0;
        if(navigation.state &&  navigation.state.index === 2){
            if(navigation.state.routes[navigation.state.index].index === 0){
                centerBackgroundColor = "#000000";
            } else if(navigation.state.routes[navigation.state.index].index === 1){
                centerBackgroundColor = "#01a3ff";
            }
        } else if(navigation.state &&  navigation.state.index === 0){
            centerMarginTop = -20;
        } else {
            centerBackgroundColor = "white"
        }
        const tabBarButtons = navigation.state.routes.map(this.renderTabBarButton.bind(this));
        return (
            <View style={{flexDirection:"row", marginBottom : isIphoneX() ? 34 : 0, justifyContent:"space-around", alignItems:"center",...style, borderTopWidth:0, shadowOpacity:0, elevation:0, marginTop:centerMarginTop, }}>
            

                        <View style={{
                            position:"absolute", 
                            height:40, zIndex:-1,
                            backgroundColor: centerBackgroundColor,
                            left : config.deviceInfo.width/2 - 100, top:0,
                            borderBottomLeftRadius : 200,
                            borderBottomRightRadius : 200,
                            width : 400, 
                            //borderTopColor: "#e2e2e2",
                            //shadowOpacity:0.1
                        }} />
                        <View style={{
                            position:"absolute", 
                            height:40, zIndex: -1, top:0,
                            borderTopWidth:1,borderRightWidth:1,
                            width : config.deviceInfo.width/2-20, backgroundColor:"white",
                            borderTopRightRadius : 400,
                            //shadowOpacity:0.1,
                            borderTopColor: "#e2e2e2",
                            borderRightColor: "#e2e2e2",
                        }} />   
                        <View style={{
                            position:"absolute", 
                            height:40,  zIndex: -1,backgroundColor:"white",
                            left : config.deviceInfo.width/2+20,  top:0,
                            borderTopWidth:1,borderLeftWidth:1,
                            width : config.deviceInfo.width/2-20, 
                            borderTopLeftRadius : 400,
                            //shadowOpacity:0.1,
                            borderTopColor: "#e2e2e2",
                            borderLeftColor: "#e2e2e2",
                            //elevation:3
                        }} />
                        {tabBarButtons}
                        
            </View>
        );
    }
}