import React, {Component, useRef}  from "react";
import {View, FlatList, Keyboard, Image, Text, TouchableOpacity, Alert, StyleSheet} from "react-native";
import config from 'src/config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';
import axios from 'axios';
import Entypo from 'react-native-vector-icons/Entypo';
const CancelToken = axios.CancelToken;
let cancel1, cancel2;

export default class BusSearchModal extends Component {
  constructor() {
    super();
      this.state = {
        searchValue : '',
        searchBus : [], searchStation : [],
        keyboardHeight: 0,
        tabPressIndex : 0,
        
      }
      this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.searchValue !== this.state.searchValue){
      /*
      if(this.state.searchValue.length === 1 && prevState.searchValue.length === 0){
        console.log("didupdate : value length 1")
        this.fetchBusData(this.state.searchValue);
        this.fetchStationData(this.state.searchValue);
      } else if(this.state.searchValue.length === 0){
        console.log("didupdate : value length 0")
        this.setState({searchBus : [], searchStation : []});
      }
      */
     this.fetchBusData(this.state.searchValue);
        this.fetchStationData(this.state.searchValue);
    }
  }

  filterBusData = (searchValue) => {
    const {searchBus} = this.state;
    if(!searchValue){
      return [];
    } else {
      return searchBus;
    }

    const newSearchBus = searchBus.filter(item => {
      return item.name.indexOf(searchValue) > -1;
    })

    return newSearchBus;
    /*
    const newSearchBus = searchBus.filter(item => {
      //return item.name.indexOf(searchValue) > -1;
      return item.name.substring(0, searchValue.length) === searchValue;
    })
    return newSearchBus.sort(function(a,b){
      return a.name < b.name ? -1 : a.name > b.name ? 1 : 0
    });
    */
  }

  filterStationData = (searchValue) => {
    const {searchStation} = this.state;
    if(!searchValue){
      return [];
    } else {
      return searchStation;
    }

    const newSearchStation =  (searchStation || []).filter(item => {
      return item.name.indexOf(searchValue) > -1 || item.ars_id.indexOf(searchValue) > -1
    })
    return newSearchStation;

    /*
    const newSearchStation1 = searchStation.filter(item => {
      return item.ars_id.substring(0, searchValue.length) === searchValue;
      //return item.ars_id.substring(0, searchValue.length) === searchValue
    })
    newSearchStation1.sort(function(a,b){
      return a.ars_id < b.ars_id ? -1 : a.ars_id > b.ars_id ? 1 : 0
    });
    const newSearchStation2 = searchStation.filter(item => {
      return item.name.indexOf(searchValue) > -1;
      //return item.ars_id.substring(0, searchValue.length) === searchValue
    })
    newSearchStation2.sort(function(a,b){
      return a.name < b.name ? -1 : a.name > b.name ? 1 : 0
    });
    return [...new Set([...newSearchStation1, ...newSearchStation2])];
    */
  }

  fetchBusData =  async (searchValue) => {
    console.log("fetchBusData")

    if (cancel1 !== undefined) {
      cancel1();
    }
    /*
    if (!searchValue){
      return setTimeout(()=>this.setState({searchBus:[]}),0);
    }
    */

    try{
      
      const respBus =   await axios.post(config.url.apiSvr + "/bus/SearchBusRoute", {
          "data": {
            "device": {
              "device_id": config.deviceInfo.device_id,
              "session_id": config.sessionInfo.session_id,
            },
            "search_key": searchValue
          }
        }, 
        { cancelToken: new CancelToken(function executor(c) {
          cancel1 = c;
        }),
      });
        console.log("respBus : ", respBus.data.data);
        const {route_list} = respBus.data.data;
        this.setState({
          searchBus:route_list,    
        })
    } catch(e){
      console.log("e : ", e);
      if (axios.isCancel(e)) {
        console.log("post Request canceled");
      } else {
        setTimeout(()=>
        this.setState({
          searchBus:[],
          searchStation:[],
        }),0)
      } 
    }
  }
  

  fetchStationData =  async (searchValue) => {
    
    console.log("AAA")

    if (cancel2 !== undefined) {
      cancel2();
    }
    /*
    if (!searchValue){
      return setTimeout(()=>this.setState({searchStation:[]}),0)
    }
    */
    try{
      const respStation = await  axios.post(config.url.apiSvr + "/bus/SearchStation", {
          "data": {
            "device": {
              "device_id": config.deviceInfo.device_id,
              "session_id": config.sessionInfo.session_id,
            },
            "search_key": searchValue
          }
        }, 
        { cancelToken: new CancelToken(function executor(c) {
          cancel2 = c;
        }),
      });
        console.log("respStation : ", respStation.data.data);
        const {station_list} = respStation.data.data;
        this.setState({  
          searchStation:station_list, 
        })
    } catch(e){
      console.log("e : ", e);
      if (axios.isCancel(e)) {
        console.log("post Request canceled");
      } else {
        setTimeout(()=>
          this.setState({
            searchStation:[],})
        ,0);
      }
    }
  }
   
  

  onChangeText = (text)=> {
    this.setState({
      searchValue : text
    });
    console.log("onChangeText :", text)
  }
  
  onClearText = () => {
    this.setState({
      searchValue : "", 
      searchBus : [], searchStation : []
    })
  }

  selectTab = (index) => {
    this.setState({tabPressIndex:index})
    
  }

  

  submit = (item) => {
    console.log("item : ", item);
    if(item && item.ars_id){
      this.props.onSubmit({...item, type:"정류장"});
    } else {
      Alert.alert(
        '알림',
        "개발중입니다.",
        [{text: 'OK', onPress: () => {
        }}],
        {cancelable: false},
      );
    }
  }

  searchHeader = () => {
    const { searchValue, tabPressIndex} = this.state;
    const { toggleSearch, onSubmit, searchTitle,  } = this.props;
    const { onChangeText, onClearText } = this;

    const tabList = ["전체", "버스", "정류장"];

    let tabs = tabList.map((item, index) => {
      
      let dot = null;
      if(index === tabPressIndex){
        dot = <Entypo name={'dot-single'} size={18} color={"#01a3ff"}/>
      } 
      return (
        <TouchableOpacity key={index + "tab"} style={{flex:1, justifyContent:"center", alignItems:"center"}} onPress={()=>this.selectTab(index)}>
          <StyledText style={{color:tabPressIndex === index? "#01a3ff" :"grey", textAlign: 'center',
                    textAlignVertical: 'center',}}>{item}</StyledText>
          <View style={styles.tabDot}>
            {dot}
          </View>
        </TouchableOpacity>
      )
    })
   
    return (
      <View style={{height:120, backgroundColor:"white", width:"100%", zIndex:1}}>
        <View style={{height:40, flexDirection:"row",
          margin : 10, marginBottom:20}} >
            <MaterialCommunityIcons
              name="chevron-left" color="grey" size={36}
              onPress={toggleSearch} style={{alignSelf:"center"}}
            />
          <View style={{flex:1, marginLeft:5, borderRadius:10, borderWidth:1, borderColor:"#9e9e9e", flexDirection:"row"}}>
            <StyledTextInput style={{ flex:9, fontSize: 14, marginLeft:10, color:"#000000", alignSelf:"center"}}
                ref={(input) => this.textInput = input}
                onSubmitEditing={onSubmit}
                onChangeText={onChangeText}
                placeholder={searchTitle} placeholderTextColor="#9e9e9e"
              >
              {searchValue}
            </StyledTextInput>
            {searchValue ? <Ionicons style={{alignSelf:"center", width:24, marginRight:10}} name="close-circle-outline" color="grey" size={24}
              onPress={onClearText}/>: <></>}
          </View>
        </View>
        <View style={{height:50, width:"100%", flexDirection:"row",
          borderColor:"lightgrey", borderWidth:1}} >
            {tabs}  
        </View>
      </View>
    )
  }

  handleClearText = () => {
    this.setState({
      searchValue : "", searchBus :[],
    });
  }
        
  componentDidMount() {
    console.log("SearchInputModal :")
    this._isMounted = true;
    this.textInput && this.textInput.getInnerRef().focus();
    this.subs = [
      Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
      Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
    this._isMounted = false;
  }

  _keyboardDidShow(e) {
    if(this._isMounted)
      this.setState({keyboardHeight: e.endCoordinates.height});
  }

  _keyboardDidHide(e) {
    if(this._isMounted) 
      this.setState({keyboardHeight: 0});
  }
  
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          alignSelf:"center",
          width: '100%',
          backgroundColor: '#bdbdbd',
        }}
      />
    );
  }

  render() {
    const {searchValue, searchBus, searchStation, tabPressIndex} = this.state;
    //const data = tabPressIndex === 0 ? this.filterBusData(searchValue) : this.filterStationData(searchValue)
    let data = [];
    if(tabPressIndex === 0){
      if(searchStation.length === 0 && searchBus.length === 0){
        data=[];
      } else if(searchStation.length > 0 && searchBus.length > 0){
        data = [{name:"정류장시작"},...searchStation, {name:"버스시작"}, ...searchBus];
      } else if(searchStation.length > 0){
        data = [{name:"정류장시작"},...searchStation];
      } else if(searchBus.length > 0){
        data = [{name:"버스시작"},...searchBus];
      }
      
    } else if(tabPressIndex === 1){
      data = [...searchBus];
    } else if(tabPressIndex === 2){
      data = [...searchStation];
    }
    console.log("searchValue :", searchValue);
    console.log("data  : ", data);

    const getAllData = (item) => {
      if(item.name === "정류장시작"){
        return (
          <View style={{height:50, width:"100%", backgroundColor:"#eeeeee", justifyContent:"center"}}>
            <StyledText style={{fontSize:13, color:"#616161", paddingLeft:16}}>정류장</StyledText>
          </View>
        )
      } else if(item.name === "버스시작"){
        return (
          <View style={{height:50, width:"100%", backgroundColor:"#eeeeee",justifyContent:"center"}}>
           <StyledText style={{fontSize:13, color:"#616161", paddingLeft:16}}>버스</StyledText>
          </View>
        )
      } else {
        return (
          <View style={{flexDirection:"row", alignItems:"center", paddingLeft:16}}>
            <StyledText style={{fontSize:16, color:"#000000"}}>{item.name}</StyledText>
          </View>                        
        )
      }
    }

    const {busTypes, busImages} = config.bus;
    
    return(
          <View style={{ width:"100%", height : "100%",backgroundColor:"#FFFFFF"}}>
            <View style={{width : "100%", height:Platform.OS === 'ios'? getStatusBarHeight(): 0}} />
                <FlatList 
                  style={{backgroundColor:"#FFFFFF",}}
                  data={data}
                  renderItem={({ item, index }) => {
                    return(
                    <View style={{height:50}} key={index}>
                      <TouchableOpacity style={{flex:1, justifyContent:"center"}} onPress={()=>{this.submit(item)}}>
                        {tabPressIndex === 0 && getAllData(item)}
                        {tabPressIndex === 1 && 
                          <View style={{flexDirection:"row", alignItems:"center", marginLeft:16}}>
                            <Image style={{width:24, height:24}} source={busImages[parseInt(item.route_type)]} />
                            <StyledText style={{fontSize:16, color:"#000000", marginLeft:4}}>{item.name}</StyledText>
                            <StyledText style={{fontSize:11, color:"#9e9e9e", marginLeft:4}}>{busTypes[parseInt(item.route_type)]}</StyledText>
                          </View>
                        }
                        {tabPressIndex === 2 && 
                          <View style={{justifyContent:"center", marginLeft:16}}>
                            <StyledText style={{fontSize:16, color:"#000000", marginLeft:4}}>{item.name}</StyledText>
                            <StyledText style={{fontSize:11, color:"#9e9e9e", marginLeft:4}}>{item.ars_id}</StyledText>
                          </View>
                        }
                        
                        
                      </TouchableOpacity>
                    </View>
                    )}
                  }
                  keyExtractor={(item, index) => "key" + index}
                  ItemSeparatorComponent={this.renderSeparator}
                  ListHeaderComponent={this.searchHeader}
                  keyboardShouldPersistTaps="always"
                  stickyHeaderIndices={[0]}
                />
            </View>
      )
  }     
}

const styles = StyleSheet.create({
  tabDot: {
      position: 'absolute',
      width: '100%',
      height: '25%',
      bottom: '5%',
      alignItems: 'center',
      justifyContent: 'center',
  }
});
