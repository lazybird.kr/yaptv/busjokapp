import React, {Component, useRef}  from "react";
import {View, FlatList, Keyboard, TextInput, Text, TouchableOpacity} from "react-native";
import config from 'src/config';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {StyledTextInput, StyledText} from 'src/components/StyledComponents';

export default class SearchModal extends Component {
  constructor() {
    super();
      this.state = {
        searchValue : '',
        searchData : [],
        keyboardHeight: 0,
      }
      this._isMounted = false;
  }

  onChangeText = (text)=> {
    if(!text){
      this.setState({searchValue : "", searchData : []});
      return;
    }
    
    /* fetch Data */
    let tempData = ["A", "B", "C"];
    this.setState({searchValue : text, searchData:tempData})
  }
  
  onClearText = () => {
    this.setState({searchValue : "", searchData : []})
  }
  

  searchHeader = () => {
    const { searchValue } = this.state;
    const { toggleSearch, onSubmit, searchTitle } = this.props;
    const { onChangeText, onClearText } = this;
   
    return (
        <View style={{height:40, flexDirection:"row",
          margin : 10}} >
            <MaterialCommunityIcons
              name="chevron-left" color="grey" size={36}
              onPress={toggleSearch} style={{alignSelf:"center"}}
            />
          <View style={{flex:1, marginLeft:5, borderRadius:10, borderWidth:1, borderColor:"grey", flexDirection:"row"}}>
            <StyledTextInput style={{ flex:9, fontSize: 14, marginLeft:10, color:"grey", alignSelf:"center"}}
                ref={(input) => this.textInput = input}
                onSubmitEditing={onSubmit}
                onChangeText={onChangeText}
                placeholder={searchTitle} placeholderTextColor="grey"
              >
              {searchValue}
            </StyledTextInput>
            {searchValue ? <Ionicons style={{alignSelf:"center", flex:1}} name="close-circle-outline" color="grey" size={24}
              onPress={onClearText}/>: <></>}
          </View>
        </View>
    )
  }

  handleClearText = () => {
    this.setState({
      searchValue : "", searchData :[],
    });
  }
        
  componentDidMount() {
    console.log("SearchInputModal :")
    this._isMounted = true;
    this.textInput && this.textInput.getInnerRef().focus();
    this.subs = [
      Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
      Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
    this._isMounted = false;
  }

  _keyboardDidShow(e) {
    if(this._isMounted)
      this.setState({keyboardHeight: e.endCoordinates.height});
  }

  _keyboardDidHide(e) {
    if(this._isMounted) 
      this.setState({keyboardHeight: 0});
  }
  
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          alignSelf:"center",
          width: '100%',
          marginLeft:10,
          backgroundColor: '#CED0CE',
        }}
      />
    );
  }

  render() {
      return(
          <View style={{ width:"100%", height : "100%",backgroundColor:"#FFFFFF"}}>
            <View style={{width : "100%", height:Platform.OS === 'ios'? getStatusBarHeight(): 0}} />
                <FlatList 
                  style={{backgroundColor:"#FFFFFF"}}
                  data={this.state.searchData}
                  renderItem={({ item, index }) => (
                    <View style={{flex:1, margin:5, marginLeft:10}} key={index}>
                      <TouchableOpacity style={{flex:1}} onPress={()=>{this.props.onSubmit(item)}}>
                        <StyledText style={{fontSize:14, color:"black", marginLeft:10}}>{item}</StyledText>
                      </TouchableOpacity>
                    </View>
                    )
                  }
                  keyExtractor={(item, index) => "key" + index}
                  ItemSeparatorComponent={this.renderSeparator}
                  ListHeaderComponent={this.searchHeader}
                  keyboardShouldPersistTaps="always"
                  stickyHeaderIndices={[0]}
                />
            </View>
      )
  }     
}