import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text, ScrollView,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import config from 'src/config';
import {StyledText} from 'src/components/StyledComponents';

export default class Tabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: undefined,
        };

        this.tabList = props.tabList;
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }
    onPressTab = (item, index) => {
        const {onPressGroup} = this.props;

        onPressGroup(item, index);
    };

    render() {
        const {currentGroup, tabList} = this.props;
        let tabs = null;
        let tabPressed = currentGroup? currentGroup: tabList[0];


        if (this.state.layout) {
            const w = this.state.layout.width / tabList.length;
            tabs = tabList.map((item, index) => {
                let tabStyle = {
                    width: w,
                    borderBottomWidth: 0.5,
                    borderColor: config.colors.lineColor,
                };
                let textStyle = {
                    //flex: 1,
                    justifyContent: 'center',
                    //lineHeight: config.size._100px,
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    color: config.colors.greyColor,
                };
                let selectedDot = null;
                if (item === tabPressed) {
                    textStyle = {
                        ...textStyle,
                        color: config.colors.baseColor,
                    };
                    selectedDot = (
                        <Entypo name={'dot-single'} size={18} color={config.colors.baseColor}/>
                    )
                }

                let tabTitle = (
                    <StyledText style={textStyle}>
                        {item}
                    </StyledText>
                );
 
                return (
                    <View
                        style={tabStyle}
                        key={'tab' + index}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.tab}
                            onPress={() => this.onPressTab(item, index)}>
                            {tabTitle}
                            <View style={styles.tabDot}>
                                {selectedDot}
                            </View>
                        </TouchableOpacity>
                    </View>
                );
            });
        }

        return (
            <View style={styles.container}
                  onLayout={e => {
                      this.setState({
                          layout: e.nativeEvent.layout,
                      });
                  }}
            >
                <ScrollView style={styles.scrollTabs} horizontal={true} showsHorizontalScrollIndicator={false}>
                    {tabs}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollTabs: {
        flex: 1,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabDot: {
        position: 'absolute',
        width: '100%',
        height: '25%',
        bottom: '5%',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
