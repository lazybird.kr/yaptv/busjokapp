import {Platform, ToastAndroid, View, StyleSheet} from 'react-native';
import {StyledText} from 'src/components/StyledComponents'
import Toast from 'react-native-simple-toast';
import React from 'react';

export default showToast = (message, duration) => {
    if(Platform.OS === "android"){
      ToastAndroid.showWithGravityAndOffset(
        message,
        duration === "LONG" ? ToastAndroid.LONG : ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    } else {
      Toast.showWithGravity(message, duration === "LONG" ?Toast.LONG : 1, Toast.BOTTOM,['RCTModalHostViewController', 'UIAlertController']);
    }
  }