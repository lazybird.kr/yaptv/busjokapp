import React, {Component}  from "react";
import {StyledText} from 'src/components/StyledComponents';
import config from 'src/config';
export default class HeaderTitle extends Component {

  render() {
    const {title, style} = this.props;
    return(
      <StyledText style={[style, {fontSize:18, fontFamily:config.defaultMediumFontFamily}]}>
          {title}
      </StyledText>
    )
  }
}
