import React, {Component}  from "react";
import {Image} from "react-native";

export default class HeaderImage extends Component {
  
  render() {
    const {img, style} = this.props;
    return(
        <Image style={{...style}} source={img} />
    )
  }     
}