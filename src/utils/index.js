
import {
    Dimensions, Platform,
} from 'react-native';

export function isIphoneX() {
    const dimen = Dimensions.get('window');
    // console.log("dimem.height : ", dimen.height)
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        (
            (dimen.height === 812 || dimen.width === 812) ||   // iphone x
            (dimen.height === 896 || dimen.width === 896) ||   // iphone 11
            (dimen.height === 926 || dimen.width === 926)      // iphone 12
        )
    );
}
