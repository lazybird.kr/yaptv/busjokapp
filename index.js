/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';




AppRegistry.registerComponent(appName, () => App)

/*
AppRegistry.registerRunnable(appName, async initialProps => {
    
    appInit = () => {
        const App = require('./App').default;
        AppRegistry.registerComponent(appName, () => App);
        AppRegistry.runApplication(appName, initialProps);
    }    
    notification.mount(()=>{appInit()});
});
*/