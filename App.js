import React, { Component } from 'react';
import Main  from 'src/containers/Main';
import 'react-native-gesture-handler';
import {Text, TextInput, Dimensions} from 'react-native';
import config from 'src/config';
import SplashScreen from 'react-native-splash-screen';

export default class App extends Component {

  componentDidMount(){
    /* device text 사이즈에 영향 안받게 설정 */
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    Text.defaultProps.style={fontFamily:config.defaultFontFamily, includeFontPadding:false}
    if (TextInput.defaultProps == null) TextInput.defaultProps = {};
    TextInput.defaultProps.allowFontScaling = false; 
    TextInput.defaultProps.style={fontFamily:config.defaultFontFamily}
    
    Dimensions.addEventListener("change", ({window, screen}) => {
      console.log("window", window);
      console.log("screen", screen);

      
      config.deviceInfo =  {
        oneThirdWidth: window.width / 3,
        width: window.width,
        height: window.height,
        
      }
      
    })
    
   SplashScreen.hide();
  }

  render() {

    return (
      <Main />
    );
  }
}

